/*
 * =====================================================================================
 *
 *       Filename:  findAddress.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  01/04/2009 09:07:10
 *       Revision:  none
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog:
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <sys/time.h>

//#include "vconvme.h"
#include "midas.h"

#include "mvmestd.h" 

#include "v792.h"
#include "v830.h"
#include "sis3320drv.h"

#define NUM_MOD_CRATE 21
DWORD ba[NUM_MOD_CRATE];

int QDC_MOD_COUNTER;
int ADC_MOD_COUNTER;
int TDC_MOD_COUNTER;
int SCLR_MOD_COUNTER;
int FPGA_MOD_COUNTER;


MVME_INTERFACE *myvme;
HNDLE hDB, hKey;


int qdccounter = 0;
int tdccounter = 0;
int adccounter = 0;
int sclrcounter = 0;
int fpgacounter = 0;

int i_g;
int i_counter;

void addchecker(DWORD baseaddress)
{
	if( i_g < NUM_MOD_CRATE){
		ba[i_g++] = baseaddress;
		i_counter++;
	}

}


WORD  readVersionADC(DWORD baseaddress)
{


	WORD vers=-1;

 	//mvme_set_am(myvme, MVME_AM_A32);
	//mvme_set_dmode(myvme, MVME_DMODE_D16);
	
	vers = (WORD)mvme_read_value( myvme, baseaddress+0x8032 );


	return ( vers&0xff) ;

	//return  vers;

}


int readVersionQDC(DWORD baseaddress)
{


	WORD vers=-1;

 	//mvme_set_am(myvme, MVME_AM_A32);
	//mvme_set_dmode(myvme, MVME_DMODE_D16);
	
	vers = (WORD)mvme_read_value( myvme, baseaddress+0x8032 );


	return ( vers & 0xff);



}

char* ADCVersion( DWORD base )
{
	int v=0;
	v = readVersionADC(base);
	char* version_name = malloc(sizeof(char)*10);


	switch(v) {
		case	0x11:
			sprintf(version_name,"AA,0x11");
			break;	
		case	0x12:
			sprintf(version_name,"AB,0x12");
			break;	
		case	0x13:
			sprintf(version_name,"AC,0x13");
			break;	
		case	0x14:
			sprintf(version_name,"AD,0x14");
			break;	
		case	0x15:
			sprintf(version_name,"AE,0x15");
			break;	
		case	0x16:
			sprintf(version_name,"AF,0x16");
			break;	
		case	0x17:
			sprintf(version_name,"AG,0x17");
			break;	
		case	0x18:
			sprintf(version_name,"AH,0x18");
			break;	
		default:
			sprintf(version_name,"Unknown");
			break;	
	}
	//printf("Version found %s , v= %d \n",version_name,v);
	return version_name;
}
int readBoardID(DWORD baseaddress)
{

	WORD B_MSB,B_MID,B_LSB,BID;

 	//mvme_set_am(myvme, MVME_AM_A32);
	//mvme_set_dmode(myvme, MVME_DMODE_D16);
	
	B_MSB = (WORD)mvme_read_value( myvme, baseaddress+0x8036 );
//	printf(" MSB : 0x%x \n",B_MSB );
	B_MID = mvme_read_value( myvme, baseaddress+0x803A );
//	printf(" MID : 0x%x \n",B_MID );
	B_LSB = mvme_read_value( myvme, baseaddress+0x803E );
//	printf(" LSB : 0x%x \n",B_LSB );
	BID = ((B_MSB & 0x00ff )<< 12) | ((B_MID & 0x00ff) << 8) | (B_LSB & 0x00ff);

	return BID;
}

int readBoardIDTDC(DWORD baseaddress)
{

	WORD B_MSB,B_MID,B_LSB,BID;

 	//me_set_am(myvme, MVME_AM_A24);
	//me_set_dmode(myvme, MVME_DMODE_D16);
	
	B_MSB = (WORD)mvme_read_value( myvme, baseaddress+0x4034 );
//	printf(" MSB : 0x%x \n",B_MSB );
	B_MID = mvme_read_value( myvme, baseaddress+0x4038 );
//	printf(" MID : 0x%x \n",B_MID );
	B_LSB = mvme_read_value( myvme, baseaddress+0x403C );
//	printf(" LSB : 0x%x \n",B_LSB );
	BID = ((B_MSB & 0x00ff )<< 12) | ((B_MID & 0x00ff) << 8) | (B_LSB & 0x00ff);

	return BID;
}

int readTDCVers(DWORD baseaddress)
{
	WORD vers=-1;

 	//mvme_set_am(myvme, MVME_AM_A32);
	//mvme_set_dmode(myvme, MVME_DMODE_D16);
	
	vers = (WORD)mvme_read_value( myvme, baseaddress+0x4030 );


	return vers;
}



int clashchecker()
{

	int i,j;

	DWORD checkq,checker;

	for(i=0;i < i_counter;i++){
		checkq = ba[i];	
		for(j=0;j< i_counter;j++){
			checker = ba[j];

			//printf("checkq[%d]: 0x%X , checker[%d: 0x%X \n",i,checkq,j,checker);
			if( checkq == checker && (i!=j)){

				printf("\n Base address 0x%X (checkq)[%d] is clashing with Base Address 0x%X (checker)[%d] , i_counter [%d]  !!!!! \n",checkq,i,checker,j,i_counter);
				return 1;
			}
		}
	}

	return 0;
}

int updateModDB(DWORD baseA,int bid,int modNum)
{
	int size, status = 0;
	char set_str[80];

	sprintf(set_str,"/Equipment/VME/V%d_%d",bid,modNum);
	size = sizeof(baseA);
	status = db_set_value(hDB,0,set_str,&baseA,size,1,TID_DWORD);

	 if (status != DB_SUCCESS) {
         cm_msg(MERROR, "updateModDB", "Cannot update vme module %d",modNum);
		 return status;
      }

	return status;
}

int checkforQDC(DWORD base_addy)
{
		DWORD reg = 0x1000;
  	   	DWORD value = 0xfffffff;
		DWORD base = base_addy+reg;
		value = mvme_read_value( myvme, base );
		int boardid = readBoardID(base_addy);
		char *myvers = ADCVersion(base_addy);
		if( boardid == 792 ){
		//if( ((value>>8)&0xf) == 0x9 && ((value)&0xf) == 0x4 ){
				printf(" Found QDC Module [%d]  \n",++qdccounter);
				printf(" \t BOARDID: %d \n",boardid);
				printf(" \t VERSION: %s \n",myvers);
				printf(" \t Base Address: 0x%08x \n",base_addy);
				printf(" \t Firmware Rev: %d.%d \n",(value>>8&0xf),(value&0xf));
				QDC_MOD_COUNTER++;
	//			updateModDB(base_addy,boardid,QDC_MOD_COUNTER);
				return 1;
		}
		free(myvers);
	return 0;
}

int checkforADC(DWORD base_addy)
{
		DWORD reg = 0x1000;
   	  	DWORD value = 0xfffffff;
		DWORD base = base_addy+reg;
		//printf("\t\t Checking ADC @ [0x%x] \n",base_addy);	
		value = mvme_read_value( myvme, base );
		int boardid = readBoardID(base_addy);
		char *myvers = ADCVersion(base_addy);
		//printf("\t\t\t returned checked reg value: 0x%x , boardid %d \n",value,boardid);
		if( boardid == 785) {
		//if( ((value>>8)&0xf) == 0x9 && ((value)&0xf) == 0x4){
				printf(" Found ADC Module [%d]  \n",++adccounter);
				printf(" \t BOARDID: %d \n",boardid);
				//printf(" \t VERSION: %x \n",readVersionADC(base_addy));
				printf(" \t VERSION: %s\n",myvers);
				printf(" \t Base Address: 0x%08x \n",base_addy);
				printf(" \t Firmware Rev: %d.%d \n",(value>>8&0xf),(value&0xf));
				ADC_MOD_COUNTER++;
	//			updateModDB(base_addy,boardid,ADC_MOD_COUNTER);
				return 1;
		}
	
		free(myvers);
	return 0;
}


int checkforSCLR(DWORD base_addy)
{
		DWORD reg = 0x1132;
     	DWORD value = 0xfffffff;
		DWORD base = base_addy+reg;
		value = mvme_read_value( myvme, base );
		int boardid = readBoardID(base_addy);
		if( boardid == 830) {
		//if( ((value>>4)&0xf) == 0x0 && ((value)&0xf) == 0x4){
				printf(" Found Scalar Module [%d]  \n",++sclrcounter);
				printf(" \t BOARDID: %d \n",boardid);
				printf(" \t Base Address: 0x%08x \n",base_addy);
				printf(" \t Firmware Rev: %d.%d \n",(value>>4&0xf),(value&0xf));
				SCLR_MOD_COUNTER++;
	//			updateModDB(base_addy,boardid,SCLR_MOD_COUNTER);
				return 1;
		}
	

	return 0;
}


int checkforTDC(DWORD base_addy)
{
		//printf("\t Checking for TDC @ base[0x%x] \n",base_addy);
		DWORD reg = 0x1026;
    	 	DWORD value = 0xfffffff;
		DWORD base = base_addy+reg;
 		//mvme_set_am(myvme, MVME_AM_A24_ND);
		//mvme_set_dmode(myvme, MVME_DMODE_D16);
//		printf("\t\t TDC register Check [0x%x] \n",base);	
		value = mvme_read_value( myvme, base );
//		printf("\t\t Value returned [0x%x] \n",value );
		int boardid = readBoardIDTDC(base_addy);
		if( boardid == 1190 || boardid == 1290 ){
		//if( ((value>>4)&0xf) == 0x0 && ((value)&0xf) == 0x5){
				
				int verstdc = readTDCVers(base_addy);
				printf(" Found TDC Module [%d]  \n",++tdccounter);
				printf(" \t BOARDID: %d , Vers %d \n",boardid,verstdc);
				printf(" \t Base Address: 0x%08x \n",base_addy);
				printf(" \t Firmware Rev: %d.%d \n",(value>>4&0xf),(value&0xf));
				TDC_MOD_COUNTER++;
	//			updateModDB(base_addy,boardid,TDC_MOD_COUNTER);
				return 1;
		}

	return 0;
}

int checkforFPGA(DWORD base_addy)
{
		DWORD reg = 0x8138;
     	DWORD value = 0xfffffff;
		DWORD base = base_addy+reg;
		
		value = mvme_read_value( myvme, base );
		int boardid = readBoardID(base_addy);
		if( boardid == 1495 ){
		//if( ((value)&0xf) == 0x5){
				printf(" Found FPGA Module [%d]  \n",++fpgacounter);
				printf(" \t BOARDID: %d \n",boardid);
				printf(" \t Base Address: 0x%08x \n",base_addy);
				printf(" \t Firmware Rev: %d.%d \n",(value>>8&0xf),(value&0xf));
				FPGA_MOD_COUNTER++;
	//			updateModDB(base_addy,boardid,FPGA_MOD_COUNTER);

				return 1;
		}

	return 0;
}

int checkforSIS3320( DWORD base_addy )
{
	int dmode=0;
	
	mvme_set_am(myvme, MVME_AM_A32);
	mvme_get_dmode(myvme, &dmode);
	mvme_set_dmode(myvme, MVME_DMODE_D32);

	if( mvme_read_value(myvme,base_addy+0x4) == 0x33021415 ){
		sis3320_Status(myvme,base_addy);
		return 1;
	}
	mvme_set_dmode(myvme, dmode);


	return 0;
}

int ModChecks(DWORD base)
{

	if(checkforQDC(base) ){
		addchecker(base);
		return 1;
	}

	if(checkforADC(base) ){
		addchecker(base);
		return 1;
	}
	if(checkforTDC(base) ){
		addchecker(base);
		return 1;
	}

	if(checkforSCLR(base)){
		addchecker(base);
		return 1;
	}

	if(checkforFPGA(base) ){
		addchecker(base);
		return 1;
	}

	if(checkforSIS3320(base) ){
		addchecker(base);
		return 1;
	}



	return 0;
}


int main( int argc, char* argv[] ){

	DWORD BASE_ADDRESS = 0x0;
	int status = 0;
	int i;
	DWORD reg;

	QDC_MOD_COUNTER=0;
	ADC_MOD_COUNTER=0;
	TDC_MOD_COUNTER=0;
	SCLR_MOD_COUNTER=0;
	FPGA_MOD_COUNTER=0;


	if (argc>1) {
		sscanf(argv[1],"%x",&reg);
		printf(" searching for reg 0x%08x\n",reg);
	}
	

	status = cm_connect_experiment("xiafe","k600test","ODBTest",NULL);
	printf(" Connect Status: %d \n",status );
//	if( status != 1 )
//		return 0;


	cm_get_experiment_database(&hDB,&hKey);
	cm_msg(MINFO, "ODBTest", "Hello World");


	int avail_slots[22];
	status = mvme_open(&myvme,0);
	if( status != 1 ) { exit(1); };
	int i_c = 0;

	for( i_c=0;i_c<NUM_MOD_CRATE;i_c++){
		ba[i_c]=0x0;
	}

 	mvme_set_am(myvme, MVME_AM_A32);
	mvme_set_dmode(myvme, MVME_DMODE_D16);
	i = 0;
	int avail_slot_count = 0;
	int slot_count=0;
	for( i = 1 ;  i < 22 ; i++ )
	{
		int slot = i;
		usleep(1000000);
		BASE_ADDRESS = ((0x00000000|i)<<17);

		printf("Base Address 0x%x \n",BASE_ADDRESS);
			if( ModChecks(BASE_ADDRESS)){
			 printf("Base Address [0x%x] @ slot [%d] is occupied! \n",BASE_ADDRESS,i);

		}else{
			avail_slots[slot_count++] = slot;
			++avail_slot_count;
			printf("\nBase Address [0x%x] @ slot [%d] is {available}!\n",BASE_ADDRESS,i);
		}


	}/*  end forloop */


	printf("\nNumber of Slots available [%d], Slots: ",avail_slot_count);
	int j = 0;
	for(j=0;j<slot_count;j++){
		printf("%d,",avail_slots[j]);
	}

	printf("\n");
	if(clashchecker())
	{
		printf("\n\n\t >>>>>>>>>>>>> BASE ADDRESS ARE CLASHING <<<<<<<<<<<<<<\n");
	}else{
		printf("\n\n\t NO BA CLASHES \n");
	}


	status = mvme_close(myvme);

	//cm_disconnect_experiment();
	return 0;
}
