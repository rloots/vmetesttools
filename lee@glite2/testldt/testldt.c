/******************************************************************************
*
* Filename: 	testldt.c
* 
* Description:	Long Duration Timer sample application for Linux.
*
* $Revision$
*
* $Date$
*
* $Source$
*
* Copyright 2000-2009 Concurrent Technologies.
*
* This program is free software; you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation; either version 2 of the License, or 
* any later version.
*		
* CCT does not admit liability nor provide warranty for this software. 
* This material is provided "AS-IS" and at no charge.
*
******************************************************************************/

#include <pthread.h>
#include <stdio.h> 
#include <stdlib.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include "cctldt.h"

#ifndef DEVICE
#define DEVICE  "/dev/cctldt"
#endif

int timerfd, tmp1;
unsigned long timer;
int thread_args;
pthread_t thread_id;
int tick_count = 0;

void* mythread (void* parameters)
{
    printf("Thread start\n");

    while(1)
    {
        pthread_testcancel(); // Check to see if we are to exit

        /*
         * Wait for notification of interrupt. This will block
         * until the next interrupt occurs
         */
        if ( ioctl(timerfd, LDTIOC_NOTIFY) < 0 ) 
   	    {
            printf("LDTIOC_NOTIFY ioctl FAILED\n");
            exit(1);
   	    }
        tick_count++;        
    }
}

int main()
{
    int loop;

	if ( ( timerfd = open(DEVICE, O_WRONLY) ) < 0 )
    {
        printf("/dev/cctldt Open failed \n");
        exit(1);
    }          
       
    printf("Timer opened on fd %d\n", timerfd);

    /*
     * First test is a PIT test. Set timer to 100Hz, spawn a thread to
     * handle interrupt notification and use 'sleep' to wait for a second
     * Then print out interrupt count...
     */
    printf("PIT Test - count ticks for a second. Timer set to 100Hz so\n");
    printf("it should be 100 ticks\n");

	tmp1 = LDT_MODE_PIT_100;  //Mode = PIT 100Hz

	if ( ioctl(timerfd, LDTIOC_SETMODE, &tmp1) < 0 ) 
	{
		printf("LDTIOC_SETMODE ioctl FAILED\n");
		exit(1);
	}
	
	timer = 0x0000000L; // Clear timer before enabling
	if ( ioctl(timerfd, LDTIOC_SETTIMER, &timer) < 0 ) 
	{
		printf("LDTIOC_SETTIMER ioctl FAILED\n");
		exit(1);
	}

	thread_args = 0;
	
	pthread_create(&thread_id, NULL, &mythread, &thread_args);

    printf("Start timer\n");
	if ( ioctl(timerfd, LDTIOC_RUN, &tmp1) < 0 ) 
	{
		printf("LDTIOC_RUN ioctl FAILED\n");
		exit(1);
	}	                  
	
    sleep(1);

    printf("Stop timer...\n");
	if ( ioctl(timerfd, LDTIOC_STOP, &tmp1) < 0 ) 
    {
        printf("LDTIOC_STOP ioctl FAILED\n");
        exit(1);
    }

    printf("Ticks: %d\n", tick_count);
    printf("Cancel thread...\n");

    /* Cancel thread... */
    pthread_cancel(thread_id);
	         
    /*
     * As thread is likely to be 'stuck' in a blocking notify ioctl we can
     * trigger a 'fake' notify event to release it. The 'pthread_testcancel'
     * will notice the above 'pthread_cancel' and cause the thread to exit
     */
	if ( ioctl(timerfd, LDTIOC_CLRNOTIFY, &tmp1) < 0 ) 
    {
        printf("LDTIOC_INTERRUPT ioctl FAILED\n");
        exit(1);
    }

    /* We should wait for the thread to exit here... */
    pthread_join(thread_id, NULL);

    printf("LDT test - count for 5 seconds reading timer value each second\n");
	tmp1 = LDT_MODE_LDT;  //Mode = LDT

	if ( ioctl(timerfd, LDTIOC_SETMODE, &tmp1) < 0 ) 
	{
		printf("LDTIOC_SETMODE ioctl FAILED\n");
		exit(1);
	}
	
	timer = 0x0000000L; // Clear timer before enabling
	if ( ioctl(timerfd, LDTIOC_SETTIMER, &timer) < 0 ) 
	{
		printf("LDTIOC_SETTIMER ioctl FAILED\n");
		exit(1);
	}

    printf("Start timer\n");
	if ( ioctl(timerfd, LDTIOC_RUN, &tmp1) < 0 ) 
	{
		printf("LDTIOC_RUN ioctl FAILED\n");
		exit(1);
	}	                  
	
    for(loop=0;loop<5;loop++)
    {
        sleep(1);

    	if ( ioctl(timerfd, LDTIOC_GETTIMER, &timer) < 0 ) 
	    {
	    	printf("LDTIOC_GETTIMER ioctl FAILED\n");
	    	exit(1);
        }

        printf("Loop: %d Timer count: %010d\n", loop, timer);
    }

    printf("Stop timer...\n");
	if ( ioctl(timerfd, LDTIOC_STOP, &tmp1) < 0 ) 
    {
        printf("LDTIOC_STOP ioctl FAILED\n");
        exit(1);
    }

    close(timerfd);
    printf("Done\n");
}
