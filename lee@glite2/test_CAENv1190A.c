

#include <mvmestd.h>

//#include "test.h"
#include <stdlib.h>
#include <stdio.h>
#include "vconvme.h"
#include <sys/time.h>
#include "v1190A.h"


enum
{
	GLOBAL_HEADER = 8,
	GLOBAL_TRAILER = 10,
	GLOBAL_TRIGGER_TIME_TAG = 11,
	FILLER = 0x18,
	TDC_DATA = 0,
	TDC_HEADER = 1,
	TDC_MEASUREMENT = 0
};


int main (int argc, char* argv[]) 
{


//	DWORD VMEIO_BASE = 0x0;
	unsigned int* V1190A_BASE;
	
	
	V1190A_BASE = (unsigned int*)calloc(7,sizeof(unsigned int));
	
	V1190A_BASE[0]=0x40000;
	V1190A_BASE[1]=0x50000;
	V1190A_BASE[2]=0x60000;
	V1190A_BASE[3]=0x70000;
	V1190A_BASE[4]=0x80000;
	V1190A_BASE[5]=0x90000;
	V1190A_BASE[6]=0xb0000;
	
	MVME_INTERFACE *myvme;





//          struct timeval tc1, td1, tp1;
//	  struct timeval tc2, td2, tp2;
//	  unsigned int  dt1, dt2, dt3;

	int status; 
	int result;	    
 	int i,k,j;

//	if (argc>1) {
//		sscanf(argv[1],"%.08lx",&V1190A_BASE);
//	}

	status = mvme_open(&myvme, 0);
	if (status!=1) exit(1);

	int naddr = 7;
	result = initListDMATransfer( myvme, V1190A_BASE, naddr );
	
	
	DWORD mode = 0x4;

	DWORD *tdcdata,edge,channel,time,xdata;
	int nentry;
	int count;
	int totalcounts;
	int wire;
	int modcount = 1;

	for( k = 0; k < modcount;k++ ){
	    	 printf("@@@@@@@@@@@@ %d @@@@@@@@@@ \n",k); 
		 mvme_set_am(myvme,MVME_AM_A32_ND);
		 mvme_set_dmode(myvme, MVME_DMODE_D16);
		 v1190A_Setup( myvme, V1190A_BASE[k], mode);
		 v1190A_BERRset(myvme,V1190A_BASE[k]);
//	 	 v1190A_ReadStatBerrFlag(myvme,V1190A_BASE[k] );
	}


	for( k = 0; k < modcount;k++ ){
	    	 printf("@@@@@@@@@@@@ %d @@@@@@@@@@ \n",k); 
		 v1190A_SoftReset(myvme, V1190A_BASE[k]);
	}

	v1190A_GeoWrite(myvme,V1190A_BASE[0],0x1);
	v1190A_GeoWrite(myvme,V1190A_BASE[1],0x2);
	v1190A_GeoWrite(myvme,V1190A_BASE[2],0x3);
	v1190A_GeoWrite(myvme,V1190A_BASE[3],0x4);
	v1190A_GeoWrite(myvme,V1190A_BASE[4],0x5);
	v1190A_GeoWrite(myvme,V1190A_BASE[5],0x6);
	v1190A_GeoWrite(myvme,V1190A_BASE[6],0x7);




	
	nentry =128;
	tdcdata = (DWORD*)calloc(nentry,sizeof(DWORD));
	for(i=0;i<modcount;i++){
  		
//		memset(tdcdata,kDataSize,sizeof(DWORD));
//		memset(tdcdata,'\0',sizeof(kDataSize));
		
		//geo=0;
		
  	//	storedeventcounter[tdcmodulecounter]  = v1190A_EvtStored(k600vme,V1190_BASE[tdcmodulecounter]);//mvme_read_value(k600vme, V1190_BASE[tdcmodulecounter]+0x1020);
	//	eventcounter[tdcmodulecounter]=v1190A_EvtCounter(k600vme,V1190_BASE[tdcmodulecounter]);
	//	mvme_set_dmode(k600vme,cmode );

//		diffcounter =  abs( storedeventcounter[tdcmodulecounter] - prevcounter );
	//	diffcounter =  abs( storedeventcounter[tdcmodulecounter] - prevcounter );
	//	eventdiffcounter=eventcounter[tdcmodulecounter]-prevevcounter;
	///	if( diffcounter > 1 ){
	//		cm_msg(MINFO,"Read_TDC","DataSkipped in mod %d diffcounter %d", tdcmodulecounter,diffcounter );
	//	}
	//	if(eventdiffcounter!=1){
	//		cm_msg(MINFO,"Read_TDC","DataSkipped eventcount in mod %d : %d != %d +1 ", tdcmodulecounter,eventcounter[tdcmodulecounter],prevevcounter);
	//	}
		

//		count = v1190A_EventRead(myvme, V1190A_BASE[i], tdcdata, &nentry);  //Single event read
	
		printf(" tdcdata address 0x%.08x \n",tdcdata );	
		count = v1190A_DataRead(myvme, V1190A_BASE[i], tdcdata, nentry);   //DMA block level transfer (blt) Read
		
		//assert(data != NULL );
		
		//printf("[1] TDC count ---> <%d> , [2] TDC counts ---> <%d> \n",count,counts );

		if (count > 0){ // we have data
//			counts=127; // TODO !!!!!!!! REMOVE THIS ITS DUE TO DataRead returning the number of words read which define to be 200 due to ensuring double wire firings.
		//	printf("reading TDCmodule[0x%08x]  %d got %d words\n",V1190A_BASE[i],i,counts);
	//***		*pdata32++=tdcmodulecounter+(0x1f<<27);
			DWORD code = 0xf;
			for (j = 0; j < nentry; j++) 
			{
				code = (tdcdata[j]>>27)&0x1f;
				//DWORD code = (tdcdata[j]>>27);
				
				
	//				printf(" tdcdata[%d] = 0x%.08X  ,  code = 0x%.08X \n",j,tdcdata[j],code);

				if (tdcdata[j] == 0) 
					continue;

				switch (code) {
					case 0x0: // valid measurent word
					        printf(" TDC Data, code = 0x%X \n",code); 
								
						edge = 0x1&(tdcdata[j]>>26);
						channel = 0x7F&(tdcdata[j]>>19);
						time = 0x3FFFF&tdcdata[j];
						if(i==0){
							if(channel<2){
								wire=600+channel;
							}
						}
						xdata=time+((wire<<19)); //data can contain 19 bits of info, map the wire above that.
						printf("\t module : %d tdc %3d: 0x%08x, code 0x%02x, edge %d, chan %2d,time %6d \n", i, j, tdcdata[j], code, edge, channel, time);
        					break;
	                                 case 0x01: // TDC Header
						printf("TDC Header: 0x%08x, tdc: %d eventid: %d bunch id: 0%d\n",tdcdata[j], (tdcdata[j]>>24)&0xf,(tdcdata[j]>>12)&0xfff, (tdcdata[j]&0xfff));
						break;
				         case 0x03: // TDC Trailer
					  	printf("TDC Trailer: 0x%08x, tdc: %d, event id: %d word count: %d\n",tdcdata[j], (tdcdata[j]>>24)&0x3,(tdcdata[j]>>12)&0x3ffff,tdcdata[j]);
						break;
					case FILLER:
					//case 0x18:
						printf("Filler Data code = 0x%X\n", code );
						break;
					case 0x8:
						printf("Global Header 0x%X Geo = 0x%01X \n",code, 0xf&(tdcdata[j]) );
						DWORD eventcount = 0x3ffff&(tdcdata[j]>>5);
						printf(" GH event count = 0x%X  \n",eventcount );
						break;
					case 0x10:
						printf("Global Trailer 0x%X \n",code);
						DWORD statust = 0x3&(tdcdata[j]>>24);
						DWORD wordcount = 0xffff&(tdcdata[j]>>5);
						DWORD Geot = 0xf&(tdcdata[j]);
						printf("\tstatus = 0x%X, wordcount = 0x%X, Geo = 0x%01X \n",statust,wordcount,Geot );

						break;
					case 0x11:
						printf("Global Trigger Time Tag 0x%X \n",code );
						break;



					default:
						printf("Unknown Format 0x%X \n",code );
						break;
				}// switch statement




			} // for j
			
			totalcounts+=nentry;
		
		} //count >0
#ifdef DEBUG2
		else		printf("count %d, we have no data\n",count);
#endif

		}//for tdcmodulecounter







	status = mvme_close(myvme);
	return 1;
}	


/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
