#include "mvmestd.h"
#include <stdlib.h>
#include <stdio.h>

#define  V792_FIRM_REV        (DWORD) (0x1000)
#define  V1190_FIRM_REV_RO    (DWORD) (0x1026)
#define  V82X_FIRM_REV        (DWORD) (0x1132)
#define  V259_FAF5            (DWORD) (0x00FA)

int main (int argc, char* argv[]) {
	DWORD BASE = 0x40000;
	MVME_INTERFACE *myvme;

	int status, csr, i;
	short scsr;
	DWORD    cnt, array[10000];

	// Test under vmic
	status = mvme_open(&myvme, 0);
	//printf("st=%d\n",status);
	if (status!=1) exit(1);

	// Set am to A24 non-privileged Data
	//mvme_set_am(myvme, MVME_AM_A24_ND);
	mvme_set_am(myvme, MVME_AM_A24_SD);

	// Set dmode to D16
	mvme_set_dmode(myvme, MVME_DMODE_D16);
	mvme_set_blt(myvme,MVME_BLT_NONE);

	// Get Firmware revision

	for (BASE=0x200000;BASE<0x300000;BASE+=0x10000) {
		csr = mvme_read_value(myvme, BASE+V82X_FIRM_REV);
		if (csr!=0xffff) 
			printf("@0x%06x: V82X Firmware revision: 0x%x\n",BASE, csr&0xff);
	}
	for (BASE=0x300000;BASE<=0x310000;BASE+=0x10000) {
		csr = mvme_read_value(myvme, BASE+V792_FIRM_REV);
		if (csr!=0xffff) 
			printf("@0x%06x: V792 Firmware revision: 0x%x\n",BASE, csr);
	}
	for (BASE=0x400000;BASE<0x500000;BASE+=0x10000) {
		csr = mvme_read_value(myvme, BASE+V1190_FIRM_REV_RO);
		if (csr!=0xffff) 
			printf("@0x%06x: V1190 Firmware revision: 0x%x\n",BASE, csr);
	}
/*
	for (BASE=0;BASE<0x200000;BASE+=0x10000) {
		csr = mvme_read_value(myvme, BASE+V792_FIRM_REV);
		if (csr!=0xffff) 
			printf("@0x%06x: V792 Firmware revision: 0x%x\n",BASE, csr);
		csr = mvme_read_value(myvme, BASE+V82X_FIRM_REV);
		if (csr!=0xffff) 
			printf("@0x%06x: V82X Firmware revision: 0x%x\n",BASE, csr&0xff);
	}
*/



	mvme_set_dmode(myvme, MVME_DMODE_D16);
	for (BASE=0x100000;BASE<=0x130000;BASE+=0x10000) {
		csr=1;
		status = mvme_write(myvme, BASE+0x20, &csr,4);
		printf("@0x%06x: V259 reset st=%x\n",BASE, status);
		for (i=0;i<1;i++) {
			//printf("%3d ",i);
			status = mvme_read(myvme, &csr, BASE+0xFA, 2);
			printf("@0x%06x: V259 ID: 0x%4x st=%x\n",BASE, csr,status);
			//status = mvme_read(myvme, &csr, BASE+0xFB, 2);
			//printf("@0x%06x: V259 ID: 0x%x st=%x\n",BASE, csr,status);
			//csr = mvme_read_value(myvme, BASE+0xFC);
			//printf("@0x%06x: V259 V : 0x%4x\n",BASE, csr);
			//csr = mvme_read_value(myvme, BASE+0xFE);
			//printf("@0x%06x: V259 MA: 0x%4x\n",BASE, csr);
			//csr = mvme_read_value(myvme, BASE+0x26);
			//printf("@0x%06x: V259 PR: 0x%4x\n",BASE, csr);
			//csr = mvme_read_value(myvme, BASE+0x28);
			//status = mvme_read(myvme, &scsr, BASE+0x28, 2);
			//printf("@0x%06x: V259 MR: 0x%4x st=%x\n",BASE, scsr,status);
		}
	}
	//if (csr==0xff || csr==0xffff) return 1;
	/* Close VME channel */
	status = mvme_close(myvme);
	return 1;
}
