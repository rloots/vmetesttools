#ifndef DAQ_SRT_H
#define DAQ_SRT_H

#include <sched.h>
#include <sys/mman.h>

class Sched {
    const static int Policy=SCHED_FIFO;
    static int Pmin,Pmax;
    static struct sched_param Par;

public:
    /** sets realtime priority */
    inline static int FAST(void) {
#ifdef __linux__
		Par.sched_priority=Pmax;
	    return sched_setscheduler(0, Policy, &Par);
#endif
		return 0;
	}
    /** sets normal priority */
    inline static int NORM(void) {
#ifdef __linux__
	    Par.sched_priority=0;
	    return sched_setscheduler(0, SCHED_OTHER, &Par);
#endif
		return 0;
    }
    
    static int Init(void);
};
#endif


