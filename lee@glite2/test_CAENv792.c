#include "v792.h"
//#include "test.h"
#include <stdlib.h>
#include "vconvme.h"
#include <sys/time.h>

int main (int argc, char* argv[]) {

	DWORD VMEIO_BASE = 0x0;
	DWORD V792_BASE = 0x0000;
  
	MVME_INTERFACE *myvme;

	int status, csr, i;

	if (argc>1) {
		sscanf(argv[1],"%lx",&V792_BASE);
	}

	// Test under vmic
	status = mvme_open(&myvme, 0);
	if (status!=1) exit(1);

	// Set am to A24 non-privileged Data
	//mvme_set_am(myvme, MVME_AM_A24_ND);

	// Set dmode to D32
	mvme_set_dmode(myvme, MVME_DMODE_D32);

	//  v792_SingleShotReset(myvme, V792_BASE);
	{
		WORD     threshold[32];
		for (i=0;i<V792_MAX_CHANNELS;i++) {
			if( i == 0 ){
				threshold[i] = 0x000;
			}
			else{ 
				threshold[i] = 0x7ff;
			}
		//	printf("Threshold[%2i] = 0x%4.4x\t   -  \n ", i, threshold[i]);

		}
		
		v792_ThresholdWrite(myvme, V792_BASE, threshold);
	}
	v792_Status(myvme, V792_BASE);
	v792_SoftReset(myvme, V792_BASE );
	csr = v792_DataReady(myvme, V792_BASE);
	printf("Data Ready: 0x%x\n", csr);
	{
		// Read Event Counter
		DWORD cnt=0;
		v792_EvtCntRead(myvme, V792_BASE, &cnt);
		printf("Event counter: 0x%lx = %ld\n", cnt,cnt);
	}

	// Set 0x3 in pulse mode for timing purpose
	//status = mvme_write_value(myvme, VMEIO_BASE+0x8, 0xF); 
	//printf("st=%d\n",status);

	// Write pulse for timing purpose
	//status = mvme_write_value(myvme, VMEIO_BASE+0xc, 0x2);

	#if 0
	{
		DWORD dest[1000];
		v792_DataRead(myvme, V792_BASE, dest, &cnt);
		printf("Event counter: 0x%lx = %d\n", cnt,cnt);
		
		// Write pulse for timing purpose
		//status = mvme_write_value(myvme, VMEIO_BASE+0xc, 0x8);
		
		for (i=0;i<32;i++) {
			//printf("Data[%2i]=0x%6lx %6ld\n", i, dest[i]&0xFFFFFF,dest[i]&0xFFFFFF);
		}
	}
#endif  


	//  status = 32;

		int j;
		int nEntry;
		DWORD evCnt;
		DWORD data[1000];
		memset(data,sizeof(DWORD)*1000,0);
	
		//v792_Status(myvme, V792_BASE);


		v792_EvtCntReset(myvme, V792_BASE);
		csr = 0x0;
		for (j=0;j<3;j++) {
		
			//mvme_set_dmode(myvme, 2);	
			//printf(" \t vme read value 0x%02x (100c), 0x%02x ( 100e ) ,x%02x (1010 ) \n ", mvme_read_value( myvme, 0xb100c ),
			//									     mvme_read_value( myvme, 0xb100e ),
			//								     	     mvme_read_value( myvme, 0xb1010 ));	     
			
			csr = v792_DataReady(myvme, V792_BASE);
			

				
			//printf(".");fflush(stdout);
			//usleep(1);
			if ( csr ) {
				printf("%10d Data Ready: 0x%x\n",j++, csr);
				v792_EvtCntRead(myvme, V792_BASE, &evCnt);
				printf("%10d Event counter: 0x%lx = %ld\n",j, evCnt,evCnt);
#if 0				
				v792_EventRead(myvme, V792_BASE, &data[0], &nEntry);
				//printf("%10d nEntry: 0x%x %d\n",j, nEntry,nEntry);
				for (i=0;i<nEntry;i++) {
					//printf("Data[%2i]=0x%6lx %6ld\n", i, data[i]&0xFFF,data[i]&0xFFF);
				}
#endif
				
				v792_DataRead(myvme, V792_BASE, data, &nEntry);
				

				printf("nEntry: 0x%x = %d\n", nEntry,nEntry);
				int h=data[0];
				printf("header: 0x%4x geo=%2x crate=%2x cnt=%2d\n",h,(h&0xFF000000)>>26,(h&0xFF0000)>>16,(h&0xff00)>>8);
				v792_Data d;
/*
				for (i=1;i<33;i++) {
					int v=data[i];
					printf("Data[%2i]=0x%6x t=%2d ch=%3d v=%6d ov=%1d un=%1d\n",
						   i, v&0xFFFFFFFF, 
						   (v>>24)&0x7,
						   (v&0xFF0000)>>16, (v&0xFFF),
						   (v&0x1000)>>12,(v&0x2000)>>13);
				}
*/
				int eob=data[33];
				printf("eob: 0x%4x geo=%2x crate=%2x cnt=%2x\n",h,(h&0xFF000000)>>26,h&0xFF0000>>16,h&0xff00>>8);
				 

						
			}else{
				printf("CSR not set\n");
			}
		}
		
	status = mvme_close(myvme);
	return 1;
}	

/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
