/*********************************************************************

  Name:         v792.h
  Created by:   Pierre-Andre Amaudruz
  Contributors: Sergio Ballestrero

  Contents:     V792 32ch. QDC include
                V785 32ch. Peak ADC include
                V792 and V785 are mostly identical;
                v792_ identifies commands supported by both
                v792_ identifies commands supported by V792 only
                v785_ identifies commands supported by V785 only
                
  $Log: v792.h,v $
*********************************************************************/
#ifndef  V792N_INCLUDE_H
#define  V792N_INCLUDE_H

#include <stdio.h>
#include <string.h>
#include "mvmestd.h"

#ifdef __cplusplus
extern "C" {
#endif

#define  V792N_MAX_CHANNELS    (DWORD) 16
#define  V792N_REG_BASE        (DWORD) (0x1000)
#define  V792N_FIRM_REV        (DWORD) (0x1000)
#define  V792N_GEO_ADDR_RW     (DWORD) (0x1002)
#define  V792N_BIT_SET1_RW     (DWORD) (0x1006)
#define  V792N_BIT_CLEAR1_WO   (DWORD) (0x1008)
#define  V792N_SOFT_RESET      (DWORD) (0x1<<7)
#define  V792N_INT_LEVEL_WO    (DWORD) (0x100A)
#define  V792N_INT_VECTOR_WO   (DWORD) (0x100C)
#define  V792N_CSR1_RO         (DWORD) (0x100E)
#define  V792N_CR1_RW	        (DWORD) (0x1010)
#define  V792N_SINGLE_RST_WO   (DWORD) (0x1016)
#define  V792N_EVTRIG_REG_RW   (DWORD) (0x1020)
#define  V792N_CSR2_RO         (DWORD) (0x1022)
#define  V792N_EVT_CNT_L_RO    (DWORD) (0x1024)
#define  V792N_EVT_CNT_H_RO    (DWORD) (0x1026)
#define  V792N_INCR_EVT_WO     (DWORD) (0x1028)
#define  V792N_INCR_OFFSET_WO  (DWORD) (0x102A)
#define  V792N_DELAY_CLEAR_RW  (DWORD) (0x102E)
#define  V792N_BIT_SET2_RW     (DWORD) (0x1032)
#define  V792N_BIT_CLEAR2_WO   (DWORD) (0x1034)
#define  V792N_TEST_EVENT_WO   (DWORD) (0x103E)
#define  V792N_EVT_CNT_RST_WO  (DWORD) (0x1040)
#define	 V792N_IPED_RW		(DWORD) (0x1060)
#define  V792_TEST_ADDRESS_R   (DWORD) (0x1064)
#define  V785_SLIDECONST_RW   (DWORD) (0x106A)
#define  V792N_THRES_BASE      (DWORD) (0x1080)

void v792N_EvtCntRead(MVME_INTERFACE *mvme, DWORD base, DWORD *evtcnt);
void v792N_EvtCntReset(MVME_INTERFACE *mvme, DWORD base);
void v792N_CrateSet(MVME_INTERFACE *mvme, DWORD base, DWORD *evtcnt);
void v792N_DelayClearSet(MVME_INTERFACE *mvme, DWORD base, int delay);
int  v792N_DataRead(MVME_INTERFACE *mvme, DWORD base, DWORD *pdest, int *nentry);
int  v792N_EventRead(MVME_INTERFACE *mvme, DWORD base, DWORD *pdest, int *nentry);
int  v792N_ThresholdWrite(MVME_INTERFACE *mvme, DWORD base, WORD *threshold);
int  v792N_ThresholdRead(MVME_INTERFACE *mvme, DWORD base, WORD *threshold);
int  v792N_DataReady(MVME_INTERFACE *mvme, DWORD base);
void v792N_SingleShotReset(MVME_INTERFACE *mvme, DWORD base);
void v792N_Status(MVME_INTERFACE *mvme, DWORD base);
void v792N_IntSet(MVME_INTERFACE *mvme, DWORD base, int level, int vector);
void v792N_IntDisable(MVME_INTERFACE *mvme, DWORD base);
void v792N_IntEnable(MVME_INTERFACE *mvme, DWORD base, int level);
int  v792N_CSR1Read(MVME_INTERFACE *mvme, DWORD base);
int  v792N_CSR2Read(MVME_INTERFACE *mvme, DWORD base);
int  v792N_BitSet2Read(MVME_INTERFACE *mvme, DWORD base);
void v792N_BitSet2Set(MVME_INTERFACE *mvme, DWORD base, WORD pat);
void v792N_BitSet2Clear(MVME_INTERFACE *mvme, DWORD base, WORD pat);
int  v792N_DataReady(MVME_INTERFACE *mvme, DWORD base);          
int  v792N_isEvtReady(MVME_INTERFACE *mvme, DWORD base);
void v792N_EvtTriggerSet(MVME_INTERFACE *mvme, DWORD base, int count);
void v792N_DataClear(MVME_INTERFACE *mvme, DWORD base);
void v792N_OnlineSet(MVME_INTERFACE *mvme, DWORD base);
void v792N_LowThEnable(MVME_INTERFACE *mvme, DWORD base);
void v792N_EmptyEnable(MVME_INTERFACE *mvme, DWORD base);
void v792N_EvtCntReset(MVME_INTERFACE *mvme, DWORD base);
int  v792N_Setup(MVME_INTERFACE *mvme, DWORD base, int mode);
void v792N_DelayClearSet(MVME_INTERFACE *mvme, DWORD base, int delay);
void v792N_SoftReset(MVME_INTERFACE *mvme, DWORD base);
void v792N_ControlRegister1Write(MVME_INTERFACE *mvme, DWORD base, WORD pat);
WORD v792N_ControlRegister1Read(MVME_INTERFACE *mvme, DWORD base);
void v792N_SetPedestal(MVME_INTERFACE *mvme, DWORD base, int pedestal);
void v792N_SetTestRandom(MVME_INTERFACE *mvme, DWORD base);
void v792N_SetTestAcquisition(MVME_INTERFACE *mvme, DWORD base, WORD value);

  enum v792N_DataType {
    v792N_typeMeasurement=0,
    v792N_typeHeader     =2,
    v792N_typeFooter     =4,
    v792N_typeFiller     =6
  };

  typedef union {
    DWORD raw;
    struct EntryN {
      unsigned adc:12; // bit0 here
      unsigned ov:1;
      unsigned un:1;
      unsigned _pad_1:3;
      unsigned channel:4;
      unsigned _pad_2:3;
      unsigned type:3;
      unsigned geo:5;
    } data ;
    struct HeaderN {
      unsigned _pad_1:8; // bit0 here
      unsigned cnt:6;
      unsigned _pad_2:2;
      unsigned crate:8;
      unsigned type:3;
      unsigned geo:5;
    } header;
    struct FooterN {
      unsigned evtCnt:24; // bit0 here
      unsigned type:3;
      unsigned geo:5;
    } footer;
  } v792N_Data;    

  typedef union {
    DWORD raw;
    struct {
      unsigned DataReady:1; // bit0 here
      unsigned GlobalDataReady:1;
      unsigned Busy:1;
      unsigned GlobalBusy:1;
      unsigned Amnesia:1;
      unsigned Purge:1;
      unsigned TermOn:1;
      unsigned TermOff:1;
      unsigned EventReady:1; //bit 8 here
    };
  } v792N_StatusRegister1;
  typedef union {
    DWORD raw;
    struct {
      unsigned _pad_1:1; // bit0 here
      unsigned BufferEmpty:1;
      unsigned BufferFull:1;
      unsigned _pad_2:1;
      unsigned PB:4;
      //unsigned DSEL0:1;
      //unsigned DSEL1:1;
      //unsigned CSEL0:1;
      //unsigned CSEL1:1;
    };
  } v792N_StatusRegister2;
  typedef union {
    DWORD raw;
    struct {
      unsigned _pad_1:2;
      unsigned BlkEnd:1;
      unsigned _pad_2:1;
      unsigned ProgReset:1;
      unsigned BErr:1;
      unsigned Align64:1;
    };
  } v792N_ControlRegister1;
  typedef union {
    DWORD raw;
    struct {
      unsigned MemTest:1;
      unsigned OffLine:1;
      unsigned ClearData:1;
      unsigned OverRange:1;
      unsigned LowThresh:1;
      unsigned _pad_1:1;//bit5
      unsigned TestAcq:1;
      unsigned SLDEnable:1;
      unsigned StepTH:1;
      unsigned _pad_2:2;//bits 9-10
      unsigned AutoIncr:1;
      unsigned EmptyProg:1;
      unsigned SlideSubEnable:1;
      unsigned AllTrg:1;
    };
  } v792N_BitSet2Register;

  void v792N_printEntry(const v792N_Data* v);
  
#ifdef __cplusplus
}
#endif

#endif // V792N_INCLUDE_H

/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * tab-width: 8
 * c-basic-offset: 2
 * End:
 */
