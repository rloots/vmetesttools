###############################################################################
#
# Filename: 	Makefile
# 
# Description:	Makefile for Linux LDT device driver
#
# $Revision$
#
# $Date$
#
# $Source$
#
# Copyright 2000-2005 Concurrent Technologies.
#
###############################################################################

# Defaults
POWER_PC = FALSE

src ?= .
obj ?= .

ifeq ($(POWER_PC),TRUE)
KERNEL_VERSION := 2.4.20
KERNEL_SOURCES := $(shell test -d /opt/VP754/LINUX_20x && echo /opt/VP754/LINUX_20x || echo /usr/src/linux)
KERNEL_OUTPUT := $(KERNEL_SOURCES)
else
KERNEL_VERSION := $(shell uname -r)
KERNEL_MODLIB := /lib/modules/$(KERNEL_VERSION)
KERNEL_SOURCES := $(shell test -d $(KERNEL_MODLIB)/source && echo $(KERNEL_MODLIB)/source || echo $(KERNEL_MODLIB)/build)
KERNEL_OUTPUT := $(KERNEL_SOURCES)
endif

ifeq ($(POWER_PC),TRUE)
KERNEL := kernel-ppc
else
KERNEL := kernel-$(KERNEL_VERSION)
endif

# Select module build process, depending on kernel version
ifeq ($(findstring 2.4,$(KERNEL_VERSION)),2.4)
USE_KBUILD := FALSE
else
ifeq ($(findstring 2.6,$(KERNEL_VERSION)),2.6)
USE_KBUILD := TRUE
endif
endif

TARGET := cctldt_driver
ifeq ($(USE_KBUILD),FALSE)
OBJS := $(TARGET).o
SRC := cctldt.c
endif

ifeq ($(POWER_PC),TRUE)
CC 	= ppc_74xx-gcc
AR	= ppc_74xx-ar
LD	= ppc_74xx-ld
INCLUDE = $(KERNEL_SOURCES)/include/
else
CC 	= gcc
AR	= ar
LD	= ld
INCLUDE = $(KERNEL_SOURCES)/include/
endif

ifeq ($(USE_KBUILD),FALSE)
CFLAGS = -O2 -Wall -DMODULE -D__KERNEL__ -DLINUXOS -I./ -I$(INCLUDE)
else
EXTRA_CFLAGS += -DLINUXOS
endif



all: ldtall

ifeq ($(USE_KBUILD),FALSE)

depend .depend dep:
	$(CC) $(CFLAGS) -M *.c > $@

$(TARGET).o: $(SRC:.c=.o)
	$(LD) -r $^ -o $@

$(KERNEL):
	mkdir $(KERNEL)

$(KERNEL)/$(TARGET).o: $(TARGET).o
	cp $(TARGET).o $(KERNEL)

kernel: $(KERNEL) $(KERNEL)/$(TARGET).o

ldtall: .depend kernel

clean:
	rm -f *.o *.a *~ core .depend Module.symvers
	rm -f $(KERNEL)/*.o
	rmdir $(KERNEL)
else

LDT_OBJS := cctldt.o
ifneq ($(KERNELRELEASE),)
obj-m	:= $(TARGET).o
$(TARGET)-objs :=  $(LDT_OBJS)
else
KDIR	:= $(KERNEL_SOURCES)
PWD	:= $(shell pwd)
default:
	$(MAKE) -C $(KDIR) SUBDIRS=$(PWD) modules
endif

$(KERNEL):
	mkdir $(KERNEL)

ldtall: $(KERNEL)
	$(MAKE) -C $(KDIR) SUBDIRS=$(PWD) $(KBUILD_OPTIONS) modules 
	@if ! [ -f $(TARGET).ko ]; then \
	  echo "$(TARGET).ko failed to build!"; \
	  exit 1; \
	fi
	cp $(TARGET).ko $(KERNEL)
	
clean:
	rm -f *.o *.a *.ko .*.cmd *.mod.c Module.symvers
	rm -f -r .tmp_versions
	rm -f $(KERNEL)/*.ko	
	rmdir $(KERNEL)
endif
