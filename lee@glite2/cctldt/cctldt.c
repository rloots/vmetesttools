/******************************************************************************
*
* Filename: 	cctldt.c
* 
* Description:	Long Duration Timer device driver for Linux.
*
* This program is free software; you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation; either version 2 of the License, or 
* any later version.
*		
* CCT does not admit liability nor provide warranty for this software. 
* This material is provided "AS-IS" and at no charge.
*
* $Revision$
*
* $Date$
*
* $Source$
*
* Copyright 2000-2009 Concurrent Technologies.
*
******************************************************************************/
#ifndef __KERNEL__
#define __KERNEL__
#endif


#ifndef LINUX_VERSION_CODE
#include <linux/version.h>
#endif

#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,18))
#include <linux/utsrelease.h>
#include <linux/autoconf.h>
#else
#include <linux/config.h>
#endif

#include <linux/module.h>
#ifndef MODULE
#include <linux/init.h>
#define INIT_FUNC 	static int ldt_init
#define CLEANUP_FUNC 	static void ldt_cleanup
#else
#define INIT_FUNC 	int init_module
#define CLEANUP_FUNC 	void cleanup_module
#endif

#include <linux/kernel.h>
#include <linux/reboot.h>
#include <linux/ioport.h>
#include <linux/types.h>
#include <asm/uaccess.h>
#include <linux/fs.h>
#include <linux/pci.h>
#include <linux/mm.h>
#include <linux/init.h>
#include <linux/ioctl.h>
#include <linux/interrupt.h>
#include <asm/io.h>
#include <linux/watchdog.h>
#include <linux/miscdevice.h>                                         
#include "cctldt.h"

int irqmode = 1; // will remove as an option - cant' see why you wouldn't do this
static DECLARE_WAIT_QUEUE_HEAD(ldt_wait_queue); 
static int ldt_irq = 5;	//Harcoded Interrupt Line = 5         
static int device_open = 0; //To check if device is already open  
static spinlock_t ldt_lock;
static int ldt_wq_active = 0;

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,5))
MODULE_PARM(irqmode, "i"); //1 if you want the ISR to clear the interrupt
#else
module_param( irqmode, int, 0 );
#endif
MODULE_PARM_DESC(irqmode, "1 for Interrupt to be acknowledged by ISR, 0 otherwise");

static int ldt_ioctl (struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg);
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0))
static void ldt_isr(int irq, void *dev_id, struct pt_regs *regs);
#else
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,20))
irqreturn_t ldt_isr( int irq, void *dev_id );
#else
irqreturn_t ldt_isr(int irq, void *dev_id, struct pt_regs *regs);
#endif
#endif

static int ldt_open (struct inode *inode, struct file *file);

/*
 * Function Name:   ldt_release.
 * Parameter:       struct inode *inode, struct file *file.
 * Return Value:    0 - successful.
 * Description:     This function is used to release the device @ close
 */
static int ldt_release (struct inode *inode, struct file *file)
{                         
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0))
	MOD_DEC_USE_COUNT;
#else
	module_put( THIS_MODULE );
#endif
  	device_open--;
  	return 0;
}

/*
 * Function Name:   ldt_open.
 * Parameter:       struct inode *inode, struct file *file.
 * Return Value:    0 - successful, negative value - failed.
 * Description:     This function is used to open the device.
 */
static int ldt_open (struct inode *inode, struct file *file)
{
        if (device_open)
  	        return -EBUSY;
		device_open++;         
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0))
		MOD_INC_USE_COUNT;
        return SUCCESS;        
#else
	if( try_module_get( THIS_MODULE ))
		return SUCCESS;
	else
		return -EIO;

#endif
}

/*
 * Function Name:   ldt_ioctl.
 * Parameter:       struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg.
 * Return Value:    0 - successful, negative value - failed.
 * Description:     This function is used to provide IO interface.
 */                                                               
 
static int ldt_ioctl (struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)
{                       
	unsigned int val;
	unsigned long mode;
	unsigned long timeval;
        
	switch (cmd) 
    {
        case LDTIOC_STOP:
            spin_lock(&ldt_lock);
            val = inb(LDT_SC);
            outb(val & 0xFE, LDT_SC);
            spin_unlock(&ldt_lock);
            //printk("cctldt: timer stopped\n");
            break;			
			
        case LDTIOC_RUN:
            spin_lock(&ldt_lock);
            val = inb(LDT_SC);
            val &= 0xEF; // Clear pending interrupt bit - otherwise it will stick
            outb(val | 0x01, LDT_SC);
            spin_unlock(&ldt_lock);
            //printk("cctldt: timer running\n");
            break;
		
        case LDTIOC_SETCLOCKSOURCE:
            if ( get_user(mode, (unsigned long *) arg) )
                return -EFAULT;
            spin_lock(&ldt_lock);
            val = inb(LDT_SC);
            if(mode==0)
                val = val & 0xDF;  //Source 1MHz
            else
                val = val | 0x20;  //Source 31.25KHz
            outb(val, LDT_SC);
            spin_unlock(&ldt_lock);
            break;

        case LDTIOC_SETMODE:
            if ( get_user(mode, (unsigned long *) arg) )
                return -EFAULT;
            spin_lock(&ldt_lock);
            val = inb(LDT_SC);
            //printk("cctldt: setmode val = 0x%X\n", val);
            if (mode == 0)
                val = val & 0xF0; //LDT mode. Clear mode value.
            else
                val = val & 0xD0; //PIT mode. Clear mode value and Clock select bit.
            val = val | (unsigned int)(mode << 1); //Set mode value.
            outb(val, LDT_SC);
            spin_unlock(&ldt_lock);		
            //printk("cctldt: mode set 0x%lX, stat= 0x%X\n", mode, val);
            break;
		
		case LDTIOC_CLRINTERRUPT:
            spin_lock(&ldt_lock);
            val = inb(LDT_SC);
            outb(val & 0xEF, LDT_SC); //Clear interrupt
            spin_unlock(&ldt_lock);
            //printk("cctldt: interrupt cleared\n");
            break;      
			
        case LDTIOC_READSTAT:
            spin_lock(&ldt_lock);
            val = inb(LDT_SC);			
            spin_unlock(&ldt_lock);
            //printk("cctldt: status read = 0x%X\n", val);
            return put_user(val,(unsigned char *)arg);
		  
        case LDTIOC_NOTIFY:
            //printk("cctldt: notify\n");
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0))
            ldt_wq_active = 1;
            interruptible_sleep_on(&ldt_wait_queue);
            ldt_wq_active = 0;
#else
            ldt_wq_active = 1;
            wait_event_interruptible( ldt_wait_queue, (ldt_wq_active==0) );
            ldt_wq_active = 0;
#endif
            //printk("cctldt: notify exit\n");
            break;
        
        case LDTIOC_CLRNOTIFY:
            if (ldt_wq_active == 1)
            {
                //printk("cctldt: wakeup_interruptible\n");
                ldt_wq_active = 0;
                wake_up_interruptible(&ldt_wait_queue);
            }
            //printk("cctldt: clrnotify\n");
            break;

        case LDTIOC_SETTIMER: 
            if ( get_user(timeval, (unsigned long *) arg) )
                return -EFAULT;                  
            spin_lock(&ldt_lock);
            val = inb(LDT_SC);
            outb(val & 0xFE, LDT_SC);//Stop timer
            val = (unsigned char) timeval;			
            outb(val, LDT_BADDR);
            val = (unsigned char) (timeval >> 8);			
            outb(val, LDT_BADDR + 1);
            val = (unsigned char) (timeval >> 16);		
            outb(val, LDT_BADDR + 2);
            val = (unsigned char) (timeval >> 24);
            outb(val, LDT_BADDR + 3);		
            spin_unlock(&ldt_lock);
            //printk("cctldt: timer set = 0x%lX\n", timeval);
            break;
		   
        case LDTIOC_GETTIMER: 
            spin_lock(&ldt_lock);
            /*
             * We do not need to stop the timer to read it. Reading the
             * LSB latches the value
             */
            val = inb(LDT_BADDR) & 0xff;
            timeval = val;			
            val = inb(LDT_BADDR + 1) & 0xff;
            timeval |= val << 8;
            val = inb(LDT_BADDR + 2) & 0xff;
            timeval |= val << 16;
            val = inb(LDT_BADDR + 3) & 0xff;
            timeval |= val << 24;

            spin_unlock(&ldt_lock);
            //printk("cctldt: timer get = 0x%lX\n", timeval);
            return put_user(timeval,(unsigned long *)arg);
		   
        default:
            return -ENOTTY;
    }
	return 0;        
}

/*
 * Function Name:   ldt_isr.
 * Parameter:       int irq - irq number, void *dev_id, struct pt_regs *regs
 * Return Value::   None.
 * Description:     This is the interrupt service routine of the LDT.
 */
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0))
static void ldt_isr(int irq, void *dev_id, struct pt_regs *regs)
#else
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,20))
irqreturn_t ldt_isr( int irq, void *dev_id )
#else
irqreturn_t ldt_isr(int irq, void *dev_id, struct pt_regs *regs)
#endif
#endif
{
    unsigned long val;

    val = inb(LDT_SC);
    //printk("cctldt: ldt_isr called. Status 0x%x\n", (int)val);

    if( ( val & 0x10 ) == 0 )
    {
        //printk("cctldt: LDT no interrupt\n");
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0))
       return IRQ_NONE;      //Interrupt has not occurred
#else
       return;
#endif
    }

    if (irqmode == 1)
      outb(val & 0xEF, LDT_SC); //Clear the interrupt

    ldt_wq_active = 0;
    wake_up_interruptible(&ldt_wait_queue); 
    //printk("cctldt: LDT interrupt detected...\n");
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0))
    return IRQ_HANDLED;      //Interrupt occurred
#else
    return;
#endif
}                           

static struct file_operations ldt_fops = {
  owner:  THIS_MODULE,
  ioctl:  ldt_ioctl,
  open:   ldt_open,
  release: ldt_release,
};

/*
 * Function Name:   ldt_init.
 * Parameter:       None.
 * Return Value:    0 - successful, negative value - failed.
 * Description:     This function is used to register the driver.
 */
INIT_FUNC(void)
{  
	int ret_val, ret;    
	unsigned int baddr = LDT_BADDR;
	spin_lock_init(&ldt_lock);

	//REGISTER CHRDEV
	//printk("cctldt: registering chrdev...\n");
	ret_val = register_chrdev(MAJOR_NUM, DEVICE_NAME, &ldt_fops);
	if (ret_val < 0)
	  {
	    printk("%s failed with %d\n", "Error registering the char device ", ret_val);
	    return ret_val;
	  }
	//printk("cctldt: chrdev registered\n");
	
	//REQUEST IO REGION
	//printk("cctldt: requesting IO region...\n");
	if (!request_region(baddr, LDT_IO_SIZE, LDT_DRIVER_NAME)) 
	{
		printk (KERN_ERR "cctldt: I/0 %d is not free\n",
				baddr);
		ret = unregister_chrdev(MAJOR_NUM, DEVICE_NAME);
		if (ret < 0)
		  //printk("Error unregistering the char device %d\n", ret);
  
		return -EIO;
	}       
	//printk("cctldt: IO region requested successfully\n");
	
	//REQUEST IRQ
	//printk("cctldt: requesting IRQ %d...\n", ldt_irq);
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0))
	if ( request_irq(ldt_irq, ldt_isr, (IRQF_DISABLED | IRQF_SHARED), "cctldt", (void*)(ldt_isr)) )
#else
	if ( request_irq(ldt_irq, ldt_isr, SA_INTERRUPT | SA_SHIRQ, "cctldt", (void*)(ldt_isr)) ) /*Does not allow shared interrupts*/

#endif
    {
	    printk ("IRQ %d is not free.\n", ldt_irq);
	    release_region(LDT_BADDR, LDT_IO_SIZE);
	    ret = unregister_chrdev(MAJOR_NUM, DEVICE_NAME);
	    if (ret < 0)
	      printk("Error unregistering the char device %d\n", ret);
	    return -EIO;
    }     
	printk("cctldt driver module loaded\n");
	return 0;
}

CLEANUP_FUNC (void)
{
  int ret;
  unsigned int baddr = LDT_BADDR;

  //printk("cctldt: Cleanup start\n");
  free_irq(ldt_irq, ldt_isr);
  release_region(baddr, LDT_IO_SIZE);
  ret = unregister_chrdev(MAJOR_NUM, DEVICE_NAME);
  if (ret < 0)
    printk("Error unregistering the char device %d\n", ret);
  printk("cctldt: module exited Ok\n");
}

#ifndef MODULE
module_init(ldt_init);
module_exit(ldt_cleanup);
#endif
MODULE_AUTHOR("jfuhrer@cct.co.uk");
MODULE_DESCRIPTION("LDT driver for CCT boards");
MODULE_LICENSE("GPL");
//EXPORT_NO_SYMBOLS;
