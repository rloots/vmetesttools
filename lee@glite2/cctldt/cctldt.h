/*******************************************************************************
 *     Copyright(c) 2005  Concurrent Technologies. All Rights Reserved.
 *
 *
 *     Long Duration Timer (LDT) driver for CCT boards.
 *
 *     This program is free software; you can redistribute it and/or modify 
 *     it under the terms of the GNU General Public License as published by 
 *     the Free Software Foundation; either version 2 of the License, or 
 *     any later version.
 *		
 *     CCT does not admit liability nor provide warranty for this software. 
 *     This material is provided "AS-IS" and at no charge.
 *
 *
 *     Module Name:      ldt.c
 *
 *     Abstract:         This module supports CCT boards LDT. 
 *                       All of the driver functions are provided in this file.
 * 
 *     Environment:      This file is intended to be specific to Linux 
 *                       operating system.
 *
 *
 *     Version 1.0      1st drop for beta linux driver packages for ldt
 *     September 22 2005
 *******************************************************************************/

#ifndef CCTLDT_H
#define CCTLDT_H

#include <linux/ioctl.h>

#define LDT_BADDR 0x218L

//LDT Status and Control Register (I/O 21Ch)
#define LDT_SC 0x21cL	

#define LDT_IO_SIZE 5   
#define LDT_DRIVER_NAME "cctldt"
#define DEVICE_NAME "cctldt"    
#define LDT_IOCTL_BASE 60
#define MAJOR_NUM 60
#define SUCCESS 0


////////////
/* IOCTLs */
////////////

//Stop the timer
#define	LDTIOC_STOP   		_IOWR(LDT_IOCTL_BASE,8, unsigned long)

//Start the timer
#define	LDTIOC_RUN   		_IOWR(LDT_IOCTL_BASE,9, unsigned long)

//Set LDT/PIT mode. Parameter:
/*
   0 = LDT
   1 = PIT 100Hz
   2 = PIT 200Hz
   3 = PIT 500Hz
   4 = PIT 1000Hz
   5 = PIT 2000Hz
   6 = PIT 5000Hz
   7 = PIT 10000Hz
*/
typedef enum {
    LDT_MODE_LDT = 0,
    LDT_MODE_PIT_100,
    LDT_MODE_PIT_200,
    LDT_MODE_PIT_500,
    LDT_MODE_PIT_1000,
    LDT_MODE_PIT_2000,
    LDT_MODE_PIT_5000,
    LDT_MODE_PIT_10000,
} cctldt_ldt_mode;

#define	LDTIOC_SETMODE 		_IOWR(LDT_IOCTL_BASE,10, unsigned long)

//Clear INTERRUPT FLAG bit of Stat&Ctrl register
#define	LDTIOC_CLRINTERRUPT	_IOWR(LDT_IOCTL_BASE,11, unsigned long)

//Read Status & Control register
#define	LDTIOC_READSTAT   	_IOR(LDT_IOCTL_BASE,12, unsigned long)

//User mode program wait on interrupt
#define	LDTIOC_NOTIFY  		_IOWR(LDT_IOCTL_BASE,13, unsigned long)

//Stop waiting on interrupt
#define	LDTIOC_CLRNOTIFY	_IOWR(LDT_IOCTL_BASE,14, unsigned long)

//Set timer register value (Parameter: 4 bytes value)
#define	LDTIOC_SETTIMER		_IOWR(LDT_IOCTL_BASE,15, unsigned long)

//Get timer register value (Parameter: 4 bytes value)
#define	LDTIOC_GETTIMER		_IOWR(LDT_IOCTL_BASE,16, unsigned long)

//Set LDT/PIT clock source; Parameter:
// 0 = 1MHz, 1 = 31.25KHz
#define	LDTIOC_SETCLOCKSOURCE	_IOWR(LDT_IOCTL_BASE,17, unsigned long)

        
#endif


