/*
 * =====================================================================================
 *
 *       Filename:  vmecon.h
 *
 *    Description:   VME enhanced interface for the Concurrent VME board processor(tsi148)
 *                   using the mvmestd.h VME call convention. This code was taken
 *                   from the vmicvme.h source written by Pierre-Andre Amaudruz.    
 *
 *
 *                   
 *        Version:  1.0
 *        Created:  01/22/2009 12:51:17 PM
 *       Revision:  Removing Legacy VME api and insert Enhanced VME api
 *       Compiler:  gcc
 *
 *         Author:  Lee Pool (mr), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Bases Sciences
 *
 * =====================================================================================
 */
#ifndef  _VMECON__INC
#define  _VMECON__INC

/* #####   HEADER FILE INCLUDES - Local - ############################################## */
#include 	"mvmestd.h"

#endif   /* ----- #ifndef _VMECON__INC  ----- */
