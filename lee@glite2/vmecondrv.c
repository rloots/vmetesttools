/*
 * =====================================================================================
 *
 *       Filename:  vmecondrv.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  19/02/2009 12:15:09
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 * =====================================================================================
 */

/* #####   HEADER FILE INCLUDES   ################################################### */
#include	<string.h>
#include	<stdlib.h>
#include	<stdio.h>
#include	<signal.h>
#include	<assert.h>
#include	<unistd.h>
#include	<asm/page.h>
#include 	<sys/ioctl.h>
#include 	<errno.h>

#include "vmecondrv.h"

/* #####   MACROS  -  LOCAL TO THIS SOURCE FILE   ################################### */
#define A32_CHUNK	0x00FFFFFF
#define DEBUG		0
//#define MAIN_ENABLE

#define VME_MASTER_MAGIC 0x114a0002
#define VME_MASTER_MAGIC_NULL 0x0

/* #####   DATA TYPES  -  LOCAL TO THIS SOURCE FILE   ############################### */
vme_bus_handle_t	bus_handle;

vme_interrupt_handle_t	int_handle;

struct	sigevent	event;
struct	sigaction	handler_act;
INT_INFO		int_info;
UINT32 _VME_BASE_ADDR_  = 0xb0000;






/*-----------------------------------------------------------------------------
 *  Helper Section!!!
 *-----------------------------------------------------------------------------*/
void *vme_master_window_phys_addr( vme_bus_handle_t bus_handle,
				   vme_master_handle_t handle )
{
	/*
	if( (NULL == handle) || (VME_MASTER_MAGIC != handle->magic) ){
		errno = EINVAL;
		return NULL;
	}
	*/
	return NULL;
	//return handle->ctl.paddr;
}




int vme_init( vme_bus_handle_t *handle )
{
	//int result;
	int* devHandle;

	if( handle == NULL ){
		perror("Error, Initialized devHandle is set to NULL!!!");
		return(MVME_INVALID_PARAM);
	}

	*devHandle = vme_openDevice("ctl");
	
	if( devHandle >= 0  ) {
		//handle = &devHandle; /* Set device/bus handle */
		handle = devHandle; /* Set device/bus handle */
	}
	else {
	       perror("Error, Initialization of ctl interface failed");
		return(MVME_NO_INTERFACE);

	}
	

	//vme_closeDevice(*handle);

	return(MVME_SUCCESS);
}


int vme_term( vme_bus_handle_t devHandle )
{
	
	int result;
	if( devHandle == 0 ){
		perror("Error, Device Handle is NULL cannot close device!");
		return(MVME_NO_INTERFACE);
	}
	
	result = vme_closeDevice( (UINT32)devHandle );
	if( result < 0 ){
		perror("Error, Could NOT close Device");
		return(MVME_NO_INTERFACE);
	}

	devHandle = -1;	
	return(MVME_SUCCESS);	
}


void *vme_mmap_phys( int devHandle, unsigned long paddr, size_t size )
{
	unsigned long mapaddr, off, mapsize;
	size_t ps = getpagesize();
	UINT32 *ptr;
	int result;

	/* Align Page */
	off = paddr % ps;
	mapaddr = paddr - off;
	mapsize = size + off;
	mapsize += (mapsize % ps) ? ps - (mapsize % ps) : 0 ;

	result = vme_mmap( devHandle , off, mapsize, ptr );
	if( result <  0 ){
		perror(" Error in mmap to phys!");
		return NULL;
	}

	return ptr;
}

int vme_unmap_phys( unsigned long vaddr, size_t size,  int devHandle )
{
	unsigned long mapaddr, off, mapsize;
	size_t	ps = getpagesize();
	int result;

	off = vaddr % ps;
	mapaddr = vaddr - off;
	mapsize = size + off;
	mapsize += (mapsize % ps ) ? ps - (mapsize % ps ) : 0 ;

	result = vme_unmap( devHandle, (UINT32)vaddr, (UINT32)mapsize );
        
	if( result < 0 ){
		perror(" Error in unmap!");
		return(ERROR);
	}	

	return(MVME_SUCCESS);
}


/*-----------------------------------------------------------------------------
 *  create pci/vme image windows
 *-----------------------------------------------------------------------------*/
int vme_master_window_create_( vme_bus_handle_t bus_handle,
			      UINT32 *handle,
			      UINT32 vme_addr, 
			      int am, 
			      size_t size, 
			      int flags, 
			      void *phys_addr )
{

	EN_PCI_IMAGE_DATA idata;
	int devHandle, result;
	
	if( handle == NULL ){
		perror("Error, handle is NULL!");
		return(MVME_NO_INTERFACE);
	}


	idata.pciAddress = 0x00000000;
        idata.pciAddressUpper=0;
        idata.pciBusSpace=0;
        idata.vmeAddressUpper=0;
        idata.sizeUpper = 0;
        idata.postedWrites = 1;
	idata.readPrefetch=1;
        idata.prefetchSize= 3;
        idata.dataWidth = EN_VME_D16;
        idata.addrSpace = am;
        idata.type = EN_LSI_DATA;
        idata.mode = EN_LSI_USER;
        idata.vmeCycle = 0;
        idata.sstbSel =0;
        idata.ioremap = 1;
	idata.vmeAddress = vme_addr;
	idata.size = size;
	
	
	
	
	
	
	result = vme_enablePciImage( devHandle , &idata );
	if( result < 0 ) {
		perror("Error Failed to init Image\n");
		return(MVME_ACCESS_ERROR);
	}
	

	// (*handle)-> magic  = VME_MASTER_MAGIC; /* TO BE CHANGED */	
			

	return(MVME_SUCCESS);
}

int vme_master_window_release( vme_bus_handle_t bus_handle,
			       vme_master_handle_t handle )
{

	//int rval = 0;
	int result;


	if( handle == 0 ) {
		perror("Error: Handle passed as NULL couldn't close image");
		return(MVME_ACCESS_ERROR);
	}

	result = vme_disablePciImage(handle);
	if( result < 0 ){
		perror("Error: Could not close device!");
		return(MVME_ACCESS_ERROR);
	}

	
	return(MVME_SUCCESS);
}

void* vme_master_window_phys_paddr( vme_bus_handle_t bus_handle,
				   vme_master_handle_t *handle )
{
	if( handle == NULL ){
		perror("Error");
		return(MVME_ACCESS_ERROR);
	}

				
	//return (*handle)->ctl.paddr;
	
	return NULL;
}


void* vme_master_window_mmap( vme_bus_handle_t bus_handle,
			     vme_master_handle_t *handle,
			     int flags )
{

	//int result;
	//UINT32 offset, length;
	UINT32 *uptr;

	if( handle == NULL ){
		perror("Error!");
		return(MVME_ACCESS_ERROR);
	}

	
	//result = vme_mmap_phys( bus_handle , (unsigned long)(*handle)->ctl.paddr, (*handle)->ctl.size );
	
	//(*handle)->vptr = uptr;

	return uptr;
}


/* Will be revised */
void* vme_master_window_unmmap( vme_bus_handle_t bus_handle,
		               vme_master_handle_t handle )
{
	
	if( handle == NULL ) {
		perror("Error!");
		return(MVME_ACCESS_ERROR);
	}

	//return vme_unmap_phys( (unsigned long) handle->vptr, handle->ctl.size );

	return NULL;
}



int vcon_mmap( MVME_INTERFACE *mvme, 
	       mvme_addr_t vme_addr, 
	       mvme_size_t n_bytes )
{

	int j;
	void *phys_addr = NULL;
	VME_TABLE2 *table;


	table = (VME_TABLE*)mvme->table;
	
	/* Find Clean slot in this table */
	j = 0;
	while( table[j].valid ) { j++; }


	/* Either Found a Table or Table is full */
	if( j < MAX_VME_SLOTS ) {
		/* Create Image */
		table[j].addr = vme_addr;
		table[j].am = ((mvme->am == 0) ? MVME_AM_DEFAULT : mvme->am );
		table[j].nbytes = n_bytes;
	}
	

	return(MVME_SUCCESS);
}




int vcon_unmap( MVME_INTERFACE *mvme, mvme_addr_t vme_addr, mvme_size_t size )
{
	int 		j,result;
	VME_TABLE	*table;
	

	table = (VME_TABLE*)mvme->table;


	for( j = 0; table[j].valid; j++ ) {
		if( (vme_addr == table[j].low) && ((vme_addr+size) == table[j].high )){
			break;
		}
	}

	if( !table[j].valid ) {
		return(MVME_SUCCESS);
	}

	/* remove map */
	if( table[j].ptr ) {
		result = vme_master_window_unmmap( (vme_bus_handle_t)mvme->handle, table[j].wh );
		if( result < 0 ){
			perror("vme_master_window_unmap");
			return(ERROR);
		}

		result = vme_master_window_release( (vme_bus_handle_t)mvme->handle, table[j].wh );
		if(result < 0 ){
			perror("vme_master_window_release");
			return(ERROR);
		}
	}

	table[j].wh = 0;
	return(MVME_SUCCESS);
}


mvme_addr_t vcon_mapcheck( MVME_INTERFACE *mvme, mvme_addr_t vme_addr, mvme_size_t n_bytes )
{
	int j;
	int result;
	VME_TABLE *table;
	mvme_addr_t addr;
	
	table = (VME_TABLE*)mvme->table;
	
	for( j=0; table[j].valid; j++ ){
		if( mvme->am != table[j].am )
			continue;

		if( (vme_addr >= table[j].low) &&
		    ((vme_addr + n_bytes) < table[j].high) ){
			break;
		}
	}
	
	if( !table[j].valid ){
		if( vme_addr > A32_CHUNK ){
			addr = vme_addr & (~A32_CHUNK);
	        }else{
			addr = 0x00000000;
		}

		result = vcon_mmap( mvme, addr, mvme->am );
		if(result < 0 ){
			perror("cannot create vme map window");
			abort();
		}
	}
	
	for(j = 0; table[j].valid; j++ ){
		if( mvme->am != table[j].am ){
			continue;
		}
		
		if( (vme_addr >= table[j].low) &&
		    ((vme_addr + n_bytes) < table[j].high) ){
			break;
		}
	}

	if( !table[j].valid ){
		perror("Map not found at j = %d ");
		printf(" j = %d \n ", j );
		abort();
	}

	addr = (mvme_addr_t)(table[j].ptr) + (mvme_addr_t)(vme_addr - table[j].low);
	
	return addr;
}

int vcon_init_devices( MVME_INTERFACE *mvme, mvme_addr_t vme_base_addr, mvme_size_t resolution )
{
	int devHandle, result;
	VME_TABLE2 *table,*t2;

	table = (VME_TABLE2*)mvme->table;
	
	printf(" size of table = %u \n", sizeof(table));
		
	EN_PCI_IMAGE_DATA data;		/* D16 and D32 image data */
	
	/* default Image data */
	data.pciAddress=0x00000000;
        data.pciAddressUpper=0;
        data.pciBusSpace=0;
	data.vmeAddress=vme_base_addr;
        data.vmeAddressUpper=0;
        data.size = resolution;
        data.sizeUpper = 0;
        data.postedWrites = 1;
	data.readPrefetch=1;
        data.prefetchSize= 3;
        data.addrSpace = EN_VME_A32;		/*Default*/
        data.type = EN_LSI_DATA;
        data.mode = EN_LSI_USER;
        data.vmeCycle = 0;
        data.sstbSel =0;
        data.ioremap = 1;
		
	EN_VME_DIRECT_TXFER tdata;		/* Direct DMA transfer(READ) */

	/* default dma data */
	tdata.direction = EN_DMA_READ;
	tdata.offset = 0;			/* Start of DMA buffer 
					 	 * On v792 its 0x0 
					 	 */

	tdata.size = 4096;
	tdata.vmeAddress = vme_base_addr;
	
	tdata.txfer.timeout = 200;		/* 2 second time out */
	tdata.txfer.vmeBlkSize = TSI148_4096;
	tdata.txfer.vmeBackOffTimer = 0;
	tdata.txfer.pciBlkSize = TSI148_4096;	
        tdata.txfer.pciBackOffTimer = 0;

	tdata.access.sstMode = TSI148_SST320;
	tdata.access.vmeCycle = 0;
	tdata.access.sstbSel = 0;

/* CHAGEABLE */	tdata.access.dataWidth = EN_VME_D32;	/* v792 D32 */
/* CHANGEABLE*/	tdata.access.addrSpace = EN_VME_A32;	/* default am */

	tdata.access.type = EN_LSI_DATA;
	tdata.access.mode = EN_LSI_USER;

	/* Create D16 pci device */
	devHandle = vme_openDevice("lsi2");
	if( devHandle < 0 ){
		perror("[vcon_init_devices] Could not create device for d16\n");
		return(MVME_ERROR);
	}else {
		data.dataWidth = EN_VME_D16;
		
		result = vme_enablePciImage( devHandle, &data );
		if( result < 0 ){
			perror("[vcon_init_devices] Could not create PCI Image {2} for d16\n");
			return(MVME_ERROR);
		}
		
		table[0].devHandle = devHandle;
		table[0].nbytes = 2;
		table[0].addr = vme_base_addr;
		table[0].am = ((mvme->am == 0) ? MVME_AM_DEFAULT : mvme->am );
		table[0].valid = 1;
	}/**D16*/

	/* Kill previous device Handle */
	devHandle = -1;


	/* Create D32 pci device */
	devHandle = vme_openDevice("lsi3");
	if( devHandle < 0 ){
		perror("[vcon_init_devices] Could not create device for d16\n");
		return(MVME_ERROR);
	}else {
		data.dataWidth = EN_VME_D32;
		
		result = vme_enablePciImage( devHandle, &data );
		if( result < 0 ){
			perror("[vcon_init_devices] Could not create PCI Image {3} for d16\n");
			return(MVME_ERROR);
		}
		
		table[1].devHandle = devHandle;
		table[1].nbytes = 2;
		table[1].addr = vme_base_addr;
		table[1].am = ((mvme->am == 0) ? MVME_AM_DEFAULT : mvme->am );
		table[1].valid = 1;
	}/**D32*/
	
	/* Kill previous device Handle */
	devHandle = -1;

	/* Create D32 DMA device */
	devHandle = vme_openDevice("dma0");
	if( devHandle >= 0 ){
		result = vme_allocDmaBuffer( devHandle, &resolution );
		if( result < 0 ){
			perror("[vcon_init_devices] Could not allocDmaBuffer \n");
			return(MVME_ERROR);
		}else{
			printf("[vcon_init_devices] DMA Buffer allocated, size = %u \n",resolution);
		}
					
		result = vme_dmaDirectTransfer( devHandle, &tdata );
		if( result < 0 ){
			perror("[vcon_init_devices] DMA direct transfer failed\n");
			return(MVME_ERROR);
		}
		
		table[2].devHandle = devHandle;
		table[2].nbytes = 128;
		table[2].addr = vme_base_addr;
		table[2].am = ((mvme->am == 0) ? MVME_AM_DEFAULT : mvme->am );
		table[2].valid = 1;
	}/**DMA*/
	
	
	mvme->table = table;
//	t2 = (VME_TABLE2*)(mvme->table);
//	printf(" D16 Handle = %u , equales mvme %u \n", table[0].devHandle,t2[0].devHandle );
//	printf(" D32 Handle = %u \n", table[1].devHandle );
//	printf(" DMA Handle = %u \n", table[2].devHandle );
	

	return(MVME_SUCCESS);
}


void Write_Value( MVME_INTERFACE *myvme,  mvme_addr_t vme_Addr, EN_PCI_IMAGE_DATA *data, UINT8 *Value  )
{
  int devHandle, result;
  UINT32 Offset;
  int numbyts = 0;	
  UINT32 pci_addr = 0x00000000;	

  if (myvme->dmode == MVME_DMODE_D16){
 	
//	printf("\n\t\t\t D16 Mode \t "); 
	data->pciAddress=pci_addr;
        data->pciAddressUpper=0;
        data->pciBusSpace=0;
	data->vmeAddress=_VME_BASE_ADDR_;
        data->vmeAddressUpper=0;
        data->size = 0x10000;
        data->sizeUpper = 0;
        data->postedWrites = 1;
	data->readPrefetch=1;
        data->prefetchSize= 3;
        data->dataWidth = EN_VME_D16;
        data->addrSpace = EN_VME_A32;
        data->type = EN_LSI_DATA;
        data->mode = EN_LSI_USER;
        data->vmeCycle = 0;
        data->sstbSel =0;
        data->ioremap = 1;
	numbyts = 2;

  }
  else if (myvme->dmode == MVME_DMODE_D32){
 	
//	printf("\n\t\t\t D32 Mode \t "); 
	data->pciAddress=pci_addr;
	data->pciAddressUpper=0;
        data->vmeAddress=_VME_BASE_ADDR_;
        data->vmeAddressUpper=0;
        data->size = 0x10000;
        data->sizeUpper = 0;
        data->readPrefetch=1;
        data->prefetchSize= 3;
        data->dataWidth = EN_VME_D32;
        data->addrSpace = EN_VME_A32;
        data->type = EN_LSI_DATA;
        data->mode = EN_LSI_USER;
        data->vmeCycle = 0;
        data->sstbSel =0;
        data->ioremap = 1;
	numbyts = 4;

  }
//  	 printf("\n VME Base addr = 0x%02x \n ", _VME_BASE_ADDR_ );
//	 printf(" VME Offset + VME BASE  = 0x%02x \n ", vme_Addr );
//	 Offset =  vme_Addr - _VME_BASE_ADDR_;
//	 printf(" VME Addr - VME BASE  = 0x%02x \n ", Offset );

        Offset = vme_Addr - _VME_BASE_ADDR_;
	devHandle = vme_openDevice("lsi6");
	if( devHandle < 0 ){
		printf("[Write_Value] Unable to open PCI image lsi6, to write value %x \n ",*Value);
		return(MVME_ERROR);
	}

	
	result = vme_enablePciImage(devHandle, data );
	if( result < 0 ) {
		printf("[Write_value] Failed to enable PCI Image\n");
		return(MVME_ERROR);
	}
	
//	printf("\n\t [Trying to Write] devHandle = [%d], Offset = [0x%02x], Value = [0x%02x], numbyts = [%d] \n ", devHandle, 
//											       Offset,
//											       *Value,
//											       numbyts );
	result = vme_write( devHandle, Offset , Value , numbyts );
	
	if( result < 0 ) {
		printf("[Write_value] Failed to write data \n");
	}else {
//	   printf(" Value Read = <<<< %d >>>> \n ", addr );
	}


	


	result = vme_disablePciImage( devHandle );
	if( result < 0 ){
		printf("[Write_value] Failed to disable PCI Image\n");
		return(MVME_ERROR);
	}
	vme_closeDevice(devHandle);
}

void Read_Value( MVME_INTERFACE *myvme,  mvme_addr_t vme_Addr, EN_PCI_IMAGE_DATA *data, DWORD *DST )
{
  int devHandle, result;
  UINT32 Offset;
  int numbyts;
  mvme_addr_t *addr;
  UINT32 pci_addr = 0x00000000;
  DWORD dst = 0xFFFFFFFF;
  UINT8 *buffer;

  if (myvme->dmode == MVME_DMODE_D16){
 	
	data->pciAddress=pci_addr;
        data->pciAddressUpper=0;
        data->vmeAddress=_VME_BASE_ADDR_;
        data->vmeAddressUpper=0;
        data->size = 0x10000;
        data->sizeUpper = 0;
        data->readPrefetch=1;
        data->prefetchSize= 3;
        data->sstMode = TSI148_SST320;
        data->dataWidth = EN_VME_D16;
        data->addrSpace = EN_VME_A32;
        data->type = EN_LSI_DATA;
        data->mode = EN_LSI_USER;
        data->vmeCycle = 0;
        data->sstbSel =0;
        data->ioremap = 1;
	numbyts = 2;

  }
  else if (myvme->dmode == MVME_DMODE_D32){
 	
	data->pciAddress=pci_addr;
        data->pciAddressUpper=0;
        data->vmeAddress=_VME_BASE_ADDR_;
        data->vmeAddressUpper=0;
        data->size = 0x10000;
        data->sizeUpper = 0;
        data->readPrefetch=1;
        data->prefetchSize= 3;
        data->sstMode = TSI148_SST320;
        data->dataWidth = EN_VME_D32;
        data->addrSpace = EN_VME_A32;
        data->type = EN_LSI_DATA;
        data->mode = EN_LSI_USER;
        data->vmeCycle = 0;
        data->sstbSel =0;
        data->ioremap = 1;
	numbyts = 4;
  }


	Offset = vme_Addr - _VME_BASE_ADDR_;
	
	devHandle = vme_openDevice("ctl");
	UINT8 enable = 1;	
	result = vme_setByteSwap( devHandle, enable>>6 );
	vme_closeDevice(devHandle);
	devHandle = -1;

	devHandle = vme_openDevice("lsi0");
	if( devHandle < 0 ){
		printf("[Read_Value] Unable to open PCI image lsi0 \n ");
		return(MVME_ERROR);
	}

	
	result = vme_enablePciImage(devHandle, data );
	if( result < 0 ) {
		printf("[Read_value] Failed to enable PCI Image\n");
		return(MVME_ERROR);
	}
	
	result = vme_read( devHandle, Offset , DST , numbyts );

	if( result < 0 ) {
		printf("[Read_value] Failed to read data \n");
	}else {
	        DWORD dst = 0xFFFFFFFF;

//		int k = 0;
//		for( k = 0; k < 3; k++ ){
//			printf(" 0x%02x\n",buffer[k]);
//		}

//	   printf(" Addr: 0x%8x \n , Offset 0x%8x , Read = <<<< 0x%02x >>>> , MODE %d , bytes %d , result %d \n", vme_Addr, Offset, *DST, myvme->dmode, numbyts , result );
	}



	result = vme_disablePciImage( devHandle );
	if( result < 0 ){
		printf("[Read_value] Failed to disable PCI Image\n");
		return(MVME_ERROR);
	}
	vme_closeDevice(devHandle);
}


/* Private Functions */

void simpleTestDMADirectTransfer( int devdma, EN_VME_DIRECT_TXFER data ) 
{

	UINT32 *buffer;
	int result;
	UINT32 offset = 0;
	UINT32 length = 32;
	int i;
	int width = 256;


	buffer  = (UINT32 *)malloc(width*sizeof(UINT32));
	
	
	result = vme_dmaDirectTransfer( devdma , &data );
	if( result < 0 ){
		perror("DMA transfer failled");
		return(MVME_ERROR);
	}
	else {

		result = vme_read( devdma, offset, buffer, width );
		if( result > 0 ){
			
			for( i = 0; i < width; i++ ) {
				printf(" %02x ",buffer[i]);
			}
			printf("\n");
		}
	}

	result = vme_freeDmaBuffer( devdma );
	if(result < 0){
		perror(" Error freeing dma buffer!\n");
		return(MVME_ERROR);
	}	


}


