/******************************************************************************
*
* Filename: 	Readme.txt
* 
* Description:	Long Duration Timer device driver & test program for Linux.
*
* $Revision$
*
* $Date$
*
* $Source$
*
* Copyright 2000-2009 Concurrent Technologies.
*
* This program is free software; you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation; either version 2 of the License, or 
* any later version.
*		
* CCT does not admit liability nor provide warranty for this software. 
* This material is provided "AS-IS" and at no charge.
*
******************************************************************************/

This sample package comprises two parts, the kernel driver (cctldt) and a
sample application (testldt). They can be used to access the Long Duration
Timer (LDT) hardware found on Concurrent Technology Single Board Computers.

This hardware has two distinct modes of operation, namely Long Duration Timer
(LDT) and Periodic Interrupt Timer (PIT).

LDT Operation
=============

Here the timer is started and can be read back at any time. The default clock
used to strobe the timer is 1MHz, however a 31.25kHz clock can optionally
be selected. With 1 MHz the timer 'rolls over' after approximately 72
minutes, or around 38 hours with the 31.25kHz clock. 

An interrupt is generated when this occurs.


PIT Operation
=============

This provides a regular 'tick' interrupt to the application. With the 1MHz
clock, preset values of the tick of 100Hz, 200Hz, 500Hz, 1kHz, 2kHz, 5kHz and
10kHz are possible. The 31.25kHz clock reduces each of these by a factor of
32.

To ensure the first 'tick' is the full duration, the timer should be cleared
before starting.

Requirements
============

The kernel driver is Linux specific and has only been tested on Redhat
Enterprise Linux (RHEL) 5.2 at time of writing. This is kernel 2.6.18 based,
so other distibutions with a similar kernel version should also work.

The kernel module needs to be compiled against the headers for the kernel
being used. These headers together with a valid installation of the compiler
must be available for the module compilation to succeed. This can be
achieved on RHEL 5.2 by performing a default installation with the addition
of 'development tools'.


Build kernel module
===================

[root@VP317 ldt]# cd cctldt/
[root@VP317 cctldt]# make
mkdir kernel-2.6.18-92.el5
make -C /lib/modules/2.6.18-92.el5/source SUBDIRS=/root/ldt/cctldt  modules 
make[1]: Entering directory `/usr/src/kernels/2.6.18-92.el5-i686'
  CC [M]  /root/ldt/cctldt/cctldt.o
  LD [M]  /root/ldt/cctldt/cctldt_driver.o
  Building modules, stage 2.
  MODPOST
  CC      /root/ldt/cctldt/cctldt_driver.mod.o
  LD [M]  /root/ldt/cctldt/cctldt_driver.ko
make[1]: Leaving directory `/usr/src/kernels/2.6.18-92.el5-i686'
cp cctldt_driver.ko kernel-2.6.18-92.el5

Install kernel module
=====================

[root@VP317 cctldt]# ./ins
Loading cctldt_driver module...
Removing old device files...
Creating new device files...
Installation complete
[root@VP317 cctldt]#

Build sample application
========================

[root@VP317 cctldt]# cd ../testldt/
[root@VP317 testldt]# make
gcc -I ../cctldt -o testldt testldt.c -lpthread
[root@VP317 testldt]# 

Run sample application
======================

[root@VP317 testldt]# ./testldt 
Timer opened on fd 3
PIT Test - count ticks for a second. Timer set to 100Hz so
it should be 100 ticks
Start timer
Thread start
Stop timer...
Ticks: 100
Cancel thread...
LDT test - count for 5 seconds reading timer value each second
Start timer
Loop: 0 Timer count: 0001001571
Loop: 1 Timer count: 0002002433
Loop: 2 Timer count: 0003003303
Loop: 3 Timer count: 0004004149
Loop: 4 Timer count: 0005005008
Stop timer...
Done
[root@VP317 testldt]# 


