#ifndef V259_INCLUDE_H
#define V259_INCLUDE_H

#include "mvmestd.h"


#define  V259_MODULE_RESET_WO	(DWORD)	(0x20)
#define  V259_PATTERN_RST_RO    (DWORD) (0x22)
#define  V259_ID_RO		(DWORD) (0xFA)
#define  V259_VERSION_RO        (DWORD) (0xFC)
#define  V259_MANUFACT_RO       (DWORD) (0xFE)

#ifdef __cplusplus
extern "C" {
#endif
	
	int  v259_Status(MVME_INTERFACE *mvme, DWORD base);
	//int  v259_DataRead(MVME_INTERFACE *mvme, DWORD base, DWORD *pdest, int nentry);
	//int v259_ReadCounter(MVME_INTERFACE *mvme, DWORD base, int n);
	void v259_SoftReset(MVME_INTERFACE *mvme, DWORD base);
	void v259_SoftClear(MVME_INTERFACE *mvme, DWORD base);
	//void v259_SetTriggerRandom(MVME_INTERFACE *mvme, DWORD base);
	//void v259_SetTriggerDisabled(MVME_INTERFACE *mvme, DWORD base);
	//void v259_SoftTrigger(MVME_INTERFACE *mvme, DWORD base);
	//int v830_ReadTriggerCounter(MVME_INTERFACE *mvme, DWORD base);
	int v259_EvtCounter(MVME_INTERFACE *mvme, DWORD base);
	WORD v259_ReadPatternReset(MVME_INTERFACE *mvme, DWORD base);
#ifdef __cplusplus
} //extern "C"
#endif

#endif // V259_INCLUDE_H


