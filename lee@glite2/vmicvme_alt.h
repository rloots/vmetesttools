/*
 * =====================================================================================
 *
 *       Filename:  vmicvme_alt.h
 *
 *    Description:   VME enhanced interface for the Concurrent VME board processor(tsi148)
 *                   using the mvmestd.h VME call convention. This code was taken
 *                   from the vmicvme.h source written by Pierre-Andre Amaudruz.    
 *
 *
 *                   
 *        Version:  1.0
 *        Created:  01/22/2009 12:51:17 PM
 *       Revision:  Removing Legacy VME api and insert Enhanced VME api
 *       Compiler:  gcc
 *
 *         Author:  Lee Pool (mr), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Bases Sciences
 *
 * =====================================================================================
 */
#ifndef  _VMICVME_ALT__INC
#define  _VMICVME_ALT__INC

/* #####   HEADER FILE INCLUDES   ################################################### */
#include	<stdio.h>
#include	<string.h>
#include	"vme_api_en.h"

/* #####   HEADER FILE INCLUDES - Local - ############################################## */
#include 	"mvmestd.h"

/* #####   EXPORTED DEFINE STATEMENTS  ################################################# */
#ifndef MIDAS_TYPE_DEFINED
typedef unsigned long int 	DWORD;
typedef unsigned short int 	WORD;
typedef unsigned char	   	BYTE;
#endif

#ifndef SUCCESS
#define SUCCESS	 		(int) 1
#endif

#define ERROR			(int) -1000
#define MVME_ERROR		(int) -1000
#define MAX_VME_SLOTS		(int) 32
#define DEFAULT_SRC_ADD 	0x000000
#define DEFAULT_NBYTES		0xFFFFFF	/* 16MB */
#define DEFAULT_DMA_NBYTES 	0x100000	/* max DMA size in bytes */

/* #####   EXPORTED DATA TYPES   #################################################### */
typedef struct {
	int deviceHandle; 
	void *id;
	ULONG vaddr;
	unsigned long size;
	int am;
	long flags;
	void *paddr;
}vmectl_window_t;



//typedef struct {
//	int handle;
//}vme_bus_handle_t;	


typedef UINT32 vme_master_handle_t;
typedef UINT32 vme_bus_handle_t;


typedef struct {
	int handle;
}vme_dma_handle_t;

typedef struct {
	int handle;
}vme_interrupt_handle_t;

typedef struct {
	vmectl_window_t ctl;
	int magic;
	void *vptr;
}_vme_master_handle_t; 




typedef struct {
	vme_master_handle_t  wh;
	mvme_size_t	nbytes;
	mvme_addr_t	low;
	mvme_addr_t	high;
	int	am;
	int	valid;
	void	*ptr;
}VME_TABLE;


typedef struct {
	UINT32 	devHandle;
	mvme_size_t	nbytes;
	mvme_addr_t	addr;
//	mvme_addr_t	high;
	int	am;
	int	valid;
	void	*ptr;
}VME_TABLE2;


typedef struct {
	vme_dma_handle_t dma_handle;
	void *dma_ptr;
}DMA_INFO;


typedef struct {
	vme_interrupt_handle_t	handle;
	int	level;
	int 	vector;
	int 	flags;
}INT_INFO;	



#endif   /* ----- #ifndef _VMICVME_ALT__INC  ----- */
