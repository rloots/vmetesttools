#include "CAENv785.hh"
#include "daqsrt.hh"
#include <cstdlib>
#include <unistd.h>

int MAXEVENT=100000;

#include <sys/time.h>
inline double tdiff(struct timeval T1, struct timeval T2) {
        return ( static_cast<double>(T2.tv_sec) - static_cast<double>(T1.tv_sec) ) +
                (static_cast<double>(T2.tv_usec) - static_cast<double>(T1.tv_usec))/1000000.0;
}


int main (int argc, char* argv[]) {

	DWORD VMEIO_BASE = 0x780000;
	DWORD V792_BASE = 0x300000;
  
	MVME_INTERFACE *myvme;

	if (argc>1) {
		sscanf(argv[1],"%lx",&V792_BASE);
	}

	Sched::Init();
	Sched::FAST();

	// Test under vmic
	int status = mvme_open(&myvme, 0);
	if (status!=1) exit(1);

	// Set am to A24 non-privileged Data
	mvme_set_am(myvme, MVME_AM_A24_ND);

	// Set dmode to D32
	mvme_set_dmode(myvme, MVME_DMODE_D32);

	CAENv785 adc(myvme,V792_BASE);

	//  adc.SingleShotReset();
	{
		for (int i=0;i<CAENv785::MAX_CHANNELS;i++) {
			adc.threshold[i] = 20;
		}
		adc.ThresholdWrite();
		adc.cr1.BlkEnd=1;
		adc.ControlRegister1Write();
		CAENv7XX::BitSet2Register bs;
		bs.LowThresh=1;
		adc.BitSet2RegisterSet(bs);
	}

	adc.getStatus();

    int csr = adc.DataReady();
	printf("Data Ready: 0x%x\n", csr);

	{
		// Read Event Counter
		DWORD cnt=adc.EvtCntRead();
		printf("Event counter: 0x%lx = %ld\n", cnt,cnt);
	}
	// Set 0x3 in pulse mode for timing purpose
	//status = mvme_write_value(myvme, VMEIO_BASE+0x8, 0xF); 
	//printf("st=%d\n",status);

	// Write pulse for timing purpose
	//status = mvme_write_value(myvme, VMEIO_BASE+0xc, 0x2);

	#if 0
	{
		DWORD dest[1000];
		int cnt=adc.DataRead(dest);
		printf("Event counter: 0x%lx = %d\n", cnt,cnt);
		
		// Write pulse for timing purpose
		//status = mvme_write_value(myvme, VMEIO_BASE+0xc, 0x8);
		
		for (i=0;i<32;i++) {
			printf("Data[%2i]=0x%6lx %6ld\n", i, dest[i]&0xFFFFFF,dest[i]&0xFFFFFF);
		}
	}
    #endif  


	//  status = 32;
	
	{
		//DWORD evCnt;
		DWORD data[1000];
		memset(data,sizeof(DWORD)*1000,0);
		
		adc.EvtCntReset();

        struct timeval T1,T2;
        struct timezone TZ;

		gettimeofday (&T1, &TZ);

		int nc=0;
		for (int j=0;j<MAXEVENT;) {
			int csr = adc.DataReady();
			//printf(".");fflush(stdout);
			//usleep(1);
			if ( csr ) {
				//printf("%10d Data Ready: 0x%x\n",j, csr);
				++j;

				//adc.EvtCntRead();
				//printf("%10d Event counter: 0x%lx = %ld\n",j, adc.evtCnt,adc.evtCnt);
#if 0				
				adc.EventRead(myvme, V792_BASE, &data[0], &nEntry);
				printf("%10d nEntry: 0x%x %d\n",j, nEntry,nEntry);
				for (int i=0;i<nEntry;i++) {
					printf("Data[%2i]=0x%6lx %6ld\n", i, data[i]&0xFFF,data[i]&0xFFF);
				}
#endif
				int nEntry=adc.DataRead(data);
				//printf("nEntry: 0x%x = %d\n", nEntry,nEntry);


				CAENv7XX::DataHeader dh(data[0]);
				CAENv7XX::DataFooter df(data[33]);
				if (dh.type!=CAENv7XX::HEADER || df.type!=CAENv7XX::FOOTER) {
					printf("Data Buffer Out of sync! (%d,%d)\n",dh.type,df.type);
					continue;
				}
				nc++;
					
#if 1
				printf("Header : 0x%8lx t=%1x geo=%2x crate=%2x cnt=%2d\n",
					   dh.raw,dh.type,dh.geo,dh.crate,dh.cnt);
				for (int i=1;i<33;i++) {
					CAENv7XX::DataEntry de=data[i];
					printf("Data[%2i]=0x%8lx t=%1x ch=%3d v=%6d ov=%1d un=%1d\n",
						   i, de.raw,de.type,de.channel,de.adc,de.ov,de.un);
				}
				printf("Footer : 0x%8lx t=%1x geo=%2x evtCnt=%6x\n",
					   df.raw,df.type,df.geo,df.evtCnt);
#endif
				
			}
		}
        gettimeofday (&T2, &TZ);
		DWORD cnt=adc.EvtCntRead();
        printf("********** Finished read loop ********\n");
		printf("Event counter: %ld\n", cnt);
        double DT=tdiff(T1,T2);
        printf("Events:%d Time:%fs Ev/s:%f / %f\n",nc,DT,nc/DT,cnt/DT);
	}

	status = mvme_close(myvme);
	return 0;
}	

/* emacs
 * Local Variables:
 * mode:C++
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
