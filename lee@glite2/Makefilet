#
#
#	Makefile for VME Boards Tests
#	
#
#	Compiler   : gcc (GCC) 4.1.2 20071124 (Red Hat 4.1.2-42)
#	Linker     : GNU ld version 2.17.50.0.6-6.el5 20061020
#	System     : Scientific Linux 5.2 - kernel-2.6.18-92.1.6.el5
#	VME Boards : CAEN-v259, CAEN-v792, CAEN-v830
#
#	VME Driver : Concurrent 417/427 Boards with 
#		     Tsi148/Universe(II) Chips. 
#	Test Driver: 417x Universe(II) chipset.
#
#	Author     : Lee Pool
#	Company	   : iThemba Labs - [inhouse use only]
#	Date	   : 19/02/2009 13:33:22 
#       Version	   : 1.0
#       Revision   : 
#
#       Changelog  :
#
##############################################################################

#
# Directories
#
VME_DIR = ../vme_universe

#
# Compiler and Linker
#
CC = gcc
CXX = g++
LD = ld

#
# Compiler Flags 
# 
CFLAGS = -g -02 -Wall
CXXFLAGS = -02

#
# Linker Flags
#
LDFLAGS = -L. -lcctvmeen

#
# Alternative Flags
#
MAINFLAG = -DMAIN_ENABLE

#
# Libraries
#
LDLIBS = -lcctvmeen

#
# Targets
#
TARGETS = test_CAENv792 test_CAENv259 test_CAENv785 test_CAENv830 test_CAENv1190B vmescan

ALL: $(TARGETS)

vmescan: vmescan.c vmicvme_alt.o
        $(CC) $(CFLAGS) $? $(LDFLAGS) -o $@
test_CAENv830: test_CAENv830.o v830.o daqsrt.o vmicvme_alt.o
        $(CC) $(CFLAGS) $? $(LDFLAGS) -o $@
test_CAENv785: test_CAENv785.o CAENv785.o daqsrt.o vmicvme_alt.o
        $(CC) $(CFLAGS) $? $(LDFLAGS) -o $@
test_CAENv259: test_CAENv259.o v259.o daqsrt.o vmicvme_alt.o
        $(CC) $(CFLAGS) $? $(LDFLAGS) -o $@
test_CAENv1190B : test_CAENv1190B.o v1190B.o daqsrt.o .o
        $(CC) $(CFLAGS) $? $(LDFLAGS) -o $@
test_CAENv792 : test_CAENv792.o v792.o daqsrt.o vmicvme_alt.o
        $(CC) $(CFLAGS) $? $(LDFLAGS) -o $@

#
# Suffix Rules
#
.SUFFIXES: .cc .c

.cc.o:
	$(CXX) $(CXXFLAGS) -c $<
.cc :
	$(CXX) $(CXXFLAGS) $< -o $@ $(LDLIBS)

.c.o:
	$(CC) $(CFLAGS) -c $<
.c :
	$(CC) $(CFLAGS) $< -o $@ $(LDLIBS)


#
# Dependencies
#
SRC_C = v792.c v830.c v1190B.c vmicvme_alt.c
SRC_CXX = test_CAEN785.cc CAENv785.cc daqsrt.cc

OBJ_C = $(addsuffix .o, $(basename $(SRC)))
OBJ_CXX = $(addsuffix .o, $(basename $(SRC_CXX)))

#
# makedepend
#

%.dep: %.cc
	$(CXX) $(CXXFLAGS) -M $< > $@
%.dep: %.c
	$(CC) $(CFLAGS) -M $< > $@

include $(SRCS_CC:%.cc=%.dep)
include $(SRCS_C:%.c=%.dep)