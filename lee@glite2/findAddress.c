/*
 * =====================================================================================
 *
 *       Filename:  findAddress.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  01/04/2009 09:07:10
 *       Revision:  none
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog:
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <sys/time.h>

#include "vconvme.h"
/* #include "mvmestd.h" */

#include "v792.h"
#include "v830.h"
#include "v259.h"


int main( int argc, char* argv[] ){

	DWORD V82X_BASE = 0x200000;
	DWORD BASE_ADDRESS = 0x0;
	int MAX = 1;
	MVME_INTERFACE *myvme;
	int status;
	int i,j,k;
	int result;
	DWORD icount = 0x0;
	DWORD reg;
	int cmode;

	if (argc>1) {
		sscanf(argv[1],"%x",&reg);
	}else{
		reg = 0x1032;
	}
	printf(" searching for reg 0x%08x\n",reg);

	status = mvme_open(&myvme,0);
	if( status != 1 ) { exit(1); };

	//v830
	BASE_ADDRESS = 0x0;
	
       // Set am to A24 non-privileged Data
	
	mvme_set_am(myvme, MVME_AM_A24_ND);
	
	status = mvme_get_am(myvme,&result);
	
	printf(" doom da = 0x%08x \n ", 0xffffffff&0x1);	
	printf(" am(0x39) = 0x%x \n ",result );
	// Set dmode to D16
	mvme_set_dmode(myvme, MVME_DMODE_D16);
	status = mvme_get_dmode(myvme,&result);
	printf(" dmode(2) = %d \n ",result );
	


	DWORD value;
	for( icount = 0x0; icount < 0xf0000 ; icount = icount + 0x10000 ){
		
		//usleep(1000000);
		BASE_ADDRESS = icount + reg;
		
		//v82X_Status(myvme,BASE_ADDRESS);
		value = mvme_read_value( myvme, BASE_ADDRESS );
		//
		printf(" BASE_ADDRESS = 0x%.08x , value = 0x%.08x \n ",BASE_ADDRESS,value);

	}

	//mvme_get_am(myvme,&cmode);
	mvme_set_am(myvme,MVME_AM_A32_ND);
	mvme_set_dmode(myvme, MVME_DMODE_D16);


	//int k;
//	for( k = 0; k < 10000 ; k++ ){
//	WORD microHS = mvme_read_value(myvme, 0x41030);
//	printf(" \n --- read value = 0x%.08x at base adress 0x91030 , AM = [%d] \n",microHS, MVME_AM_A32_ND );
//	}
	status = mvme_close(myvme);

	return 0;
}
