#include "CAENv785.hh"

/**
 * Read nentry of data from the data buffer. Will use the DMA engine
 * if size is larger then 127 bytes.
 * @param this v785 structure
 * @pdest pointer to destination buffer
 * @param nentry
 */
int CAENv785::DataRead(DWORD *pdest, int nentry) {
	if (!DataReady()) return 0;
	int  cmode;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D32);
	mvme_read(mvme, pdest, base, nentry*4);
	mvme_set_dmode(mvme, cmode);
	return nentry;
}


/**
 *
 */
DWORD CAENv7XX::EvtCntRead() {
	int cmode;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	evtCnt = mvme_read_value(mvme, base+EVT_CNT_L_RO);
	//printf("evCnt(%p,%lx)=0x%08lx\n",mvme,base+EVT_CNT_L_RO,evtCnt);
	evtCnt += (mvme_read_value(mvme, base+EVT_CNT_H_RO)<<16);
	//printf("evCnt(%p,%lx)=0x%08lx\n",mvme,base,evtCnt);
	mvme_set_dmode(mvme, cmode);
	return evtCnt;
}

/**
 *
 */
void CAENv7XX::EvtCntReset() {
	int cmode;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	mvme_write_value(mvme, base+EVT_CNT_RST_WO, 1);
	mvme_set_dmode(mvme, cmode);
	evtCnt=0;
}

/**
 *
 */
int CAENv7XX::DataReady() {
	int cmode;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	int data_ready = mvme_read_value(mvme, base+SR1_RO) & 0x1;
	//printf("DataReady(%p,%lx): SR1=0x%04x\n",mvme,base+SR1_RO,data_ready);
	mvme_set_dmode(mvme, cmode);
	return data_ready;
}

/**
 *
 */
void CAENv7XX::StatusRegister1Read() {
	int cmode;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	sr1.raw = mvme_read_value(mvme, base+SR1_RO);
	mvme_set_dmode(mvme, cmode);
}

/**
 *
 */
void CAENv7XX::StatusRegister2Read() {
	int cmode;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	sr2.raw = mvme_read_value(mvme, base+SR2_RO);
	mvme_set_dmode(mvme, cmode);
}
/**
 *
 */
void CAENv7XX::ControlRegister1Read() {
	int cmode;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	cr1.raw = mvme_read_value(mvme, base+CR1_RW);
	mvme_set_dmode(mvme, cmode);
}

/**
 *
 */
void CAENv7XX::ControlRegister1Write() {
	int cmode;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	mvme_write_value(mvme, base+CR1_RW,cr1.raw);
	mvme_set_dmode(mvme, cmode);
}

/**
 *
 */
void CAENv7XX::BitSet2RegisterRead() {
	int cmode;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	bs2r.raw = mvme_read_value(mvme, base+BIT_SET2_RW);
	mvme_set_dmode(mvme, cmode);
}
/**
 *
 */
void CAENv7XX::BitSet2RegisterSet(BitSet2Register& r) {
	int cmode;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	mvme_write_value(mvme, base+BIT_SET2_RW,r.raw);
	mvme_set_dmode(mvme, cmode);
}
/**
 *
 */
void CAENv7XX::BitSet2RegisterClear(BitSet2Register& r) {
	int cmode;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	mvme_write_value(mvme, base+BIT_CLEAR2_WO,r.raw);
	mvme_set_dmode(mvme, cmode);
}

/**
 * Write Thresholds
 */
int CAENv7XX::ThresholdWrite() {
	int cmode;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	for (int k=0; k<MAX_CHANNELS ; k++) {
		mvme_write_value(mvme, base+THRES_BASE+2*k, threshold[k] & 0x1FF);
	}
	//ThresholdRead(); // CHECK - do I really need to read them back ??
	mvme_set_dmode(mvme, cmode);
	return MAX_CHANNELS;
}

/**
 * Write Thresholds
 */
void CAENv7XX::ThresholdRead() {
	int cmode;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	for (int k=0; k<MAX_CHANNELS ; k++) {
		threshold[k] = mvme_read_value(mvme, base+THRES_BASE+2*k) & 0x1FF;
	}
	mvme_set_dmode(mvme, cmode);
}

/**
 *
 *
 void CAENv7XX::EvtCntResetx() {
 int cmode;
 mvme_get_dmode(mvme, &cmode);
 mvme_set_dmode(mvme, MVME_DMODE_D16);
 //mvme_write_value(mvme, base+EVT_CNT_RST_WO, 1);
 mvme_set_dmode(mvme, cmode);
 evtCnt=0;
 }
*/


/**
 *
 */
void  CAENv7XX::getStatus() {
	int cmode;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	printf("v7XX Status for %lx\n", base);
	int status = mvme_read_value(mvme, base+FIRM_REV);
	printf("Firmware revision: 0x%x\n", status);

	StatusRegister1Read();
	printf("DataReady    :%s\t", sr1.DataReady ? "Y" : "N");
	printf(" - Global DReady:%s\t", sr1.GlobalDataReady ? "Y" : "N");
	printf(" - Busy         :%s\n", sr1.Busy ? "Y" : "N");
	printf("Global Busy  :%s\t", sr1.GlobalBusy ? "Y" : "N");
	printf(" - Amnesia      :%s\t", sr1.Amnesia ? "Y" : "N");
	printf(" - Purge        :%s\n", sr1.Purge ? "Y" : "N");
	printf("Term ON      :%s\t", sr1.TermOn ? "Y" : "N");
	printf(" - TermOFF      :%s\t", sr1.TermOff ? "Y" : "N");
	printf(" - Event Ready  :%s\n", sr1.EventReady ? "Y" : "N");

	StatusRegister2Read();
	printf("Buffer Empty :%s\t", sr2.BufferEmpty ? "Y" : "N");
	printf(" - Buffer Full  :%s\n", sr2.BufferFull ? "Y" : "N");
    printf("PiggyBack    :0x%x\n",sr2.PB);

	ControlRegister1Read();
	printf("Block End    :%s\t", cr1.BlkEnd ? "Y" : "N");
	printf(" - BErr        :%s\n", cr1.BErr ? "Y" : "N");
	printf("Align64      :%s\t", cr1.Align64 ? "Y" : "N");
	printf(" - Prog Reset  :%s\n", cr1.ProgReset ? "Y" : "N");

	BitSet2RegisterRead();
	printf("MemTest      :%s\t", bs2r.MemTest ? "Y" : "N");
	printf(" - OffLine     :%s\t", bs2r.OffLine ? "Y" : "N");
	printf(" - ClearData   :%s\n", bs2r.ClearData ? "Y" : "N");
	printf("OverRange    :%s\t", bs2r.OverRange ? "Y" : "N");
	printf(" - Low Thresh  :%s\t", bs2r.LowThresh ? "Y" : "N");
	printf(" - Test Acq    :%s\n", bs2r.TestAcq ? "Y" : "N");

	ThresholdRead();
	for (int i=0;i<MAX_CHANNELS;i+=2) {
		printf("Threshold[%2i] = 0x%4.4x\t   -  ", i, threshold[i]);
		printf("Threshold[%2i] = 0x%4.4x\n", i+1, threshold[i+1]);
	}
	mvme_set_dmode(mvme, cmode);
}

/* emacs
 * Local Variables:
 * mode:C++
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
