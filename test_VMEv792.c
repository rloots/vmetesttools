#include "v792.h"
//#include "test.h"
#include <stdlib.h>
#include "vconvme.h"
#include <sys/time.h>
#include <math.h>
//#include "midas.h"

DWORD V792_BASE = 0x20000;
//DWORD V792_BASE = 0xb0000;
DWORD prev_eventcount;
	MVME_INTERFACE *myvme;

void V792_SETUP()
{
	/* base address */
	v792_Setup(myvme,V792_BASE,3);

    v792_EvtCntReset(myvme, V792_BASE);
    v792_Status(myvme,V792_BASE);

}
/*  THIS IS CURRENTLY CALLED BY readout thread */
int my_poll_eventB()
{
  //int i;
  //int status = 0;
  DWORD current_eventcount = 0;
  int current_size = 0;
  DWORD the_diff = 0x0;

  //FILE* fl = fopen("QDC_POLLEVENT.log","a+");
  
/*  
  //Get event count
  v792_EvtCntRead(myvme, V792_BASE, &current_eventcount);

  printf("Current Event count %d \n",current_eventcount );

  //if( current_eventcount != 0xFFFFFF ) 
  	the_diff = current_eventcount - prev_eventcount;


  if( the_diff >= 1 ){
    prev_eventcount = current_eventcount;
    current_size = the_diff;
    //printf("Event Count Diff [%d] >=  prev event count %d \n",the_diff,prev_eventcount );
    current_eventcount = 0;
    //fclose(fl);
    return current_size;
  }
*/
  int drdy = 0;

  drdy = v792_DataReady(myvme,V792_BASE);

  printf(" drdy [%d] \n",drdy );
  int myevc = 0;
   v792_EvtCntRead(myvme,V792_BASE, &myevc);

   printf(" Event Counter: [%d] \n",myevc );


  if(drdy)
    return 1;

  //fclose(fl);
  return 0;
}


int main (int argc, char* argv[]) {

	DWORD VMEIO_BASE = 0x0;
	

	int w_entries = (7*128+2);



	
	DWORD *returnData = malloc(1024*sizeof(DWORD));

	int status, csr, i;
	FILE *fp = fopen("SAVEMEJEBUS_ACD.log","a+");
	if (argc>1) {
		sscanf(argv[1],"%lx",&V792_BASE);
	}

	// Test under vmic
	status = mvme_open(&myvme, 0);
	if (status!=1) exit(1);

	V792_SETUP();

	int i_l = 0;
	int number_readout_runs = 10;
	do{ 
		if( my_poll_eventB() ){ 
			mvme_set_blt(myvme,MVME_BLT_BLT32);
			//mvme_set_blt(k600vme,MVME_BLT_MBLT64);
			//returns number of bytes used in DMA transfer
			int w_transfer_size = w_entries*sizeof(DWORD); 
			int wcount = v792_DataRead(myvme,V792_BASE,returnData,&w_transfer_size);
		    int myevc = 0;
   			v792_EvtCntRead(myvme,V792_BASE, &myevc);

	
			printf("wcount = %d, event counter %d \n",wcount,myevc );

			int i_k = 0;
			for (i_k = 0; i_k < wcount; i_k++ ){
				DWORD gdata = returnData[i_k];
				if( gdata != 0x0 )
					fprintf(fp,"gdata 0x%08x \n",gdata );
			}
			fprintf(fp,"\n");
			  v792_DataClear(myvme, V792_BASE);

		}
		i_l++;
	}while(i_l < number_readout_runs );
	status = mvme_close(myvme);
	fclose(fp);
	free(returnData);
	return 1;
}	

/* Local Variables:
 * mode:C
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
