/*
 * =====================================================================================
 *
 *       Filename:  findAddress.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  01/04/2009 09:07:10
 *       Revision:  none
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog:
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <sys/time.h>

//#include "vconvme.h"
#include "midas.h"
//#include "mvmestd.h" 

#include "v792.h"
#include "v830.h"
#include "v259.h"

MVME_INTERFACE *myvme;


int main( int argc, char* argv[] ){

	DWORD V1495_FPGA = NULL;

	int status;
	int i,j,k;
	int result;
	DWORD writevalue = 0;
	printf(" argc: %d \n",argc);

	for(i=0;i<argc;i++){
		printf(" argv: %s \n",argv[i]);
	}

	if (argc>1) {
		sscanf(argv[2],"%x",&writevalue);
		sscanf(argv[1],"%x",&V1495_FPGA);
	}

	printf(" base_address: 0x%x, value: 0x%x\n",V1495_FPGA,writevalue);

	if( V1495_FPGA == NULL)
		exit(1);

	status = mvme_open(&myvme,0);
	if( status != 1 ) { exit(1); };

	/*  On G0 */
	mvme_set_am(myvme,MVME_AM_A32);
	mvme_set_dmode(myvme,MVME_DMODE_D32 );

	status = mvme_write_value(myvme,V1495_FPGA+0x8020,0xdeadbeef);
	printf("writing value to scratch32 :  0x%x , value: 0x%x \n",V1495_FPGA+0x8020, 0xdeadbeef);

	//mvme_write_value(myvme,V1495_FPGA+0x800a,0x0);
	printf("checking out dah firmware: reading 0x%08x \n",V1495_FPGA+writevalue);
	DWORD firm = 0x0;
	mvme_set_dmode(myvme,MVME_DMODE_D16 );
	firm = mvme_read_value(myvme,V1495_FPGA+writevalue);

	if( firm < 0 )
		printf(" error in writing : status %d \n", firm );
	else
	printf(" firmware revision : 0x%08x , [%d] : [%d] \n",firm,((firm>>8)&0xff),firm&0xff);

	status = mvme_close(myvme);

	return 0;
}
