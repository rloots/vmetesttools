Long Duration Timer (LDT) driver by CCT
---------------------------------------

* This driver has only been tested on Red Hat Enterprise 5.2 where kernel is
version 2.6.18-92.el5

* To compile and install run:

    make clean
    make
    ./ins 

Note: The 'ins' script creates the required character device under /dev as
well as installing the module.

* Include cctldt.h in your user programs to access the LDT driver

* See 'testldt' for example usage of the driver

* The ioctls in cctldt.h are:

LDTIOC_STOP

Stop the timer


LDTIOC_RUN

Start the timer. The interrupt pending bit is also cleared.


LDTIOC_SETMODE 

Sets timer mode to either 'long duration timer' (LDT) or 'periodic interrupt
timer' (PIT). The periodic value is one of 100, 200, 500, 1k, 2k, 5k 
or 10k Hz. The long duration timer will roll over after approximately 72
minutes (assuming a 1MHz clock).


LDTIOC_CLRINTERRUPT

Clears interrupt pending bit in status register. 'Timer start' now does this
so this ioctl is not needed.


LDTIOC_READSTAT

Reads back contented of status and control register (debug)


LDTIOC_NOTIFY

The calling program is blocked on this call until the next interrupt. In PIT
mode the interrupt will trigger at the rate set by LDTIOC_SETMODE. In LDT mode
the interrupt will trigger when the count rolls over from 0xFFFFFFFF to 0x0.


LDTIOC_CLRNOTIFY

This triggers a 'fake' interrupt that will cause a thread blocked on a 
LDTIOC_NOTIFY to continue as though an interrupt had occurred. Can be used to
get the thread to exit in a controlled fashion.


LDTIOC_SETTIMER

Sets the timer registers to a particular value. Useful for clearing the
timer by setting it to zero. This should be done prior to starting the
timer in PIT mode to ensure the first tick is the correct length.

Note that the timer is stopped before write to prevent spurios operation.
The timer must be restarted manually.


LDTIOC_GETTIMER

Reads the current timer register value. Note that the hardware allows for 
the timer to be read whilst running. Reading the LSB value causes the complete
value to be latched.


LDTIOC_SETCLOCKSOURCE

Sets the clock source used to driver the timer. By default this is 1MHz (the
ioctl does not need to be called to set this), however an alternative
frequency of 31.25KHz can be used if desired. This affect the duration
of the LDT and PIT timings appropriately.


