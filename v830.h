
#ifndef V82X_INCLUDE_H
#define V82X_INCLUDE_H

#include "mvmestd.h"


#define  V82X_FIRM_REV_RO       (DWORD) (0x1132)
#define  V830_MEB_BASE_RO		(DWORD)	(0x1000)
#define  V82X_COUNTER_BASE_RO	(DWORD)	(0x1000)
#define  V82X_CR_RW				(DWORD) (0x1108)
#define  V82X_CR_SET_WO			(DWORD) (0x110A)
#define  V82X_CR_CLEAR_WO		(DWORD) (0x110C)
#define  V82X_ST_RO				(DWORD) (0x110E)
#define  V82X_IRL_RW			(DWORD)	(0x1112)
#define  V82X_MODULE_RESET_WO	(DWORD)	(0x1120)
#define  V82X_SOFT_CLEAR_WO		(DWORD)	(0x1122)
#define  V82X_SOFT_TRIGGER_WO	(DWORD)	(0x1124)
#define  V830_TRIG_CNT_RO		(DWORD)	(0x1128)
#define  V830_EVT_CNT_RO		(DWORD)	(0x1134)

#ifdef __cplusplus
extern "C" {
#endif
	
	int  v82X_Status(MVME_INTERFACE *mvme, DWORD base);
	//int  v82X_DataRead(MVME_INTERFACE *mvme, DWORD base, DWORD *pdest, int nentry);
	int v82X_ReadCounter(MVME_INTERFACE *mvme, DWORD base, int n);
	void v82X_SoftReset(MVME_INTERFACE *mvme, DWORD base);
	void v82X_SoftClear(MVME_INTERFACE *mvme, DWORD base);
	void v82X_SetTriggerRandom(MVME_INTERFACE *mvme, DWORD base);
	void v82X_SetTriggerDisabled(MVME_INTERFACE *mvme, DWORD base);
	void v82X_SoftTrigger(MVME_INTERFACE *mvme, DWORD base);
	int v830_ReadTriggerCounter(MVME_INTERFACE *mvme, DWORD base);
	int v830_EvtCounter(MVME_INTERFACE *mvme, DWORD base);
#ifdef __cplusplus
} //extern "C"
#endif

#endif // V82X_INCLUDE_H


