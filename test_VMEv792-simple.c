//#include "test.h"
#include <stdlib.h>
#include <sys/time.h>

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <curses.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <asm/page.h>
//#include "vconvme.h"
//#include "v792.h"
#include "vme_api_en.h"                 

  
	//MVME_INTERFACE *myvme;

                                                         
extern int errno;
int main (int argc, char* argv[]) {
	int status, csr, i;

	int devHandle, result;
	EN_PCI_IMAGE_DATA data16;
	
	data16.pciAddress=0x00000000;
        data16.pciAddressUpper=0;
        data16.pciBusSpace=0;
	data16.vmeAddress=0xb0000;
        data16.vmeAddressUpper=0;
	data16.size = 0x100000;
        data16.sizeUpper = 0;
	data16.postedWrites = 0;
	data16.readPrefetch=0;
        data16.prefetchSize= 0;
        data16.dataWidth = EN_VME_D16;
	data16.addrSpace = EN_VME_A32;		/*Default*/
        data16.type = EN_LSI_DATA;
        data16.mode = EN_LSI_USER;
        data16.vmeCycle = 0;
        data16.sstbSel =0;
        data16.ioremap = 1;

	devHandle = vme_openDevice("lsi0");
	if( devHandle < 0 ){
		perror("[initpciD16] Could not create device for d16\n");
		return(-1);
	}else{
		result = vme_enablePciImage( devHandle, &data16 );
		if( result < 0 ){
			perror("[initpciD16] Could not create PCI Image {2} for d16\n");
			return(-1);
		}
	}/**D16*/

	printf(" V792_BASE 0xb0000 \n" );
	result= vme_read(devHandle,0xb0000+0x100e,&csr,2);//v792_CSR1Read(myvme,V792_BASE);
	printf(" V792_BASE 0xb0000  csr is : 0x%08x result =0x%08x\n", csr, result );
	
	//v792_SoftReset(myvme,V792_BASE);

//	v792_Setup(myvme,V792_BASE,1);
//	csr= v792_CSR1Read(myvme,V792_BASE);
	printf(" V792_BASE 0xb0000  csr is : 0x%08x\n", csr );
  /*      v792_DataClear(myvme, V792_BASE );
		{
		WORD     threshold[32];
		for (i=0;i<V792_MAX_CHANNELS;i++) {
			if( i >= 0 && i <= 16 ){
				threshold[i] = 0x000;
			}
			else{ 
				threshold[i] = 0x7ff;
			}
		//	printf("Threshold[%2i] = 0x%4.4x\t   -  \n ", i, threshold[i]);

		}
		
		v792_ThresholdWrite(myvme, V792_BASE, threshold);
	}
*/
/*
	DWORD *temp;
	int bb = 128;

	if( mvme_read(myvme,temp,0xb0000,bb)>0 ){
	int j;
	for(j = 0; j < bb; j++)
		printf(" 0x%08x \n",temp[i] );	
	}
*///	status = mvme_close(myvme);
	vme_closeDevice(devHandle);
	return 1;
}	

/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
