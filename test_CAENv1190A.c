#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <assert.h>
#include <byteswap.h>
#include <time.h>


#include "midas.h"
//#include "mcstd.h"
#include "mvmestd.h"
//#include "msystem.h"

#include "v1190A.h"

int modcount = 1;

enum
{
	GLOBAL_HEADER = 8,
	GLOBAL_TRAILER = 10,
	GLOBAL_TRIGGER_TIME_TAG = 11,
	FILLER = 0x18,
	TDC_DATA = 0,
	TDC_HEADER = 1,
	TDC_MEASUREMENT = 0
};


/*  word count TDC Trailer */
int getWordCountTDC( DWORD *ptr )
{
  DWORD a=(*ptr);
  int wordcount = (a)&0xfff;

  return wordcount;
}


/*  is the word a TDC Global Header */
int isGlobalHeader( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD header = (a>>30)&0x3;
	DWORD geo = (a&0x1f);
	if( header == 0x1 ){
		return 1;
	}
	return 0;
}


/*  is the word a TDC Global Trailer */
int isGlobalTrailer( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD trailer = (a>>27)&0x1f;
	DWORD geo = a&0x1f;
	if( trailer == 0x10 ){
		return 1;
	}
	
	return 0;
}

/*  is the word a TDC Data Header word */
int isTDCHeader( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD header = (a>>27)&0x7;

	if( header == 0x1 ){
		return 1;
	}
	return 0;
}



/*  is the word a TDC Data Trailer */
int isTDCTrailer( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD trailer = (a>>27)&0x7;

	if( trailer == 0x3 ){
		return 1;
	}
	
	return 0;
}

/*  is the word a TDC Data Error */
int isTDCError( DWORD *ptr )
{
  	DWORD a=(*ptr);
	DWORD worderror = (a>>27)&0x7;

	if( worderror == 0x4 ){
	  return 1;
	}

	return 0;
}


DWORD getTDCErrorFlag( DWORD *ptr )
{
	DWORD a=(*ptr);
	
	DWORD eflag = (a&0x7fff);

	return eflag;
}

/*  is the word a TDC Data Measurement */
int isTDCMeasurement( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD tdcm = (a>>27)&0x1f;

	if( tdcm == 0x0 ){
	  return 1;
	}

	return 0;
}

/*  is the word a TDC Filler word */
int isTDCFiller( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD filler = (a>>27)&0x1f;

	if( filler == 0x18 ){
	  return 1;
	}

	return 0;
}



/*  is TDC Data Measurement , trailing or leading. 
 *  inconjunction with above function */
int isTDCTrailingMeasurement( DWORD *ptr )
{
  	DWORD a=(*ptr);
	DWORD istrail = (a>>26);

	if( istrail == 0x1 )
	  return 1;

	return 0;
}



int getTDCChip( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD tdchip = (a>>24)&0x3;

	return tdchip;

}

int getTDCChannel( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD tdcchannel = (a>>19)&0x7f;

	return tdcchannel;

}
int isTDCData( DWORD *ptr )
{
  	DWORD a=(*ptr);
	DWORD tdcdata = (a>>27)&0x7;

	if( tdcdata == 0x0 ||
	    tdcdata == 0x3 ||
	    tdcdata == 0x4 ){

	  return 1;
	}

	return 0;
}


DWORD getTDCEventID( DWORD *ptr )
{

  DWORD a=(*ptr);


  return ((a>>12)&0xfff);
}



void debugretief( FILE *fp, int tmod, DWORD vword )
{

	
	if( isGlobalHeader(&vword ) )
	{
	  fprintf(fp," === TDC Module - [%d] - Global Header : 0x%08x \n",tmod,vword );
	}else if( isTDCHeader(&vword) ){
	  fprintf(fp,"\t\t  TDC Header: 0x%08x , EventID 0x%08x , TDC Chip [%d] \n",vword,getTDCEventID(&vword),getTDCChip(&vword));

	}else if( isTDCMeasurement(&vword) ){
	  fprintf(fp,"\t\t\t  Module [%d] TDC MeasureMent: 0x%08x , Channel %d \n",tmod,vword, getTDCChannel(&vword) );
	  DWORD mword = vword&(0x7ffff);
	  int edge = (vword>>26)&0x1;
	  if( edge == 1 ){
		fprintf(fp,"\t\t\t\t  Leading Time %d , Width %d \n", mword, ((mword>>12)&0x7f) );
	  }else{
		fprintf(fp,"\t\t\t\t  Trailing Time %d \n", mword);
	  }
	}else if( isTDCError(&vword) ){
	  fprintf(fp,"\t\t\t  Module [%d] TDC Error: 0x%x , TDC Chip %d , Error Flag 0x%x \n",tmod,vword, getTDCChip(&vword), getTDCErrorFlag(&vword) );
	  		//1-0	
			if( vword&0x1 ){
				fprintf(fp,"\t\t\t\t\t[ERROR] Hit lost in group 0 from read-out FIFO overflow \n");
			}
			//2-1
			if( vword&0x2 ){
				fprintf(fp,"\t\t\t\t\t[ERROR] Hit lost in group 0 from L1 buffer overflow \n");
			}
			//3-2
			if( vword&0x4 ){
				fprintf(fp,"\t\t\t\t\t[ERROR] Hit error have been detected in group 0 \n");
			}
			//4-3
			if( vword&0x8 ){
				fprintf(fp,"\t\t\t\t\t[ERROR] Hit lost in group 1 from read-out FIFO overflow \n");
			}
			//5-4
			if( vword&0x10 ){
				fprintf(fp,"\t\t\t\t\t[ERROR] Hit lost in group 1 from L1 buffer overflow \n");
			}
			//6-5
			if( vword&0x20 ){
				fprintf(fp,"\t\t\t\t\t[ERROR] Hit error have benn detected in group 1\n");
			}
			//7-6
			if( vword&0x40 ){
				fprintf(fp,"\t\t\t\t\t[ERROR] Hit data lost in group 2 from read-out FIFO overflow\n");
			}
			//8-7
			if( vword&0x80 ){
				fprintf(fp,"\t\t\t\t\t[ERROR] Hit lost in group 2 from L1 buffer overflow\n");
			}
			//9-8
			if( vword&0x100 ){
				fprintf(fp,"\t\t\t\t\t[ERROR] Hit error have been detected in group 2\n");
			}
			//10-9
			if( vword&0x200 ){
				fprintf(fp,"\t\t\t\t\t[ERROR] Hit lost in group 3 from read-out FIFO overflow\n");
			}
			//11-10
			if( vword&0x400 ){
				fprintf(fp,"\t\t\t\t\t[ERROR] Hit lost in group 3 from L1 buffer overflow\n");
			}
			//12-11
			if( vword&0x800 ){
				fprintf(fp,"\t\t\t\t\t[ERROR] Hit error have been detected in group 3 \n");
			}
			//13-12
			if( vword&0x1000 ){
				fprintf(fp,"\t\t\t\t\t[ERROR] Hits rejected because of programmed event size limit \n");
			}
			//14-13
			if( vword&0x2000 ){
				fprintf(fp,"\t\t\t\t\t[ERROR] Event lost (trigger FIFO overflow) \n");
			}
			//15-14
			if( vword&0x4000 ){
				fprintf(fp,"\t\t\t\t\t[ERROR] Internal fatal chip error has been detected\n");
			}
	}else if( isTDCTrailer(&vword) ){
	  fprintf(fp,"\t\t  TDC Trailer: 0x%08x , EventID 0x%08x , Word Count %d , TDC Chip [%d] \n",vword,getTDCEventID(&vword),getWordCountTDC(&vword),getTDCChip(&vword));
	}else if( isGlobalTrailer(&vword ) )
	{
	  fprintf(fp," === TDC Module - [%d] - Global Trailer : 0x%08x , WC %d, Status %d, \n",tmod,vword,(vword>>5)&0xffff,(vword>>24)&0x7 );
	}
	else if( isTDCFiller(&vword ) )
	{  
	fprintf(fp," === TDC Module - [%d] -  TDC Filler Word : 0x%08x  \n",tmod,vword );
	}
	else{
//		  fprintf(fp," UNKNOWN WORD 0x%08x \n",vword );
	}

}

int CBLTRead(MVME_INTERFACE *mvme, DWORD base, DWORD *pdest, int nentry)
{
  int amode, cmode, status;
  

    mvme_get_dmode(mvme, &cmode);
	//mvme_get_am(mvme,&amode);
    //mvme_set_am(mvme,MVME_AM_A32_NB);
	mvme_set_dmode(mvme, MVME_DMODE_D32);
    mvme_set_blt(mvme,MVME_BLT_BLT32);

	printf(" CBLT READ, am 0x%04x, dmode 0x%04x \n",MVME_AM_A32_SB, MVME_DMODE_D32);
    status = mvme_read(mvme, pdest, base, nentry*(sizeof(DWORD))); 

    mvme_set_dmode(mvme, cmode);
	//mvme_set_am(mvme,amode);

    if(status != MVME_SUCCESS ){
		return status;
	}



	
	return status;
}

int CBLT_mvme_read( MVME_INTERFACE *mvme, DWORD bases[], void *dests[], int nb[] )
{

  int amode, cmode, status;
  

    mvme_get_dmode(mvme, &cmode);
    //mvme_get_am(mvme,&amode);
    //mvme_set_am(mvme,MVME_AM_A32_NB);
    mvme_set_dmode(mvme, MVME_DMODE_D32);
    mvme_set_blt(mvme,MVME_BLT_BLT32);

// 	status = gefvme_read_dma_multiple(mvme, modcount, dests, bases, nb );
    
//	status = mvme_read(mvme, dests, bases, nentry*(sizeof(DWORD))); 


	//int gefvme_read_dma_multiple(MVME_INTERFACE *mvme, int nseg, void* dstaddr[], const mvme_addr_t vmeaddr[], int nbytes[])
	
		
	mvme_set_dmode(mvme, cmode);
    //mvme_set_am(mvme,amode);
    
    if(status != MVME_SUCCESS ){
		return status;
	}



     return status;
}
int main (int argc, char* argv[]) 
{


//	DWORD VMEIO_BASE = 0x0;
	unsigned int* V1190A_BASE;
	
	
	V1190A_BASE = (unsigned int*)calloc(8,sizeof(unsigned int));
	DWORD *V1190A_DEST[7];
	int V1190A_NB[7];

	V1190A_BASE[0]=0x140000;
	V1190A_BASE[1]=0x50000;
	V1190A_BASE[2]=0x60000;
	V1190A_BASE[3]=0x70000;
	V1190A_BASE[4]=0x80000;
	V1190A_BASE[5]=0x90000;
	V1190A_BASE[6]=0xa0000;
	
	MVME_INTERFACE *myvme;





//          struct timeval tc1, td1, tp1;
//	  struct timeval tc2, td2, tp2;
//	  unsigned int  dt1, dt2, dt3;

	int status; 
	int result;	    
 	int i,k,j;

	int cmode;
//	if (argc>1) {
//		sscanf(argv[1],"%.08lx",&V1190A_BASE);
//	}

	status = mvme_open(&myvme, 0);
	if (status!=1) exit(1);

	//int naddr = 7;
	//result = initListDMATransfer( myvme, V1190A_BASE, naddr );
	
	
	DWORD mode = 0x1;

	DWORD *tdcdata,edge,channel,time,xdata;
	int nentry;
	int count;
	int totalcounts;
	int wire;
	nentry = 512;


	FILE *fp = fopen("cblt_test_.log","w");
 	void** mydest[7];


	for( k = 0; k < modcount;k++ ){
	    	 printf("@@@@@@@@@@@@ %d @@@@@@@@@@ \n",k); 
		 //mvme_set_am(myvme,MVME_AM_A32_ND);
		 //mvme_get_dmode(myvme,&cmode);
		 //mvme_set_dmode(myvme, MVME_DMODE_D16);
		 v1190A_Setup( myvme, V1190A_BASE[k], mode);
		 mydest[k] = calloc(nentry,sizeof(DWORD));

	 
		 //*V1190A_DEST[k] = (DWORD*)calloc(nentry,sizeof(DWORD));
		 V1190A_NB[k] = nentry*sizeof(DWORD);

	}

  
//	printf(" Setting GEO address \n");
//	v1190A_GeoWrite(myvme,V1190A_BASE[0],0x1);
//	v1190A_GeoWrite(myvme,V1190A_BASE[1],0x2);
//	v1190A_GeoWrite(myvme,V1190A_BASE[2],0x3);
//	v1190A_GeoWrite(myvme,V1190A_BASE[3],0x4);
/*
	printf(" Setting MCST Base address... \n");
	v1190A_setMCSTBaseAddress(myvme, V1190A_BASE[0], 0xAA );
	v1190A_setMCSTBaseAddress(myvme, V1190A_BASE[1], 0xAA );
	v1190A_setMCSTBaseAddress(myvme, V1190A_BASE[2], 0xAA );
	v1190A_setMCSTBaseAddress(myvme, V1190A_BASE[3], 0xAA );
	
	printf(" Setting CBLT CSR... \n");
	v1190A_setCBLT_CSR(myvme,V1190A_BASE[0],0x02 );   //First board
	v1190A_setCBLT_CSR(myvme,V1190A_BASE[1],0x03 );   // active intermediate
	v1190A_setCBLT_CSR(myvme,V1190A_BASE[2],0x00 );   // active intermediate
	v1190A_setCBLT_CSR(myvme,V1190A_BASE[3],0x01 );   //active last board


	printf(" Sending Softreset via MCST ... \n");
	v1190A_MCST_SoftReset(myvme,0xAA);
*/


	for( i = 0; i < modcount; i++ ) {
		printf(" does Module [%d] have data ready %d \n",i,v1190A_DataReady(myvme, V1190A_BASE[i]));

	}

			tdcdata = (DWORD*)calloc(1024,sizeof(DWORD));
		//memset(tdcdata,0,nentry);
//		count = v1190A_EventRead(myvme, V1190A_BASE[i], tdcdata, &nentry);  //Single event read
	
		//for(kkk = 0 ; kkk < 20; kkk++ ) {
		//printf(" Doing CBLT on 4 modules, base address 0x%08x\n",0xAA000000 );	
		
	///	count = CBLTRead(myvme, 0xAA000000, tdcdata, nentry);   //DMA block level transfer (blt) Read

		//if (count > 0){ // we have data
		//
//		 gefvme_set_dma_debug(6);
//		 DWORD *mydata = (DWORD*)calloc(nentry,sizeof(DWORD));
//		
//		if( v1190A_DataReady(myvme, V1190A_BASE[i]) ) 
//		{
//			int dmastatus = CBLT_mvme_read( myvme, V1190A_BASE, mydest, V1190A_NB );
//		
//			if( dmastatus != MVME_SUCCESS )
//				printf(" DMA EROROR %d \n",dmastatus );
//			
//			i = 0;	
//			for( i =0; i < modcount; i++ ){
//
//
//
//				//mydata = (DWORD*)mydest[i];
//				//int ymode = 0;
//
//				//mvme_get_dmode( myvme, &ymode);
//				//mvme_set_dmode( myvme, MVME_DMODE_D32 );
//				//mvme_set_blt( myvme, MVME_BLT_BLT32 );
//				//mvme_read( myvme,  mydata, V1190A_BASE[i],128*sizeof(DWORD) );
//				//mvme_set_dmode( myvme, ymode );
//
//				fprintf(fp," ----- MODULE %d ------ \n",i);
//				//DWORD *data = malloc(128*sizeof(DWORD));
//				//memset(data,0,128);
//				
//				for (j = 0; j < nentry; j++) 
//				{
//
//					DWORD v = bswap_32( mydest[i][j] );
//					debugretief(fp,i,v );
//					//debugretief(fp,i,mydata[j] );
//					//fprintf(fp,"DATA: 0x%08x \n",mydest[i][j] );
//
//
//				} // for j
//			}
//
//		}
//
					
		//} //count >0
	
	fclose(fp);	
	//free(mydata);
	free(tdcdata);
	//free(mydest);
	status = mvme_close(myvme);
	return 1;
}	


/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
