/************************************************************************

  VME.h
  
*************************************************************************/

#ifndef _VME_H
#define _VME_H

#include "CAENVMElib.h"
#undef WIN32
#ifdef WIN32
#define Wait_ms(t)   Sleep(t)
#else
#define Wait_ms(t)   usleep((t)*1000)
#endif

int VME_Init(short Link, short BdNum);
int VME_Write(unsigned long Addr, unsigned long Data, int Size);
int VME_Read(unsigned long Addr, unsigned long *Data, int Size);
int VME_End(void);

#endif 


