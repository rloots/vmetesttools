/************************************************************************

  VME.c
  
*************************************************************************/

#include "vme.h"

#undef _CAENVME
#ifdef _CAENVME
    #include "CAENVMElib.h"
#else
    // Put your include files for the VME here
    #include "midas.h"
    #include "mvmestd.h"
#endif

#ifdef _CAENVME
    long Handle;                      // handle for the VME controller
    unsigned long Wbuff[2048];        // Buffers for the Multi Read/Write
    unsigned long Addrs[2048];
    unsigned long *Rbuff;
    CVAddressModifier AMs[2048];
    CVDataWidth DWs[2048];
    CVErrorCodes ECs[2048];
    int Wpnt = 0, Rpnt = 0;
#else
   
    MVME_INTERFACE *myvme;


#endif



//########################################################################
// VME functions
//########################################################################
// You must provide the following functions for the VME access if you
// don't use the V1718 nor the V2718 as VME controller:
//
// int VME_Init(short Link, short BdNum)
// -----------------------------------------------------------------------
// Description: open and initialize the VME controller.
// Return 0 when succeeded, a negative value when failed
// Parameters: 
//    Link      : The link number (don't care for V1718).
//    BdNum     : The board number in the link.
//
// int VME_Write(unsigned long Addr, unsigned long Data, int Size)
// -----------------------------------------------------------------------
// Description: make a single data write access to the VME bus (A32 or A24)
// Parameters: Addr is the VME address, Data is the data to write
// (cast to unsigned long for both D16 and D32 access), Size is the type
// of VME access (2 for D16, 4 for D32)
// Return 0 when succeeded, a negative value when failed
//
// int VME_Read(unsigned long Addr, unsigned long *Data, int Size)
// -----------------------------------------------------------------------
// the same as for the VME write access except for Data that is a pointer.
//
// int VME_End(void)
// -----------------------------------------------------------------------
// Description: close the VME controller.
// Return 0 when succeeded, a negative value when failed
//
// NOTE: in the case of CAEN's bridges, it is possible to optimize the
// transfer rate of the series of single data acccess by using the
// Multi Read/Write functions. To do that, the single read and write
// to the flash memory are not executed immediately; data are saved in
// a buffer and then read/written all together.
// This optimization is implemented in the VME function in the
// VME_Write() and VME_Read() functions.

#ifndef _CAENVME

extern unsigned long RW_Flash;   // Address of the R/W Flash Register
extern unsigned long Sel_Flash;  // Address of the Select Flash Register

	void mvmeerror(int stats) {
			
		switch(stats){
			case MVME_NO_INTERFACE:{
						       printf("MVME No Interface\n");
						       break;
					       }
			case MVME_INVALID_PARAM:{
							printf("Invalid Parameter supplied\n");
							break;
						}
			case MVME_ACCESS_ERROR:{
						       printf("mvme access error\n");
						       break;
					       }
			case MVME_NO_CRATE:{
						   printf("mvme no crate\n");
						   break;
					   }
			case MVME_UNSUPPORTED:{
						      printf("mvme unsupported\n");
						      break;
					      }
			case MVME_NO_MEM:{
						 printf("mvme no memory\n");
						 break;
					 }
			default:
				printf("Unknown ErrorCode %d\n",stats);
		}
	


	}/*  mvmeeror */	
    

int VME_Init(short Link, short BdNum)
{
	// Put your code for the VME initialization here
	//
	int status=0;

	status = mvme_open( &myvme, 0);
	if( status != MVME_SUCCESS ){
		printf("Could not Open vme interface\n");
		mvmeerror(status);
		return -1;
	}

	return 0;
}/*  VME_INIT */

    int VME_Write(unsigned long Addr, unsigned long Data, int Size)
    {
    // Put your code for the VME single data write access here
    
	int status=0;
	
	mvme_addr_t vmeAddr = (mvme_addr_t)Addr;
	//DWORD val = (DWORD)Data;
	//DWORD val16 = (DWORD)Data;

//	printf(" Write: Address 0x%0x , \
//			        Data	0x%0x , \
//					Size	%d \n",Addr,Data,Size);

	int amode=0;
	int dmode=0;

	mvme_get_am(myvme,&amode);
	mvme_set_am(myvme,MVME_AM_A32);

	mvme_get_dmode(myvme,&dmode);

	if(Size == 2 )//D16 
	{


		mvme_set_dmode(myvme,MVME_DMODE_D16);

		status = mvme_write_value(myvme,Addr,Data);

		mvme_set_am(myvme,amode);
		mvme_set_dmode(myvme,dmode);

		if( status != MVME_SUCCESS ){
			printf("mvme_write_value failure\n");
			mvmeerror(status);
			return -1;
		}


	}

	if(Size==4)//D32
	{
		mvme_set_dmode(myvme,MVME_DMODE_D32);

		status = mvme_write_value(myvme,Addr,Data);

		mvme_set_am(myvme,amode);
		mvme_set_dmode(myvme,dmode);

		if( status != MVME_SUCCESS ){
			printf("mvme_write_value failure\n");
			mvmeerror(status);
			return -1;
		}


	}


	return 0;
    }

    int VME_Read(unsigned long Addr, unsigned long *Data, int Size)
    {
    // Put your code for the VME single data read access here
    
	
	mvme_addr_t vmeAddr = (mvme_addr_t)Addr;

	int amode=0;
	int dmode=0;

	mvme_get_am(myvme,&amode);
	mvme_set_am(myvme,MVME_AM_A32);

	mvme_get_dmode(myvme,&dmode);

	if(Size == 2 )//D16 
	{


		mvme_set_dmode(myvme,MVME_DMODE_D16);

		*Data = mvme_read_value(myvme,Addr);
	
		mvme_set_am(myvme,amode);
		mvme_set_dmode(myvme,dmode);

//		printf("data read 0x%0x\n",*Data); 

//		printf(" Read: Address 0x%0x , \
//			        Data	0x%0x , \
//					Size	%d \n",Addr,((*Data)&0xff),Size);


//		if( ((*Data)&0xffff)==0xffff ) 
//			 return -1;



	}

	if(Size==4)//D32
	{
		mvme_set_dmode(myvme,MVME_DMODE_D32);

		*Data = (DWORD)mvme_read_value(myvme,Addr);

		mvme_set_am(myvme,amode);
		mvme_set_dmode(myvme,dmode);

	}


	return 0;

     }/*  VME_READ  */

    int VME_End(void)
    {
    // Put your code for closing the VME device here
    
	int status=0;

	status = mvme_close(myvme);
	if( status != MVME_SUCCESS ){
		printf("Could Not Close MVME Interface\n");
		mvmeerror(status);
		return -1;
	}
	
	return 0;
     }/*  VME_END */

#else

extern unsigned long RW_Flash;   // Address of the R/W Flash Register
extern unsigned long Sel_Flash;  // Address of the Select Flash Register


    // -----------------------------------------------------------------------
    int VME_Init(short Link, short BdNum)
    {
        if( CAENVME_Init(cvV2718, Link, BdNum, &Handle) == cvSuccess )
            return 0;
        else if( CAENVME_Init(cvV1718, Link, BdNum, &Handle) == cvSuccess )
            return 0;
        else
            return -1;
    }

    // -----------------------------------------------------------------------
    int VME_Write(unsigned long Addr, unsigned long Data, int Size)
    {
        int ret = 0;
        unsigned short Data16;

        if (Addr != RW_Flash) {
            if (Wpnt > 0) {
                ret |= CAENVME_MultiWrite(Handle, Addrs, Wbuff, Wpnt, AMs, DWs, ECs);
                Wpnt = 0;
            }

            if (Rpnt > 0) {
                ret |= CAENVME_MultiRead(Handle, Addrs, Rbuff, Rpnt, AMs, DWs, ECs);
                Rpnt = 0;
            }

            if (Size == 2) { // D16 access
                Data16 = (unsigned short)Data;
				ret |= CAENVME_WriteCycle(Handle, Addr, &Data16, cvA32_U_DATA, (CVDataWidth)Size);
            } else {
				ret |= CAENVME_WriteCycle(Handle, Addr, &Data, cvA32_U_DATA, (CVDataWidth)Size);
            }
        }

        // Access to the flash memory (use a MultiWrite)
        else {
            Wbuff[Wpnt] = Data;
            Addrs[Wpnt] = Addr;
            AMs[Wpnt] = cvA32_U_DATA;
			DWs[Wpnt] = (CVDataWidth)Size;
            Wpnt++;
        }
        return (ret);
    }

    // -----------------------------------------------------------------------
    int VME_Read(unsigned long Addr, unsigned long *Data, int Size)
    {
        int ret = 0;
        unsigned short Data16;

        if (Wpnt > 0) {
            ret |= CAENVME_MultiWrite(Handle, Addrs, Wbuff, Wpnt, AMs, DWs, ECs);
            Wpnt = 0;
        }

        if (Addr != RW_Flash) {
            if (Size == 2) { // D16 access
                ret |= CAENVME_ReadCycle(Handle, Addr, &Data16, cvA32_U_DATA, (CVDataWidth)Size);
                *Data = (unsigned long)Data16;
            } else {
                ret |= CAENVME_ReadCycle(Handle, Addr, Data, cvA32_U_DATA, (CVDataWidth)Size);
            }
        }

        // Access to the flash memory (use a MultiRead)
        else {
            if (Rpnt == 0)
                Rbuff = Data;
            Addrs[Rpnt] = Addr;
            AMs[Rpnt] = cvA32_U_DATA;
            DWs[Rpnt] = (CVDataWidth)Size;
            Rpnt++;
        }
        return (ret);
    }

    // -----------------------------------------------------------------------
    int VME_End(void)
    {
        return(CAENVME_End(Handle));
    }


#endif
//########################################################################




