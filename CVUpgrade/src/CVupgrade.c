/************************************************************************

 CAEN SpA - Viareggio
 www.caen.it

 Program: CVupgrade
 Date:    11/10/2007
 Author:  Carlo Tintori (c.tintori@caen.it)

 ------------------------------------------------------------------------
 Description
 ------------------------------------------------------------------------
 This program allows to upgrade the firmware of any CAEN VME module that
 supports the firmware download through the VME bus (except for the
 bridges V1718 and V2718 that use the program 'CAENVMEUpgrade').
 The firmware upgrade consists in writing the configuration file (Altera
 Raw Binary Format) into the flash memory of the board. Usually the flash
 memory contains two images of the configuration file, called 'standard'
 and 'backup'. The 'standard' image only will normally be overwritten ; 
 the new firmware will be loaded by the FPGA after the first power cycle.
 If an error occurs while upgrading, it may happen that the FPGA is not
 properly configured and the board does not respond to the VME access.
 In this case you can restore the old firmware (i.e. the 'backup' image)
 just moving the relevant jumper from STD to BKP and power cycling the
 board. Now you can retry to upgrade the 'standard' image.
 Warning: never upgrade the 'backup' image until you are sure that the
 'standard' works properly.
 This program reads some parameters that define the type of the board
 being upgraded from a file called CVupgrade_params.txt that must be
 in the same directory of CVupgrade. There is one CVupgrade_params file
 for each type of board that can be upgraded.

 ------------------------------------------------------------------------
 Usage
 ------------------------------------------------------------------------
 Syntax: CVupgrade ConfFile BaseAddress [options]

 Options:
 -backup: write backup image
 -no_verify: disable verify (don't read and compare the flash content)
 -verify_only: read the flash content and compare with the ConfFile file
 -param filename: allow to specify the file that contain the parameters 
                  for the board that is being upgraded (default is 
                  CVupgrade_params.txt)


 ------------------------------------------------------------------------
 Portability
 ------------------------------------------------------------------------
 This program is ANSI C and can be compilated on any platform, except
 for the functions that allow to initialize and access the VME bus.
 The low level VMEBus access functions (Init, read, write) are implemented
 in the vme.c file. 
 If CAEN's VME bridges (both V1718 and V2718) are used as VME masters,
 the  functions in vme.c file are already implemented and they only need
 a proper installation of the CAENVMELib library DLL on PC.
 If a different VME Bridge/CPU is used, the functions in vme.c MUST be
 reimplemented.

*************************************************************************/
/*------------------------------------------------------------------------
  Modification history:
  ------------------------------------------------------------------------
  Version  | Author | Date       | Changes made
  ------------------------------------------------------------------------
  1.0      | CTIN   | 01.02.2008 | inital version.
  1.1      | LCOL   | 29.05.2008 | Updated WriteFlashPage to account for 
           |        |            | a longer flash erase times 
           |        |            | (increase from 20 to 40 ms).
  1.2      | LCOL   | 25.06.2008 | Added flash enable polarity parameter.         
  ------------------------------------------------------------------------*/

#define REVISION  "1.2"

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <unistd.h>

#undef _CAENVME

#include "vme.h"

// Opcodes for the flash memory
#define MAIN_MEM_PAGE_READ          0xD2
#define MAIN_MEM_PAGE_PROG_TH_BUF1  0x82


// Global Variables
unsigned long RW_Flash;   // Address of the R/W Flash Register
unsigned long Sel_Flash;  // Address of the Select Flash Register

int RegSize;              // Register Data Size (2 = D16, 4 = D32)
int PageSize;             // Flash Page Size
int FirstPageStd;         // First page of the Standard image
int FirstPageBck;         // First page of the Backup image
int FlashEnablePolarity=0;// Flash device enable polarity
short Link  = 0;
short BdNum = 0;

// *****************************************************************************
// WriteFlashPage: write one page of the flash memory
// Parameters: data: pointer to the data buffer
//             pagenum: flash page number
// Return: 0 on Success, < 0 on error
// *****************************************************************************
int WriteFlashPage(unsigned char *data, int pagenum)
{
    int i, res = 0;
    unsigned long flash_addr;

    flash_addr = (unsigned long)pagenum << 9;

    // Enable flash 
    if (FlashEnablePolarity)
      res |= VME_Write(Sel_Flash, 1, RegSize);
    else
      res |= VME_Write(Sel_Flash, 0, RegSize);

    // Opcode
    res |= VME_Write(RW_Flash, MAIN_MEM_PAGE_PROG_TH_BUF1, RegSize);
    // Page address
    res |= VME_Write(RW_Flash, ((flash_addr>>16) & 0xFF), RegSize);
    res |= VME_Write(RW_Flash, ((flash_addr>>8) & 0xFF), RegSize);
    res |= VME_Write(RW_Flash, (flash_addr & 0xFF), RegSize);

	// Data
    for (i=0; i < PageSize; i++){
        res |= VME_Write(RW_Flash, (unsigned char)data[i], RegSize);

	}
    // Disable flash 
    if (FlashEnablePolarity)
      res |= VME_Write(Sel_Flash, 0, RegSize);
    else
      res |= VME_Write(Sel_Flash, 1, RegSize);

    // wait for Tep (Time of erase and programming)
    Wait_ms(40); /* 40 ms */
    
	return res;
}

// *****************************************************************************
// ReadFlashPage: read one page of the flash memory
// Parameters: data: pointer to the data buffer
//             pagenum: flash page number
// Return: 0 on Success, < 0 on error
// *****************************************************************************
int ReadFlashPage(unsigned char *data, int pagenum)
{
    int i, res = 0;
    unsigned long flash_addr;
    unsigned long Data32[2048];
	unsigned short data16;
    flash_addr = (unsigned long)pagenum << 9;
    
    // Enable flash 
    if (FlashEnablePolarity)
      res |= VME_Write(Sel_Flash, 1, RegSize);
    else
      res |= VME_Write(Sel_Flash, 0, RegSize);
    // Opcode
    res |= VME_Write(RW_Flash, MAIN_MEM_PAGE_READ, RegSize);
    // Page address
    
	res |= VME_Write(RW_Flash, (flash_addr>>16) & 0xFF, RegSize);
    res |= VME_Write(RW_Flash, (flash_addr>>8) & 0xFF, RegSize);
    res |= VME_Write(RW_Flash, flash_addr & 0xFF, RegSize);
 

	
	// additional don't care bytes
    for (i=0; i<4; i++)
        res |= VME_Write(RW_Flash, 0, RegSize);

    // Read the page
    for (i=0; i<PageSize; i++){
        res |= VME_Read(RW_Flash, &data16, RegSize);
		Data32[i] = (unsigned char)data16;
	}
    // Disable flash 
    if (FlashEnablePolarity)
      res |= VME_Write(Sel_Flash, 0, RegSize);
    else
      res |= VME_Write(Sel_Flash, 1, RegSize);
    
    // transfer the 32bit data buffer to the 8 bit buffer
    // NOTE: if CAEN's bridges are used, the data are actually read from
    // the flash memory with a single MultiRead that is executed before
    // the access to the Sel_Flash register
    for (i=0; i<PageSize; i++)
       data[i] = (unsigned char)Data32[i];

    return res;
}



//******************************************************************************
// MAIN
//******************************************************************************
int main(int argc,char *argv[])
{
    int i, j, page, pa, CFsize, NumPages, err=0, done;
    int image=0, noverify=0, verifyonly=0;
    char ConfFile[1000];
    char ParamFile[1000] = "CVupgrade_params.txt";
    char Models[1000] = "";
    unsigned char c, *CFdata;
    unsigned char pdr[2048];
    unsigned long BaseAddress;

    FILE *cf, *bdf;

    printf("\n");
    printf("********************************************************\n");
    printf(" CAEN SpA - Front-End Division                          \n");
    printf("--------------------------------------------------------\n");
    printf(" CAEN VME Firmware Upgrade                              \n");
    printf(" Version %s                                             \n", REVISION);
    printf("********************************************************\n\n");

    // Check command arguments (must be at least 2)
    if (argc < 3)  {
        printf("Syntax: CVupgrade ConfFile BaseAddress [options]\n\n");
        printf("Description: write the file 'ConfFile' (Altera Raw Binary Format) into\n");
        printf("the flash memory of board at the specific Base Address (Hex 32 bit)\n\n");
        printf("Options:\n");
        printf("-backup: write backup image\n\n");
        printf("-no_verify: disable verify (don't read and compare the flash content)\n\n");
        printf("-verify_only: read the flash content and compare with the ConfFile file\n\n");
        printf("-param filename: allow to specify the file that contain the parameters for the\n");
        printf("                 board that is being upgraded (default is CVupgrade_params.txt)\n");
        return(-1);
    }
    sprintf(ConfFile, argv[1]);
    sscanf(argv[2], "%x", &BaseAddress);
    for (i=3; i<argc; i++) {
        if ( strcmp(argv[i],"-backup") == 0 )
            image = 1;
        if ( strcmp(argv[i],"-no_verify") == 0 )
            noverify = 1;
        if ( strcmp(argv[i],"-verify_only") == 0 )
            verifyonly = 1;        
        if ( strcmp(argv[i],"-param") == 0 )
            sprintf(ParamFile, "%s", argv[++i]);  
		    
    }

    // open the board descriptor file
    bdf = fopen(ParamFile,"r");
    if(bdf == NULL) {
        printf("Can't open file %s\n", ParamFile);
        return (-2); 
    }
    fgets(Models, 1000, bdf);
    fscanf(bdf, "%x", &Sel_Flash);
    Sel_Flash += BaseAddress;
    fscanf(bdf, "%x", &RW_Flash);
    RW_Flash  += BaseAddress;
    fscanf(bdf, "%d", &RegSize);
    fscanf(bdf, "%d", &PageSize);
    fscanf(bdf, "%d", &FirstPageStd);
    fscanf(bdf, "%d", &FirstPageBck);
    if (!feof(bdf))
      fscanf(bdf, "%d", &FlashEnablePolarity);    
    fclose(bdf);


    // open and read the configuration file
    cf = fopen(ConfFile,"rb");
    if(cf == NULL) {
        printf("Can't open file %s\n",ConfFile);
        return(-3);
    }
    // calculate the size
    fseek (cf, 0, SEEK_END);
    CFsize = ftell (cf);
    fseek (cf, 0, SEEK_SET);
    if ( (CFdata = (unsigned char *)malloc(CFsize)) == NULL ) {
        printf("Can't allocate %d bytes\n",CFsize);
        return(-3);
    }

    for(i=0; i<CFsize; i++) {
        // read one byte from file and mirror it (lsb becomes msb)
        c = (unsigned char)fgetc(cf);
        CFdata[i]=0;
        for(j=0; j<8; j++){
            CFdata[i] |= ( (c >> (7-j)) & 1) << j;
		}
		//printf("CFdata[%d]:0x%x\n",i,CFdata[i]);
    }
    fclose(cf);

    NumPages = (CFsize % PageSize == 0) ? (CFsize / PageSize) : (CFsize / PageSize) + 1;

	printf("Number of Pages %d, RegSize %d, PageSize %d, Firstpagestd %d\n",NumPages,RegSize,PageSize,FirstPageStd);
    /* initialize the vme */
    if (VME_Init(Link, BdNum) < 0) {
        printf("Cannot open the VME controller!\n");
        return(-4);
    }

    printf("Board Types: %s\n", Models);
    if (image == 0) {
        if(!verifyonly)
          printf("Overwriting Standard image of the firmware with %s\n", ConfFile);
        else
            printf("Verifying Standard image of the firmware with %s\n", ConfFile);
        pa = FirstPageStd;
    }
    else {
        if(!verifyonly)
            printf("Overwriting Backup image of the firmware with %s\n", ConfFile);
        else
            printf("Verifying Backup image of the firmware with %s\n", ConfFile);
        pa = FirstPageBck;
    }

    printf("0%% Done\n");
    done = 10;

    // ************************************************
    // Start for loop
    // ************************************************
    for(page=0; page < NumPages; page++)  {
        if(!verifyonly) {
			printf("Writing\n");
            // Write Page
            if (WriteFlashPage(CFdata + page*PageSize, pa + page) < 0) {
                printf("\nCommunication Error: the board at Base Address %08X does not respond\n", BaseAddress);
                err = 1;
                break;
                }
        }

        if(!noverify) {
			printf("Reading\n");
            // Read Page
            if (ReadFlashPage(pdr, pa + page) < 0) {
                printf("\nCommunication Error: the board at Base Address %08X does not respond\n", BaseAddress);
                err = 1;
                break;
            }

			printf(" CFsize = %d , PageSize = %d\n",CFsize, PageSize);
            // Verify Page
            for(i=0; (i<PageSize) && ((page*PageSize+i) < CFsize); i++)  {
				printf("comparing: pdr[%d]:0x%x with CFData[%d]:0x%x\n",i,pdr[i],(page*PageSize+i),CFdata[page*PageSize + i] );
                if((pdr[i]&0xff) != CFdata[page*PageSize + i])  {
                    printf("\nFlash verify error (byte %d of page %d)!\n", i, pa + page);
                    if ((image == 0) && !verifyonly)
                        printf("The STD image can be corrupted! \nMove the jumper to Backup image and cycle the power\n");
                    err = 1;
                    break;
                }
            }
        }
        if (err)
            break;

        if (page == (NumPages-1)) {
            printf("100%% Done\n");
        } else if ( page >= (NumPages*done/100) ) {
            printf("%d%% Done\n", done);
            done += 10;
        }
    }  // end of for loop

    if(!err) {
        if (verifyonly) {
            printf("\nFirmware verified successfully. Read %d bytes\n", CFsize);
        } else {
            printf("\nFirmware updated successfully. Written %d bytes\n", CFsize);
            printf("The new firmware will be loaded after a power cycle\n");
        }
    }

    if (CFdata != NULL) 
      free(CFdata);
    
    VME_End();
    Wait_ms(1000);
    if (err)
        return (-5);
    else
        return 0;
}


