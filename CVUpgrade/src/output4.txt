
********************************************************
 CAEN SpA - Front-End Division                          
--------------------------------------------------------
 CAEN VME Firmware Upgrade                              
 Version 1.2                                             
********************************************************

Number of Pages 633, RegSize 2, PageSize 264, Firstpagestd 768
Board Types: V1190

Overwriting Standard image of the firmware with v1190_rev_0.C.1.rbf
0% Done
Writing
gefvme_openWindow: Slot 0, VME addr 0x00081032, am 0x00, size 0x00000004 4
Reading
 CFsize = 166965 , PageSize = 264
comparing: pdr[0]:0x0 with CFData[0]:0xff

Flash verify error (byte 0 of page 768)!
The STD image can be corrupted! 
Move the jumper to Backup image and cycle the power
