  -----------------------------------------------------------------------------

                   --- CAEN SpA - Front End R&D Division --- 

  -----------------------------------------------------------------------------
  -----------------------------------------------------------------------------

    January 2008

  -----------------------------------------------------------------------------
  
  
   Content 
   -------
  
   CVupgradeREADME.txt    :  This file.
                          
   doc                    :  Directory for documentation (application note).
   
   lib                    :  Directory for import library file 
                             (needed for Windows build only).
                             
   params                 :  Directory for CVupgrade configuration files 
                             for all supported boards.
                             
   src                    :  Directory for C source files.
                          
   inc                    :  Directory for include files.
                          
   build\                 
      Win32               :  Directory for MS VC++ 2005 solution/project.
      Linux               :  Directory for makefile
                          
   bin\                   
      Win32               :  Directory for CVUpgrade executable (Win version).
      Linux               :  Directory for CVUpgrade executable (Linux version).
  
  
   System Requirements
   -------------------
   
   - CAEN V1718 USB-VME Bridge and/or CAEN A2818 PCI-CONET Board
   - Windows 98/2000/XP
   - Linux kernel 2.4 or 2.6 and GNU C/C++ compiler 
        
   Note
   ----
   
   CAEN A2718 PCI-CONET Board Windows driver is based on PLX API libraries.
   Copyright (c) 2003 PLX Technology, Inc.
  
