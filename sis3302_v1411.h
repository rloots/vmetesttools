/***************************************************************************/
/*                                                                         */
/*  Filename: SIS3302_v1411.h                                              */
/*                                                                         */
/*  Funktion: headerfile for SIS3302-Gamma		                 		   */
/*                                                                         */
/*  Autor:                TH                                               */
/*  date:                 10.01.2006                                       */
/*  last modification:    07.06.2006                                       */
/*  last modification:    10.12.2007 MKI                                   */
/*  last modification:    01.08.2008 TH                                    */
/*  last modification:    27.03.2009 TH    (Version V_1205)                */
/*  last modification:    27.05.2009 TH    (Version V_1405)                */
/*  last modification:    04.09.2009 TH    (Version V_1406 and V1407)      */
/*  last modification:    01.06.2010 TH    (Version V_1410)                */
/*                        add SIS3302_REDUCED_VME_ADDR_WINDOW              */
/*  last modification:    23.11.2010 TH    (Version V_1411)                */
/*                        add SIS3302_SINGLE_READ_FSM.....                 */
/*                                                                         */
/* ----------------------------------------------------------------------- */
/*                                                                         */
/*  SIS  Struck Innovative Systeme GmbH                                    */
/*                                                                         */
/*  Harksheider Str. 102A                                                  */
/*  22399 Hamburg                                                          */
/*                                                                         */
/*  Tel. +49 (0)40 60 87 305 0                                             */
/*  Fax  +49 (0)40 60 87 305 20                                            */
/*                                                                         */
/*  http://www.struck.de                                                   */
/*                                                                         */
/*  � 2010                                                                 */
/*                                                                         */
/***************************************************************************/

//#define SIS3302_REDUCED_VME_ADDR_WINDOW		 

// SIS3302 Standard
#define SIS3302_CONTROL_STATUS                      			0x0	  /* read/write; D32 */
#define SIS3302_MODID                               			0x4	  /* read only; D32 */
#define SIS3302_IRQ_CONFIG                          			0x8      /* read/write; D32 */
#define SIS3302_IRQ_CONTROL                          			0xC      /* read/write; D32 */
#define SIS3302_ACQUISITION_CONTROL                  			0x10      /* read/write; D32 */


#define SIS3302_CBLT_BROADCAST_SETUP                			0x30      /* read/write; D32 */
#define SIS3302_ADC_MEMORY_PAGE_REGISTER            			0x34      /* read/write; D32 */

#define SIS3302_DAC_CONTROL_STATUS                  			0x50      /* read/write; D32 */
#define SIS3302_DAC_DATA                            			0x54      /* read/write; D32 */
	
#define SIS3302_SINGLE_READ_FSM_CONTROL_ADDRESS 				0xC0      /* read/write; D32 */
#define SIS3302_SINGLE_READ_FSM_LENGTH 							0xC4      /* read/write; D32 */
#define SIS3302_SINGLE_READ_FSM_ADC_CHANNEL_ENABLE				0xC8      /* read/write; D32 */
#define SIS3302_SINGLE_READ_FSM_FIFO_READ						0xCC      /* read D32 */



// Key Addresses  
#define SIS3302_KEY_RESET                           			0x400	  /* write only; D32 */

#define SIS3302_KEY_0x404_SAMPLE_LOGIC_RESET           			0x404	  /* write only; D32 */

#define SIS3302_KEY_SAMPLE_LOGIC_RESET              			0x410	  /* write only; D32 */
#define SIS3302_KEY_DISARM                         			0x414	  /* write only; D32 */
#define SIS3302_KEY_TRIGGER                         			0x418	  /* write only; D32 */
#define SIS3302_KEY_TIMESTAMP_CLEAR                 			0x41C	  /* write only; D32 */
#define SIS3302_KEY_DISARM_AND_ARM_BANK1           			0x420	  /* write only; D32 */
#define SIS3302_KEY_DISARM_AND_ARM_BANK2           			0x424	  /* write only; D32 */

#define SIS3302_KEY_RESET_DDR2_LOGIC                			0x428	  /* write only; D32 */

  /* gamma  MCA */
  /********************************/
#define SIS3302_MCA_SCAN_NOF_HISTOGRAMS_PRESET	    			0x80	  /* read/write; D32 */
#define SIS3302_MCA_SCAN_HISTOGRAM_COUNTER	    			0x84	  /* read only; D32  */
#define SIS3302_MCA_SCAN_SETUP_PRESCALE_FACTOR    			0x88	  /* read only; D32  */
#define SIS3302_MCA_SCAN_CONTROL			    		0x8C	  /* read/write; D32  */

#define SIS3302_MCA_MULTISCAN_NOF_SCANS_PRESET	    			0x90	  /* read/write; D32 */
#define SIS3302_MCA_MULTISCAN_SCAN_COUNTER	    			0x94	  /* read only; D32  */
#define SIS3302_MCA_MULTISCAN_LAST_SCAN_HISTOGRAM_COUNTER		0x98	  /* read only; D32  */

#define SIS3302_KEY_MCA_SCAN_LNE_PULSE           			0x410	  /* write only; D32 */
#define SIS3302_KEY_MCA_SCAN_ARM         				0x414	  /* write only; D32 */
#define SIS3302_KEY_MCA_SCAN_START           				0x418	  /* write only; D32 */
#define SIS3302_KEY_MCA_SCAN_DISABLE           				0x41C	  /* write only; D32 */

#define SIS3302_KEY_MCA_MULTISCAN_START_RESET_PULSE			0x420	  /* write only; D32 */
#define SIS3302_KEY_MCA_MULTISCAN_ARM_SCAN_ARM        			0x424	  /* write only; D32 */
#define SIS3302_KEY_MCA_MULTISCAN_ARM_SCAN_ENABLE			0x428	  /* write only; D32 */
#define SIS3302_KEY_MCA_MULTISCAN_DISABLE           			0x42C	  /* write only; D32 */



/**********************************************************/

// SIS3302 reduced Address window A23:A0
#ifdef SIS3302_REDUCED_VME_ADDR_WINDOW
  // all ADC FPGA groups
  #define SIS3302_EVENT_CONFIG_ALL_ADC                			0x00100000	  
  #define SIS3302_END_ADDRESS_THRESHOLD_ALL_ADC       			0x00100004	    /* Gamma */
  #define SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ALL_ADC  		0x00100008	    /* Gamma */
  #define SIS3302_RAW_DATA_BUFFER_CONFIG_ALL_ADC         		0x0010000C	    /* Gamma */

  #define SIS3302_ENERGY_SETUP_GP_ALL_ADC   				0x00100040      /* Gamma */
  #define SIS3302_ENERGY_GATE_LENGTH_ALL_ADC				0x00100044      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_LENGTH_ALL_ADC				0x00100048      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX1_ALL_ADC			0x0010004C      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX2_ALL_ADC			0x00100050      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX3_ALL_ADC			0x00100054      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC1357				0x00100058      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC2468				0x0010005C      /* Gamma */

  #define SIS3302_EVENT_EXTENDED_CONFIG_ALL_ADC          		0x00100070	  

  // ADC12 FPGA group
  #define SIS3302_EVENT_CONFIG_ADC12                			0x00200000 		/* Gamma */	  
  #define SIS3302_END_ADDRESS_THRESHOLD_ADC12      	 		0x00200004	    /* Gamma */
  #define SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC12  		0x00200008	    /* Gamma */
  #define SIS3302_RAW_DATA_BUFFER_CONFIG_ADC12       			0x0020000C	    /* Gamma */

  #define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC1          			0x00200010	  
  #define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC2          			0x00200014	  
  #define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC1   			0x00200018	  
  #define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC2   			0x0020001C	  

  #define SIS3302_ACTUAL_SAMPLE_VALUE_ADC12           			0x00200020	  
  #define SIS3302_DDR2_TEST_REGISTER_ADC12				0x00200028      

  #define SIS3302_TRIGGER_SETUP_ADC1                  			0x00200030	  
  #define SIS3302_TRIGGER_THRESHOLD_ADC1              			0x00200034	  
  #define SIS3302_TRIGGER_SETUP_ADC2                  			0x00200038	  
  #define SIS3302_TRIGGER_THRESHOLD_ADC2              			0x0020003C	  

  #define SIS3302_ENERGY_SETUP_GP_ADC12   				0x00200040      /* Gamma */
  #define SIS3302_ENERGY_GATE_LENGTH_ADC12				0x00200044      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_LENGTH_ADC12				0x00200048      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX1_ADC12			0x0020004C      /* Gamma */

  #define SIS3302_ENERGY_SAMPLE_START_INDEX2_ADC12			0x00200050      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX3_ADC12			0x00200054      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC1				0x00200058      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC2				0x0020005C      /* Gamma */

  #define SIS3302_EVENT_EXTENDED_CONFIG_ADC12       			0x00200070 		   
  #define SIS3302_TRIGGER_EXTENDED_SETUP_ADC1             		0x00200078	  
  #define SIS3302_TRIGGER_EXTENDED_SETUP_ADC2             		0x0020007C	  



  // ADC34 FPGA group
  #define SIS3302_EVENT_CONFIG_ADC34                			0x00280000 		/* Gamma */	  
  #define SIS3302_END_ADDRESS_THRESHOLD_ADC34      	 		0x00280004	    /* Gamma */
  #define SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC34  		0x00280008	    /* Gamma */
  #define SIS3302_RAW_DATA_BUFFER_CONFIG_ADC34       			0x0028000C	    /* Gamma */

  #define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC3          			0x00280010	  
  #define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC4          			0x00280014	  
  #define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC3   			0x00280018	  
  #define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC4   			0x0028001C	  

  #define SIS3302_ACTUAL_SAMPLE_VALUE_ADC34           			0x00280020	  
  #define SIS3302_DDR2_TEST_REGISTER_ADC34				0x00280028      

  #define SIS3302_TRIGGER_SETUP_ADC3                  			0x00280030	  
  #define SIS3302_TRIGGER_THRESHOLD_ADC3              			0x00280034	  
  #define SIS3302_TRIGGER_SETUP_ADC4                  			0x00280038	  
  #define SIS3302_TRIGGER_THRESHOLD_ADC4              			0x0028003C	  

  #define SIS3302_ENERGY_SETUP_GP_ADC34   				0x00280040      /* Gamma */
  #define SIS3302_ENERGY_GATE_LENGTH_ADC34				0x00280044      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_LENGTH_ADC34				0x00280048      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX1_ADC34			0x0028004C      /* Gamma */

  #define SIS3302_ENERGY_SAMPLE_START_INDEX2_ADC34			0x00280050      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX3_ADC34			0x00280054      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC3				0x00280058      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC4				0x0028005C      /* Gamma */

  #define SIS3302_EVENT_EXTENDED_CONFIG_ADC34       			0x00280070 		   
  #define SIS3302_TRIGGER_EXTENDED_SETUP_ADC3             		0x00280078	  
  #define SIS3302_TRIGGER_EXTENDED_SETUP_ADC4             		0x0028007C	  

  // ADC56 FPGA group
  #define SIS3302_EVENT_CONFIG_ADC56                			0x00300000 		/* Gamma */	  
  #define SIS3302_END_ADDRESS_THRESHOLD_ADC56      	 		0x00300004	    /* Gamma */
  #define SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC56  		0x00300008	    /* Gamma */
  #define SIS3302_RAW_DATA_BUFFER_CONFIG_ADC56       			0x0030000C	    /* Gamma */

  #define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC5          			0x00300010	  	
  #define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC6          			0x00300014	  
  #define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC5   			0x00300018	  
  #define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC6   			0x0030001C	  

  #define SIS3302_ACTUAL_SAMPLE_VALUE_ADC56           			0x00300020	  
  #define SIS3302_DDR2_TEST_REGISTER_ADC56				0x00300028      

  #define SIS3302_TRIGGER_SETUP_ADC5                  			0x00300030	  
  #define SIS3302_TRIGGER_THRESHOLD_ADC5              			0x00300034	  
  #define SIS3302_TRIGGER_SETUP_ADC6                  			0x00300038	  
  #define SIS3302_TRIGGER_THRESHOLD_ADC6              			0x0030003C	  

  #define SIS3302_ENERGY_SETUP_GP_ADC56   				0x00300040      /* Gamma */
  #define SIS3302_ENERGY_GATE_LENGTH_ADC56				0x00300044      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_LENGTH_ADC56				0x00300048      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX1_ADC56			0x0030004C      /* Gamma */

  #define SIS3302_ENERGY_SAMPLE_START_INDEX2_ADC56			0x00300050      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX3_ADC56			0x00300054      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC5				0x00300058      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC6				0x0030005C      /* Gamma */

  #define SIS3302_EVENT_EXTENDED_CONFIG_ADC56        			0x00300070 		   
  #define SIS3302_TRIGGER_EXTENDED_SETUP_ADC5             		0x00300078	  
  #define SIS3302_TRIGGER_EXTENDED_SETUP_ADC6             		0x0030007C	  


  // ADC78 FPGA group
  #define SIS3302_EVENT_CONFIG_ADC78                			0x00380000 		/* Gamma */	  
  #define SIS3302_END_ADDRESS_THRESHOLD_ADC78      	 		0x00380004	    /* Gamma */
  #define SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC78  		0x00380008	    /* Gamma */
  #define SIS3302_RAW_DATA_BUFFER_CONFIG_ADC78       			0x0038000C	    /* Gamma */

  #define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC7          			0x00380010	  
  #define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC8          			0x00380014	  
  #define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC7   			0x00380018	  
  #define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC8   			0x0038001C	  

  #define SIS3302_ACTUAL_SAMPLE_VALUE_ADC78           			0x00380020	  
  #define SIS3302_DDR2_TEST_REGISTER_ADC78				0x00380028      

  #define SIS3302_TRIGGER_SETUP_ADC7                  			0x00380030	  
  #define SIS3302_TRIGGER_THRESHOLD_ADC7              			0x00380034	  
  #define SIS3302_TRIGGER_SETUP_ADC8                  			0x00380038	  
  #define SIS3302_TRIGGER_THRESHOLD_ADC8              			0x0038003C	  

  #define SIS3302_ENERGY_SETUP_GP_ADC78   				0x00380040      /* Gamma */
  #define SIS3302_ENERGY_GATE_LENGTH_ADC78				0x00380044      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_LENGTH_ADC78				0x00380048      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX1_ADC78			0x0038004C      /* Gamma */

  #define SIS3302_ENERGY_SAMPLE_START_INDEX2_ADC78			0x00380050      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX3_ADC78			0x00380054      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC7				0x00380058      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC8				0x0038005C      /* Gamma */


  #define SIS3302_EVENT_EXTENDED_CONFIG_ADC78               		0x00380070 		   
  #define SIS3302_TRIGGER_EXTENDED_SETUP_ADC7             		0x00380078	  
  #define SIS3302_TRIGGER_EXTENDED_SETUP_ADC8             		0x0038007C	  


  #define SIS3302_ADC1_OFFSET                         			0x00400000	  
  #define SIS3302_ADC2_OFFSET                         			0x00480000	  
  #define SIS3302_ADC3_OFFSET                         			0x00500000	  
  #define SIS3302_ADC4_OFFSET                         			0x00580000	  
  #define SIS3302_ADC5_OFFSET                         			0x00600000	  
  #define SIS3302_ADC6_OFFSET                         			0x00680000	  
  #define SIS3302_ADC7_OFFSET                         			0x00700000	  
  #define SIS3302_ADC8_OFFSET                         			0x00780000	  

  #define SIS3302_NEXT_ADC_OFFSET                     			0x0080000	  
  #define SIS3302_MAX_ADC_MEMORY_PAGE_BYTE_LENGTH			SIS3302_NEXT_ADC_OFFSET
  #define SIS3302_ADC_MEMORY_PAGE_REG_ADDR_SHIFT			19
  #define SIS3302_ADC_MEMORY_PAGE_REG_ADDR_MASK				0x7F




  #define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC1357    		0x00100060	  /* write only; D32 */
  #define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC2468    		0x00100064	  /* write only; D32 */
  #define SIS3302_MCA_HISTOGRAM_PARAM_ALL_ADC    			0x00100068	  /* write only; D32 */


  #define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC1    			0x00200060	  /* read/write; D32 */
  #define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC2    			0x00200064	  /* read/write; D32 */
  #define SIS3302_MCA_HISTOGRAM_PARAM_ADC12    				0x00200068	  /* read/write; D32 */

  #define SIS3302_MCA_TRIGGER_START_COUNTER_ADC1 			0x00200080	  /* read only; D32 */
  #define SIS3302_MCA_PILEUP_COUNTER_ADC1 				0x00200084	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC1 				0x00200088	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC1 				0x0020008C	  /* read only; D32 */

  #define SIS3302_MCA_TRIGGER_START_COUNTER_ADC2 			0x00200090	  /* read only; D32 */
  #define SIS3302_MCA_PILEUP_COUNTER_ADC2 				0x00200094	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC2 				0x00200098	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC2 				0x0020009C	  /* read only; D32 */


  #define SIS3302_MCA_TRIGGER_START_COUNTER_ADC3 			0x00280080	  /* read only; D32 */
  #define SIS3302_MCA_PILEUP_COUNTER_ADC3 				0x00280084	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC3 				0x00280088	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC3 				0x0028008C	  /* read only; D32 */

  #define SIS3302_MCA_TRIGGER_START_COUNTER_ADC4 			0x00280090	  /* read only; D32 */
  #define SIS3302_MCA_PILEUP_COUNTER_ADC4 				0x00280094	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC4 				0x00280098	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC4 				0x0028009C	  /* read only; D32 */

  #define SIS3302_MCA_TRIGGER_START_COUNTER_ADC5 			0x00300080	  /* read only; D32 */
  #define SIS3302_MCA_PILEUP_COUNTER_ADC5 				0x00300084	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC5 				0x00300088	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC5 				0x0030008C	  /* read only; D32 */

  #define SIS3302_MCA_TRIGGER_START_COUNTER_ADC6 			0x00300090	  /* read only; D32 */
  #define SIS3302_MCA_PILEUP_COUNTER_ADC6 				0x00300094	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC6 				0x00300098	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC6 				0x0030009C	  /* read only; D32 */

  #define SIS3302_MCA_TRIGGER_START_COUNTER_ADC7 			0x00380080	  /* read only; D32 */
  #define SIS3302_MCA_PILEUP_COUNTER_ADC7 				0x00380084	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC7 				0x00380088	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC7 				0x0038008C	  /* read only; D32 */

  #define SIS3302_MCA_TRIGGER_START_COUNTER_ADC8 			0x00380090	  /* read only; D32 */
  #define SIS3302_MCA_PILEUP_COUNTER_ADC8 				0x00380094	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC8 				0x00380098	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC8 				0x0038009C	  /* read only; D32 */

#endif


/**********************************************************/


// SIS3302 Address window A26:A0
#ifndef SIS3302_REDUCED_VME_ADDR_WINDOW

  // all ADC FPGA groups
  #define SIS3302_EVENT_CONFIG_ALL_ADC                			0x01000000	  
  #define SIS3302_END_ADDRESS_THRESHOLD_ALL_ADC       			0x01000004	    /* Gamma */
  #define SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ALL_ADC  		0x01000008	    /* Gamma */
  #define SIS3302_RAW_DATA_BUFFER_CONFIG_ALL_ADC         		0x0100000C	    /* Gamma */

  #define SIS3302_ENERGY_SETUP_GP_ALL_ADC   				0x01000040      /* Gamma */
  #define SIS3302_ENERGY_GATE_LENGTH_ALL_ADC				0x01000044      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_LENGTH_ALL_ADC				0x01000048      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX1_ALL_ADC			0x0100004C      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX2_ALL_ADC			0x01000050      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX3_ALL_ADC			0x01000054      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC1357				0x01000058      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC2468				0x0100005C      /* Gamma */

  #define SIS3302_EVENT_EXTENDED_CONFIG_ALL_ADC          		0x01000070	  

  // ADC12 FPGA group
  #define SIS3302_EVENT_CONFIG_ADC12                			0x02000000 		/* Gamma */	  
  #define SIS3302_END_ADDRESS_THRESHOLD_ADC12      	 		0x02000004	    /* Gamma */
  #define SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC12  		0x02000008	    /* Gamma */
  #define SIS3302_RAW_DATA_BUFFER_CONFIG_ADC12       			0x0200000C	    /* Gamma */

  #define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC1          			0x02000010	  
  #define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC2          			0x02000014	  
  #define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC1   			0x02000018	  
  #define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC2   			0x0200001C	  

  #define SIS3302_ACTUAL_SAMPLE_VALUE_ADC12           			0x02000020	  
  #define SIS3302_DDR2_TEST_REGISTER_ADC12				0x02000028      

  #define SIS3302_TRIGGER_SETUP_ADC1                  			0x02000030	  
  #define SIS3302_TRIGGER_THRESHOLD_ADC1              			0x02000034	  
  #define SIS3302_TRIGGER_SETUP_ADC2                  			0x02000038	  
  #define SIS3302_TRIGGER_THRESHOLD_ADC2              			0x0200003C	  

  #define SIS3302_ENERGY_SETUP_GP_ADC12   				0x02000040      /* Gamma */
  #define SIS3302_ENERGY_GATE_LENGTH_ADC12				0x02000044      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_LENGTH_ADC12				0x02000048      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX1_ADC12			0x0200004C      /* Gamma */

  #define SIS3302_ENERGY_SAMPLE_START_INDEX2_ADC12			0x02000050      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX3_ADC12			0x02000054      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC1				0x02000058      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC2				0x0200005C      /* Gamma */

  #define SIS3302_EVENT_EXTENDED_CONFIG_ADC12       			0x02000070 		   
  #define SIS3302_TRIGGER_EXTENDED_SETUP_ADC1             		0x02000078	  
  #define SIS3302_TRIGGER_EXTENDED_SETUP_ADC2             		0x0200007C	  



  // ADC34 FPGA group
  #define SIS3302_EVENT_CONFIG_ADC34                			0x02800000 		/* Gamma */	  
  #define SIS3302_END_ADDRESS_THRESHOLD_ADC34      	 		0x02800004	    /* Gamma */
  #define SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC34  		0x02800008	    /* Gamma */
  #define SIS3302_RAW_DATA_BUFFER_CONFIG_ADC34       			0x0280000C	    /* Gamma */

  #define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC3          			0x02800010	  
  #define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC4          			0x02800014	  
  #define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC3   			0x02800018	  
  #define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC4   			0x0280001C	  

  #define SIS3302_ACTUAL_SAMPLE_VALUE_ADC34           			0x02800020	  
  #define SIS3302_DDR2_TEST_REGISTER_ADC34				0x02800028      

  #define SIS3302_TRIGGER_SETUP_ADC3                  			0x02800030	  
  #define SIS3302_TRIGGER_THRESHOLD_ADC3              			0x02800034	  
  #define SIS3302_TRIGGER_SETUP_ADC4                  			0x02800038	  
  #define SIS3302_TRIGGER_THRESHOLD_ADC4              			0x0280003C	  

  #define SIS3302_ENERGY_SETUP_GP_ADC34   				0x02800040      /* Gamma */
  #define SIS3302_ENERGY_GATE_LENGTH_ADC34				0x02800044      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_LENGTH_ADC34				0x02800048      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX1_ADC34			0x0280004C      /* Gamma */

  #define SIS3302_ENERGY_SAMPLE_START_INDEX2_ADC34			0x02800050      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX3_ADC34			0x02800054      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC3				0x02800058      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC4				0x0280005C      /* Gamma */

  #define SIS3302_EVENT_EXTENDED_CONFIG_ADC34       			0x02800070 		   
  #define SIS3302_TRIGGER_EXTENDED_SETUP_ADC3             		0x02800078	  
  #define SIS3302_TRIGGER_EXTENDED_SETUP_ADC4             		0x0280007C	  

  // ADC56 FPGA group
  #define SIS3302_EVENT_CONFIG_ADC56                			0x03000000 		/* Gamma */	  
  #define SIS3302_END_ADDRESS_THRESHOLD_ADC56      	 		0x03000004	    /* Gamma */
  #define SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC56  		0x03000008	    /* Gamma */
  #define SIS3302_RAW_DATA_BUFFER_CONFIG_ADC56       			0x0300000C	    /* Gamma */

  #define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC5          			0x03000010	  	
  #define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC6          			0x03000014	  
  #define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC5   			0x03000018	  
  #define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC6   			0x0300001C	  

  #define SIS3302_ACTUAL_SAMPLE_VALUE_ADC56           			0x03000020	  
  #define SIS3302_DDR2_TEST_REGISTER_ADC56				0x03000028      

  #define SIS3302_TRIGGER_SETUP_ADC5                  			0x03000030	  
  #define SIS3302_TRIGGER_THRESHOLD_ADC5              			0x03000034	  
  #define SIS3302_TRIGGER_SETUP_ADC6                  			0x03000038	  
  #define SIS3302_TRIGGER_THRESHOLD_ADC6              			0x0300003C	  

  #define SIS3302_ENERGY_SETUP_GP_ADC56   				0x03000040      /* Gamma */
  #define SIS3302_ENERGY_GATE_LENGTH_ADC56				0x03000044      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_LENGTH_ADC56				0x03000048      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX1_ADC56			0x0300004C      /* Gamma */

  #define SIS3302_ENERGY_SAMPLE_START_INDEX2_ADC56			0x03000050      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX3_ADC56			0x03000054      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC5				0x03000058      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC6				0x0300005C      /* Gamma */

  #define SIS3302_EVENT_EXTENDED_CONFIG_ADC56        			0x03000070 		   
  #define SIS3302_TRIGGER_EXTENDED_SETUP_ADC5             		0x03000078	  
  #define SIS3302_TRIGGER_EXTENDED_SETUP_ADC6             		0x0300007C	  


  // ADC78 FPGA group
  #define SIS3302_EVENT_CONFIG_ADC78                			0x03800000 		/* Gamma */	  
  #define SIS3302_END_ADDRESS_THRESHOLD_ADC78      	 		0x03800004	    /* Gamma */
  #define SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC78  		0x03800008	    /* Gamma */
  #define SIS3302_RAW_DATA_BUFFER_CONFIG_ADC78       			0x0380000C	    /* Gamma */

  #define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC7          			0x03800010	  
  #define SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC8          			0x03800014	  
  #define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC7   			0x03800018	  
  #define SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC8   			0x0380001C	  

  #define SIS3302_ACTUAL_SAMPLE_VALUE_ADC78           			0x03800020	  
  #define SIS3302_DDR2_TEST_REGISTER_ADC78				0x03800028      

  #define SIS3302_TRIGGER_SETUP_ADC7                  			0x03800030	  
  #define SIS3302_TRIGGER_THRESHOLD_ADC7              			0x03800034	  
  #define SIS3302_TRIGGER_SETUP_ADC8                  			0x03800038	  
  #define SIS3302_TRIGGER_THRESHOLD_ADC8              			0x0380003C	  

  #define SIS3302_ENERGY_SETUP_GP_ADC78   				0x03800040      /* Gamma */
  #define SIS3302_ENERGY_GATE_LENGTH_ADC78				0x03800044      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_LENGTH_ADC78				0x03800048      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX1_ADC78			0x0380004C      /* Gamma */

  #define SIS3302_ENERGY_SAMPLE_START_INDEX2_ADC78			0x03800050      /* Gamma */
  #define SIS3302_ENERGY_SAMPLE_START_INDEX3_ADC78			0x03800054      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC7				0x03800058      /* Gamma */
  #define SIS3302_ENERGY_TAU_FACTOR_ADC8				0x0380005C      /* Gamma */


  #define SIS3302_EVENT_EXTENDED_CONFIG_ADC78               		0x03800070 		   
  #define SIS3302_TRIGGER_EXTENDED_SETUP_ADC7             		0x03800078	  
  #define SIS3302_TRIGGER_EXTENDED_SETUP_ADC8             		0x0380007C	  


  #define SIS3302_ADC1_OFFSET                         			0x04000000	  
  #define SIS3302_ADC2_OFFSET                         			0x04800000	  
  #define SIS3302_ADC3_OFFSET                         			0x05000000	  
  #define SIS3302_ADC4_OFFSET                         			0x05800000	  
  #define SIS3302_ADC5_OFFSET                         			0x06000000	  
  #define SIS3302_ADC6_OFFSET                         			0x06800000	  
  #define SIS3302_ADC7_OFFSET                         			0x07000000	  
  #define SIS3302_ADC8_OFFSET                         			0x07800000	  

  #define SIS3302_NEXT_ADC_OFFSET                     			0x00800000	  
  #define SIS3302_MAX_ADC_MEMORY_PAGE_BYTE_LENGTH			SIS3302_NEXT_ADC_OFFSET
  #define SIS3302_ADC_MEMORY_PAGE_REG_ADDR_SHIFT			23 // 19
  #define SIS3302_ADC_MEMORY_PAGE_REG_ADDR_MASK				0x7 // 0x7F


  /* gamma  MCA */
  /********************************/
  #define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC1357    		0x01000060	  /* write only; D32 */
  #define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC2468    		0x01000064	  /* write only; D32 */
  #define SIS3302_MCA_HISTOGRAM_PARAM_ALL_ADC    			0x01000068	  /* write only; D32 */


  #define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC1    			0x02000060	  /* read/write; D32 */
  #define SIS3302_MCA_ENERGY2HISTOGRAM_PARAM_ADC2    			0x02000064	  /* read/write; D32 */
  #define SIS3302_MCA_HISTOGRAM_PARAM_ADC12    				0x02000068	  /* read/write; D32 */

  #define SIS3302_MCA_TRIGGER_START_COUNTER_ADC1 			0x02000080	  /* read only; D32 */
  #define SIS3302_MCA_PILEUP_COUNTER_ADC1 				0x02000084	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC1 				0x02000088	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC1 				0x0200008C	  /* read only; D32 */

  #define SIS3302_MCA_TRIGGER_START_COUNTER_ADC2 			0x02000090	  /* read only; D32 */
  #define SIS3302_MCA_PILEUP_COUNTER_ADC2 				0x02000094	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC2 				0x02000098	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC2 				0x0200009C	  /* read only; D32 */


  #define SIS3302_MCA_TRIGGER_START_COUNTER_ADC3 			0x02800080	  /* read only; D32 */
  #define SIS3302_MCA_PILEUP_COUNTER_ADC3 				0x02800084	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC3 				0x02800088	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC3 				0x0280008C	  /* read only; D32 */

  #define SIS3302_MCA_TRIGGER_START_COUNTER_ADC4 			0x02800090	  /* read only; D32 */
  #define SIS3302_MCA_PILEUP_COUNTER_ADC4 				0x02800094	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC4 				0x02800098	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC4 				0x0280009C	  /* read only; D32 */

  #define SIS3302_MCA_TRIGGER_START_COUNTER_ADC5 			0x03000080	  /* read only; D32 */
  #define SIS3302_MCA_PILEUP_COUNTER_ADC5 				0x03000084	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC5 				0x03000088	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC5 				0x0300008C	  /* read only; D32 */

  #define SIS3302_MCA_TRIGGER_START_COUNTER_ADC6 			0x03000090	  /* read only; D32 */
  #define SIS3302_MCA_PILEUP_COUNTER_ADC6 				0x03000094	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC6 				0x03000098	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC6 				0x0300009C	  /* read only; D32 */

  #define SIS3302_MCA_TRIGGER_START_COUNTER_ADC7 			0x03800080	  /* read only; D32 */
  #define SIS3302_MCA_PILEUP_COUNTER_ADC7 				0x03800084	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC7 				0x03800088	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC7 				0x0380008C	  /* read only; D32 */

  #define SIS3302_MCA_TRIGGER_START_COUNTER_ADC8 			0x03800090	  /* read only; D32 */
  #define SIS3302_MCA_PILEUP_COUNTER_ADC8 				0x03800094	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2HIGH_COUNTER_ADC8 				0x03800098	  /* read only; D32 */
  #define SIS3302_MCA_ENERGY2LOW_COUNTER_ADC8 				0x0380009C	  /* read only; D32 */

#endif

#define SIS3302_MAX_ADC_MEMORY_BYTE_LENGTH				0x04000000 // 64MByte
#define SIS3302_ADC_MEMORY_32MBYTE_OFFSET				0x02000000 // 32MByte


/*************************************************************************************************************************/


/* define sample clock */
#define SIS3302_ACQ_SET_CLOCK_TO_100MHZ                 0x70000000  /* default after Reset  */
#define SIS3302_ACQ_SET_CLOCK_TO_50MHZ                  0x60001000
#define SIS3302_ACQ_SET_CLOCK_TO_25MHZ                  0x50002000
#define SIS3302_ACQ_SET_CLOCK_TO_10MHZ                  0x40003000
#define SIS3302_ACQ_SET_CLOCK_TO_1MHZ                   0x30004000
#define SIS3302_ACQ_SET_CLOCK_TO_LEMO_RANDOM_CLOCK_IN   0x20005000
#define SIS3302_ACQ_SET_CLOCK_TO_LEMO_CLOCK_IN          0x10006000
//#define SIS3302_ACQ_SET_CLOCK_TO_P2_CLOCK_IN            0x00007000
#define SIS3302_ACQ_SET_CLOCK_TO_SECOND_100MHZ           0x00007000



#define SIS3302_ACQ_DISABLE_LEMO_TRIGGER            	0x01000000 /* GAMMA, 091207 */
#define SIS3302_ACQ_ENABLE_LEMO_TRIGGER             	0x00000100 /* GAMMA, 091207 */
#define SIS3302_ACQ_DISABLE_LEMO_TIMESTAMPCLR       	0x02000000 /* GAMMA, 091207 */
#define SIS3302_ACQ_ENABLE_LEMO_TIMESTAMPCLR        	0x00000200 /* GAMMA, 091207 */

// new 16.3.2009
#define SIS3302_ACQ_DISABLE_EXTERNAL_LEMO_IN3       	0x01000000 /* GAMMA, up V_1205 */
#define SIS3302_ACQ_ENABLE_EXTERNAL_LEMO_IN3        	0x00000100 /* GAMMA, up V_1205 */
#define SIS3302_ACQ_DISABLE_EXTERNAL_LEMO_IN2       	0x02000000 /* GAMMA, up V_1205 */
#define SIS3302_ACQ_ENABLE_EXTERNAL_LEMO_IN2        	0x00000200 /* GAMMA, up V_1205 */
#define SIS3302_ACQ_DISABLE_EXTERNAL_LEMO_IN1       	0x04000000 /* GAMMA, up V_1205 */
#define SIS3302_ACQ_ENABLE_EXTERNAL_LEMO_IN1        	0x00000400 /* GAMMA, up V_1205 */

#define SIS3302_ACQ_SET_LEMO_IN_MODE0              	0x00070000  /* GAMMA, up V_1205   */
#define SIS3302_ACQ_SET_LEMO_IN_MODE1         		0x00060001  /* GAMMA, up V_1205   */
#define SIS3302_ACQ_SET_LEMO_IN_MODE2           	0x00050002  /* GAMMA, up V_1205   */
#define SIS3302_ACQ_SET_LEMO_IN_MODE3      		0x00040003  /* GAMMA, up V_1205   */
#define SIS3302_ACQ_SET_LEMO_IN_MODE4          		0x00030004  /* GAMMA, up V_1205   */
#define SIS3302_ACQ_SET_LEMO_IN_MODE5   		0x00020005  /* GAMMA, up V_1205   */
#define SIS3302_ACQ_SET_LEMO_IN_MODE6          		0x00010006  /* GAMMA, up V_1205   */
#define SIS3302_ACQ_SET_LEMO_IN_MODE7           	0x00000007  /* GAMMA, up V_1205   */
#define SIS3302_ACQ_SET_LEMO_IN_MODE_BIT_MASK      	0x00000007  /* GAMMA, up V_1205   */

#define SIS3302_ACQ_SET_LEMO_OUT_MODE0             	0x00300000  /* GAMMA, up V_1205   */
#define SIS3302_ACQ_SET_LEMO_OUT_MODE1         		0x00200010  /* GAMMA, up V_1205   */
#define SIS3302_ACQ_SET_LEMO_OUT_MODE2           	0x00100020  /* GAMMA, up V_1205   */
#define SIS3302_ACQ_SET_LEMO_OUT_MODE3      		0x00000030  /* GAMMA, up V_1205   */
#define SIS3302_ACQ_SET_LEMO_OUT_MODE_BIT_MASK 		0x00000030  /* GAMMA, up V_1205   */

#define SIS3302_ACQ_SET_FEEDBACK_INTERNAL_TRIGGER 	0x00000040  /* GAMMA, up V_1205   */
#define SIS3302_ACQ_CLR_FEEDBACK_INTERNAL_TRIGGER 	0x00400000  /* GAMMA, up V_1205   */




#define SIS3302_BROADCAST_MASTER_ENABLE        		0x20	
#define SIS3302_BROADCAST_ENABLE               		0x10	


/* gamma  */
#define EVENT_CONF_ADC2_NP1_NB_GATE_ENABLE_BIT		0x8000	  /*     */ 
#define EVENT_CONF_ADC2_NM1_NB_GATE_ENABLE_BIT		0x4000	  /*     */ 
#define EVENT_CONF_ADC2_EXTERN_GATE_ENABLE_BIT		0x2000	  /* GAMMA, up V_1205   */ 
#define EVENT_CONF_ADC2_INTERN_GATE_ENABLE_BIT		0x1000	  /* GAMMA, up V_1205   */
#define EVENT_CONF_ADC2_EXTERN_TRIGGER_ENABLE_BIT	0x800	   
#define EVENT_CONF_ADC2_INTERN_TRIGGER_ENABLE_BIT	0x400	
#define EVENT_CONF_ADC2_INPUT_INVERT_BIT			0x100	  

#define EVENT_CONF_ADC1_NP1_NB_GATE_ENABLE_BIT		0x80	  /*     */ 
#define EVENT_CONF_ADC1_NM1_NB_GATE_ENABLE_BIT		0x40	  /*     */ 
#define EVENT_CONF_ADC1_EXTERN_GATE_ENABLE_BIT		0x20	  /* GAMMA, up V_1205   */ 
#define EVENT_CONF_ADC1_INTERN_GATE_ENABLE_BIT		0x10	  /* GAMMA, up V_1205   */
#define EVENT_CONF_ADC1_EXTERN_TRIGGER_ENABLE_BIT	0x8	  
#define EVENT_CONF_ADC1_INTERN_TRIGGER_ENABLE_BIT	0x4	 
#define EVENT_CONF_ADC1_INPUT_INVERT_BIT			0x1	   



#define DECIMATION_DISABLE				0x00000000
#define DECIMATION_2					0x10000000
#define DECIMATION_4					0x20000000
#define DECIMATION_8					0x30000000


/* gamma  MCA */


#define SIS3302_ACQ_SET_MCA_MODE 		0x00000008  /* GAMMA, up V_1205   */
#define SIS3302_ACQ_CLR_MCA_MODE 		0x00080000  /* GAMMA, up V_1205   */
