#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <pthread.h>
#include <vme_api_en.h>

int done;
int CTRL_devHandle;

int getVerror(int timeout)
{
    int retval;
    UINT32 intNum;
    UINT32 selectedInts;

    /*
     * Note: This function is not thread safe - good enough for a test
     */

    selectedInts = 1L <<EN_VERR;
    intNum = 0;
    retval = vme_waitInterrupt(CTRL_devHandle, selectedInts,
                               timeout, &intNum);
    return retval;
}

void sighandler(int num)
{
    done = 1;
}

void *verr_thread(void *arg)
{
    int rc;

    printf("Thread starting\n");
    while (1)
    {
        pthread_testcancel();
        rc = getVerror(1000);

        if(rc < 0)
        {
            /*
             * Call will timeout - we need the timeout to allow the
             * thread to be cancelled. Note current Solaris driver has a 
             * bug where it does not set the error code for 'ETIME' in the
             * case of a timeout.
             */
            if((errno != ETIME) && (errno !=0))
            {
                printf("Error - failed to receive interrupt, errno %d (%s)\n", 
                       errno, strerror(errno));
            }
        }
//        else
//        {
//        }
    }
}


int main( void )
{
    int status;
    pthread_t thread_id;

    printf("VME Int Test\n");

    signal(SIGINT, sighandler);

    /*  Open the Control device */
    CTRL_devHandle = vme_openDevice( "ctl" );
    if ( CTRL_devHandle < 0 )
    {   
        printf("Error - failed to open ctl\n");
        exit(0);   
    }
 
    printf("Starting thread.....\n");
    pthread_create(&thread_id, NULL, verr_thread, NULL);

    printf("Sleeping....\n");
    sleep(10);

    printf("Writing - press ctrl-C to exit.....\n");

    while(!done)
    {
        printf("looping...\n");
        sleep(1);
    }
    printf("Exit on signal\n");

    /* Shutdown thread */
    printf("Cancel thread\n");
    pthread_cancel(thread_id);
    pthread_join(thread_id, NULL);

    printf("Close control device\n");
    vme_closeDevice(CTRL_devHandle);

    printf("Exit\n");
    return 0;
}
