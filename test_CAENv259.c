#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include "v259.h"
#include <sys/time.h>
#include "mvmestd.h"


int MAXEVENT=1000;



int main () {

	DWORD V259_BASE = 0xC0000000;
	MVME_INTERFACE *myvme;

	
	// Test under vmic   
	int status = mvme_open(&myvme, 0);
  
	printf(" base addr = 0x%.08x , status %d \n",V259_BASE,status);	

	// Set am to A24 non-privileged Data
	mvme_set_am(myvme, MVME_AM_A24);

	// Set dmode to D16
	mvme_set_dmode(myvme, MVME_DMODE_D16);
	
	// Get Firmware revision
	//int k = 100000;

	int csr = mvme_read_value(myvme, V259_BASE+V259_VERSION_RO);
	printf("Firmware revision: 0x%.08x\n", csr&0xff);
	//v259_SoftReset(myvme, V259_BASE);
	//v259_Status(myvme, V259_BASE);

	//v259_SoftClear(myvme, V259_BASE);
	//
//	for (int j=0;j<MAXEVENT;j++) {
//		usleep(10000);
//		printf("Pattern: %d\n",v259_ReadPatternReset(myvme, V259_BASE));
//	}
	
	status = mvme_close(myvme);
	return 0;
}	

/* emacs
 * Local Variables:
 * mode:C++
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
