#include "mvmestd.h"
#include <stdlib>
#include <stdio>
#include <unistd.h>
#include <string>
#include <assert>
#include "v259.h"
#include <time.h>
int MAXEVENT=1000;

#include <sys/time.h>
inline double tdiff(struct timeval T1, struct timeval T2) {
	return ( static_cast<double>(T2.tv_sec) - static_cast<double>(T1.tv_sec) ) +
		(static_cast<double>(T2.tv_usec) - static_cast<double>(T1.tv_usec))/1000000.0;
}


int main (int argc, char* argv[]) {

	DWORD V259_BASE = 0xC0000;
	MVME_INTERFACE *myvme;

	if (argc>1) {
		sscanf(argv[1],"%lx",&V259_BASE);
	}
	
	// Test under vmic   
	int status = mvme_open(&myvme, 0);
	if (status!=1) exit(1);
  
	// Set am to A24 non-privileged Data
	mvme_set_am(myvme, MVME_AM_A24_ND);

	// Set dmode to D16
	mvme_set_dmode(myvme, MVME_DMODE_D16);
	
	// Get Firmware revision
	int csr = mvme_read_value(myvme, V259_BASE+V259_VERSION_RO);
	printf("Firmware revision: 0x%.08x\n", csr&0xff);
	if (csr==0xff || csr==0xffff) return 1;
	
	//v259_SoftReset(myvme, V259_BASE);
	v259_Status(myvme, V259_BASE);

printf(" base addr = 0x%.08x \n",V259_BASE);	
	//v259_SoftClear(myvme, V259_BASE);
	//
//	for (int j=0;j<MAXEVENT;j++) {
//		usleep(10000);
//		printf("Pattern: %d\n",v259_ReadPatternReset(myvme, V259_BASE));
//	}
	
	status = mvme_close(myvme);
	return 0;
}	

/* emacs
 * Local Variables:
 * mode:C++
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
