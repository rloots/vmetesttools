/*
 * =====================================================================================
 *
 *       Filename:  vconvme.c
 *
 *    Description:  
 *
 *        Version:  1.1-rc
 *        Created:  20/02/2009 12:20:20
 *       
 *       Revision:  1.) Removing test functions.
 *       		Cleaning up code.
 *
 *       Compiler:  gcc (GCC) 4.1.2 20071124 (Red Hat 4.1.2-42)
 *         Linker:  GNU ld version 2.17.50.0.6-6.el5 20061020
 *	   System:  Scientific Linux 5.2, kernel-2.6.18-92.1.6.el5
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 * =====================================================================================
 */

/* #####   HEADER FILE INCLUDES   ################################################### */
#include	<string.h>
#include	<stdlib.h>
#include	<stdio.h>
#include	<signal.h>
#include	<assert.h>
#include	<unistd.h>
#include	<asm/page.h>
#include	<byteswap.h>

/* #####   HEADER FILE INCLUDES   ################################################### */
#include 	"vconvme.h"


/* #####   HEADER FILE INCLUDES   ################################################### */
#include 	<sys/ioctl.h>
#include	<sys/time.h>
#include 	<errno.h>

/* #####   MACROS  -  LOCAL TO THIS SOURCE FILE   ################################### */


/* #####   DATA TYPES  -  LOCAL TO THIS SOURCE FILE   ############################### */

volatile int devHandle_D16_A32;
volatile int devHandle_D32_A32;
volatile int devHandle_DMA_A32;
volatile int devHandle_ctrl;
volatile int globalErrorCounter;
/*-----------------------------------------------------------------------------
 *  Implementation
 *-----------------------------------------------------------------------------*/
/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  initDMABuffer
 *  Description :  
 *  Parameters  :  
 *  Returns     :  
 * =====================================================================================
 */
int initDMABufferA32( )
{

	//int devHandle;;
        int  result;
	//VME_TABLE2 *table;
	
	/* Create D32 DMA device */
	devHandle_DMA_A32 = vme_openDevice("dma0");
	if( devHandle_DMA_A32 > 0 ){
		UINT32 res = 4*PAGE_SIZE;			
		result = vme_allocDmaBuffer( devHandle_DMA_A32, &res );
		if( result < 0 ){
			perror("[initDMABuffer] Could not allocDmaBuffer \n");
			return(MVME_ERROR);
		}
					
	}/**DMA*/
	else{
		perror("could not open dma device\n");
		return(MVME_ERROR);
	}

	printf("DMA devHandle %d \n",devHandle_DMA_A32 );
	return(MVME_SUCCESS);
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  initpciD16
 *  Description :  
 *  Parameters  :  
 *  Returns     :  
 * =====================================================================================
 */
int initpciD16A32()
{
	int devHandle, result;
	EN_PCI_IMAGE_DATA idata;
		
		idata.pciAddress=0;
		idata.pciAddressUpper=0;
		idata.vmeAddress=0;
		idata.vmeAddressUpper=0;
		idata.size = 0x100000;
		idata.sizeUpper = 0;
		idata.readPrefetch=0;
		idata.prefetchSize= 0;
		idata.sstMode = 0;
		idata.dataWidth = EN_VME_D16;
		idata.addrSpace = EN_VME_A32;
		idata.type = EN_LSI_DATA;
		idata.mode = EN_LSI_USER;
		idata.vmeCycle = 0;
		idata.sstbSel =0;
		idata.ioremap = 1;

	devHandle = vme_openDevice("lsi0");
	if( devHandle < 0 ){
		perror("[initpciD16] Could not create device for d16\n");
		return(MVME_ERROR);
	}else{
		result = vme_enablePciImage( devHandle, &idata );
		if( result < 0 ){
			perror("[initpciD16] Could not create PCI Image {2} for d16\n");
			return(MVME_ERROR);
		}
	}/**D16*/

	devHandle_D16_A32 = devHandle;
	return(MVME_SUCCESS);
}
/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  initpciD32
 *  Description :  
 *  Parameters  :  
 *  Returns     :  
 * =====================================================================================
 */
int initpciD32A32( )
{
	int devHandle, result;
		EN_PCI_IMAGE_DATA idata;
	
		
		idata.pciAddress=0;
		idata.pciAddressUpper=0;
		idata.vmeAddress=0;
		idata.vmeAddressUpper=0;
		idata.size = 0x100000;
		idata.sizeUpper = 0;
		idata.readPrefetch=1;
		idata.prefetchSize= 3;
		idata.sstMode = 0;
		idata.dataWidth = EN_VME_D32;
		idata.addrSpace = EN_VME_A32;
		idata.type = EN_LSI_DATA;
		idata.mode = EN_LSI_USER;
		idata.vmeCycle = 0;
		idata.sstbSel =0;
		idata.ioremap = 1;

	
	devHandle = vme_openDevice("lsi1");
	if( devHandle < 0 ){
		perror("[initpciD32] Could not create device for d16\n");
		return(MVME_ERROR);
	}else {
		result = vme_enablePciImage( devHandle, &idata );
		if( result < 0 ){
			perror("[initpciD32] Could not create PCI Image {3} for d16\n");
			return(MVME_ERROR);
		}
	}/**D32*/
	
	devHandle_D32_A32 = devHandle;	
	return(MVME_SUCCESS);
}


int getControlDevice()
{
	return devHandle_ctrl;
}

/*-----------------------------------------------------------------------------
 *  MIDAS API FOR VME
 *-----------------------------------------------------------------------------*/
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_open
 *  Description:  Open VME channel. One bus handle per crate.
 *  Parameters :  @param *mvme	// VME structure
 *  		  @param index	// VME interface index
 *  		  @return  MVME_SUCCESS,ERROR
 * =====================================================================================
 */
int mvme_open (MVME_INTERFACE **mvme, int index) 
{	
	//VME_TABLE2 *table;
	globalErrorCounter = 0;


	if( index != 0 ){
		perror("[mvme_open]: index != 0 ");
		(*mvme)->handle = 0;
		return(MVME_INVALID_PARAM);
	}
	
	/* Allocate MVME_INTERFACE */
	*mvme = (MVME_INTERFACE*)calloc(1,sizeof(MVME_INTERFACE));

	/* Allocate MVME_TABLE for the *mvme interface */
	(*mvme)->table = (char*)malloc(MAX_VME_SLOTS*sizeof(VME_TABLE2));
	
	/* Initialize the table, fill it with 0 with the size = MAX_VME_SLOTS*sizeof(VME_TABLE) */
	memset( (char*)(*mvme)->table, 0, MAX_VME_SLOTS*sizeof(VME_TABLE2) );

	/* Set Default Parameters */
	(*mvme)->am = MVME_AM_A32_ND;

	devHandle_ctrl = vme_openDevice("ctl");
	initpciD16A32();
	initpciD32A32();
	initDMABufferA32();


	return(MVME_SUCCESS);
}/* --------------- end of function mvme_open -------------------*/


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_close
 *  Description:  close and release(free) ALL openend VME channels
 *  Parameters :  @param *mvme	VME_structure
 *  		  @return MVME_SUCCESS,ERROR
 * =====================================================================================
 */
int mvme_close ( MVME_INTERFACE *mvme )
{
	int 		result;
	printf("mvme_close:\n");

	result = vme_disablePciImage(devHandle_D16_A32 );
	if( result < 0 ){
		perror("[mvme_close] Failed to close PCI D16 Image ");
		return(MVME_ACCESS_ERROR);
	}else
		vme_closeDevice(devHandle_D16_A32);
	

	result = vme_disablePciImage(devHandle_D32_A32 );
	if( result < 0 ){
		perror("[mvme_close] Failed to close PCI D32 Image ");
		return(MVME_ACCESS_ERROR);
	}else
		vme_closeDevice(devHandle_D32_A32);



	vme_freeDmaBuffer(devHandle_DMA_A32);
	vme_closeDevice(devHandle_DMA_A32);
	vme_closeDevice(devHandle_ctrl);
	free(mvme->info);			
	mvme->table = NULL;
	free (mvme->table);
	free(mvme);
	return(MVME_SUCCESS);
}/* --------------- end of function mvme_close -------------------*/


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_write
 *  Description:  NOT NEEDED NOW
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  		  @param vme_addr	mvme_addr_t
 *  		  @param *src		void
 *  		  @param n_bytes	mvme_size_t
 *   Returns   :  @return Error/MVME_SUCCESS
 * =====================================================================================
 */
int mvme_write(MVME_INTERFACE *mvme, mvme_addr_t vme_addr, void *src, mvme_size_t n_bytes)
{
	/* NOT NEEDED AT THE MOMENT */
	return MVME_SUCCESS;
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_read
 *  Description:  Read from VME bus.
 *  Parameters :  @param *mvme		VME structure
 *  		  @param *dst		destination pointer
 *  		  @param vme_addr	source address (VME location)
 *  		  @param n_bytes	requested transfer size
 *  Returns    :  @return		MVME_SUCCESS,ERROR
 * =====================================================================================
 */
int mvme_read( MVME_INTERFACE *mvme, void *dst, mvme_addr_t vme_addr, mvme_size_t n_bytes)
{
    EN_VME_DIRECT_TXFER tdata;		/* Direct DMA transfer(READ) */
    memset((char *)&tdata,0,sizeof(EN_VME_DIRECT_TXFER));
    int  result = 0;

  if ((mvme->blt_mode == MVME_BLT_BLT32) ||
      (mvme->blt_mode == MVME_BLT_MBLT64)
      ){

    	if( n_bytes < 128 ){
	  perror("nbytes NOT >= 128 \n ");
	  return(MVME_ERROR);

	}
        	
	if( devHandle_DMA_A32 > 0 ){

            tdata.direction = EN_DMA_READ;
            tdata.offset = 0x0;       			/* start of DMA buffer */
            tdata.size = n_bytes;  			/* read 4KB */
            tdata.vmeAddress = (UINT32)vme_addr;

            tdata.txfer.timeout = 200;  		/* (jiffies)2 second timeout */
	    tdata.txfer.vmeBlkSize = 0;			/* VME Bus block size in bytes 0=32, 1=64, 2=128 , 3=256,
							 * 4=512 , 5=1024 , 6=2048, 7=4096(Applicable only to TSI148)
							 *
							 */

	    tdata.txfer.vmeBackOffTimer = 0;
	    tdata.txfer.pciBlkSize = 0;			/* PCI Bus block size in bytes 0=32, 1=64, 2=128 , 3=256,
							 * 4=512 , 5=1024 , 6=2048, 7=4096(Applicable only to TSI148)
							 *
							 * */	

	    tdata.txfer.pciBackOffTimer = 0;		

	    tdata.access.sstMode = 0;			/* 2eSST Mode 0=SST160, 1=SST267,
							 * 2=SST320(Applicable only to TSI148)
							 * */

	    if( mvme->blt_mode == MVME_BLT_BLT32 )
		    tdata.access.vmeCycle = 1;			// 1=BLT, 2=MBLT
	    else if( mvme->blt_mode == MVME_BLT_MBLT64 ) 
		    tdata.access.vmeCycle = 2;

	    tdata.access.sstbSel = 0;
            tdata.access.dataWidth = EN_VME_D32;
            tdata.access.addrSpace = EN_VME_A32;
            tdata.access.type = EN_LSI_DATA;   		/* data AM code */
            tdata.access.mode = 0;   			/* non-privileged */


	  result = vme_dmaDirectTransfer( devHandle_DMA_A32, &tdata );
	  if( result < 0 ){
		  perror("DMA transfer Failed \n");
		  return(MVME_ERROR);
	  }

	  result = vme_read( devHandle_DMA_A32,0,dst, n_bytes );

	  if( result < 0 ){
		  perror(" count do dma read! \n");
		  return(MVME_ERROR);
	  }
  				
	}/* If proper DMA handle was created */
	else{
		perror("could not open dma device\n");
		return(MVME_ERROR);
	}
  }/* Correct blt mode */
	
  return(MVME_SUCCESS);
  //
}/* --------------- end of function mvme_read -------------------*/


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_write_value
 *  Description:  Writes a single value
 *  Parameters : @param *mvme		MVME_INTERFACE
 *  		 @param vme_addr	mvme_addr_t
 *  		 @param value		DWORD
 *  Return     : @returns	Error,MVME_SUCCESS
 * =====================================================================================
 */
int mvme_write_value(MVME_INTERFACE *mvme, mvme_addr_t vme_addr, DWORD value)
{
	int result;		
	  
	/* Perform write */
	if (mvme->dmode == MVME_DMODE_D16){
	        DWORD v1 = bswap_16(value);
		result = vme_write(devHandle_D16_A32,vme_addr,(UINT8*)&v1,2);
		//printf(" v 0x%08x , uint8 v 0x%08x \n",value,(UINT8)value );		
//		result = vme_write(devHandle_D16_A32,vme_addr,(UINT8*)&value,2);
		if(result < 0){
			perror("[mvme_write_value] error reading vme, D16A32 \n");
			return(MVME_ERROR);
		}
	}else if (mvme->dmode == MVME_DMODE_D32){
		DWORD v1 = bswap_32(value);
		result = vme_write(devHandle_D32_A32,vme_addr,(UINT8*)&v1,4);
//		result = vme_write(devHandle_D32_A32,vme_addr,(UINT8*)&value,4);
		if(result < 0){
			perror("[mvme_write_value] error reading vme, D32A32 \n");
			return(MVME_ERROR);
		}
	}

	return MVME_SUCCESS;
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_read_value
 *  Description:  Reads a single value D16 or D32
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  		  @param vme_addr	mvme_addr_t
 *  Returns    :  @returns dst		DWORD
 * =====================================================================================
 */

DWORD mvme_read_value(MVME_INTERFACE *mvme, mvme_addr_t vme_addr)
{
   DWORD dst = 0xffffffff;
   DWORD tdst;
   int result; 
   /* Perform read */
   if (mvme->dmode == MVME_DMODE_D16){
	result = vme_read(devHandle_D16_A32,vme_addr,(UINT8*)&tdst,2);
	if(result < 0){
		perror("[mvme_read_value] error reading vme, D16A32 \n");
		return(MVME_ERROR);
	}
	dst = bswap_16((DWORD)tdst);

   }
   else if (mvme->dmode == MVME_DMODE_D32){
	result = vme_read(devHandle_D32_A32,vme_addr,(UINT8*)&tdst,4);
	if(result < 0){
		perror("[mvme_read_value] error reading vme, D32A32 \n");
		return(MVME_ERROR);
	}
	dst = bswap_32(tdst);
	//dst = tdst;
   }
  
   return dst;
}
DWORD mvme_read_value_nobs(MVME_INTERFACE *mvme, mvme_addr_t vme_addr)
{
   DWORD dst = 0xffffffff;
   DWORD tdst;
   int result; 
   /* Perform read */
   if (mvme->dmode == MVME_DMODE_D16){
	result = vme_read(devHandle_D16_A32,vme_addr,(UINT8*)&tdst,2);
	if(result < 0){
		perror("[mvme_read_value_obs] error reading vme, D16A32 \n");
		return(MVME_ERROR);
	}
	dst = tdst;

   }
   else if (mvme->dmode == MVME_DMODE_D32){
	result = vme_read(devHandle_D32_A32,vme_addr,(UINT8*)&tdst,4);
	if(result < 0){
		perror("[mvme_read_value_obs] error reading vme, D32A32 \n");
		return(MVME_ERROR);
	}
	dst = tdst;
   }
  
   return dst;
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_set_am
 *  Description:  Sets Address Modifier
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  		  @param am		int
 *  Returns    :  @returns	MVME_SUCCESS
 * =====================================================================================
 */
int mvme_set_am( MVME_INTERFACE *mvme, int am )
{
	mvme->am = am;
	return MVME_SUCCESS;
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_get_am
 *  Description:  Gets Address Modifier
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  		  @param am		int
 *  Returns    :  @returns 	MVME_SUCCESS
 * =====================================================================================
 */
int EXPRT mvme_get_am( MVME_INTERFACE *mvme, int *am )
{
	*am = mvme->am;
	return MVME_SUCCESS;
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_set_dmode
 *  Description:  Sets dmode
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  		  @param dmode		int
 *  Returns    :  @returns 	MVME_SUCCESS
 * =====================================================================================
 */
int mvme_set_dmode( MVME_INTERFACE *mvme, int dmode )
{
	mvme->dmode = dmode;
	return MVME_SUCCESS;
}
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_get_dmode
 *  Description:  gets dmode
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  		  @param dmode		int
 *  Returns    :  @returns 	MVME_SUCCESS
 * =====================================================================================
 */
int mvme_get_dmode( MVME_INTERFACE *mvme, int *dmode )
{
	*dmode = mvme->dmode;
	return MVME_SUCCESS;
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_sysreset
 *  Description:  system reset / software reset
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  Returns    :  @returns 	MVME_SUCCESS
 * =====================================================================================
 */
int mvme_sysreset( MVME_INTERFACE *mvme )
{
	return MVME_SUCCESS;
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_set_blt
 *  Description:  
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  		  @param mode		int
 *  Returns    :  @returns 	MVME_SUCCESS
 * =====================================================================================
 */
int mvme_set_blt( MVME_INTERFACE *mvme, int mode )
{
	mvme->blt_mode = mode;
	return MVME_SUCCESS;
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_get_blt
 *  Description:  
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  		  @param mode		int
 *  Returns    :  @returns 	MVME_SUCCESS
 * =====================================================================================
 */
int mvme_get_blt( MVME_INTERFACE *mvme, int *mode )
{
	*mode = mvme->blt_mode;
	return MVME_SUCCESS;
}

int mvme_disableInterrupt( unsigned int level )
{
	int result = vme_disableInterrupt(devHandle_ctrl,level );
	return result;
}

int mvme_enableInterrupt( unsigned int level )
{
	int result = vme_enableInterrupt(devHandle_ctrl,level );
	return result;
}


/* DEBUGGING */
void mvme_getdriverStats()
{
	int result;
	int i = 0;

	EN_VME_DRIVER_STAT sData;
	result = vme_getStats(devHandle_ctrl, VME_STATUS_CTRL ,&sData);			

		if( result == 0 )
		{
			printf("\nControl Device Stats:");
                        printf("\n--------------------");
                        printf("\nDriver Version:%s",sData.ctlStats.version);
                        printf("\nBoard Name:%s",sData.ctlStats.brdName);
                        printf("\nBridge Device Id:%x",sData.ctlStats.devId);
                        printf("\nRegister base address:%x",sData.ctlStats.regBase);
		}

		result =  vme_getStats(devHandle_ctrl, VME_STATUS_LSI0 ,&sData);
		
		if( result == 0 )
		{
                        printf("\n\nPCI Image 0 Stats:");
                        printf("\n------------------");
                        printf("\nDevice ID:%x",sData.imageStats.devId);

                        printf("\nTotal Reads :%d",sData.imageStats.readCount);
                        printf("\nTotal Writes:%d",sData.imageStats.writeCount);
                        printf("\nTotal Errors:%d",sData.imageStats.errorCount);

                        printf("\nDevice Register 1: %x",sData.imageStats.devReg1);
                        printf("\nDevice Register 2: %x",sData.imageStats.devReg2);
                        printf("\nDevice Register 3: %x",sData.imageStats.devReg3);
                        printf("\nDevice Register 4: %x",sData.imageStats.devReg4);
                        printf("\nDevice Register 5: %x",sData.imageStats.devReg5);
                        printf("\nDevice Register 6: %x",sData.imageStats.devReg6);
                        printf("\nDevice Register 7: %x",sData.imageStats.devReg7);
                        printf("\nDevice Register 8: %x",sData.imageStats.devReg8);
		}

		result =  vme_getStats(devHandle_ctrl, VME_STATUS_DMA0 ,&sData);

		if( result == 0 )
		{
			 printf("\n\nDMA0 Device Stats:");
                         printf("\n-----------------");

                         printf("\nDMA Reads:%d",sData.dmaStats.readCount);
                         printf("\nDMA Writes:%d",sData.dmaStats.writeCount);
                         printf("\nDMA Errors:%d",sData.dmaStats.errorCount);
                         printf("\nDMA Transfers:%d",sData.dmaStats.txferCount);
                         printf("\nDMA Transfer Timeouts:%d",sData.dmaStats.timeoutCount);
                         printf("\nDMA Transfer Errors:%d",sData.dmaStats.txferErrors);
                         printf("\nDMA Command Pkt Count:%d",sData.dmaStats.cmdPktCount);
                         printf("\nDMA Command Pkt Bytes:%d",sData.dmaStats.cmdPktBytes);
                         printf("\nDevice Register 1: %x",sData.dmaStats.devReg1);
                         printf("\nDevice Register 2: %x",sData.dmaStats.devReg2);
                         printf("\nDevice Register 3: %x",sData.dmaStats.devReg3);
                         printf("\nDevice Register 4: %x",sData.dmaStats.devReg4);
                         printf("\nDevice Register 5: %x",sData.dmaStats.devReg5);
                         printf("\nDevice Register 6: %x",sData.dmaStats.devReg6);
                         printf("\nDevice Register 7: %x",sData.dmaStats.devReg7);
                         printf("\nDevice Register 8: %x",sData.dmaStats.devReg8);
                         printf("\nDevice Register 9: %x",sData.dmaStats.devReg9);
                         printf("\nDevice Register 10: %x",sData.dmaStats.devReg10);
                         printf("\nDevice Register 11: %x",sData.dmaStats.devReg11);
		}
	
		result = vme_getStats(devHandle_ctrl, VME_STATUS_INTS, &sData );

		if( result == 0 )
                {
                         printf("\n\nInterrupt Stats:");
                         printf("\n----------------");

                         for( i=0; i < 24; i++ )
                         {
                                printf("\nInterrupt %d Count: %d",i,sData.intStats.intCounter[i]);
			 }	

                         printf("\n\nTotal Interrupts:%d",sData.intStats.totalIntCount);
                         printf("\nOther Interrupts:%d\n",sData.intStats.otherIntCount);
		}
}

