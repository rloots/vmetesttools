/*
 * =====================================================================================
 *
 *       Filename:  vconvme.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  20/02/2009 12:20:41
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 * =====================================================================================
 */

#ifndef  VCONVME_H_INC
#define  VCONVME_H_INC


/* #####   HEADER FILE INCLUDES   ################################################### */
#include	<vme_api_en.h>	/** Concurrent Enhanced API */
#include	<mvmestd.h>

/* #####   EXPORTED DEFINE STATEMENTS  ################################################# */

#ifndef MIDAS_TYPE_DEFINED
typedef uint32_t 	DWORD;
typedef uint16_t 	WORD;
typedef char	   	BYTE;
#endif

extern int timedt;
#ifndef SUCCESS
#define SUCCESS	 		(int) 1
#endif

#define ERROR			(int) -1000
#define MVME_ERROR		(int) -1000
#define MAX_VME_SLOTS		(int) 21
#define DEFAULT_SRC_ADD 	0x000000
#define DEFAULT_NBYTES		0xFFFFFF	/* 16MB */
#define DEFAULT_DMA_NBYTES 	0x10000		/* max DMA size in bytes */


#define DEBUG		0


/* FUNCTION PROTOTYPES */
int getControlDevice();
int mvme_disableInterrupt( unsigned int level );
int mvme_enableInterrupt( unsigned int level );
void mvme_getdriverStats();
DWORD mvme_read_valueD16A24( MVME_INTERFACE *mvme, mvme_addr_t vme_addr );


/* #####   EXPORTED DATA TYPES   #################################################### */
typedef struct {
	UINT32 	devHandle;
	mvme_size_t	nbytes;
	mvme_addr_t	addr;
	int	am;
	int	valid;
	void	*ptr;
}VME_TABLE2;


typedef struct {
	UINT32 dma_handle;
	EN_VME_DIRECT_TXFER tdata;	/* Direct VME DMA Transfer */
	void *dma_ptr;
}DMA_INFO;

#endif   /* ----- #ifndef VCONVME_H_INC  ----- */

