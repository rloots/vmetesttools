#include <cstdio>
#include <cerrno>
#include <unistd.h>
#include "daqsrt.hh"

int Sched::Pmin,Sched::Pmax;
struct sched_param Sched::Par;

int Sched::Init(void)  {
#ifdef __linux__
    // check wether we have permission to change scheduling
    // and to lock in memory.
    // complain if we don't.
    int res=0;
    // get the priority range for the specified scheduling policy
    Pmin=sched_get_priority_min(Policy);
    Pmax=sched_get_priority_max(Policy);
    printf("Sched: Fast Priority range: %i->%i\n", Pmin,Pmax);
    // try to set realtime scheduling
    if( FAST() ) {
		res=true;
		perror("Sched::FAST");
    }
    // check it
    printf ("Sched: scheduler: %i\n",sched_getscheduler(0));
    // go back to normal scheduling
    if ( NORM() ) {
		res=true;
		perror("Sched::NORM");
    }
    // set (not so) nice level
    if ( nice(-20)!=-20 ) {
		res=true;
		perror("Sched::NICE");
    }
    printf ("Sched: scheduler: %i\n",sched_getscheduler(0));

    /*
	 * keep all memory locked into physical mem, 
	 * to guarantee realtime-behaviour
	 */
    if(mlockall(MCL_CURRENT|MCL_FUTURE)){
		res=true;
		perror("Sched::MLOCK");
    }
    if (res) {
		// complain bitterly; insult the user for not being root.
		printf("WARNING: Running this program as non-superuser means:\n"
			   " - No privileged scheduling\n"
			   "   (it will be slow)\n"
			   " - No memory locking \n"
			   "   (it will be swapped out of RAM)\n"
			   "So you really should be root...\07\n");
		//annoy()
		sleep(1);
    }
    return res;
#else
	return false;
#endif
}

#ifdef UNIT_TEST
int main (void) {
	Sched::Init();
	Sched::FAST();
	Sched::NORM();
}
#endif
