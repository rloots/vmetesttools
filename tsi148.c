/*
===============================================================================
                            COPYRIGHT NOTICE

    Copyright (c) 2007, GE Fanuc Embedded Systems, Inc.
    All Rights Reserved.

-------------------------------------------------------------------------------

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

   o Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer.
   o Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   o Neither the name of GE Fanuc nor the names of its contributors may be used
     to endorse or promote products derived from this software without specific
     prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------

    $Workfile: tsi148.c $
    $Revision: 34 $
    $Modtime: 2/26/07 3:06p $

Modified to support Linux 2.6 x86 platforms
===============================================================================
*/

//------------------------------------------------------------------------------  
//title: Tundra TSI148 (Tempe) PCI-VME Kernel Driver
//version: Linux 3.1
//date: February 2004
//designer: Tom Armistead
//programmer: Tom Armistead
//platform: Linux 2.4.x
//------------------------------------------------------------------------------  
//  Purpose: Provide interface to the Tempe chip.
//  Docs:                                  
//------------------------------------------------------------------------------  
//-----------------------------------------------------------------------------
#include <linux/version.h>
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
#define MODULE
#endif
//#include <linux/config.h>
#ifdef CONFIG_SMP
#define __SMP__
#endif

#ifdef CONFIG_MODVERSIONS
  #define MODVERSIONS

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
#include <linux/modversions.h>
#endif

#endif

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,5,0)
#include <linux/interrupt.h>
#endif

#include <linux/module.h>
#include <linux/mm.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/proc_fs.h>
#include <linux/pci.h>
#include <linux/poll.h>
#include <linux/delay.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/msr.h>
#include "vmedrv.h"
#include "tsi148.h"

//----------------------------------------------------------------------------
// Shoehorn 64-bitness into silly data structures used by this driver
//----------------------------------------------------------------------------

static inline u32 ptr_low(const void* p)  { return ((u64)p)&0xFFFFFFFF; }
static inline u32 ptr_high(const void* p) { return ((u64)p)>>32; }

static inline u32 u64_low(u64 p)  { return (p)&0xFFFFFFFF; }
static inline u32 u64_high(u64 p) { return (p)>>32; }

//----------------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------------
extern struct vmeSharedData *vmechip_interboard_data;
extern const int vmechip_revision;
extern const int vmechip_devid;
extern const int vmechip_irq;
extern int vmechipIrqOverHeadTicks;
extern char *vmechip_baseaddr;
extern const int vme_slotnum;
extern int vme_syscon;
extern char* out_image_va[];
extern unsigned int vme_irqlog[8][0x100];
extern struct pci_dev *vme_pci_dev;
//----------------------------------------------------------------------------
// Types
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Vars and Defines
//----------------------------------------------------------------------------
extern wait_queue_head_t dma_queue[];
extern wait_queue_head_t lm_queue;
extern wait_queue_head_t mbox_queue;

extern	unsigned int vmeGetTime(void);
extern	void	vmeSyncData(void);
extern	void	vmeFlushLine( char * );
extern	int	tb_speed;

unsigned int	tempeIrqTime;
unsigned int	tempeDmaIrqTime[2];
unsigned int	tempeLmEvent;

static spinlock_t tempe_lock;

void ts(const char* text)
{
#if 0
  static int t0 = 0;
  static int t1 = 0;
  int t;
  rdtscl(t);
  if (text == NULL)
    t0 = t;
  printk("tsi148::ts ---> %d ticks, rel %9d, %5d usec, %5d usec --> %s\n", t, t-t0, (t-t0)/2000, (t-t1)/2000, text);
  t1 = t;
#endif
}

//----------------------------------------------------------------------------
//  add64hi - calculate upper 32 bits of 64 bit addition operation.
//----------------------------------------------------------------------------
unsigned int
add64hi(
unsigned int lo0,
unsigned int hi0,
unsigned int lo1,
unsigned int hi1
)
{
   if((lo1 + lo0) < lo1){
      return(hi0 + hi1 + 1);
   }
   return(hi0 + hi1);
}

//----------------------------------------------------------------------------
//  sub64hi - calculate upper 32 bits of 64 bit subtraction operation
//----------------------------------------------------------------------------
int
sub64hi(
unsigned int lo0,
unsigned int hi0,
unsigned int lo1,
unsigned int hi1
)
{
   if(lo0 < lo1){
      return(hi0 - hi1 - 1);
   }
   return(hi0 - hi1);
}


//----------------------------------------------------------------------------
//  tempe_procinfo()
//----------------------------------------------------------------------------
int tempe_procinfo(char *buf, char **start, off_t fpos, int lenght, int *eof, void *data)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  char *p;
  unsigned int x;

  p = buf;

  // Display outbound decoders
  for (x=0;x<8;x+=1) {
    p += sprintf(p,"O%d: %08X %08X:%08X %08X:%08X: %08X:%08X\n",
                 x,
                 longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].otat)),
                 longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].otsau)),
                 longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].otsal)),
                 longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].oteau)),
                 longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].oteal)),
                 longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].otofu)),
                 longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].otofl))
                 );
  }  

  p += sprintf(p,"\n");  

  *eof = 1;
  return p - buf;
}

//-----------------------------------------------------------------------------
// Function   : tempeSetupAttribute
// Description: helper function for calculating attribute register contents.
//-----------------------------------------------------------------------------
static
int
tempeSetupAttribute(
addressMode_t addrSpace, 
int userAccessType, 
int dataAccessType,
dataWidth_t maxDataWidth,
int xferProtocol,
vme2esstRate_t xferRate2esst
)
{
  int tempCtl = 0;

  // Validate & initialize address space field.
  switch(addrSpace){
    case VME_A16:
      tempCtl |= 0x0;
      break;
    case VME_A24:
      tempCtl |= 0x1;
      break;
    case VME_A32:
      tempCtl |= 0x2;
      break;
    case VME_A64:
      tempCtl |= 0x4;
      break;
    case VME_CRCSR:
      tempCtl |= 0x5;
      break;
    case VME_USER1:
      tempCtl |= 0x8;
      break;
    case VME_USER2:
      tempCtl |= 0x9;
      break;
    case VME_USER3:
      tempCtl |= 0xA;
      break;
    case VME_USER4:
      tempCtl |= 0xB;
      break;
    default:
      return(-EINVAL);
  }

  // Setup CTL register.
  if(userAccessType & VME_SUPER)
    tempCtl |= 0x0020;
  if(dataAccessType & VME_PROG)
    tempCtl |= 0x0010;
  if(maxDataWidth == VME_D16)
    tempCtl |= 0x0000;
  if(maxDataWidth == VME_D32)
    tempCtl |= 0x0040;
  switch(xferProtocol){
    case VME_SCT:
      tempCtl |= 0x000;
      break;
    case VME_BLT:
      tempCtl |= 0x100;
      break;
    case VME_MBLT:
      tempCtl |= 0x200;
      break;
    case VME_2eVME:
      tempCtl |= 0x300;
      break;
    case VME_2eSST:
      tempCtl |= 0x400;
      break;
    case VME_2eSSTB:
      tempCtl |= 0x500;
      break;
  }
  switch(xferRate2esst){
    case VME_SSTNONE:
    case VME_SST160:
      tempCtl |= 0x0000;
      break;
    case VME_SST267:
      tempCtl |= 0x800;
      break;
    case VME_SST320:
      tempCtl |= 0x1000;
      break;
  }

  return(tempCtl);
}

//----------------------------------------------------------------------------
//  tempeBusErrorChk()
//  Return zero if no VME bus error has occured, 1 otherwise.
//  Optionally, clear bus error status.
//----------------------------------------------------------------------------
int
tempeBusErrorChk(int clrflag)
{
  unsigned long irqflags;

  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  int tmp;

  spin_lock_irqsave(&tempe_lock, irqflags);

    tmp = longswap(ioread32(&pTempe->lcsr.veat));
    if (tmp & 0x80000000) {  // VES is Set
      if(clrflag) {
        iowrite32(longswap(0x20000000),&pTempe->lcsr.veat);
      }

      spin_unlock_irqrestore(&tempe_lock, irqflags);
      return(1);
    }

  spin_unlock_irqrestore(&tempe_lock, irqflags);
  return(0);
}


//-----------------------------------------------------------------------------
// Function   : DMA_tempe_irqhandler
// Description: Saves DMA completion timestamp and then wakes up DMA queue
//-----------------------------------------------------------------------------
void DMA_tempe_irqhandler(int channelmask)
{

  if(channelmask & 1){
      tempeDmaIrqTime[0] = tempeIrqTime;
      wake_up(&dma_queue[0]);
  }
  if(channelmask & 2){
      tempeDmaIrqTime[1] = tempeIrqTime;
      wake_up(&dma_queue[1]);
  }
}

//-----------------------------------------------------------------------------
// Function   : LERR_tempe_irqhandler
// Description: Display error & status message when LERR (PCI) exception
//    interrupt occurs.
//-----------------------------------------------------------------------------
void LERR_tempe_irqhandler(void)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  printk("LERR_tempe_irqhandler: VME PCI Exception at lcsr.edpa address: 0x%08x%08x, attributes: 0x%08x\n",
         longswap(ioread32(&pTempe->lcsr.edpau)),
         longswap(ioread32(&pTempe->lcsr.edpal)),
         longswap(ioread32(&pTempe->lcsr.edpat)));
  printk("LERR_tempe_irqhandler: PCI-X attribute reg: 0x%08x, PCI-X split completion reg: 0x%08x\n",
         longswap(ioread32(&pTempe->lcsr.edpxa)),
         longswap(ioread32(&pTempe->lcsr.edpxs)));
  iowrite32(longswap(0x20000000),&pTempe->lcsr.edpat);
  vmeSyncData();
}

//-----------------------------------------------------------------------------
// Function   : VERR_tempe_irqhandler
// Description:  Display error & status when VME error interrupt occurs.
// Not normally enabled or used.  tempeBusErrorChk() is used instead.
//-----------------------------------------------------------------------------
void VERR_tempe_irqhandler(void)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  printk("VERR_tempe_irqhandler: VME Exception at address: 0x%08x:%08x, attributes: %08x\n",
     longswap(ioread32(&pTempe->lcsr.veau)), longswap(ioread32(&pTempe->lcsr.veal)), 
     longswap(ioread32(&pTempe->lcsr.veat)));
  iowrite32(longswap(0x20000000),&pTempe->lcsr.veat);
  vmeSyncData();
}

//-----------------------------------------------------------------------------
// Function   : MB_tempe_irqhandler
// Description: Wake up mail box queue.
//-----------------------------------------------------------------------------
void MB_tempe_irqhandler(long mboxMask)
{
  if(vmechipIrqOverHeadTicks != 0){
     wake_up(&mbox_queue);
  }
}

//-----------------------------------------------------------------------------
// Function   : LM_tempe_irqhandler
// Description: Wake up location monitor queue
//-----------------------------------------------------------------------------
void LM_tempe_irqhandler(long lmMask)
{
  tempeLmEvent = lmMask;
  wake_up(&lm_queue);
}

//-----------------------------------------------------------------------------
// Function   : VIRQ_tempe_irqhandler
// Description: Record the level & vector from the VME bus interrupt.
//-----------------------------------------------------------------------------
void VIRQ_tempe_irqhandler(long virqMask)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  unsigned char iackvec;
  int i;

  for(i = 7; i > 0; i--){
    if(virqMask & (1 << i)){
       iackvec = ioread8(&pTempe->lcsr.viack[(i*4)+3]);
       vme_irqlog[i][iackvec]++;

       if (vme_irqlog[i][iackvec] > 100) {
          printk("tsi148::VIRQ_tempe_irqhandler: VME IRQ%d vector 0x%02x count reached %d, looks like stuck spurious interrupt, disabling it\n", i, iackvec, vme_irqlog[i][iackvec]);

          int inten = longswap(ioread32(&pTempe->lcsr.inten));
          int inteo = longswap(ioread32(&pTempe->lcsr.inteo));

          inten &= ~(1<<i);
          inteo &= ~(1<<i);

          iowrite32(longswap(inten),&pTempe->lcsr.inten);
          iowrite32(longswap(inteo),&pTempe->lcsr.inteo);
          vmeSyncData();

          printk("tsi148: Interrupt enable register     INTEN: 0x%08x\n", longswap(ioread32(&pTempe->lcsr.inten)));
          printk("tsi148: Interrupt enable out register INTEO: 0x%08x\n", longswap(ioread32(&pTempe->lcsr.inteo)));
       }
    }
  }
}

//static int irqcount = 0;

//-----------------------------------------------------------------------------
// Function   : tempe_irqhandler
// Description: Top level interrupt handler.  Clears appropriate interrupt
// status bits and then calls appropriate sub handler(s).
//-----------------------------------------------------------------------------
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
static void tempe_irqhandler(int irq, void *dev_id, struct pt_regs *regs)
#else
static int tempe_irqhandler(int irq, void *dev_id, struct pt_regs *regs)
#endif
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  long stat, enable;
  int handled = 0;
  unsigned long irqflags;

  static int dead = 0;

  if (dead)
     {
        // this is wrong - we could be stealing interrupts from the next driver!
        // but if we return 0 and nobody else wants this interrupt,
        // then Linux will disable the irq, and if it is shared
        // with some other devices, i.e. libata on the V7865, they stop working.
        // K.O. 28-Jul-2009
        return IRQ_RETVAL(1);
     }

  spin_lock_irqsave(&tempe_lock, irqflags);

#ifdef DEBUG
  printk("  tsi148: tempe_irqhandler irq: %02X\n", irq);
#endif

  // Save time that this IRQ occurred at.
#ifdef PPC
  tempeIrqTime = vmeGetTime();
#else
  rdtscl(tempeIrqTime);
#endif

  // Determine which interrupts are unmasked and active.  
  enable = longswap(ioread32(&pTempe->lcsr.inteo));
  stat = longswap(ioread32(&pTempe->lcsr.ints));

  static int once = 0;
  if (once==0)
     printk("tsi148::tempe_irqhandler: linux irq %d, tsi148 interrupts enabled: 0x%08x, active: 0x%08x\n", irq, (int)enable, (int)stat);
  once = 1;

  if (0)
     {
        int temp = longswap(ioread32(&pTempe->pcfs.stat));
        printk("tsi148: tempe_irqhandler: stat register 0x%08x\n", temp);
        temp = longswap(ioread32(&pTempe->pcfs.cmmd));
        printk("tsi148: tempe_irqhandler: cmmd register 0x%08x\n", temp);
        temp = longswap(ioread32(&pTempe->lcsr.inten));
        printk("tsi148: tempe_irqhandler: inten register 0x%08x\n", temp);
        temp = longswap(ioread32(&pTempe->gcsr.ctrl));
        printk("tsi148: tempe_irqhandler: gctrl register 0x%08x\n", temp);
        temp = longswap(ioread32(&pTempe->gcsr.revid));
        printk("tsi148: tempe_irqhandler: revid register 0x%08x\n", temp);
        temp = longswap(ioread32(&pTempe->crcsr.csrbcr));
        printk("tsi148: tempe_irqhandler: csrbcr register 0x%08x\n", temp);
        temp = longswap(ioread32(&pTempe->crcsr.csrbsr));
        printk("tsi148: tempe_irqhandler: csrbsr register 0x%08x\n", temp);
     }

  if (enable == 0xFFFFFFFF || stat == 0xFFFFFFFF)
     {
        printk("tsi148: tempe_irqhandler: something is very wrong, disabling tsi148 interrupts: enabled: 0x%08x, active: 0x%08x\n", (int)enable, (int)stat);
        //disable_irq(irq); // cannot disable interrupt - it is shared with the system disk

        // clear the interrupt, probably has no effect with "enable" and "stat" reading crazy values
        iowrite32(0x00000000,&pTempe->lcsr.inteo);
        iowrite32(0xFFFFFFFF,&pTempe->lcsr.intc);
        vmeSyncData();
        stat = 0;
        enable = 0;

        int temp = longswap(ioread32(&pTempe->pcfs.stat));
        printk("tsi148: tempe_irqhandler: stat register 0x%08x\n", temp);

        if (temp == 0xFFFFFFFF)
           {
              printk("tsi148: tempe_irqhandler: something is very wrong, pcfs.stat register reads 0x%08x, VME bridge probably hung and nothing will work. Good bye!\n", (int)temp);
              dead = 1;
           }
     }

  stat = stat & enable;

  // Clear them.
  iowrite32(longswap(stat),&pTempe->lcsr.intc);
  vmeSyncData();

  // Call subhandlers as appropriate
  if (stat & 0x03000000)	// DMA irqs
    {
#ifdef DEBUG
  printk("  tsi148: DMA_tempe_irqhandler irq: %02X\n", irq);
#endif
      DMA_tempe_irqhandler((stat & 0x03000000) >> 24);
      handled = 1;
    }
  if (stat & 0x02000)		// PCI bus error.
    {
#ifdef DEBUG
  printk("  tsi148: LERR_tempe_irqhandler irq: %02X\n", irq);
#endif
      LERR_tempe_irqhandler();
      handled = 1;
    }
  if (stat & 0x01000)		// VME bus error.
    {
#ifdef DEBUG
  printk("  tsi148: VERR_tempe_irqhandler irq: %02X\n", irq);
#endif
      VERR_tempe_irqhandler();
      handled = 1;
    }
  if (stat & 0xF0000)		// Mail box irqs
    {
#ifdef DEBUG
  printk("  tsi148: MB_tempe_irqhandler irq: %02X\n", irq);
#endif
      MB_tempe_irqhandler((stat & 0xF0000) >> 16);
      handled = 1;
    }
  if (stat & 0xF00000)		// Location monitor irqs.
    {
#ifdef DEBUG
  printk("  tsi148: LM_tempe_irqhandler irq: %02X\n", irq);
#endif
      LM_tempe_irqhandler((stat & 0xF00000) >> 20);
      handled = 1;
    }
  if (stat & 0x0000FE)		// VME bus irqs.
    {
#ifdef DEBUG
  printk("  tsi148: VIRQ_tempe_irqhandler irq: %02X\n", irq);
#endif
      VIRQ_tempe_irqhandler(stat & 0x0000FE);
      handled = 1;
    }

  spin_unlock_irqrestore(&tempe_lock, irqflags);

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,5,0)
  return IRQ_RETVAL(handled);
#endif
}

//-----------------------------------------------------------------------------
// Function   : tempeGenerateIrq
// Description: Generate a VME bus interrupt at the requested level & vector.
// Wait for system controller to ack the interrupt.
//-----------------------------------------------------------------------------
int
tempeGenerateIrq( virqInfo_t *vmeIrq)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  int timeout;
  int looptimeout;
  unsigned int tmp;
  unsigned long irqflags;

#ifdef DEBUG
  printk("tempeGenerateIrq: level %x vec %x\n",vmeIrq->level,vmeIrq->vector);
#endif
  timeout = vmeIrq->waitTime;
  if(timeout == 0){
     timeout++;		// Wait at least 1 tick...
  }
  looptimeout = HZ/20;	// try for 1/20 second

  vmeIrq->timeOutFlag = 0;

  spin_lock_irqsave(&tempe_lock, irqflags);

  // Validate & setup vector register.
  tmp = longswap(ioread32(&pTempe->lcsr.vicr));
  tmp &= ~0xFF;

  iowrite32(longswap(tmp | vmeIrq->vector),&pTempe->lcsr.vicr);
  vmeSyncData();

  // Assert VMEbus IRQ
#ifdef DEBUG
  printk("assert irq vicr val 0x%x\n",(tmp | (vmeIrq->level << 8) |vmeIrq->vector));
#endif
  iowrite32(longswap((tmp | (vmeIrq->level << 8) |vmeIrq->vector)),&pTempe->lcsr.vicr);
#ifdef DEBUG
  printk("vicr after asserted 0x%x\n",longswap(ioread32(&pTempe->lcsr.vicr)));
#endif
  vmeSyncData();

  spin_unlock_irqrestore (&tempe_lock, irqflags);

  // Wait for syscon to do iack
  while( longswap(ioread32(&pTempe->lcsr.vicr)) & 0x800 ){
    set_current_state(TASK_INTERRUPTIBLE);
    schedule_timeout(looptimeout);
    timeout = timeout - looptimeout;
    if(timeout <= 0){
       vmeIrq->timeOutFlag = 1;
       break;
    }
  }

  return(0);
}


//-----------------------------------------------------------------------------
// Function   : tempeSetArbiter
// Description: Set the VME bus arbiter with the requested attributes
//-----------------------------------------------------------------------------
int
tempeSetArbiter( vmeArbiterCfg_t *vmeArb)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  int tempCtl = 0;
  int gto = 0;
  unsigned long irqflags;

  spin_lock_irqsave (&tempe_lock, irqflags);

  tempCtl = longswap(ioread32(&pTempe->lcsr.vctrl));
  tempCtl &= 0xFFEFFF00;

  if(vmeArb->globalTimeoutTimer == 0xFFFFFFFF){
     gto = 8;
  } else if (vmeArb->globalTimeoutTimer > 2048) {
     return(-EINVAL);
  } else if (vmeArb->globalTimeoutTimer == 0) {
     gto = 0;
  } else {
     gto = 1;
     while((16 * (1 << (gto-1))) < vmeArb->globalTimeoutTimer){
        gto += 1;
     }
  } 
  tempCtl |= gto;

  if(vmeArb->arbiterMode != VME_PRIORITY_MODE){
    tempCtl |= 1 << 6;
  }
  
  if(vmeArb->arbiterTimeoutFlag){
    tempCtl |= 1 << 7;
  }

  if(vmeArb->noEarlyReleaseFlag){
    tempCtl |= 1 << 20;
  }
  iowrite32(longswap(tempCtl),&pTempe->lcsr.vctrl);
  vmeSyncData();

  spin_unlock_irqrestore (&tempe_lock, irqflags);

  return(0);
}

//-----------------------------------------------------------------------------
// Function   : tempeGetArbiter
// Description: Return the attributes of the VME bus arbiter.
//-----------------------------------------------------------------------------
int
tempeGetArbiter( vmeArbiterCfg_t *vmeArb)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  int tempCtl = 0;
  int gto = 0;

  tempCtl = longswap(ioread32(&pTempe->lcsr.vctrl));

  gto = tempCtl & 0xF;
  if(gto != 0){
    vmeArb->globalTimeoutTimer = (16 * (1 << (gto-1)));
  }

  if(tempCtl & (1 << 6)){
     vmeArb->arbiterMode = VME_R_ROBIN_MODE;
  } else {
     vmeArb->arbiterMode = VME_PRIORITY_MODE;
  }
  
  if(tempCtl & (1 << 7)){
    vmeArb->arbiterTimeoutFlag = 1;
  }

  if(tempCtl & (1 << 20)){
    vmeArb->noEarlyReleaseFlag = 1;
  }

  return(0);
}

//-----------------------------------------------------------------------------
// Function   : tempeSetRequestor
// Description: Set the VME bus requestor with the requested attributes
//-----------------------------------------------------------------------------
int
tempeSetRequestor( vmeRequesterCfg_t *vmeReq)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  int tempCtl = 0;
  unsigned long irqflags;

  spin_lock_irqsave (&tempe_lock, irqflags);

  tempCtl = longswap(ioread32(&pTempe->lcsr.vmctrl));
  tempCtl &= 0xFFFF0000;

  if(vmeReq->releaseMode == 1){
    tempCtl |=  (1 << 3);
  }

  if(vmeReq->fairMode == 1){
    tempCtl |=  (1 << 2);
  } 

  tempCtl |= (vmeReq->timeonTimeoutTimer & 7) << 8;
  tempCtl |= (vmeReq->timeoffTimeoutTimer & 7) << 12;
  tempCtl |= vmeReq->requestLevel;

  iowrite32(longswap(tempCtl),&pTempe->lcsr.vmctrl);
  vmeSyncData();

  spin_unlock_irqrestore (&tempe_lock, irqflags);
  return(0);
}


//-----------------------------------------------------------------------------
// Function   : tempeGetRequestor
// Description: Return the attributes of the VME bus requestor
//-----------------------------------------------------------------------------
int
tempeGetRequestor( vmeRequesterCfg_t *vmeReq)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  int tempCtl = 0;

  tempCtl = longswap(ioread32(&pTempe->lcsr.vmctrl));

  if(tempCtl & 0x18){
    vmeReq->releaseMode = 1;
  }

  if(tempCtl & (1 << 2)){
    vmeReq->fairMode = 1;
  }

  vmeReq->requestLevel = tempCtl & 3;
  vmeReq->timeonTimeoutTimer  = (tempCtl >> 8) & 7;
  vmeReq->timeoffTimeoutTimer  = (tempCtl >> 12) & 7;

  return(0);
}

//-----------------------------------------------------------------------------
// Function   : tempeSetInBound
// Description: Initialize an inbound window with the requested attributes.
//-----------------------------------------------------------------------------
int
tempeSetInBound( vmeInWindowCfg_t *vmeIn)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  int tempCtl = 0;
  unsigned int i,x, granularity = 0x10000;
  unsigned long irqflags;

  // Verify input data
  if(vmeIn->windowNbr > 7){
    return(-EINVAL);
  }
  i = vmeIn->windowNbr;

  switch(vmeIn->addrSpace){
    case VME_CRCSR:
    case VME_USER1:
    case VME_USER2:
    case VME_USER3:
    case VME_USER4:
      return(-EINVAL);
    case VME_A16:
      granularity = 0x10;
      tempCtl |= 0x00;
      break;
    case VME_A24:
      granularity = 0x1000;
      tempCtl |= 0x10;
      break;
    case VME_A32:
      granularity = 0x10000;
      tempCtl |= 0x20;
      break;
    case VME_A64:
      granularity = 0x10000;
      tempCtl |= 0x40;
      break;
  }

  if((vmeIn->vmeAddrL & (granularity - 1)) ||
     (vmeIn->windowSizeL & (granularity - 1)) ||
     (vmeIn->pciAddrL & (granularity - 1))){
    return(-EINVAL);
  }

  spin_lock_irqsave (&tempe_lock, irqflags);

  // Disable while we are mucking around
  iowrite32(0,&pTempe->lcsr.inboundTranslation[i].itat);
  vmeSyncData();

  iowrite32(longswap(vmeIn->vmeAddrL),&pTempe->lcsr.inboundTranslation[i].itsal);
  iowrite32(longswap(vmeIn->vmeAddrU),&pTempe->lcsr.inboundTranslation[i].itsau);

  iowrite32(longswap((vmeIn->vmeAddrL + vmeIn->windowSizeL - granularity)),
                        &pTempe->lcsr.inboundTranslation[i].iteal);

  iowrite32(longswap(add64hi(
            vmeIn->vmeAddrL, vmeIn->vmeAddrU, 
            vmeIn->windowSizeL-granularity, vmeIn->windowSizeU)),
            &pTempe->lcsr.inboundTranslation[i].iteau); 

  iowrite32(longswap((vmeIn->pciAddrL - vmeIn->vmeAddrL)),&pTempe->lcsr.inboundTranslation[i].itofl);

  iowrite32(longswap(sub64hi(
            vmeIn->pciAddrL, vmeIn->pciAddrU, vmeIn->vmeAddrL, vmeIn->vmeAddrU)),
            &pTempe->lcsr.inboundTranslation[i].itofu);

  // Setup CTL register.
  tempCtl |= (vmeIn->xferProtocol & 0x3E) << 6;
  for(x = 0; x < 4; x++){
    if((64 << x) >= vmeIn->prefetchSize){
       break;
    }
  }
  if(x == 4) x--;
  tempCtl |= (x << 16);

  if(vmeIn->prefetchThreshold)
    tempCtl |= 0x40000;

  switch(vmeIn->xferRate2esst){
    case VME_SSTNONE:
      break;
    case VME_SST160:
      tempCtl |= 0x0400;
      break;
    case VME_SST267:
      tempCtl |= 0x1400;
      break;
    case VME_SST320:
      tempCtl |= 0x2400;
      break;
  }

  if(vmeIn->userAccessType & VME_USER)
    tempCtl |= 0x4;
  if(vmeIn->userAccessType & VME_SUPER)
    tempCtl |= 0x8;
  if(vmeIn->dataAccessType & VME_DATA)
    tempCtl |= 0x1;
  if(vmeIn->dataAccessType & VME_PROG)
    tempCtl |= 0x2;

  // Write ctl reg without enable
  iowrite32(longswap(tempCtl),&pTempe->lcsr.inboundTranslation[i].itat);
  vmeSyncData();

  if(vmeIn->windowEnable)
    tempCtl |= 0x80000000;
 
  iowrite32(longswap(tempCtl),&pTempe->lcsr.inboundTranslation[i].itat);
  vmeSyncData();

  spin_unlock_irqrestore (&tempe_lock, irqflags);

  return(0);
}

//-----------------------------------------------------------------------------
// Function   : tempeGetInBound
// Description: Return the attributes of an inbound window.
//-----------------------------------------------------------------------------
int
tempeGetInBound( vmeInWindowCfg_t *vmeIn)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  int tempCtl = 0;
  unsigned int i, vmeEndU, vmeEndL;
  unsigned long irqflags;

  // Verify input data
  if(vmeIn->windowNbr > 7){
    return(-EINVAL);
  }

  spin_lock_irqsave (&tempe_lock, irqflags);

  i = vmeIn->windowNbr;

  tempCtl = longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itat));

  // Get Control & BUS attributes
  if(tempCtl & 0x80000000)
     vmeIn->windowEnable = 1;
  vmeIn->xferProtocol = ((tempCtl & 0xF8) >> 6) | VME_SCT;
  vmeIn->prefetchSize = 64 << ((tempCtl >> 16) & 3);
  vmeIn->wrPostEnable = 1;
  vmeIn->prefetchEnable = 1;
  if(tempCtl & 0x40000)
     vmeIn->prefetchThreshold = 1;
  if(tempCtl & 0x4)
    vmeIn->userAccessType |= VME_USER;
  if(tempCtl & 0x8)
    vmeIn->userAccessType |= VME_SUPER;
  if(tempCtl & 0x1)
    vmeIn->dataAccessType |= VME_DATA;
  if(tempCtl & 0x2)
    vmeIn->dataAccessType |= VME_PROG;

  switch((tempCtl & 0x70) >> 4) {
    case 0x0:
      vmeIn->addrSpace = VME_A16;
      break;
    case 0x1:
      vmeIn->addrSpace = VME_A24;
      break;
    case 0x2:
      vmeIn->addrSpace = VME_A32;
      break;
    case 0x4:
      vmeIn->addrSpace = VME_A64;
      break;
  }

  switch((tempCtl & 0x7000) >> 12) {
    case 0x0:
      vmeIn->xferRate2esst = VME_SST160;
      break;
    case 0x1:
      vmeIn->xferRate2esst = VME_SST267;
      break;
    case 0x2:
      vmeIn->xferRate2esst = VME_SST320;
      break;
  }

  // Get VME inbound start & end addresses
  vmeIn->vmeAddrL =  longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itsal));
  vmeIn->vmeAddrU =  longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itsau));
  vmeEndL =  longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].iteal));
  vmeEndU =  longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].iteau));

  // Adjust addresses for window granularity
  switch(vmeIn->addrSpace){
      case VME_A16:
          vmeIn->vmeAddrU = 0;
          vmeIn->vmeAddrL &= 0x0000FFF0;
          vmeEndU =  0;
          vmeEndL &=  0x0000FFF0;
          vmeEndL |= 0x10;
      break;
      case VME_A24:
          vmeIn->vmeAddrU = 0;
          vmeIn->vmeAddrL &= 0x00FFF000;
          vmeEndU =  0;
          vmeEndL &=  0x00FFF000;
          vmeEndL |= 0x1000;
      break;
      case VME_A32:
          vmeIn->vmeAddrU = 0;
          vmeIn->vmeAddrL &= 0xFFFF0000;
          vmeEndU =  0;
          vmeEndL &=  0xFFFF0000;
          vmeEndL |= 0x1000;
      default:
          vmeIn->vmeAddrL &= 0xFFFF0000;
          vmeEndL &=  0xFFFF0000;
          vmeEndL |= 0x10000;
      break;
  }

  // Calculate size of window.
  vmeIn->windowSizeL = vmeEndL - vmeIn->vmeAddrL;
  vmeIn->windowSizeU = sub64hi(
                         vmeEndL, vmeEndU,
                         vmeIn->vmeAddrL, vmeIn->vmeAddrU
                       );

  // Calculate coorelating PCI bus address
  vmeIn->pciAddrL = vmeIn->vmeAddrL + longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itofl));
  vmeIn->pciAddrU = add64hi(
                         vmeIn->vmeAddrL, vmeIn->vmeAddrU,
                         longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itofl)),
                         longswap(ioread32(&pTempe->lcsr.inboundTranslation[i].itofu))
                       );

  spin_unlock_irqrestore (&tempe_lock, irqflags);

  return(0);
}

//-----------------------------------------------------------------------------
// Function   : tempeSetOutBound
// Description: Set the attributes of an outbound window.
//-----------------------------------------------------------------------------
int
tempeSetOutBound( vmeOutWindowCfg_t *vmeOut)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  unsigned int tempCtl = 0;
  unsigned int i,x;
  unsigned long irqflags;
  unsigned int ofl, ofu;

  // Verify input data
  if(vmeOut->windowNbr > 7){
    return(-EINVAL);
  }
  i = vmeOut->windowNbr;

  if((vmeOut->xlatedAddrL & 0xFFFF) ||
     (vmeOut->windowSizeL & 0xFFFF) ||
     (vmeOut->pciBusAddrL & 0xFFFF)){
    return(-EINVAL);
  }

  spin_lock_irqsave (&tempe_lock, irqflags);

  tempCtl = tempeSetupAttribute(
                         vmeOut->addrSpace, 
                         vmeOut->userAccessType, 
                         vmeOut->dataAccessType,
                         vmeOut->maxDataWidth,
                         vmeOut->xferProtocol,
                         vmeOut->xferRate2esst
             );

  if(vmeOut->prefetchEnable){
    tempCtl |= 0x40000;
    for(x = 0; x < 4; x++){
      if((2 << x) >= vmeOut->prefetchSize) break;
    }
    if(x == 4) x=3;
    tempCtl |= (x << 16);
  }

  ofl = vmeOut->xlatedAddrL - vmeOut->pciBusAddrL;
  ofu = sub64hi(vmeOut->xlatedAddrL, vmeOut->xlatedAddrU, vmeOut->pciBusAddrL, vmeOut->pciBusAddrU);

  if (0)
    {
      printk("mapping tempCtl 0x%08x, pciBusAddrL 0x%08x, windowSizeL 0x%08x, vmeAddrL 0x%08x, offset 0x%08x\n",
             tempCtl,
             vmeOut->pciBusAddrL,
             vmeOut->windowSizeL,
             vmeOut->xlatedAddrL,
             ofl);
    }

  if (0)
    {
      int x;
      for (x=0; x<8; x++)
        iowrite32(0, &pTempe->lcsr.outboundTranslation[x].otat);
    }

  if (1)
    {
      iowrite32(0, &pTempe->lcsr.outboundTranslation[7].otat);
    }

  // Disable while we are mucking around
  iowrite32(0, &pTempe->lcsr.outboundTranslation[i].otat);
  vmeSyncData();

  iowrite32(0, &pTempe->lcsr.outboundTranslation[i].otbs);
  iowrite32(0, &pTempe->lcsr.outboundTranslation[i].otsal);
  iowrite32(0, &pTempe->lcsr.outboundTranslation[i].otsau);
  iowrite32(0, &pTempe->lcsr.outboundTranslation[i].oteal);
  iowrite32(0, &pTempe->lcsr.outboundTranslation[i].oteau);
  iowrite32(0, &pTempe->lcsr.outboundTranslation[i].otofl);
  iowrite32(0, &pTempe->lcsr.outboundTranslation[i].otofu);
  iowrite32(0, &pTempe->lcsr.outboundTranslation[i].otat);
  vmeSyncData();

  iowrite32(longswap(vmeOut->bcastSelect2esst),&pTempe->lcsr.outboundTranslation[i].otbs);
  iowrite32(longswap(vmeOut->pciBusAddrL),&pTempe->lcsr.outboundTranslation[i].otsal);
  iowrite32(longswap(vmeOut->pciBusAddrU),&pTempe->lcsr.outboundTranslation[i].otsau);
#ifdef PPC
  iowrite32(longswap((vmeOut->pciBusAddrL + (vmeOut->windowSizeL-0x10000))),
       &pTempe->lcsr.outboundTranslation[i].oteal);
  iowrite32(longswap(add64hi(
       vmeOut->pciBusAddrL, vmeOut->pciBusAddrU, 
       vmeOut->windowSizeL-0x10000, vmeOut->windowSizeU)),
       &pTempe->lcsr.outboundTranslation[i].oteau);
#else
  iowrite32(longswap((vmeOut->pciBusAddrL + vmeOut->windowSizeL - 1)), &pTempe->lcsr.outboundTranslation[i].oteal);
  iowrite32(longswap(add64hi(vmeOut->pciBusAddrL, vmeOut->pciBusAddrU, vmeOut->windowSizeL, vmeOut->windowSizeU)),
            &pTempe->lcsr.outboundTranslation[i].oteau);
#endif

  iowrite32(longswap(ofl), &pTempe->lcsr.outboundTranslation[i].otofl);
  iowrite32(longswap(ofu), &pTempe->lcsr.outboundTranslation[i].otofu);

  // Write ctl reg without enable
  iowrite32(longswap(tempCtl),&pTempe->lcsr.outboundTranslation[i].otat);
  vmeSyncData();

  if(vmeOut->windowEnable)
    tempCtl |= 0x80000000;
 
  iowrite32(longswap(tempCtl),&pTempe->lcsr.outboundTranslation[i].otat);

  vmeSyncData();

  if (0)
    {
      int x;
      printk("# pci_start  pci_end    vme_offset vme_addr   config\n");
      for (x=0; x<8; x++)
        printk("%d 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x\n",
               x,
               longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].otsal)),
               longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].oteal)),
               longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].otofl)),
               longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].otsal)) +
               longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].otofl)),
               longswap(ioread32(&pTempe->lcsr.outboundTranslation[x].otat)));
    }

#if 0
  printk("Configuring window #%d: ctl = %08x\n", i,longswap(pTempe->lcsr.outboundTranslation[i].otat));
  printk("otbs %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otbs)));
  printk("otsal %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otsal)));
  printk("otsau %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otsau)));
  printk("oteal %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].oteal)));
  printk("oteau %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].oteau)));
  printk("otofl %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otofl)));
  printk("otofu %08x\n", longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otofu)));
#endif

  spin_unlock_irqrestore (&tempe_lock, irqflags);

  return(0);
}


//-----------------------------------------------------------------------------
// Function   : tempeGetOutBound
// Description: Return the attributes of an outbound window.
//-----------------------------------------------------------------------------
int
tempeGetOutBound( vmeOutWindowCfg_t *vmeOut)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  int tempCtl = 0;
  unsigned int i;
  unsigned long irqflags;

  // Verify input data
  if(vmeOut->windowNbr > 7){
    return(-EINVAL);
  }
  i = vmeOut->windowNbr;

  spin_lock_irqsave (&tempe_lock, irqflags);

  // Get Control & BUS attributes
  tempCtl = longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otat));
  vmeOut->wrPostEnable = 1;
  if(tempCtl & 0x0020)
    vmeOut->userAccessType = VME_SUPER;
  else
    vmeOut->userAccessType = VME_USER;
  if(tempCtl & 0x0010)
    vmeOut->dataAccessType = VME_PROG;
  else 
    vmeOut->dataAccessType = VME_DATA;
  if(tempCtl & 0x80000000)
     vmeOut->windowEnable = 1;

  switch((tempCtl & 0xC0) >> 6){
    case 0:
      vmeOut->maxDataWidth = VME_D16;
      break;
    case 1:
      vmeOut->maxDataWidth = VME_D32;
      break;
  }
  vmeOut->xferProtocol = 1 << ((tempCtl >> 8) & 7);

  switch(tempCtl & 0xF){
    case 0x0:
      vmeOut->addrSpace = VME_A16;
      break;
    case 0x1:
      vmeOut->addrSpace = VME_A24;
      break;
    case 0x2:
      vmeOut->addrSpace = VME_A32;
      break;
    case 0x4:
      vmeOut->addrSpace = VME_A64;
      break;
    case 0x5:
      vmeOut->addrSpace = VME_CRCSR;
      break;
    case 0x8:
      vmeOut->addrSpace = VME_USER1;
      break;
    case 0x9:
      vmeOut->addrSpace = VME_USER2;
      break;
    case 0xA:
      vmeOut->addrSpace = VME_USER3;
      break;
    case 0xB:
      vmeOut->addrSpace = VME_USER4;
      break;
  }

  vmeOut->xferRate2esst = VME_SSTNONE;
  if(vmeOut->xferProtocol == VME_2eSST){

    switch(tempCtl & 0x1800){
      case 0x000:
        vmeOut->xferRate2esst = VME_SST160;
        break;
      case 0x800:
        vmeOut->xferRate2esst = VME_SST267;
        break;
      case 0x1000:
        vmeOut->xferRate2esst = VME_SST320;
        break;
    }

  }

  // Get Window mappings.

  vmeOut->bcastSelect2esst = longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otbs));
  vmeOut->pciBusAddrL = longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otsal));
  vmeOut->pciBusAddrU = longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otsau));

#ifdef PPC
  vmeOut->windowSizeL =
      (longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].oteal)) + 0x10000) - 
                             vmeOut->pciBusAddrL;
  vmeOut->windowSizeU = sub64hi(
       longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].oteal)) + 0x10000,
       longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].oteau)),
       vmeOut->pciBusAddrL, 
       vmeOut->pciBusAddrU); 
#else
  vmeOut->windowSizeL =
      (longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].oteal))) - 
                             vmeOut->pciBusAddrL;
  vmeOut->windowSizeU = sub64hi(
       longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].oteal)),
       longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].oteau)),
       vmeOut->pciBusAddrL, 
       vmeOut->pciBusAddrU); 
#endif

  vmeOut->xlatedAddrL = longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otofl)) + 
       vmeOut->pciBusAddrL;
  vmeOut->xlatedAddrU = add64hi(
         longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otofl)),
         longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otofu)),
         vmeOut->pciBusAddrL, 
         vmeOut->pciBusAddrU); 

  spin_unlock_irqrestore (&tempe_lock, irqflags);

  return(0);
}


//-----------------------------------------------------------------------------
// Function   : tempeSetupLm
// Description: Set the attributes of the location monitor
//-----------------------------------------------------------------------------
int
tempeSetupLm( vmeLmCfg_t *vmeLm)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  int tempCtl = 0;
  unsigned long irqflags;

  // Setup CTL register.
  switch(vmeLm->addrSpace){
    case VME_A16:
      tempCtl |= 0x00;
      break;
    case VME_A24:
      tempCtl |= 0x10;
      break;
    case VME_A32:
      tempCtl |= 0x20;
      break;
    case VME_A64:
      tempCtl |= 0x40;
      break;
    default:
      return(-EINVAL);
  }

  spin_lock_irqsave (&tempe_lock, irqflags);

  if(vmeLm->userAccessType & VME_USER)
    tempCtl |= 0x4;
  if(vmeLm->userAccessType & VME_SUPER)
    tempCtl |= 0x8;
  if(vmeLm->dataAccessType & VME_DATA)
    tempCtl |= 0x1;
  if(vmeLm->dataAccessType & VME_PROG)
    tempCtl |= 0x2;

  // Disable while we are mucking around
  iowrite32(0,&pTempe->lcsr.lmat);
  vmeSyncData();

  iowrite32(longswap(vmeLm->addr),&pTempe->lcsr.lmbal);
  iowrite32(longswap(vmeLm->addrU),&pTempe->lcsr.lmbau);

  tempeLmEvent = 0;
  // Write ctl reg and enable
  iowrite32(longswap(tempCtl | 0x80),&pTempe->lcsr.lmat);
  vmeSyncData();

  spin_unlock_irqrestore (&tempe_lock, irqflags);

  return(0);
}

//-----------------------------------------------------------------------------
// Function   : tempeWaitLm
// Description: Wait for location monitor to be triggered.
//-----------------------------------------------------------------------------
int
tempeWaitLm( vmeLmCfg_t *vmeLm)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  unsigned long irqflags;

  if(tempeLmEvent == 0){
    interruptible_sleep_on_timeout(&lm_queue, vmeLm->lmWait);
  }
  spin_lock_irqsave (&tempe_lock, irqflags);

  iowrite32(0,&pTempe->lcsr.lmat);
  vmeSyncData();
  vmeLm->lmEvents = tempeLmEvent;

  spin_unlock_irqrestore (&tempe_lock, irqflags);
  return(0);
}

//-----------------------------------------------------------------------------
// Function   : tempeDoRmw
// Description: Perform an RMW cycle on the VME bus.
//    A VME outbound window must already be setup which maps to the desired 
//    RMW address.
//-----------------------------------------------------------------------------
int
tempeDoRmw( vmeRmwCfg_t *vmeRmw)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  int tempCtl = 0;
  unsigned int vmeEndU, vmeEndL;
  int *rmwPciDataPtr = NULL;
  int *vaDataPtr = NULL;
  int i;
  vmeOutWindowCfg_t vmeOut;
  unsigned long irqflags;

  if(vmeRmw->maxAttempts < 1){
    return(-EINVAL);
  }


  // Find the PCI address that maps to the desired VME address 
  for(i = 0; i < 7; i++){
    tempCtl = longswap(ioread32(&pTempe->lcsr.outboundTranslation[i].otat));
    if((tempCtl & 0x80000000) == 0){
      continue;
    }
    memset(&vmeOut, 0, sizeof(vmeOut));
    vmeOut.windowNbr = i;
    tempeGetOutBound(&vmeOut);
    if(vmeOut.addrSpace != vmeRmw->addrSpace) {
       continue;
    }
    if(vmeOut.xlatedAddrU > vmeRmw->targetAddrU) {
      continue;
    }
    if(vmeOut.xlatedAddrU == vmeRmw->targetAddrU) {
      if(vmeOut.xlatedAddrL > vmeRmw->targetAddr) {
        continue;
      }
    }
    vmeEndL = vmeOut.xlatedAddrL + vmeOut.windowSizeL;
    vmeEndU = add64hi(vmeOut.xlatedAddrL, vmeOut.xlatedAddrU,
                      vmeOut.windowSizeL, vmeOut.windowSizeU);
    if(sub64hi(vmeEndL, vmeEndU, 
               vmeRmw->targetAddr, vmeRmw->targetAddrU) >= 0){
         rmwPciDataPtr = (int *)(vmeOut.pciBusAddrL+ (vmeRmw->targetAddr - vmeOut.xlatedAddrL));
         vaDataPtr = out_image_va[i] + (vmeRmw->targetAddr - vmeOut.xlatedAddrL);
         break;
    }
  }
     
  // If no window - fail.
  if(rmwPciDataPtr == NULL){
    return(-EINVAL);
  }

  spin_lock_irqsave (&tempe_lock, irqflags);

  // Setup the RMW registers.

  iowrite32((ioread32(&pTempe->lcsr.vmctrl) & longswap(~0x100000)),&pTempe->lcsr.vmctrl); 

  vmeSyncData();
  iowrite32(longswap(vmeRmw->enableMask),&pTempe->lcsr.rmwen);
  iowrite32(longswap(vmeRmw->compareData),&pTempe->lcsr.rmwc);
  iowrite32(longswap(vmeRmw->swapData),&pTempe->lcsr.rmws);
  iowrite32(longswap(ptr_high(rmwPciDataPtr)),&pTempe->lcsr.rmwau);
  iowrite32(longswap(ptr_low(rmwPciDataPtr)),&pTempe->lcsr.rmwal);

  iowrite32((ioread32(&pTempe->lcsr.vmctrl) | longswap(0x100000)),&pTempe->lcsr.vmctrl); 
  vmeSyncData();

  // Run the RMW cycle until either success or max attempts.
  vmeRmw->numAttempts = 1;
  while(vmeRmw->numAttempts <= vmeRmw->maxAttempts){

    if((*vaDataPtr & vmeRmw->enableMask) == 
       (vmeRmw->swapData & vmeRmw->enableMask)){

          break;
 
    }
    vmeRmw->numAttempts++;
  }

  iowrite32((ioread32(&pTempe->lcsr.vmctrl) & longswap(~0x100000)),&pTempe->lcsr.vmctrl); 
  vmeSyncData();

  // If no success, set num Attempts to be greater than max attempts
  if(vmeRmw->numAttempts > vmeRmw->maxAttempts){
    vmeRmw->numAttempts = vmeRmw->maxAttempts +1;
  }

  spin_unlock_irqrestore (&tempe_lock, irqflags);
  return(0);
}


//-----------------------------------------------------------------------------
// Function   : dmaSrcAttr, dmaDstAttr
// Description: Helper functions which setup common portions of the 
//      DMA source and destination attribute registers.
//-----------------------------------------------------------------------------
int
dmaSrcAttr( vmeDmaPacket_t *vmeCur)
{
  
  int dsatreg = 0;
  // calculate source attribute register
  switch(vmeCur->srcBus){
    case VME_DMA_PATTERN_BYTE:
        dsatreg = 0x23000000;
        break;
    case VME_DMA_PATTERN_BYTE_INCREMENT:
        dsatreg = 0x22000000;
        break;
    case VME_DMA_PATTERN_WORD:
        dsatreg = 0x21000000;
        break;
    case VME_DMA_PATTERN_WORD_INCREMENT:
        dsatreg = 0x20000000;
        break;
    case VME_DMA_PCI:
        dsatreg = 0x00000000;
        break;
    case VME_DMA_VME:
        dsatreg = 0x10000000;
        dsatreg |= tempeSetupAttribute(
             vmeCur->srcVmeAttr.addrSpace,
             vmeCur->srcVmeAttr.userAccessType,
             vmeCur->srcVmeAttr.dataAccessType,
             vmeCur->srcVmeAttr.maxDataWidth,
             vmeCur->srcVmeAttr.xferProtocol,
             vmeCur->srcVmeAttr.xferRate2esst);
        break;
    default:
        dsatreg = -EINVAL;
        break;
  }
  return(dsatreg); 
}

int
dmaDstAttr( vmeDmaPacket_t *vmeCur)
{
  int ddatreg = 0;
  // calculate destination attribute register
  switch(vmeCur->dstBus){
    case VME_DMA_PCI:
        ddatreg = 0x00000000;
        break;
    case VME_DMA_VME:
        ddatreg = 0x10000000;
        ddatreg |= tempeSetupAttribute(
             vmeCur->dstVmeAttr.addrSpace,
             vmeCur->dstVmeAttr.userAccessType,
             vmeCur->dstVmeAttr.dataAccessType,
             vmeCur->dstVmeAttr.maxDataWidth,
             vmeCur->dstVmeAttr.xferProtocol,
             vmeCur->dstVmeAttr.xferRate2esst);
        break;
    default:
        ddatreg = -EINVAL;
        break;
  }
  return(ddatreg);
}

//-----------------------------------------------------------------------------
// Function   : tempeStartDma
// Description: Write the DMA controller registers with the contents
//    needed to actually start the DMA operation.
//    returns starting time stamp.
//
//    Starts either direct or chained mode as appropriate (based on dctlreg)
//-----------------------------------------------------------------------------
unsigned int
tempeStartDma(
int channel,
unsigned int dctlreg,
tsi148DmaDescriptor_t *vmeLL
)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  unsigned int val;
  unsigned long irqflags;
    
  spin_lock_irqsave (&tempe_lock, irqflags);

  // Setup registers as needed for direct or chained.
  if(dctlreg & 0x800000){
     if (0)
       printk("tempeStartDma: start single dma at %p\n", vmeLL);

     // Write registers 

     iowrite32(vmeLL->dsal,&pTempe->lcsr.dma[channel].dsal);
     iowrite32(vmeLL->dsau,&pTempe->lcsr.dma[channel].dsau);
     iowrite32(vmeLL->ddal,&pTempe->lcsr.dma[channel].ddal);
     iowrite32(vmeLL->ddau,&pTempe->lcsr.dma[channel].ddau);
     iowrite32(vmeLL->dsat,&pTempe->lcsr.dma[channel].dsat);
     iowrite32(vmeLL->ddat,&pTempe->lcsr.dma[channel].ddat);
     iowrite32(vmeLL->dcnt,&pTempe->lcsr.dma[channel].dcnt);
     iowrite32(vmeLL->ddbs,&pTempe->lcsr.dma[channel].ddbs);

     if (0)
       {
         printk("tempeStartDma: single dma:\n");
         printk("dsau = 0x%08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dsau)));
         printk("dsal = 0x%08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dsal)));
         printk("ddau = 0x%08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].ddau)));
         printk("ddal = 0x%08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].ddal)));
         printk("dsat = 0x%08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dsat)));
         printk("ddat = 0x%08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].ddat)));
         printk("dcnt = 0x%08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dcnt)));
         printk("ddbs = 0x%08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].ddbs)));
       }

  } else {
     if (0)
       printk("tempeStartDma: start chained dma at 0x%08x%08x\n", ptr_high(vmeLL), ptr_low(vmeLL));

     iowrite32(longswap(ptr_high(vmeLL)),&pTempe->lcsr.dma[channel].dnlau);
     iowrite32(longswap(ptr_low(vmeLL)),&pTempe->lcsr.dma[channel].dnlal);
  }
  vmeSyncData();

  // Start the operation
  dctlreg |=0x2000000;

#ifdef DEBUG
  printk("DMA START: dctl val 0x%x\n", dctlreg);
#endif

  iowrite32(longswap(dctlreg),&pTempe->lcsr.dma[channel].dctl);

  vmeSyncData();

#ifdef PPC
  val = vmeGetTime();
#else
  rdtscl(val);
#endif
  vmeSyncData();

  spin_unlock_irqrestore (&tempe_lock, irqflags);

  return(val);
}

//-----------------------------------------------------------------------------
// Function   : tempeSetupDma
// Description: Create a linked list (possibly only 1 element long) of 
//   Tempe DMA descriptors which will perform the DMA operation described
//   by the DMA packet.  Flush descriptors from cache.
//   Return pointer to beginning of list (or 0 on failure.).
//-----------------------------------------------------------------------------

static int dma_max = 0;
static int dma_order = 3;
static tsi148DmaDescriptor_t *dma_pool[2];

tsi148DmaDescriptor_t *
tempeSetupDma(int channel, vmeDmaPacket_t *vmeDma)
{
  int iptr = 0;
  vmeDmaPacket_t *vmeCur = vmeDma;

  //printk("tempeSetupDma: dma to ba 0x%08x:%08x, bytes %d\n", vmeCur->dstAddrU, vmeCur->dstAddr, vmeCur->byteCount);

  if (dma_pool[channel] == NULL)
    {
      int size  = PAGE_SIZE*(1<<dma_order);
      dma_max = size/sizeof(tsi148DmaDescriptor_t);
      dma_pool[channel] = (tsi148DmaDescriptor_t *)__get_dma_pages(GFP_KERNEL, dma_order);
      printk("tempeSetupDma: allocated 2^%d pages for channel %d, pool size %d bytes, %d descriptors\n", dma_order, channel, size, dma_max);
    }

  while(vmeCur != 0){

     tsi148DmaDescriptor_t *currentLL = &dma_pool[channel][iptr];

     currentLL->dsau = longswap(vmeCur->srcAddrU);
     currentLL->dsal = longswap(vmeCur->srcAddr);
     currentLL->ddau = longswap(vmeCur->dstAddrU);
     currentLL->ddal = longswap(vmeCur->dstAddr);
     currentLL->dsat = longswap(dmaSrcAttr(vmeCur));
     currentLL->ddat = longswap(dmaDstAttr(vmeCur));
     currentLL->dcnt = longswap(vmeCur->byteCount);
     currentLL->ddbs = longswap(vmeCur->bcastSelect2esst);

     if (vmeCur->pNextPacket == 0)
       {
         // last packet
         currentLL->dnlau = longswap(0);
         currentLL->dnlal = longswap(1);
       }
     else
       {
         void* next = currentLL + 1;
         u64 b = virt_to_bus(next);

         currentLL->dnlal = longswap(u64_low(b));
         currentLL->dnlau = longswap(u64_high(b));
       }

     if (0)
       {
         printk("Channel %d, iptr %d, Linked List Entry Fields:\n", channel, iptr);
         printk("dsal  0x%08x\n",longswap(currentLL->dsal));
         printk("dsau  0x%08x\n",longswap(currentLL->dsau));
         printk("ddal  0x%08x\n",longswap(currentLL->ddal));
         printk("ddau  0x%08x\n",longswap(currentLL->ddau));
         printk("dsat  0x%08x\n",longswap(currentLL->dsat));
         printk("ddat  0x%08x\n",longswap(currentLL->ddat));
         printk("dnlau 0x%08x\n",longswap(currentLL->dnlau));
         printk("dnlal 0x%08x\n",longswap(currentLL->dnlal));
         printk("dcnt  0x%08x\n",longswap(currentLL->dcnt));
         printk("ddbs  0x%08x\n",longswap(currentLL->ddbs));
       }

     vmeCur = vmeCur->pNextPacket;
     iptr++;

     if (iptr >= dma_max)
       {
         printk("tempeSetupDma: attempt to use too many dma descriptors, max is %d\n", dma_max);
         return NULL;
       }
  }

  if (0)
    {
      printk("tempeSetupDma: used %d out of %d dma descriptors\n", iptr, dma_max);
    }

  return &dma_pool[channel][0];
}

//-----------------------------------------------------------------------------
// Function   : tempeFreeDma
// Description: Free all memory that is used to hold the DMA 
//    descriptor linked list.  
//
//-----------------------------------------------------------------------------
int
tempeFreeDma(tsi148DmaDescriptor_t *startLL)
{
  return 0;
}

//-----------------------------------------------------------------------------
// Function   : tempeDoDma
// Description:  
//   Sanity check the DMA request. 
//   Setup DMA attribute register. 
//   Create linked list of DMA descriptors.
//   Invoke actual DMA operation.
//   Wait for completion.  Record ending time.
//   Free the linked list of DMA descriptor.
//-----------------------------------------------------------------------------
int
tempeDoDma(vmeDmaPacket_t *vmeDma)
{
  tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
  unsigned int dctlreg=0;
  //int val;
  //char buf[100];
  int channel, x;
  vmeDmaPacket_t *curDma;
  tsi148DmaDescriptor_t *dmaLL;
  int status;
  int byteCount = 0;
  int linkCount = 0;

  ts("enter tempeDoDma");
    
  // Sanity check the VME chain.
  channel = vmeDma->channel_number;
  if(channel > 1) { return(-EINVAL); }

  curDma = vmeDma;
  while(curDma != 0){
    if(curDma->byteCount == 0){ return(-EINVAL); }
    if(dmaSrcAttr(curDma) < 0) { return(-EINVAL); }
    if(dmaDstAttr(curDma) < 0) { return(-EINVAL); }

#if 0
    printk("tsi148: DMA channel %d: link %d has %d bytes, dst start 0x%08x, end 0x%08x\n",
           channel,
           linkCount,
           curDma->byteCount,
           curDma->dstAddr,
           curDma->dstAddr+curDma->byteCount
           );
#endif

    linkCount += 1;
    byteCount += curDma->byteCount;

    curDma = curDma->pNextPacket;
    if(curDma == vmeDma) {          // Endless Loop!
       return(-EINVAL);
    }
  }

  status = longswap(ioread32(&pTempe->lcsr.dma[channel].dsta));
#if 0
  printk("tsi148: do DMA, status 0x%08x\n", status);
#endif

  if (status & 0x01000000) {
    printk("tsi148: DMA channel %d is busy, status 0x%08x\n", channel, status);

    //return -EIO;

    printk("tsi148: trying to reset!\n");

    iowrite32(longswap((1<<27)), &pTempe->lcsr.dma[channel].dctl);
    ioread32(&pTempe->lcsr.dma[channel].dctl);
    ioread32(&pTempe->lcsr.dma[channel].dsta);

    iowrite32(0,&pTempe->lcsr.dma[channel].dsal);
    iowrite32(0,&pTempe->lcsr.dma[channel].dsau);
    iowrite32(0,&pTempe->lcsr.dma[channel].ddal);
    iowrite32(0,&pTempe->lcsr.dma[channel].ddau);
    iowrite32(0,&pTempe->lcsr.dma[channel].dsat);
    iowrite32(0,&pTempe->lcsr.dma[channel].ddat);
    iowrite32(0,&pTempe->lcsr.dma[channel].dcnt);
    iowrite32(0,&pTempe->lcsr.dma[channel].ddbs);
    iowrite32(0,&pTempe->lcsr.dma[channel].dnlau);
    iowrite32(0,&pTempe->lcsr.dma[channel].dnlal);

    iowrite32(longswap((1<<25)), &pTempe->lcsr.dma[channel].dctl);
    ioread32(&pTempe->lcsr.dma[channel].dctl);
    iowrite32(longswap((1<<27)), &pTempe->lcsr.dma[channel].dctl);
    ioread32(&pTempe->lcsr.dma[channel].dctl);
    ioread32(&pTempe->lcsr.dma[channel].dsta);

    printk("tsi148: DMA channel %d is busy, dctl 0x%08x, dsta 0x%08x\n",
           channel,
           longswap(ioread32(&pTempe->lcsr.dma[channel].dctl)),
           longswap(ioread32(&pTempe->lcsr.dma[channel].dsta))
           );

    if (0) { /* this reboots the V7865 */
       iowrite32(longswap(1<<31), &pTempe->gcsr.ctrl);
       ioread32(&pTempe->lcsr.dma[channel].dsta);
       ioread32(&pTempe->lcsr.dma[channel].dsta);
       iowrite32(longswap(0<<31), &pTempe->gcsr.ctrl);
    }

    printk("tsi148: DMA channel %d is busy, gcsr.ctrl 0x%08x, dctl 0x%08x, dsta 0x%08x\n",
           channel,
           longswap(ioread32(&pTempe->gcsr.ctrl)),
           longswap(ioread32(&pTempe->lcsr.dma[channel].dctl)),
           longswap(ioread32(&pTempe->lcsr.dma[channel].dsta))
           );

    printk("tsi148: DMA channel %d is busy, status 0x%08x\n", channel, longswap(ioread32(&pTempe->lcsr.dma[channel].dsta)));

    return -EIO;
  }

  // calculate control register
  if(vmeDma->pNextPacket != 0){
    dctlreg = 0;
  } else {
    dctlreg = 0x800000;
  }

  if (vmeDma->maxVmeBlockSize >= 4096)
    x = 7;
  else {
    for(x = 0; x < 8; x++){	// vme block size
      if((32 << x) >= vmeDma->maxVmeBlockSize) { break; }
    }
    if (x == 8) x = 7;
  }

  dctlreg |= (x << 12);

  if (vmeDma->maxPciBlockSize >= 4096)
    x = 7;
  else {
    for(x = 0; x < 8; x++){	// pci block size
      if((32 << x) >= vmeDma->maxPciBlockSize) { break; }
    }
    if (x == 8) x = 7;
  }

  dctlreg |= (x << 4);

  if(vmeDma->vmeBackOffTimer){
    for(x = 1; x < 8; x++){	// vme timer
      if((1 << (x-1)) >= vmeDma->vmeBackOffTimer) { break; }
    }
    printk("tsi148: vmeBackOffTimer %d, x %d\n", vmeDma->vmeBackOffTimer, x);
    if (x == 8) x = 7;
    dctlreg |= (x << 8);
  }

  if(vmeDma->pciBackOffTimer){
    for(x = 1; x < 8; x++){	// pci timer
      if((1 << (x-1)) >= vmeDma->pciBackOffTimer) { break; }
    }
    printk("tsi148: pciBackOffTimer %d, x %d\n", vmeDma->pciBackOffTimer, x);
    if (x == 8) x = 7;
    dctlreg |= (x << 0);
  }


  ts("call tempeSetupDma");

  // Setup the dma chain
  dmaLL = tempeSetupDma(channel, vmeDma);

  if (dmaLL == NULL)
    return -EIO;

#if 0
  printk("tsi148: before DMA of %d links, %d bytes, status 0x%08x, new dctl 0x%08x\n", linkCount, byteCount, status, dctlreg);
#endif

  ts("call tempeStartDma");

  // Start the DMA
  if(dctlreg & 0x800000){
      vmeDma->vmeDmaStartTick = 
                      tempeStartDma( channel, dctlreg, dmaLL);
  } else {
      vmeDma->vmeDmaStartTick = 
                      tempeStartDma( channel, dctlreg,
                                     (tsi148DmaDescriptor_t *)virt_to_bus(dmaLL));
      ///(tsi148DmaDescriptor_t *)virt_to_phys(dmaLL));
  }

#if 0
  printk("tsi148: waiting for DMA, status 0x%08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dsta)));
#endif

  if (0)
    {
      int i;
      for (i=0; i<100; i++)
        {
          status = longswap(ioread32(&pTempe->lcsr.dma[channel].dsta));
          //printk("tsi148: wait %d, DMA status 0x%x\n", i, status);
          if (status & 0x02000000)
            break;
          udelay(10);
        }

      printk("tsi148: bytes %d, wait %d, DMA status 0x%x\n", byteCount, i, status);
    }

  status = wait_event_interruptible(dma_queue[channel], 
          ( longswap(ioread32(&pTempe->lcsr.dma[channel].dsta)) & 0x01000000) == 0);

  ts("dma done");

#if 0
  printk("tsi148: wait_event_interruptible() status %d\n", status);
#endif

  vmeDma->vmeDmaStatus = longswap(ioread32(&pTempe->lcsr.dma[channel].dsta));

  //vmeDma->vmeDmaStatus = 0xdeadbeef;

#if 0
  printk("tsi148: DMA done, links %d, bytes %d, status 0x%08x\n", linkCount, byteCount, vmeDma->vmeDmaStatus);
#endif

  ts("save dma status");

  if (vmeDma->vmeDmaStatus != 0x02000000)
    {
      vmeDma->srcAddr  = longswap(ioread32(&pTempe->lcsr.dma[channel].dcsal));
      vmeDma->srcAddrU = longswap(ioread32(&pTempe->lcsr.dma[channel].dcsau));
      vmeDma->dstAddr  = longswap(ioread32(&pTempe->lcsr.dma[channel].dcdal));
      vmeDma->dstAddrU = longswap(ioread32(&pTempe->lcsr.dma[channel].dcdau));
      vmeDma->byteCount = longswap(ioread32(&pTempe->lcsr.dma[channel].dcnt));
    }

  if (0)
    {
      printk("dma done status dsta 0x%08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dsta)));
      printk("source addr     dcsa 0x%08x 0x%08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dcsau)), longswap(ioread32(&pTempe->lcsr.dma[channel].dcsal)));
      printk("dest   addr     dcda 0x%08x 0x%08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dcdau)), longswap(ioread32(&pTempe->lcsr.dma[channel].dcdal)));
      printk("byte counter    dcnt 0x%08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dcnt)));
      printk("curr link addr  dcla 0x%08x 0x%08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dclau)), longswap(ioread32(&pTempe->lcsr.dma[channel].dclal)));
      printk("next link addr  dnla 0x%08x 0x%08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dnlau)), longswap(ioread32(&pTempe->lcsr.dma[channel].dnlal)));
      printk("source addr      dsa 0x%08x 0x%08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].dsau)), longswap(ioread32(&pTempe->lcsr.dma[channel].dsal)));
      printk("dest   addr      dda 0x%08x 0x%08x\n", longswap(ioread32(&pTempe->lcsr.dma[channel].ddau)), longswap(ioread32(&pTempe->lcsr.dma[channel].ddal)));
      printk("vmeDma %p\n", vmeDma);
    }
      
  ts("free dma data");

  // Free the dma chain
  tempeFreeDma(dmaLL);
  
  ts("leave tempeDoDma");

  return(0);
}

//-----------------------------------------------------------------------------
// Function   : tempe_shutdown
// Description: Put VME bridge in quiescent state.  Disable all decoders,
// clear all interrupts.
//-----------------------------------------------------------------------------
void
tempe_shutdown( void )
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    int i;
    //unsigned long irqflags;

    //spin_lock_irqsave (&tempe_lock, irqflags);

   /*
    *  Shutdown all inbound and outbound windows.
    */
    for(i =0; i < 8; i++){
        iowrite32(0,&pTempe->lcsr.inboundTranslation[i].itat);
        iowrite32(0,&pTempe->lcsr.outboundTranslation[i].otat);
    }

   /*
    *  Shutdown Location monitor.
    */
   iowrite32(0,&pTempe->lcsr.lmat);
   
   /*
    *  Shutdown CRG map.
    */
   iowrite32(0,&pTempe->lcsr.csrat);

   /*
    *  Clear error status.
    */
   iowrite32(0xFFFFFFFF,&pTempe->lcsr.edpat);
   iowrite32(0xFFFFFFFF,&pTempe->lcsr.veat);
   // FIXME: does something to PCI side to disable DMA, that is not reset when module is loaded?
   // iowrite32(longswap(0x07000700),&pTempe->lcsr.pstat);

   /*
    *  Remove VIRQ interrupt (if any)
    */
   if(longswap(ioread32(&pTempe->lcsr.vicr)) & 0x800){
      iowrite32(longswap(0x8000),&pTempe->lcsr.vicr);
   }

   /*
    *  Disable and clear all interrupts.
    */
   iowrite32(0,&pTempe->lcsr.inteo);
   vmeSyncData();
   iowrite32(0xFFFFFFFF,&pTempe->lcsr.intc); 
   vmeSyncData();
   iowrite32(0xFFFFFFFF,&pTempe->lcsr.inten); 
   
   /*
    *  Map all Interrupts to PCI INTA
    */
   // FIXME: not fixed by module load? iowrite32(0,&pTempe->lcsr.intm1);
   // FIXME: not fixed by module load? iowrite32(0,&pTempe->lcsr.intm2);

   vmeSyncData();

   //spin_unlock_irqrestore (&tempe_lock, irqflags);

}

//-----------------------------------------------------------------------------
// Function   : tempe_init()
// Description: Initialize the VME chip as needed by the driver.
//    Quiesce VME bridge.
//    Setup default decoders.
//    Connect IRQ handler and enable interrupts.
//    Conduct quick sanity check of bridge.
//-----------------------------------------------------------------------------
int
tempe_init(void)
{
    tsi148_t *pTempe = (tsi148_t *)vmechip_baseaddr;
    int result;
    unsigned int tmp;
    unsigned int crcsr_addr;
    unsigned int irqOverHeadStart;
    int overHeadTicks;
    unsigned long irqflags;

    spin_lock_init(&tempe_lock);

    tempe_shutdown();
#ifdef DEBUG
    printk(" Called tempe_shutdown() in tempe_init()\n");
#endif

    // Determine syscon and slot number.
    tmp = longswap(ioread32(&pTempe-> lcsr.vstat));
    if(tmp & 0x100){
#ifdef DEBUG
        printk(" vme_syscon=1  in tempe_init() pTempe->lcsr.vstat (0x23C) = 0x%x\n",tmp);
#endif
        vme_syscon = 1;
    } else {
#ifdef DEBUG
        printk(" vme_syscon=0  in tempe_init() pTempe->lcsr.vstat (0x23C) = 0x%x\n",tmp);
#endif
        vme_syscon = 0;
    }

    /* clear board fail status */
    iowrite32(longswap(0x8000),&pTempe->lcsr.vstat);
    vmeSyncData();

#ifdef DEBUG
    printk(" tempe_init() pTempe->lcsr.vstat after reset (0x23C) = 0x%x\n",
                      longswap(ioread32(&pTempe->lcsr.vstat)));
#endif

    // Initialize crcsr map
    if(vme_slotnum != -1){
      iowrite32(longswap(vme_slotnum << 3),&pTempe->crcsr.cbar);
      crcsr_addr = vme_slotnum*512*1024;
      iowrite32(longswap(virt_to_bus(vmechip_interboard_data)),&pTempe->lcsr.crol);
      iowrite32(0,&pTempe->lcsr.crou);
      vmeSyncData();
      iowrite32(0xFFFFFFFF,&pTempe->lcsr.crat);
#ifdef DEBUG
    printk(" crcsr addr 0x%x\n",crcsr_addr);
    printk(" vmechip_interboard_data 0x%x\n",(unsigned int)vmechip_interboard_data);
    printk(" phys vmechip_interboard_data 0x%lx\n",virt_to_bus(vmechip_interboard_data));
#endif

    }

    // Connect the interrupt handler.
#ifdef PPC
    result = request_irq(vmechip_irq, 
                         tempe_irqhandler, 
                         SA_INTERRUPT, 
                         "tsi148", NULL);
#else
    result = request_irq(vmechip_irq, 
                         tempe_irqhandler, 
                         //SA_SHIRQ, 
                         IRQF_SHARED, 
                         "tsi148", vme_pci_dev);
#endif

    if (result) {
      printk("  tsi148: can't get assigned pci irq vector %02X\n", vmechip_irq);
      return(0);
    } 

    // Enable DMA, mailbox, VIRQ (syscon only) & LM Interrupts
    if(vme_syscon)
       tmp = 0x03FF20FE;
    else 
       tmp = 0x03FF2000;

    iowrite32(longswap(tmp),&pTempe->lcsr.inteo);
    iowrite32(longswap(tmp),&pTempe->lcsr.inten);

    // Do a quick sanity test of the bridge
    if(longswap(ioread32(&pTempe->lcsr.inteo)) != tmp){
      printk("tsi148: tempe_init: cannot write lcsr.inteo: wrote 0x%08x, read 0x%08x\n", tmp, longswap(ioread32(&pTempe->lcsr.inteo)));
      return(0);
    }
    // Sanity check register access.
    for(tmp = 1; tmp < 0x80000000; tmp = tmp << 1){
      iowrite32(longswap(tmp),&pTempe->lcsr.rmwen);
      iowrite32(longswap(~tmp),&pTempe->lcsr.rmwc);
      vmeSyncData();
      if(longswap(ioread32(&pTempe->lcsr.rmwen)) != tmp) {
        printk("tsi148: tempe_init: cannot write lcsr.rmwen: wrote 0x%08x, read 0x%08x\n", tmp, longswap(ioread32(&pTempe->lcsr.rmwen)));
        return(0);
      }
      if(longswap(ioread32(&pTempe->lcsr.rmwc)) != ~tmp) {
        printk("tsi148: tempe_init: cannot write lcsr.rmwc: wrote 0x%08x, read 0x%08x\n", tmp, longswap(ioread32(&pTempe->lcsr.rmwc)));
        return(0);
      }
    }

    // Calculate IRQ overhead
#ifdef PPC
    irqOverHeadStart = vmeGetTime();
#else
    rdtscl(irqOverHeadStart);
#endif

    iowrite32(0,&pTempe->gcsr.mbox[0]);
    vmeSyncData();
#ifdef PPC
    for(tmp = 0; tmp < 10; tmp++) { vmeSyncData(); }
    irqOverHeadStart = vmeGetTime();
#else
    for(tmp = 0; tmp < 10; tmp++) {
      udelay(10);
      vmeSyncData();
    }
    rdtscl(irqOverHeadStart);
#endif


#ifdef DEBUG
    printk("time start val 0x%x\n",irqOverHeadStart);
#endif

    // Guarantee first interrupt processing complete
    spin_lock_irqsave(&tempe_lock, irqflags);
    iowrite32(0,&pTempe->gcsr.mbox[0]);
    spin_unlock_irqrestore(&tempe_lock, irqflags);
    vmeSyncData();

#ifdef PPC
    for(tmp = 0; tmp < 10; tmp++) { vmeSyncData(); }
#else
    for(tmp = 0; tmp < 10; tmp++) {
      udelay(10);
      vmeSyncData();
    }
#endif

#ifdef DEBUG
    printk("Calculating overhead\n");
#endif
    overHeadTicks = tempeIrqTime - irqOverHeadStart;
    if(overHeadTicks > 0){
       vmechipIrqOverHeadTicks = overHeadTicks;
    } else {
       vmechipIrqOverHeadTicks = 1;
    }

    printk("tsi148: VME master control register VMCTRL: 0x%08x\n", longswap(ioread32(&pTempe->lcsr.vmctrl)));
    printk("tsi148: VME control register         VCTRL: 0x%08x\n", longswap(ioread32(&pTempe->lcsr.vctrl)));
    printk("tsi148: VME status register          VSTAT: 0x%08x\n", longswap(ioread32(&pTempe->lcsr.vstat)));
    printk("tsi148: DMA channel %d status: 0x%08x\n", 0, longswap(ioread32(&pTempe->lcsr.dma[0].dsta)));
    printk("tsi148: DMA channel %d status: 0x%08x\n", 1, longswap(ioread32(&pTempe->lcsr.dma[1].dsta)));
    printk("tsi148: Interrupt enable register     INTEN: 0x%08x\n", longswap(ioread32(&pTempe->lcsr.inten)));
    printk("tsi148: Interrupt enable out register INTEO: 0x%08x\n", longswap(ioread32(&pTempe->lcsr.inteo)));
    printk("tsi148: Control and status register   GCTRL: 0x%08x\n", longswap(ioread32(&pTempe->gcsr.ctrl)));

    return(1);
}
