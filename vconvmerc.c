/*
 * =====================================================================================
 *
 *       Filename:  vconvme.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  20/02/2009 12:20:20
 *       
 *       Revision:  1.) Removing test functions.
 *       		Cleaning up code.
 *
 *       Compiler:  gcc (GCC) 4.1.2 20071124 (Red Hat 4.1.2-42)
 *         Linker:  GNU ld version 2.17.50.0.6-6.el5 20061020
 *	   System:  Scientific Linux 5.2, kernel-2.6.18-92.1.6.el5
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 * =====================================================================================
 */

/* #####   HEADER FILE INCLUDES   ################################################### */
#include	<string.h>
#include	<stdlib.h>
#include	<stdio.h>
#include	<signal.h>
#include	<assert.h>
#include	<unistd.h>
#include	<asm/page.h>


/* #####   HEADER FILE INCLUDES   ################################################### */
#include 	"vconvme.h"


/* #####   HEADER FILE INCLUDES   ################################################### */
#include 	<sys/ioctl.h>
#include	<sys/time.h>
#include 	<errno.h>


/* #####   MACROS  -  LOCAL TO THIS SOURCE FILE   ################################### */
#define TIMER_BYTE_0	(0x0218U)
#define TIMER_BYTE_1	(0x0219U)
#define TIMER_BYTE_2	(0x021AU)
#define TIMER_BYTE_3	(0x021BU)
#define CONTROL_STATUS	(0x021CU)

#define MODE_LDT	(0x00U)

#define MODE_RUN_MASK   (0x01U)
#define MODE_RUN_GO     (0x01U)
#define MODE_RUN_STOP   (0x01U)

static UINT32 dElapsedTime;


/* #####   DATA TYPES  -  LOCAL TO THIS SOURCE FILE   ############################### */
vme_bus_handle_t	bus_handle;

vme_interrupt_handle_t	int_handle;

struct	sigevent	event;
struct	sigaction	handler_act;
INT_INFO		int_info;
UINT32 _VME_BASE_ADDR_  = 0xb0000;

EN_VME_DIRECT_TXFER tdata;		/* Direct DMA transfer(READ) */
FILE *fp;
int timedt;
#define A32_CHUNK 0x00FFFFFF


/*-----------------------------------------------------------------------------
 *  Help Functions
 *-----------------------------------------------------------------------------*/

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  inbyte
 *  Description :  Reads in from Device Register
 *  Parameters  :  
 *  Returns     :  
 * =====================================================================================
 */
UINT32 inbyte( UINT16 regOffset  )
{
	int devHandle = 0;
	int result;
	UINT32 reg = 0x00000;
	
	devHandle = vme_openDevice("ctl");
	
	if( devHandle >= 0 ){
		result = vme_readRegister( devHandle, regOffset, &reg );
		
		if( result < 0 ) {
			printf(" [inbyte] error = <%s> \n", strerror(errno) );
			printf(" devHandle = [%u] , regOffset = [0x%02x] \n",devHandle,regOffset );
			printf(" reg = [0x%08x] \n",reg );
		}

		vme_closeDevice(devHandle);
	}
	return reg;


}


/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  outbyte
 *  Description :  Sets Device Register
 *  Parameters  :  
 *  Returns     :  
 * =====================================================================================
 */
void outbyte( UINT16 reg, UINT32 mode)
{
	int devHandle;
	int result;
	
	devHandle = vme_openDevice("ctl");
	//printf("[outbyte] ctl device handle = [%d] \n ",devHandle );
	if( devHandle >=0 ){
		result = vme_writeRegister( devHandle, reg, mode );
		
		if( result < 0 ) {
			printf(" [outbyte] error = <%s> \n", strerror(errno) );
			printf(" devHandle = [%u] , regOffset = [0x%02x] \n",devHandle,reg );
			printf(" mode = [0x%08x] \n", mode );
		}else {
			printf(" setting mode = [0x%08x] succeeded!!!! \n", mode );
		}


		vme_closeDevice(devHandle);
	}

}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  vme_init
 *  Description:  Initializes the vme ctl device to for setting access parameters.
 *  Parameters :  @param *handle	vme_bus_handle_t
 *  Returns    :  @return Error/MVME_SUCCESS
 * =====================================================================================
 */
int vme_init( vme_bus_handle_t *handle )
{
	UINT32 devHandle;

	if( handle == NULL ){
		perror("Error, Initialized devHandle is set to NULL!!!");
		return(MVME_INVALID_PARAM);
	}

	devHandle = vme_openDevice("ctl");
	
	if( devHandle >= 0  ) {
		handle = &devHandle; /* Set device/bus handle */
	}
	else {
	       perror("Error, Initialization of ctl interface failed");
		return(MVME_NO_INTERFACE);

	}
	vme_closeDevice(devHandle);
	return(MVME_SUCCESS);
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  vme_term
 *  Description:  Terminates the device with handle "devHandle"
 *  Parameters :  @param devHandle	vme_bus_handle_t
 *  Returns    :  @return Error/MVME_SUCCESS
 * =====================================================================================
 */
int vme_term( vme_bus_handle_t devHandle )
{
	int result;
	if( devHandle == 0 ){
		perror("Error, Device Handle is NULL cannot close device!");
		return(MVME_NO_INTERFACE);
	}
	
	result = vme_closeDevice( (UINT32)devHandle );
	if( result < 0 ){
		perror("Error, Could NOT close Device");
		return(MVME_NO_INTERFACE);
	}

	devHandle = -1;	
	return(MVME_SUCCESS);	
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  initpciD16
 *  Description :  
 *  Parameters  :  
 *  Returns     :  
 * =====================================================================================
 */
int initpciD16( MVME_INTERFACE *mvme, mvme_addr_t vme_base_addr, mvme_size_t resolution )
{
	int devHandle, result;
	VME_TABLE2 *table;
	EN_PCI_IMAGE_DATA data16;
	
	table = (VME_TABLE2*)mvme->table;
	
	/* default Image data 16 */
	data16.pciAddress=0x00000000;
        data16.pciAddressUpper=0;
        data16.pciBusSpace=0;
	data16.vmeAddress=0x00000000;
        data16.vmeAddressUpper=0;
	data16.size = 0x100000;
        data16.sizeUpper = 0;
	data16.postedWrites = 1;
	data16.readPrefetch=1;
        data16.prefetchSize= 3;
        data16.dataWidth = EN_VME_D16;
	data16.addrSpace = EN_VME_A32;		/*Default*/
        data16.type = EN_LSI_DATA;
        data16.mode = EN_LSI_USER;
        data16.vmeCycle = 0;
        data16.sstbSel =0;
        data16.ioremap = 1;
	
	/* Create D16 pci device */
	devHandle = vme_openDevice("lsi2");
	if( devHandle < 0 ){
		perror("[initpciD16] Could not create device for d16\n");
		return(MVME_ERROR);
	}else{
	
		
		result = vme_enablePciImage( devHandle, &data16 );
		if( result < 0 ){
			perror("[initpciD16] Could not create PCI Image {2} for d16\n");
			return(MVME_ERROR);
		}
		table[0].devHandle = devHandle;
		table[0].nbytes = 2;
		table[0].addr = 0x0;
		table[0].am = ((mvme->am == 0) ? MVME_AM_DEFAULT : mvme->am );
		table[0].valid = 1;
	}/**D16*/

	mvme->table = table;

	return(MVME_SUCCESS);
}


/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  initpciD32
 *  Description :  
 *  Parameters  :  
 *  Returns     :  
 * =====================================================================================
 */
int initpciD32( MVME_INTERFACE *mvme, mvme_addr_t vme_base_addr, mvme_size_t resolution)
{
	int devHandle, result;
	VME_TABLE2 *table;
	EN_PCI_IMAGE_DATA data32;

	table = (VME_TABLE2*)mvme->table;

	/* default Image data 32*/
	data32.pciAddress=0x00000000;
        data32.pciAddressUpper=0;
        data32.pciBusSpace=0;
	data32.vmeAddress=0x00000000;
        data32.vmeAddressUpper=0;
	data32.size = 0x100000;
        data32.sizeUpper = 0;
	data32.postedWrites = 1;
	data32.readPrefetch=1;
        data32.prefetchSize= 3;
        data32.dataWidth = EN_VME_D32;
	data32.addrSpace = EN_VME_A32;		/*Default*/
        data32.type = EN_LSI_DATA;
        data32.mode = EN_LSI_USER;
        data32.vmeCycle = 0;
        data32.sstbSel =0;
        data32.ioremap = 1;
	
	/* Create D32 pci device */
	devHandle = vme_openDevice("lsi3");
	if( devHandle < 0 ){
		perror("[initpciD32] Could not create device for d16\n");
		return(MVME_ERROR);
	}else {
				
		result = vme_enablePciImage( devHandle, &data32 );
		if( result < 0 ){
			perror("[initpciD32] Could not create PCI Image {3} for d16\n");
			return(MVME_ERROR);
		}
		
		table[1].devHandle = devHandle;
		table[1].nbytes = 2;
		table[1].addr = 0x0;
		table[1].am = ((mvme->am == 0) ? MVME_AM_DEFAULT : mvme->am );
		table[1].valid = 1;
	}/**D32*/

	mvme->table = table;



	return(MVME_SUCCESS);
}


/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  initDMABuffer
 *  Description :  
 *  Parameters  :  
 *  Returns     :  
 * =====================================================================================
 */
int initDMABuffer( MVME_INTERFACE *mvme, mvme_addr_t vme_base_addr, mvme_size_t resolution )
{

	/* default dma data */
	tdata.direction = EN_DMA_READ;
	tdata.offset = 0;			
	tdata.size = 4096;
	tdata.vmeAddress = 0x0; /* dma */
	tdata.txfer.timeout = 200;		/* 2 second time out */
	tdata.txfer.vmeBlkSize = TSI148_4096;
	tdata.txfer.vmeBackOffTimer = 0;
	tdata.txfer.pciBlkSize = TSI148_4096;	
        tdata.txfer.pciBackOffTimer = 0;
	tdata.access.sstMode = TSI148_SST320;
	tdata.access.vmeCycle = 0;
	tdata.access.sstbSel = 0;
	tdata.access.dataWidth = EN_VME_D32;	/* v792 D32 */
        tdata.access.addrSpace = EN_VME_A32;	/* default am */
	tdata.access.type = EN_LSI_DATA;
	tdata.access.mode = EN_LSI_USER;
        tdata.vmeAddress = (UINT32)vme_base_addr; /* dma */
		
//	tdata.vmeAddress = 0x0; /* dma */

	/* Create D32 DMA device */
	devHandle = vme_openDevice("dma0");
	if( devHandle >= 0 ){
		result = vme_allocDmaBuffer( devHandle, &resolution );
		if( result < 0 ){
			perror("[vcon_init_devices] Could not allocDmaBuffer \n");
			return(MVME_ERROR);
		}else{
			printf("[vcon_init_devices] DMA Buffer allocated, size = %u \n",resolution);
		}
					
		result = vme_dmaDirectTransfer( devHandle, &tdata );
	
		if( result < 0 ){
				perror("[vcon_init_devices] DMA direct transfer failed\n");
				printf(" result = <%s> \n",strerror(errno) );
				return(MVME_ERROR);
		}
			
		table[2].devHandle = devHandle;
		table[2].nbytes = DEFAULT_DMA_NBYTES ;
		table[2].addr = 0x0;
		table[2].am = ((mvme->am == 0) ? MVME_AM_DEFAULT : mvme->am );
		table[2].valid = 1;
	}/**DMA*/
	
	mvme->table = table;

	return(MVME_SUCCESS);
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  vcon_init_devices
 *  Description:  Initialize the 3 different devices for use, D16,D32 and DMA.
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  		  @param vme_base_addr	mvme_addr_t
 *  		  @param resolution	mvme_size_t
 *  Returns    :  @return	Error/MVME_SUCCESS
 * =====================================================================================
 */
int vcon_init_devices( MVME_INTERFACE *mvme, mvme_addr_t vme_base_addr, mvme_size_t resolution )
{
	int result;

	result = initpciD16(mvme,vme_base_addr,resolution);
	if( result < 0 ){
		perror("[vcon_init_devices]: could not init pci Image d16\n");
		return(MVME_ERROR);
	}

	result = initpciD32(mvme,vme_base_addr,resolution);
	if( result < 0 ){
		perror("[vcon_init_devices]: could not init pci Image d32\n");
		return(MVME_ERROR);
	}

	result = initDMABuffer(mvme,vme_base_addr,resolution);
	if( result < 0 ){
		perror("[vcon_init_devices]: could not init DMA Buffer\n");
		return(MVME_ERROR);
	}

	return(MVME_SUCCESS);
}
/*-----------------------------------------------------------------------------
 *  MIDAS API FOR VME
 *-----------------------------------------------------------------------------*/
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_open
 *  Description:  Open VME channel. One bus handle per crate.
 *  Parameters :  @param *mvme	// VME structure
 *  		  @param index	// VME interface index
 *  		  @return  MVME_SUCCESS,ERROR
 * =====================================================================================
 */
int mvme_open (MVME_INTERFACE **mvme, int index) 
{	
//	vme_bus_handle_t devHandle;
//	int devHandledma = 0;
	int result;
	DMA_INFO* info;
	VME_TABLE2 *table;

	if( index != 0 ){
		perror("[mvme_open]: index != 0 ");
		(*mvme)->handle = 0;
		return(MVME_INVALID_PARAM);
	}
	
//	fp = fopen("/root/timing.out","rb+");
//	printf(" ********************* File open [%u] \n",fp);	

	/* ==== MIDAS STUFF === */
	/* Allocate MVME_INTERFACE */
	*mvme = (MVME_INTERFACE*)calloc(1,sizeof(MVME_INTERFACE));

	/* Allocate MVME_TABLE for the *mvme interface */
	(*mvme)->table = (char*)malloc(MAX_VME_SLOTS*sizeof(VME_TABLE2));
	
	/* Initialize the table, fill it with 0 with the size = MAX_VME_SLOTS*sizeof(VME_TABLE) */
//	memset( (char*)(*mvme)->table, 0, MAX_VME_SLOTS*sizeof(VME_TABLE2) );

	/* Set Default Parameters */
	(*mvme)->am = MVME_AM_DEFAULT;
       /* ===================== */	


       /* === CONCURRENT VME STUFF  === */
       /* Init device */
       result = vme_init(  (vme_bus_handle_t*) &((*mvme)->handle) );
       if( result < 0 ){
		perror("Error initializing the Concurrent vme bus");
		(*mvme)->handle=0;
		return(MVME_NO_INTERFACE);
	}else {
	       printf(" Device ctl handle = [%u] \n", (*mvme)->handle );	
	}
	mvme_size_t res = 0x10000;

	result = vcon_init_devices( *mvme, _VME_BASE_ADDR,res);
	if( result < 0 ){
		perror("[mvme_open] Failed to init devices\n");
		(*mvme)->handle=0;
		return(MVME_NO_INTERFACE);
	}	
	
	
	table = (VME_TABLE2*)(*mvme)->table;
	
//	int k = 0;
//	while( k < 3 ){
//		printf(" Handles in mvme table are %u \n",table[k].devHandle);
//		k++;
///	}
		
	info = (DMA_INFO*) calloc(1,sizeof(DMA_INFO));
	info->dma_handle = table[2].devHandle;	
	
	(*mvme)->info = info;
	printf("mvme_open:[Concurrent tsi148] \n");
	printf("VME Bus Handle			= 0x%02x\n",(*mvme)->handle);
	printf("DMA Handle			= 0x%02x\n",(UINT32)&info->dma_handle );
	printf("DMA area size			=  %d bytes \n",DEFAULT_DMA_NBYTES);
	printf("DMA physical address		=  %p\n",(unsigned long *)info->dma_ptr);
	

	return(MVME_SUCCESS);
}/* --------------- end of function mvme_open -------------------*/


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_close
 *  Description:  close and release(free) ALL openend VME channels
 *  Parameters :  @param *mvme	VME_structure
 *  		  @return MVME_SUCCESS,ERROR
 * =====================================================================================
 */
int mvme_close ( MVME_INTERFACE *mvme )
{
	int 		j;
	int 		result;
	VME_TABLE2 	*table;
//	DMA_INFO   	*info;
	int 		devHandle;

	table = (VME_TABLE2*)mvme->table;
//	info  = ((DMA_INFO*)mvme)->info;

	printf("mvme_close:\n");
	printf("Bus handle		= 0x%x\n",mvme->handle);
	printf("DMA handle		= 0x%x\n",table[2].devHandle);

	devHandle = (int)mvme->handle;

	/* timing */	
//	fclose(fp);


	/* 1. Close DMA channel */
	result =  vme_freeDmaBuffer(table[2].devHandle);
	if( result < 0 ){
		perror("Error - Could not free DMA buffer " );
		return(MVME_ACCESS_ERROR);
	}
	
	/* 2. Close PCI D16 Image */
	result = vme_disablePciImage(table[0].devHandle );
	if( result < 0 ){
		perror("[mvme_close] Failed to close PCI D16 Image ");
		return(MVME_ACCESS_ERROR);
	}

	/* 3. Close PCI D32 Image */
	result = vme_disablePciImage(table[1].devHandle );
	if( result < 0 ){
		perror("[mvme_close] Failed to close PCI D32 Image ");
		return(MVME_ACCESS_ERROR);
	}

	for(j = 0; j < 3; j++) {
		devHandle = table[j].devHandle;
		if( devHandle >=0 )
			vme_closeDevice(devHandle);
	}

	free(mvme->info);			/* Free DMA allocation */

	/* free table pointer */
	free (mvme->table);
	mvme->table = NULL;
	/* free mvme block */
	free(mvme);

	return(MVME_SUCCESS);
}/* --------------- end of function mvme_close -------------------*/


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_write
 *  Description:  At the moment only does block DMA write.
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  		  @param vme_addr	mvme_addr_t
 *  		  @param *src		void
 *  		  @param n_bytes	mvme_size_t
 *   Returns   :  @return Error/MVME_SUCCESS
 * =====================================================================================
 */
int mvme_write(MVME_INTERFACE *mvme, mvme_addr_t vme_addr, void *src, mvme_size_t n_bytes)
{

	int	devHandle,result;
//	DMA_INFO 	*info;
	VME_TABLE2 *table;

	if( n_bytes > DEFAULT_DMA_NBYTES) {
		perror("[mvme_write] Attempt to DMA bytes > DEFAULT_DMA_NBYTES\n");
		return(MVME_ERROR);
	}

	table = (VME_TABLE2*)mvme->table;

	devHandle = table[2].devHandle;

	result = vme_write( devHandle,vme_addr, src, n_bytes );
	if( result < 0 ){
		perror("[mvme_write] Failed to unmap VME image");
		return(MVME_ERROR);
	}


  return MVME_SUCCESS;
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_read
 *  Description:  Read from VME bus.
 *  Parameters :  @param *mvme		VME structure
 *  		  @param *dst		destination pointer
 *  		  @param vme_addr	source address (VME location)
 *  		  @param n_bytes	requested transfer size
 *  Returns    :  @return		MVME_SUCCESS,ERROR
 * =====================================================================================
 */
int mvme_read( MVME_INTERFACE *mvme, void *dst, mvme_addr_t vme_addr, mvme_size_t n_bytes)
{
	VME_TABLE2 *table;
	int	devHandle,result;
 	UINT32	userMemAddrs;	

	if( n_bytes > DEFAULT_DMA_NBYTES) {
		perror("[mvme_read] Attempt to DMA bytes > DEFAULT_DMA_NBYTES\n");
		return(MVME_ERROR);
	}

	table = (VME_TABLE2*)mvme->table;
	devHandle = table[2].devHandle;

	result = vme_mmap( devHandle, 0x0 , PAGE_SIZE, &userMemAddrs );
	if( result < 0 ){
		perror("[Test: mvme_read] Failed to mmap \n");
		return(MVME_ERROR);
	}else {
		printf(" mmap succeeded \n");
	}

//	result = vme_read( devHandle, 0x0, dst, n_bytes );
//	result = vme_read( devHandle, vme_addr , dst, n_bytes );
	
	memcpy( dst, (UINT8*)userMemAddrs, n_bytes );
	
	result = vme_unmap( devHandle, userMemAddrs, PAGE_SIZE );
	if( result < 0 ){
		perror("[Test: mvme_read] Failed to unmap \n");
		return(MVME_ERROR);
	}

	return(MVME_SUCCESS);
}/* --------------- end of function mvme_read -------------------*/


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_write_value
 *  Description:  Writes a single value
 *  Parameters : @param *mvme		MVME_INTERFACE
 *  		 @param vme_addr	mvme_addr_t
 *  		 @param value		DWORD
 *  Return     : @returns	Error,MVME_SUCCESS
 * =====================================================================================
 */
int mvme_write_value(MVME_INTERFACE *mvme, mvme_addr_t vme_addr, DWORD value)
{
	int devHandle,result;
	VME_TABLE2 *table;

	table = (VME_TABLE2*)mvme->table;

	if (mvme->dmode == MVME_DMODE_D16){
		devHandle = table[0].devHandle;
		result = vme_write( devHandle, (UINT32)vme_addr,(UINT8*)&value , 2 );
		if( result < 0 ){
			perror("[mvme_write_value:D16] Error Could not perform value write !");
			return(MVME_ERROR);
		}
        }else if( mvme->dmode == MVME_DMODE_D32 ){
		devHandle = table[1].devHandle;
		result = vme_write( devHandle, (UINT32)vme_addr, (UINT8*)&value, 4 );
		if( result < 0 ){
			perror("[mvme_write_value:D32] Error Could not perform value write !");
			return(MVME_ERROR);
		 }
	}

	return MVME_SUCCESS;
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_read_value
 *  Description:  Reads a single value D16 or D32
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  		  @param vme_addr	mvme_addr_t
 *  Returns    :  @returns dst		DWORD
 * =====================================================================================
 */
DWORD mvme_read_value(MVME_INTERFACE *mvme, mvme_addr_t vme_addr)
{
	DWORD dst = 0xFFFFFFFF;
  	int devHandle,result;
  	VME_TABLE2 *table;

	table = (VME_TABLE2*)mvme->table;
		 
	if (mvme->dmode == MVME_DMODE_D16){
		devHandle = table[0].devHandle;
		result = vme_read( devHandle, (UINT32)vme_addr, (UINT8*)&dst, 2 );
		if( result < 0 ){
			perror("[mvme_read_value:D16] Error Could not perform value read !");
			return(MVME_ERROR);
		}
	}else if( mvme->dmode == MVME_DMODE_D32 ){
		devHandle = table[1].devHandle;
		result = vme_read( devHandle, (UINT32)vme_addr, (UINT8*)&dst, 4 );
		if( result < 0 ){
			perror("[mvme_read_value:D32] Error Could not perform value read !");
			return(MVME_ERROR);
		}
	}
	 
	return(dst);
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_set_am
 *  Description:  Sets Address Modifier
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  		  @param am		int
 *  Returns    :  @returns	MVME_SUCCESS
 * =====================================================================================
 */
int mvme_set_am( MVME_INTERFACE *mvme, int am )
{
	mvme->am = am;
	return MVME_SUCCESS;

}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_get_am
 *  Description:  Gets Address Modifier
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  		  @param am		int
 *  Returns    :  @returns 	MVME_SUCCESS
 * =====================================================================================
 */
int EXPRT mvme_get_am( MVME_INTERFACE *mvme, int *am )
{
	*am = mvme->am;
	return MVME_SUCCESS;

}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_set_dmode
 *  Description:  Sets dmode
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  		  @param dmode		int
 *  Returns    :  @returns 	MVME_SUCCESS
 * =====================================================================================
 */
int mvme_set_dmode( MVME_INTERFACE *mvme, int dmode )
{
	
	mvme->dmode = dmode;
	return MVME_SUCCESS;
}
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_get_dmode
 *  Description:  gets dmode
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  		  @param dmode		int
 *  Returns    :  @returns 	MVME_SUCCESS
 * =====================================================================================
 */
int mvme_get_dmode( MVME_INTERFACE *mvme, int *dmode )
{
	*dmode = mvme->dmode;
	return MVME_SUCCESS;

}

 /* :TODO:20/02/2009 13:11:23:LCP:  */
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_sysreset
 *  Description:  system reset / software reset
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  Returns    :  @returns 	MVME_SUCCESS
 * =====================================================================================
 */
int mvme_sysreset( MVME_INTERFACE *mvme )
{
	//int result;

	return MVME_SUCCESS;

}

/* :TODO:20/02/2009 13:11:32:LCP:  */
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_set_bIt
 *  Description:  sets interupt
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  		  @param mode		int
 *  Returns    :  @returns 	MVME_SUCCESS
 * =====================================================================================
 */
int mvme_set_bIt( MVME_INTERFACE *mvme, int mode )
{
	return MVME_SUCCESS;
}

/* :TODO:20/02/2009 13:11:51:LCP:  */
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_get_bIt
 *  Description:  gets interupt
 *  Parameters :  @param *mvme		MVME_INTERFACE
 *  		  @param mode		int
 *  Returns    :  @returns 	MVME_SUCCESS
 * =====================================================================================
 */
int mvme_get_bIt( MVME_INTERFACE *mvme, int *mode )
{
	return MVME_SUCCESS;
}


