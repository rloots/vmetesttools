/**********************************************************************

  Name:         v1190A.c
  Created by:   Pierre-Andre Amaudruz

  Contents:     V1190A 64ch. TDC
                
  $Log: v1190A.c,v $
*********************************************************************/
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "v1190A.h"
#include <unistd.h>
#include <byteswap.h>



/*****************************************************************/
/*****************************************************************/
/**
Read Data buffer for single event (check delimiters)
0x4... and 0xC...
@param *mvme VME structure
@param  base Module base address
@param *pdest destination pointer address
@param *nentry number of entries requested and returned.
@return
*/
int v1190A_EventRead(MVME_INTERFACE *mvme, DWORD base, DWORD *pdest, int *nentry)
{
  int cmode;
  DWORD hdata;
  
  *nentry = 0;
  mvme_get_dmode(mvme, &cmode); 
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  while(1){
  if (v1190A_DataReady(mvme, base)) {
    do {
      hdata = mvme_read_value(mvme, base);
    } while (!(hdata & 0x40000000));
    pdest[*nentry] = hdata;
    *nentry += 1;
    do {
       pdest[*nentry] = mvme_read_value(mvme, base);
       *nentry += 1;
    } while (!(pdest[*nentry-1] & 0xc0000000)); 

    nentry--;
    break;
  }//if
  }//while
  mvme_set_dmode(mvme, cmode);
  return *nentry;

/*
  header = *pbase & 0xFF000000;
  
  switch (header) {
  case 0x40000000:  // Global Header 
    break;
  case 0x00000000:  // TDC Header
    break;
  case 0x10000000:  // Data
    break;
  case 0x20000000:  // Error
    break;
  case 0x80000000:  // Trailer
    break;
  case 0xc0000000:  // Filler
    break;
  }
  
  return *nentry;
*/
}

/*****************************************************************/
/**
Read data buffer for nentry data.
@param *mvme VME structure
@param  base Module base address
@param *pdest destination pointer address
@param *nentry number of entries requested and returned.
@return
*/
int v1190A_DataRead(MVME_INTERFACE *mvme, DWORD base, DWORD *pdest, int nentry)
{
  int cmode, status;
  
    mvme_get_dmode(mvme, &cmode); 
    mvme_set_dmode(mvme, MVME_DMODE_D32);
    
	//FILE *fp = fopen("V1190_Dataread.log","a+" );
   // status = mvme_read(mvme, pdest, base, sizeof(DWORD) * nentry); 
    status = mvme_read(mvme, pdest, base, (sizeof(DWORD)*nentry)); 
    //fprintf(fp,"data read: size asked %d , size returned %d \n",sizeof(DWORD)*nentry,status );

    mvme_set_dmode(mvme, cmode);

    if(status != MVME_SUCCESS ){
		//fclose(fp);
		return status;
	}

    
	
	//fclose(fp);
	return nentry;
    /*
     *
      for (i=0 ; i<nentry ; i++) {
      if (!v1190A_DataReady(mvme, base))
      break;
      pdest[i] = mvme_read_value(mvme, base);
      }
      mvme_set_dmode(mvme, cmode);
      return i;
    *
    */
}

/*****************************************************************/
int v1190A_GeoWrite(MVME_INTERFACE *mvme, DWORD base, int geo)
{
  int cmode, data;
 
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  
  mvme_write_value(mvme, base+V1190A_GEO_REG_RW, (geo & 0x1F));
//  printf("[v1190A_GeoWrite] write value = 0x%08x \n", geo & 0x1F ); 
 // printf("[v1190A_GeoWrite] addr = [0x%.08x] \n ", base+V1190A_GEO_REG_RW);
  
  data = mvme_read_value(mvme, base+V1190A_GEO_REG_RW);
  
  printf( " data = 0x%.08x \n ",data ); 
  
  mvme_set_dmode(mvme, cmode);

  return (int) (data & 0x1F); 
}

/*****************************************************************/
void v1190A_SoftReset(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
 
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V1190A_MODULE_RESET_WO, 0);
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v1190A_SoftClear(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
 
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V1190A_SOFT_CLEAR_WO, 0);
  //printf(" cmode = 0x%.08x , addr = 0x%.08x \n ",cmode,base+V1190A_SOFT_CLEAR_WO );
  mvme_set_dmode(mvme, cmode);
}
/*****************************************************************/
void v1190A_SoftEventReset(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
 
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V1190A_SOFT_EVT_RESET_WO, 0);
  mvme_set_dmode(mvme, cmode);
}


/*****************************************************************/
void v1190A_SoftTrigger(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
 
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V1190A_SOFT_TRIGGER_WO, 0);
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
int  v1190A_AlmostFull(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode, data;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  data = mvme_read_value(mvme, base+V1190A_SR_RO);
  mvme_set_dmode(mvme, cmode);
  return ((data>>1) & 0x1);
}

/*****************************************************************/
int  v1190A_DataReady(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode, data;
 
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  data = mvme_read_value(mvme, base+V1190A_SR_RO);
  mvme_set_dmode(mvme, cmode);
  return (data & V1190A_DATA_READY);
}

/*****************************************************************/
int  v1190A_EvtStored(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode, data;
 
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  data = mvme_read_value(mvme, base+V1190A_EVT_STORED_RO);
  mvme_set_dmode(mvme, cmode);
  return (data);
}

/*****************************************************************/
int  v1190A_EvtCounter(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode, data;
 
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  data = mvme_read_value(mvme, base+V1190A_EVT_CNT_RO);
  mvme_set_dmode(mvme, cmode);
  return (data);
}
//*****************************************************************/
DWORD  v1190A_EvtFIFO(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
  DWORD data;
 
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  data = mvme_read_value(mvme, base+0x1038);
  mvme_set_dmode(mvme, cmode);
  return (data);
}

//*****************************************************************/
WORD  v1190A_EvtFIFOStored(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
  WORD data;
 
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  data = mvme_read_value(mvme, base+0x103C);
  mvme_set_dmode(mvme, cmode);
  return (data);
}


/*****************************************************************/
void v1190A_TdcIdList(MVME_INTERFACE *mvme, DWORD base)
{
  int  cmode, i, code;
  DWORD value;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);

  for (i=0; i<2 ; i++) {
    code   = V1190A_MICRO_TDCID | (i & 0x0F);
    value  = v1190A_MicroWrite(mvme, base, code);
    value  = v1190A_MicroRead(mvme, base);
    value = (v1190A_MicroRead(mvme, base) << 16) | value;
    //    printf("Received :code: 0x%04x  0x%08lx\n", code, value); 
  }
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
int  v1190A_ResolutionRead(MVME_INTERFACE *mvme, DWORD base)
{
  WORD  i, code;
  int   cmode, value;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);

  for (i=0; i<2 ; i++) {
    code = V1190A_RESOLUTION_RO | (i & 0x0F);
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroRead(mvme, base);
    // printf("Received RR :code: 0x%04x  0x%08x\n", code, value); 
  }
  mvme_set_dmode(mvme, cmode);
  return value;
}

/*****************************************************************/
void v1190A_LEResolutionSet(MVME_INTERFACE *mvme, DWORD base, WORD le)
{
  int   cmode, value;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16); 

  if ((le == LE_RESOLUTION_100) ||
      (le == LE_RESOLUTION_200) ||
      (le == LE_RESOLUTION_800)) {
    printf("le:%x\n", le);
    value = v1190A_MicroWrite(mvme, base, V1190A_LE_RESOLUTION_WO);
    value = v1190A_MicroWrite(mvme, base, le);
  } else {
    printf("Wrong Leading Edge Resolution -> Disabled\n");
    value = v1190A_MicroWrite(mvme, base, V1190A_LE_RESOLUTION_WO);
    value = v1190A_MicroWrite(mvme, base, 3);
  }
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v1190A_LEWResolutionSet(MVME_INTERFACE *mvme, DWORD base, WORD le, WORD width)
{
  printf("Not yet implemented\n");
}

/*****************************************************************/
void v1190A_AcqModeRead(MVME_INTERFACE *mvme, DWORD base)
{
  int   cmode, value;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);

  value = v1190A_MicroWrite(mvme, base, V1190A_ACQ_MODE_RO);
  value = v1190A_MicroRead(mvme, base);
  //  printf("Received AR :code: 0x%04x  0x%08x\n", V1190A_ACQ_MODE_RO, value); 
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v1190A_TriggerMatchingSet(MVME_INTERFACE *mvme, DWORD base)
{
  int   cmode, value;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);

  value = v1190A_MicroWrite(mvme, base, V1190A_TRIGGER_MATCH_WO);
  //  printf("Received MS :code: 0x%04x  0x%08x\n", V1190A_TRIGGER_MATCH_WO, value); 
  
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v1190A_ContinuousSet(MVME_INTERFACE *mvme, DWORD base)
{
  int   cmode, value;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);

  value = v1190A_MicroWrite(mvme, base, V1190A_CONTINUOUS_WO);
  //  printf("Received CS :code: 0x%04x  0x%08x\n", V1190A_CONTINUOUS_WO, value); 
  
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
/**
Set the width of the matching Window. The width parameter should be 
in the range of 1 to 4095 (0xFFF). Example 0x14 == 500ns.
@param *mvme VME structure
@param  base Module base address
@param width window width in ns units
@return
*/
void v1190A_WidthSet(MVME_INTERFACE *mvme, DWORD base, WORD width)
{
  int   cmode, value;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16); 

  //  v1190A_MicroFlush(mvme, base);
  value = v1190A_MicroWrite(mvme, base, V1190A_WINDOW_WIDTH_WO);
  value = v1190A_MicroWrite(mvme, base, width);
  //  printf("Received WS :code: 0x%04x  0x%08x\n", V1190A_WINDOW_WIDTH_WO, value); 
  
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
/**
Set the offset of the matching window with respect to the trigger.
The offset parameter should be in 25ns units. The range is
from -2048(0x800) to +40(0x28). Example 0xFE8 == 600ns.
@param *mvme VME structure
@param  base Module base address
@param  offset offset in ns units
*/
void v1190A_OffsetSet(MVME_INTERFACE *mvme, DWORD base, WORD offset)
{
  int   cmode, value;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);

  //  v1190A_MicroFlush(mvme, base);
  value = v1190A_MicroWrite(mvme, base, V1190A_WINDOW_OFFSET_WO);
  value = v1190A_MicroWrite(mvme, base, offset);
  //  printf("Received OS :code: 0x%04x  0x%08x\n", V1190A_WINDOW_OFFSET_WO, value); 
  
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v1190A_SetEdgeDetection(MVME_INTERFACE *mvme, DWORD base, int eLeading, int eTrailing)
{
  int cmode, value = 0;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);

  if (eLeading)
    value |= 2;

  if (eTrailing)
    value |= 1;

  //  v1190A_MicroFlush(mvme, base);
  v1190A_MicroWrite(mvme, base, V1190A_EDGE_DETECTION_WO);
  v1190A_MicroWrite(mvme, base, value);
  mvme_set_dmode(mvme, cmode);
}


/*****************************************************************/
int v1190A_MicroWrite(MVME_INTERFACE *mvme, DWORD base, WORD data)
{
  int cmode, i;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
//  mvme_set_am(mvme,MVME_AM_A32_ND);
  for (i=0; i<1000; i++)
  {
    
    WORD microHS = mvme_read_value(mvme, base+V1190A_MICRO_HAND_RO);
    
// printf(" %d 1  microHS = 0x%.08x, data = 0x%.08x,MICRO_WR = 0x%.08x , AM = [%d] \n",i,microHS,data,V1190A_MICRO_WR_OK, mvme->am );
// printf(" 2 v1190A_MicroWrite : mvme_read_value: Address: 0x%08x, value = 0x%08x ,answer = [%d]\n ",base+V1190A_MICRO_HAND_RO,microHS,microHS & V1190A_MICRO_WR_OK );
    
   /* printf("\n[v1190A_MicroWrite] vmebase address 0x%x, data 0x%x, microHS 0x%x, AM = [%2x] ",base,data,microHS,mvme->am ); 
    */

    if (microHS & V1190A_MICRO_WR_OK) {
      //printk("writing value ====================== Who's ur Daddy? ========== \n");
      //printk(" 1  microHS = 0x%x, data = 0x%x,MICRO_WR = 0x%x , AM = [%d] \n",microHS,data,V1190A_MICRO_WR_OK, mvme->am );
      mvme_write_value(mvme, base+V1190A_MICRO_RW, data);
      mvme_set_dmode(mvme, cmode);
      return 1;
    }
    //usleep(100);
    udelay(1000); //1 second
  }
  
  //printk("v1190A_MicroWrite: Micro not ready for writing!\n");
  mvme_set_dmode(mvme, cmode);
  return -1;
}

/*****************************************************************/
int v1190A_MicroRead(MVME_INTERFACE *mvme, const DWORD base)
{
  int cmode, i;
  int reg=-1;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
//  printf("!@#!@#  AM = [%d] \n ", mvme->am);
  for (i=100; i>0; i--) {
    WORD  microHS = mvme_read_value(mvme, base+V1190A_MICRO_HAND_RO);
    if (microHS & V1190A_MICRO_RD_OK) {
      reg = mvme_read_value(mvme, base+V1190A_MICRO_RW);
//      printf("i:%d microHS:%d %x\n", i, microHS, reg);
      mvme_set_dmode(mvme, cmode);
      return (reg);
    }
    udelay(1000);
    //udelay2(500);
  };
  mvme_set_dmode(mvme, cmode);
  return -1;
}

/*****************************************************************/
int v1190A_MicroFlush(MVME_INTERFACE *mvme, const DWORD base)
{
  int cmode, count = 0;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);

  while (1)
    {
      int data = v1190A_MicroRead(mvme, base);
      printf("microData[%d]: 0x%04x\n",count,data);
      if (data < 0)
	break;
      count++;
    }
  mvme_set_dmode(mvme, cmode);
  return count;
}




/*****************************************************************/
/**
Sets all the necessary paramters for a given configuration.
The configuration is provided by the mode argument.
Add your own configuration in the case statement. Let me know
your setting if you want to include it in the distribution.
@param *mvme VME structure
@param  base Module base address
@param mode  Configuration mode number
@param *nentry number of entries requested and returned.
@return MVME_SUCCESS
*/
int  v1190A_Setup(MVME_INTERFACE *mvme, DWORD base, int mode)
{
  WORD code, value;
  int      cmode, status = -1;
    WORD rreg;
   WORD crs;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);

  FILE *fp = fopen("V1190A_Setup.log","a+");
  fprintf(fp," ------======= v1190A_Setup configuration for TDC 0x%08x, Mode <%d> ===========---------  \n",base, mode );
//  v1190A_MicroFlush(mvme, base);
  switch (mode) {
  case 0x1:
    fprintf(fp,"Trigger Matching Setup (mode:%d)\n", mode);
    fprintf(fp,"Default setting + Width : 2000ns, Offset : -1500ns\n");
    fprintf(fp,"Time subtract, Leading Edge only\n");
    code = 0x0000;  // Trigger matching Flag
    
    if ((status = v1190A_MicroWrite(mvme, base, code)) < 0){
    	fprintf(fp,"!!@@@ ERROR Could not set Trigger Matching Mode [status <%d>] @@@!!!!\n",status);
	    return status;
    }

	/*  TRIGGER */
    /*  Matching window width + Offset = 40 clock cycles = 1000ns , 1 clock cycle = 25ns*/
	code = 0x1000;  // Width
    value = v1190A_MicroWrite(mvme, base, code);
  //  value = v1190A_MicroWrite(mvme, base, 0x3e8);   //Width : 25000ns (1000 units)
  //  value = v1190A_MicroWrite(mvme, base, 0x1b8);   //Width : 11000ns (440 units)
    value = v1190A_MicroWrite(mvme, base, 0x4c);   //Width : 1500ns (60 units)
    //value = v1190A_MicroWrite(mvme, base, 0x50);   //Width : 2000ns (80 units)
    
	code = 0x1100;  // Offset
    value = v1190A_MicroWrite(mvme, base, code);
    //value = v1190A_MicroWrite(mvme, base, 0xc40);  // offset: -10000ns (-400==e70 units) [-2048->+40 is 0x800->0x1028 12 bits so 0x1028=0x028] 
    //value = v1190A_MicroWrite(mvme, base, 0xf5f);  // offset: -10000ns (-400==e70 units) [-2048->+40 is 0x800->0x1028 12 bits so 0x1028=0x028] 
    value = v1190A_MicroWrite(mvme, base, 0xFD8);    // offset:-1000ns ( 40 units )
    
	//code = 0x1400;  // Enable Trigger Time subtraction
    //value = v1190A_MicroWrite(mvme, base, code);
//  value = v1190A_MicroWrite(mvme, base, 0x11);

    code = 0x1500;  // Disable Trigger Time Subtraction
    value = v1190A_MicroWrite(mvme, base, code);

	/*  TDC EDGE DETECTION & RESOLUTION */
	code = 0x2200;  // Leading Edge Detection
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroWrite(mvme, base, 0x2);	
    code = 0x2400;  // Set LSB of trailing/leading edge 100ps (default setting)
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroWrite(mvme, base, 0x2);
    code = 0x2811;  // Set Channel Dead time to 100 ns (11)
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroWrite(mvme, base, 0x3);

	/*  TDC READOUT */
//  code = 0x3000;  // Enable TDC Header and Trailer in readout
    code = 0x3100;  // Disable TDC Header and Trailer in readout
    value = v1190A_MicroWrite(mvme, base, code);
    code = 0x3300;  // Maximum number of hits per event
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroWrite(mvme, base, 0x9);

    code = 0x3500;  // Enable TDC error mark
    value = v1190A_MicroWrite(mvme, base, code);
    //code = 0x3600;  // Disable TDC error mark
    //value = v1190A_MicroWrite(mvme, base, code);
	//code = 0x3700;  // Enable TDC bypass if error
    //value = v1190A_MicroWrite(mvme, base, code);
    code = 0x3900;  // Enable TDC internal error type
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroWrite(mvme, base, 0x7ff);
    
	/*  CHANNEL ENABLE */
	code = 0x4200;  // Enable All channels
    value = v1190A_MicroWrite(mvme, base, code);

    mvme_get_dmode(mvme,&cmode);
    mvme_set_dmode(mvme,MVME_DMODE_D16);

  
	
	 //rreg = mvme_read_value(mvme, base+0x1000 );
     //int evt_fifo = ((rreg>>8)&0x1);
	 //fprintf(fp,"[1]  Control Register 0x%08x, Event FIFO Enabled %d \n",rreg, evt_fifo );
	 //if( evt_fifo == 0 ){
	//	 mvme_write_value(mvme, base+0x1000,rreg | 0x100 );
	//	 fprintf(fp,"[2]  Control Register 0x%08x, Event FIFO Enabled %d \n",rreg, ((rreg>>8)&0x1) );
	 //}
    
	 

    /*  READ ALmost Full Register: 0x1022
     *  This register allows the user to set the almost full level of the output Buffer.
     *  FIFO depth : 32K words
     *  min AFL: 1
     *  max AFL: 32735
     *
     *  default level is 64 words.
     *
     *  */
   // rreg = mvme_read_value(mvme, base+0x1022 );
   // printf(" [1] V1990A Setup , almost full reg 0x%08x, \n",rreg );
    
    /*  Set AFL to 0x4000 : decimal  16384*/
    //mvme_write_value(mvme, base+0x1022,0x4000 );
 
    /*  Set AFL to 0x40 : decimal 64 */
    //mvme_write_value(mvme, base+0x1022,0x4000 );

    
    /*  Set AFL to 0x400 : decimal 1024 */
    //mvme_write_value(mvme, base+0x1022,0x400 );

    /*  Set AFL to 0x100 : decimal 256 */
   // mvme_write_value(mvme, base+0x1022,0x100 );

    /*  Set AFL to 0x100 : decimal 512 */
    //mvme_write_value(mvme, base+0x1022,64 );


    /*  read back what was set */
    //  rreg = mvme_read_value(mvme, base+0x1022 );
    //  printf(" [2] V1990A Setup , almost full reg 0x%08x, \n",rreg );


    /*  READ BLT Event Number Register: 0x1024
     *  this register contains (Ne) of complete events which is desirable 
     *  to transfer via BLT. Number of events must be written in 8 bit words.
     *  Default setting is 0x0. this means BLT is disabled.*/
    rreg = mvme_read_value(mvme, base+0x1024 );
    fprintf(fp," [1] V1990A Setup , BLT event reg 0x%08x, \n",rreg );
    
    /*  0x0 NO BLT */
   //mvme_write_value(mvme, base+0x1024,0x0 );
    
    
    /*  0x1 => 1 Event in MEB... Ne := 1 */
    mvme_write_value(mvme, base+0x1024,2 );
    

    /*  0x80 <=> 128 */
    //mvme_write_value(mvme, base+0x1024,0x80 );
    
    /*  0x100 <=> 256 */
    //mvme_write_value(mvme, base+0x1024,0x100 );
   
    /*  0x200 <=> 512 */
    //mvme_write_value(mvme, base+0x1024,0x200 );

    /*  READ BACK WHAT WAS SET */	
    rreg = mvme_read_value(mvme, base+0x1024 );
    fprintf(fp," [2] V1990A Setup , BLT event reg 0x%08x, \n",rreg );
    

        crs = mvme_read_value(mvme, base+0x1022 );
    fprintf(fp," Control Register  0x%08x, \n",crs );
    

    
    mvme_set_dmode(mvme,cmode);  
    break;
  case 0x2:
    code = 0x0500;  // Default configuration
    value = v1190A_MicroWrite(mvme, base, code);
    break;
  case 0x3:
    fprintf(fp,"Trigger Matching Setup (mode:%d) - no headers \n",  mode);
    fprintf(fp,"Default setting + Width : 2000ns, Offset : -1500ns\n");
    fprintf(fp,"Time subtract, Leading Edge only\n");
    code = 0x0000;  // Trigger matching Flag
    if ((status = v1190A_MicroWrite(mvme, base, code)) < 0)
      return status;
    code = 0x1000;  // Width
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroWrite(mvme, base, 0x50);   //was 0x50 Width : 2000ns (80 units)
    code = 0x1100;  // Offset
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroWrite(mvme, base, 0xfc4);  // offset: -1500ns (-60==fc4 units) [-2048->+40 is 0x800->0x28 NOT VALID RELATION] 
    code = 0x1400;  // Enable Trigger Time subtraction
    value = v1190A_MicroWrite(mvme, base, code);
//  value = v1190A_MicroWrite(mvme, base, 0x11);
//  code = 0x1500;  // Subtraction flag
//  value = v1190A_MicroWrite(mvme, base, code);
    code = 0x2200;  // Leading Edge Detection
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroWrite(mvme, base, 0x2);	
    code = 0x2400;  // Set LSB of trailing/leading edge 100ps (default setting)
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroWrite(mvme, base, 0x2);
    code = 0x2811;  // Set Channel Dead time to 100 ns (11)
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroWrite(mvme, base, 0x3);
    code = 0x3100;  // Enable TDC Header and Trailer in readout
    value = v1190A_MicroWrite(mvme, base, code);
    code = 0x3300;  // Maximum number of hits per event
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroWrite(mvme, base, 0x9);
    code = 0x3600;  // Disable TDC error mark
    value = v1190A_MicroWrite(mvme, base, code);
    code = 0x3700;  // Enable TDC bypass if error
    value = v1190A_MicroWrite(mvme, base, code);
//    code = 0x3900;  // Enable TDC internal error type
//    value = v1190A_MicroWrite(mvme, base, code);
//    value = v1190A_MicroWrite(mvme, base, 0x7ff);
    code = 0x4200;  // Enable All channels
    value = v1190A_MicroWrite(mvme, base, code);

    
       break;
  case 0x4:
	fprintf(fp,"Trigger Matching Setup (mode:%d)\n", mode);
    fprintf(fp,"Default setting + Width : 2000ns, Offset : -1500ns\n");
    fprintf(fp,"Time subtract, Leading Edge only\n");
    code = 0x0000;  // Trigger matching Flag
    
    if ((status = v1190A_MicroWrite(mvme, base, code)) < 0){
    	fprintf(fp,"!!@@@ ERROR Could not set Trigger Matching Mode [status <%d>] @@@!!!!\n",status);
	    return status;
    }

	/*  TRIGGER */
    /*  Matching window width + Offset = 40 clock cycles = 1000ns , 1 clock cycle = 25ns*/
	code = 0x1000;  // Width
    value = v1190A_MicroWrite(mvme, base, code);
  //  value = v1190A_MicroWrite(mvme, base, 0x3e8);   //Width : 25000ns (1000 units)
  //  value = v1190A_MicroWrite(mvme, base, 0x1b8);   //Width : 11000ns (440 units)
    value = v1190A_MicroWrite(mvme, base, 0x4c);   //Width : 1500ns (60 units)
    //value = v1190A_MicroWrite(mvme, base, 0x50);   //Width : 2000ns (80 units)
    
	code = 0x1100;  // Offset
    value = v1190A_MicroWrite(mvme, base, code);
    //value = v1190A_MicroWrite(mvme, base, 0xc40);  // offset: -10000ns (-400==e70 units) [-2048->+40 is 0x800->0x1028 12 bits so 0x1028=0x028] 
    //value = v1190A_MicroWrite(mvme, base, 0xf5f);  // offset: -10000ns (-400==e70 units) [-2048->+40 is 0x800->0x1028 12 bits so 0x1028=0x028] 
    value = v1190A_MicroWrite(mvme, base, 0xFD8);    // offset:-1000ns ( 40 units )
    
	//code = 0x1400;  // Enable Trigger Time subtraction
    //value = v1190A_MicroWrite(mvme, base, code);
//  value = v1190A_MicroWrite(mvme, base, 0x11);

    code = 0x1500;  // Disable Trigger Time Subtraction
    value = v1190A_MicroWrite(mvme, base, code);

	/*  TDC EDGE DETECTION & RESOLUTION */
	code = 0x2200;  // Leading Edge Detection
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroWrite(mvme, base, 0x2);	
    code = 0x2400;  // Set LSB of trailing/leading edge 100ps (default setting)
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroWrite(mvme, base, 0x2);
    code = 0x2811;  // Set Channel Dead time to 100 ns (11)
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroWrite(mvme, base, 0x3);

	/*  TDC READOUT */
//  code = 0x3000;  // Enable TDC Header and Trailer in readout
    code = 0x3100;  // Disable TDC Header and Trailer in readout
    value = v1190A_MicroWrite(mvme, base, code);
    code = 0x3300;  // Maximum number of hits per event
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroWrite(mvme, base, 0x9);

    code = 0x3500;  // Enable TDC error mark
    value = v1190A_MicroWrite(mvme, base, code);
    //code = 0x3600;  // Disable TDC error mark
    //value = v1190A_MicroWrite(mvme, base, code);
	//code = 0x3700;  // Enable TDC bypass if error
    //value = v1190A_MicroWrite(mvme, base, code);
    code = 0x3900;  // Enable TDC internal error type
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroWrite(mvme, base, 0x7ff);
    
	/*  CHANNEL ENABLE */
	code = 0x4200;  // Enable All channels
    value = v1190A_MicroWrite(mvme, base, code);

    mvme_get_dmode(mvme,&cmode);
    mvme_set_dmode(mvme,MVME_DMODE_D16);

   
	
	 //rreg = mvme_read_value(mvme, base+0x1000 );
     //int evt_fifo = ((rreg>>8)&0x1);
	 //fprintf(fp,"[1]  Control Register 0x%08x, Event FIFO Enabled %d \n",rreg, evt_fifo );
	 //if( evt_fifo == 0 ){
	//	 mvme_write_value(mvme, base+0x1000,rreg | 0x100 );
	//	 fprintf(fp,"[2]  Control Register 0x%08x, Event FIFO Enabled %d \n",rreg, ((rreg>>8)&0x1) );
	 //}
    
	 

    /*  READ ALmost Full Register: 0x1022
     *  This register allows the user to set the almost full level of the output Buffer.
     *  FIFO depth : 32K words
     *  min AFL: 1
     *  max AFL: 32735
     *
     *  default level is 64 words.
     *
     *  */
   // rreg = mvme_read_value(mvme, base+0x1022 );
   // printf(" [1] V1990A Setup , almost full reg 0x%08x, \n",rreg );
    
    /*  Set AFL to 0x4000 : decimal  16384*/
    //mvme_write_value(mvme, base+0x1022,0x4000 );
 
    /*  Set AFL to 0x40 : decimal 64 */
    //mvme_write_value(mvme, base+0x1022,0x4000 );

    
    /*  Set AFL to 0x400 : decimal 1024 */
    //mvme_write_value(mvme, base+0x1022,0x400 );

    /*  Set AFL to 0x100 : decimal 256 */
   // mvme_write_value(mvme, base+0x1022,0x100 );

    /*  Set AFL to 0x100 : decimal 512 */
    //mvme_write_value(mvme, base+0x1022,64 );


    /*  read back what was set */
    //  rreg = mvme_read_value(mvme, base+0x1022 );
    //  printf(" [2] V1990A Setup , almost full reg 0x%08x, \n",rreg );


    /*  READ BLT Event Number Register: 0x1024
     *  this register contains (Ne) of complete events which is desirable 
     *  to transfer via BLT. Number of events must be written in 8 bit words.
     *  Default setting is 0x0. this means BLT is disabled.*/
    //rreg = mvme_read_value(mvme, base+0x1024 );
    //fprintf(fp," [1] V1990A Setup , BLT event reg 0x%08x, \n",rreg );
    
    /*  0x0 NO BLT */
   //mvme_write_value(mvme, base+0x1024,0x0 );
    
    
    /*  0x1 => 1 Event in MEB... Ne := 1 */
    mvme_write_value(mvme, base+0x1024,1 );
    

    /*  0x80 <=> 128 */
    //mvme_write_value(mvme, base+0x1024,0x80 );
    
    /*  0x100 <=> 256 */
    //mvme_write_value(mvme, base+0x1024,0x100 );
   
    /*  0x200 <=> 512 */
    //mvme_write_value(mvme, base+0x1024,0x200 );

    /*  READ BACK WHAT WAS SET */	
    rreg = mvme_read_value(mvme, base+0x1024 );
    fprintf(fp," [2] V1990A Setup , BLT event reg 0x%08x, \n",rreg );
    

    crs = mvme_read_value(mvme, base+0x1022 );
    fprintf(fp," Control Register  0x%08x, \n",crs );
    

    
    mvme_set_dmode(mvme,cmode);  
    break;

  default:
    fprintf(fp,"Unknown setup mode\n");
    mvme_set_dmode(mvme, cmode);
    fprintf(fp," ========================================== \n" );
    fclose(fp);
    return -1;
  }
  v1190A_Status(mvme, base);
  mvme_set_dmode(mvme, cmode);
   fprintf(fp," ========================================== \n" );
  fclose(fp);
  return 0;
}


/*****************************************************************/
/**
Read and return the current trigger configuration
@param *mvme VME structure
@param  base Module base address
@param Configuration structure for return
@return MVME_SUCCESS, MicroCode error
*/
int v1190A_TriggerConfig(MVME_INTERFACE *mvme, DWORD base, short *trigger_config)
{
  //WORD  i; 
  WORD code; 
  //WORD pair=0;
  int   cmode, value;
  
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  code = 0x1600;
  if ((value = v1190A_MicroWrite(mvme, base, code)) < 0){
	printf("Erorr opening microwrite\n");
    	return -value;
	}
  value = v1190A_MicroRead(mvme, base);
  trigger_config[0]=value;
  //printf("  Match Window width       : 0x%04x\n", value);  
  value = v1190A_MicroRead(mvme, base);
  trigger_config[1]=value;
  //printf("  Window offset            : 0x%04x\n", value);  
  value = v1190A_MicroRead(mvme, base);
  trigger_config[2]=value;
  //printf("  Extra Search Window Width: 0x%04x\n", value);  
  value = v1190A_MicroRead(mvme, base);
  trigger_config[3]=value;
  //printf("  Reject Margin            : 0x%04x\n", value);  
  value = v1190A_MicroRead(mvme, base);
  trigger_config[4]=value;
  //printf("  Trigger Time subtration  : %s\n",(value & 0x1) ? "y" : "n");  
 return value;
}

/*****************************************************************/
/**
Read and display the curent status of the TDC.
@param *mvme VME structure
@param  base Module base address
@return MVME_SUCCESS, MicroCode error
*/
int v1190A_Status(MVME_INTERFACE *mvme, DWORD base)
{
  WORD  i, code, pair=0;
  int   cmode, value;
  
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
//  mvme_set_am(mvme,MVME_AM_A32_ND);	
  //-------------------------------------------------
  printf("V1190A with base address 0x%04u : -- \n",base);
  printf("\n--- Trigger Section [0x1600]:\n");
  code = 0x1600;
  if ((value = v1190A_MicroWrite(mvme, base, code)) < 0){
    printf(" result = [%d] \n ",value );
    return -value;
  }
  value = v1190A_MicroRead(mvme, base);
  printf("  Match Window width       : 0x%04x\n", value);  
  value = v1190A_MicroRead(mvme, base);
  printf("  Window offset            : 0x%04x\n", value);  
  value = v1190A_MicroRead(mvme, base);
  printf("  Extra Search Window Width: 0x%04x\n", value);  
  value = v1190A_MicroRead(mvme, base);
  printf("  Reject Margin            : 0x%04x\n", value);  
  value = v1190A_MicroRead(mvme, base);
  printf("  Trigger Time subtration  : %s\n",(value & 0x1) ? "y" : "n");  


  //-------------------------------------------------
  printf("\n--- Edge Detection & Resolution Section[0x2300/26/29]:\n");
  code = 0x2300;
  value = v1190A_MicroWrite(mvme, base, code);
  pair = value = v1190A_MicroRead(mvme, base);
  printf("  Edge Detection (1:T/2:L/3:TL)           : 0x%02x\n", (value&0x3));  
  code = 0x2600;
  value = v1190A_MicroWrite(mvme, base, code);
  value = v1190A_MicroRead(mvme, base);
  if (pair==0x3) {
    value = v1190A_MicroRead(mvme, base);
    printf("  Leading Edge Resolution (see table)     : 0x%02x\n", (value&0x3));  
    printf("  Pulse Width Resolution (see table)      : 0x%02x\n", ((value>>8)&0xF));  
  } else {
    printf("  Resolution [ps] (0:800/1:200/2:100)     : 0x%02x\n", (value&0x3));  
  }
  code = 0x2900;
  value = v1190A_MicroWrite(mvme, base, code);
  value = v1190A_MicroRead(mvme, base);
  printf("  Dead Time between hit [~ns](5/10/30/100): 0x%02x\n", (value&0x3));  
  //ss_sleep(10);
  //-------------------------------------------------
  printf("\n--- Readout Section[0x3200/34/3a/3c]:\n");
  code = 0x3200;
  value = v1190A_MicroWrite(mvme, base, code);
  value = v1190A_MicroRead(mvme, base);
  printf("  Header/Trailer                            : %s\n",(value & 0x1) ? "y" : "n");  
  code = 0x3400;
  value = v1190A_MicroWrite(mvme, base, code);
  value = v1190A_MicroRead(mvme, base);
  printf("  Max #hits per event 2^n-1 (>128:no limit) : %d\n", value&0xF);  
  code = 0x3a00;
  value = v1190A_MicroWrite(mvme, base, code);
  value = v1190A_MicroRead(mvme, base);
  printf("  Internal TDC error type (see doc)         : 0x%04x\n", (value&0x7FF));  
  code = 0x3c00;
  value = v1190A_MicroWrite(mvme, base, code);
  value = v1190A_MicroRead(mvme, base);
  printf("  Effective size of readout Fifo 2^n-1      : 0x%04x\n", (value&0xF));  
  
  //-------------------------------------------------
  printf("\n--- Channel Enable Section[0x4500/47/49]:\n");
  
  
  code = 0x4500;
  value = v1190A_MicroWrite(mvme, base, code);
  value = v1190A_MicroRead(mvme, base);
  printf("  Read Enable Pattern [  0..15 ] : 0x%04x\n", value);  
  value = v1190A_MicroRead(mvme, base);
  printf("  Read Enable Pattern [ 16..31 ] : 0x%04x\n", value);  
  value = v1190A_MicroRead(mvme, base);
  printf("  Read Enable Pattern [ 32..47 ] : 0x%04x\n", value);  
  value = v1190A_MicroRead(mvme, base);
  printf("  Read Enable Pattern [ 48..63 ] : 0x%04x\n", value);  
  value = v1190A_MicroRead(mvme, base);
  printf("  Read Enable Pattern [ 64..79 ] : 0x%04x\n", value);  
  value = v1190A_MicroRead(mvme, base);
  printf("  Read Enable Pattern [ 80..95 ] : 0x%04x\n", value);  
  value = v1190A_MicroRead(mvme, base);
  printf("  Read Enable Pattern [ 96..111] : 0x%04x\n", value);  
  value = v1190A_MicroRead(mvme, base);
  printf("  Read Enable Pattern [112..127] : 0x%04x\n", value);  
  
  code = 0x4700;
  value = v1190A_MicroWrite(mvme, base, code);
  value = v1190A_MicroRead(mvme, base);
  value = (v1190A_MicroRead(mvme, base)<<16) | value;
  printf("  Read Enable Pattern 32 (0) : 0x%08x\n", value);  
  
  
  code = 0x4701;
  value = v1190A_MicroWrite(mvme, base, code);
  value = v1190A_MicroRead(mvme, base);
  value = (v1190A_MicroRead(mvme, base)<<16) | value;
  printf("  Read Enable Pattern 32 (1) : 0x%08x\n", value);  

  //-------------------------------------------------
  printf("\n--- Adjust Section[0x5100/60]:\n");
  code = 0x5100;
  value = v1190A_MicroWrite(mvme, base, code);
  value = v1190A_MicroRead(mvme, base);
  printf("  Coarse Counter Offset: 0x%04x\n", (value&0x7FF));  
  value = v1190A_MicroRead(mvme, base);
  printf("  Fine   Counter Offset: 0x%04x\n", (value&0x1F));  
  printf("\nMiscellaneous Section:\n");
  for (i=0; i<2 ; i++) {
    code = 0x6000 | (i & 0x0F);
    value = v1190A_MicroWrite(mvme, base, code);
    value = v1190A_MicroRead(mvme, base);
    value = (v1190A_MicroRead(mvme, base) << 16) | value;
    printf("  TDC ID(%i)  0x%08x  [code:0x%04x]\n", i, value, code);  
  }
  mvme_set_dmode(mvme, cmode);
  return 0;
}

/*****************************************************************/
#ifndef HAVE_UDELAY
// this is the VMIC version of udelay()
int udelay(int usec)
{
  int i, j, k = 0;
  for (i=0; i<133; i++)
    for (j=0; j<usec; j++)
      k += (k+1)*j*k*(j+1);
  return k;
}

/*
int udelay2(int usec)
{
  int i, j, k = 0;
  for (i=0; i<133; i++)
    for (j=0; j<usec; j++)
      k += (k+1)*sin(i)*cos(j)*(j+1);
  return k;
}
*/
#endif

/********************************************************************/
/*-PAA- For test purpose only */
#ifdef MAIN_ENABLE
int main () {
  
  MVME_INTERFACE *myvme;

  DWORD VMEIO_BASE = 0x780000;
  DWORD V1190A_BASE = 0xF10000;
  int status, csr, i;
  DWORD    cnt, array[10000];


  // Test under vmic   
  status = mvme_open(&myvme, 0);

  // Set am to A24 non-privileged Data
//  mvme_set_am(myvme, MVME_AM_A24_ND);

  // Set dmode to D16
  mvme_set_dmode(myvme, MVME_DMODE_D16);

  // Get Firmware revision
  csr = mvme_read_value(myvme, V1190A_BASE+V1190A_FIRM_REV_RO);
  printf("Firmware revision: 0x%x\n", csr);
  
  // Print Current status 
 v1190A_Status(myvme, V1190A_BASE);

  // Set mode 1
  // v1190A_Setup(myvme, V1190A_BASE, 1);

  csr = v1190A_DataReady(myvme, V1190A_BASE);
  printf("Data Ready: 0x%x\n", csr);

  // Read Event Counter
  cnt = v1190A_EvtCounter(myvme, V1190A_BASE);
  printf("Event counter: 0x%lx\n", cnt);

  memset(array, 0, sizeof(array));

  // Set dmode to D32
  mvme_set_dmode(myvme, MVME_DMODE_D32);

  // Set 0x3 in pulse mode for timing purpose
  mvme_write_value(myvme, VMEIO_BASE+0x8, 0xF); 

  // Write pulse for timing purpose
  mvme_write_value(myvme, VMEIO_BASE+0xc, 0x2);

  mvme_set_blt(myvme, MVME_BLT_BLT32);

  // Read Data
  v1190A_DataRead(myvme, V1190A_BASE, array, 500);

  // Write pulse for timing purpose
  mvme_write_value(myvme, VMEIO_BASE+0xc, 0x8);

  for (i=0;i<12;i++)
    printf("Data[%i]=0x%lx\n", i, array[i]);

  memset(array, 0, sizeof(array));

  // Event Data
  /*
  status = 10;
  v1190A_EventRead(myvme, V1190A_BASE, array, &status);
  printf("count: 0x%x\n", status);
  for (i=0;i<12;i++)
    printf("Data[%i]=0x%x\n", i, array[i]);
  */

  status = mvme_close(myvme);
  return 0;
}	
#endif

