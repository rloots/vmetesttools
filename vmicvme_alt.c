/*
 * =====================================================================================
 *
 *       Filename:  vmicvme_alt.c
 *
 *    Description:  VME enhacned interface for the Concurrent VME board processor(tsi148)
 *    		    using the mvmestd.h VME call convention.This modification is 
 *    		    done outside the Midas source tree, and should be treated as 
 *    		    experimental.
 * 		    ( :WARNING:01/22/2009 01:52:07 PM:LCP )
 *
 *    References:   This code was taken from Pierre-Andre Amaudruz vmicvme.c source 
 *                  file. We applied modifications to fit our tsi148 chip.   		    
 *
 *        Version:  1.0
 *        Created:  01/22/2009 12:44:03 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Bases Sciences
 *
 * =====================================================================================
 */
/* #####   HEADER FILE INCLUDES   ################################################### */
#include	<string.h>
#include	<stdlib.h>
#include	<stdio.h>
#include	<signal.h>
#include	<assert.h>
#include	<unistd.h>
#include	<asm/page.h>
//#include 	<sys/mman.h>
#include 	<sys/ioctl.h>
#include 	<errno.h>

/* #####   HEADER FILE INCLUDES -Local-  ############################################ */
#include 	"vmicvme_alt.h"
//#include 	"vme_api_en.h"
//#include	"vme_types.h"
//#include	"vme_driver.h"


/* #####   MACROS  -  LOCAL TO THIS SOURCE FILE   ################################### */
#define A32_CHUNK	0x00FFFFFF
#define DEBUG		0
//#define MAIN_ENABLE

#define VME_MASTER_MAGIC 0x114a0002
#define VME_MASTER_MAGIC_NULL 0x0




/* #####   DATA TYPES  -  LOCAL TO THIS SOURCE FILE   ############################### */
vme_bus_handle_t	bus_handle;

vme_interrupt_handle_t	int_handle;

struct	sigevent	event;
struct	sigaction	handler_act;
INT_INFO		int_info;
UINT32 _VME_BASE_ADDR_  = 0xb0000;



/* #####   PROTOTYPES  -  LOCAL TO THIS SOURCE FILE   ############################### */

mvme_size_t	FullWsze(int am);
int vcon_mmap( MVME_INTERFACE *mvme, mvme_addr_t vme_addr, mvme_size_t n_bytes);
mvme_addr_t vcon_mapcheck( MVME_INTERFACE *mvme, mvme_addr_t vme_addr, mvme_size_t n_bytes );

int vme_init( vme_bus_handle_t *handle );
int vme_term( vme_bus_handle_t devHandle );


/** TEST FUNCTIONS */
//void simpleTestDMADirectTransfer( int devdma, VME_DIRECT_TXFER data );

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  FullWsze
 *  Description:  Returns the largest Window size based on the address modifier. For 
 *  Parameters :  @param am		// address modifier
 *  		  @return mvme_size_t	// MVME size_t structure 		  
 * =====================================================================================
 */
mvme_size_t FullWsze( int am ) 
{
	switch ( am & 0xF0 ) {
		case 0x00:	
			return A32_CHUNK;
		case 0x30:	
			return 0xFFFFFF;
		case 0x20:	
			return 0xFFFF;
		default:	
			return 0;
	}/* -----  end switch  ----- */
}/* ------ end of function FullWsze ------ */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_open
 *  Description:  Open VME channel. One bus handle per crate.
 *  Parameters :  @param *mvme	// VME structure
 *  		  @param index	// VME interface index
 *  		  @return  MVME_SUCCESS,ERROR
 * =====================================================================================
 */
int mvme_open (MVME_INTERFACE **mvme, int index) 
{	
	vme_bus_handle_t devHandle;
	int devHandledma = 0;
	int result;
	DMA_INFO* info;


	/* ==== MIDAS STUFF === */
	/* Allocate MVME_INTERFACE */
	*mvme = (MVME_INTERFACE*)calloc(1,sizeof(MVME_INTERFACE));

	/* Allocate MVME_TABLE for the *mvme interface */
	(*mvme)->table = (char*)malloc(MAX_VME_SLOTS*sizeof(VME_TABLE));
	
	/* Initialize the table, fill it with 0 with the size = MAX_VME_SLOTS*sizeof(VME_TABLE) */
	memset( (char*)(*mvme)->table, 0, MAX_VME_SLOTS*sizeof(VME_TABLE) );

	/* Set Default Parameters */
	(*mvme)->am = MVME_AM_DEFAULT;
       /* ===================== */	


       /* === CONCURRENT VME STUFF  === */
       /* Init device */
       result = vme_init(  (vme_bus_handle_t*) &((*mvme)->handle) );
       if( result < 0 ){
		perror("Error initializing the Concurrent vme bus");
		(*mvme)->handle=0;
		return(MVME_NO_INTERFACE);
	}
	

	/* Create D16 Pci Image with iomap */
        
        /* Create D32 Pci Image with iomap */
	
        /* Create D32 DMA direct transfer Access */
	info = (DMA_INFO*) calloc(1,sizeof(DMA_INFO));
	
	(*mvme)->info = info;

	
	/* ===================== */
	/* ==== MIDAS STUFF === */
	/* Opening the vme device seems to be a success
	 * Show the results... :P
	 */

	//(*mvme)->handle = (vme_bus_handle_t*)devHandle; 
		
	(*mvme)->handle = (vme_bus_handle_t)devHandle; 
		
	(*mvme)->info = info;

	printf("mvme_open:[Concurrent tsi148] \n");
	printf("VME Bus Handle			= 0x%x\n",(*mvme)->handle);
	printf("DMA Handle			= 0x%x\n",devHandledma);
	printf("DMA area size			=  %d bytes \n",DEFAULT_DMA_NBYTES);
	printf("DMA physical address		=  %p\n",(unsigned long *)info->dma_ptr);

	return(MVME_SUCCESS);
}/* --------------- end of function mvme_open -------------------*/


int vme_pci_d16_create()
{
	

}

int vme_pci_d32_create()
{


}



void simpleTestDMADirectTransfer( int devdma, EN_VME_DIRECT_TXFER data ) 
{

	UINT32 *buffer;
	int result;
	UINT32 offset = 0;
	UINT32 length = 32;
	int i;
	int width = 256;


	buffer  = (UINT32 *)malloc(width*sizeof(UINT32));
	
	
	result = vme_dmaDirectTransfer( devdma , &data );
	if( result < 0 ){
		perror("DMA transfer failled");
		return(MVME_ERROR);
	}
	else {

		result = vme_read( devdma, offset, buffer, width );
		if( result > 0 ){
			
			for( i = 0; i < width; i++ ) {
				printf(" %02x ",buffer[i]);
			}
			printf("\n");
		}
	}

	result = vme_freeDmaBuffer( devdma );
	if(result < 0){
		perror(" Error freeing dma buffer!\n");
		return(MVME_ERROR);
	}	


}




/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_close
 *  Description:  close and release(free) ALL openend VME channels
 *  Parameters :  @param *mvme	VME_structure
 *  		  @return MVME_SUCCESS,ERROR
 * =====================================================================================
 */
int mvme_close ( MVME_INTERFACE *mvme )
{
	//int 		j;
	int 		result;
	//VME_TABLE 	*table = NULL;
	//DMA_INFO   	*info = NULL;
	int 		devHandle;

	//table = ((VME_TABLE*)mvme)->table;
	///info  = ((DMA_INFO*)mvme)->info;

	printf("mvme_close:\n");
	printf("Bus handle		= 0x%x\n",mvme->handle);
//	printf("DMA handle		= 0x%x\n",(int)(info)->dma_handle);
//	printf("DMA physical address	= %p\n",(unsigned long *)info->dma_ptr);

	devHandle = (int)mvme->handle;
	

	/* 1. Close DMA channel */
	result =  vme_freeDmaBuffer(devHandle);
	if( result < 0 ){
		perror("Error - Could not free DMA buffer " );
		return(MVME_ACCESS_ERROR);
	}

	free(mvme->info);			/* Free DMA allocation */
	
	/* 2. Close all the window handles */
//	for(j=0;j<MAX_VME_SLOTS;j++) {
//		if(table[j].valid){
//
//			result = vme_closeDevice( devHandle ); /* ^^^^Incorrect^^^^ */
//			
//			if( result < 0 ){
//				perror("Error Closing handle");
//				return(MVME_ACCESS_ERROR);
//			}
//		}
//	} /* :WORKAROUND:01/23/2009 09:11:21 AM:mr: Needs more work... unsure of table structure and how to obtain width from it! */
	

	/* free table pointer */
	free (mvme->table);
	mvme->table = NULL;

	/* close bus device handle */
	//f( mvme>handle != 0 ) {
	//	result = vme_closeDevice( devHandle );
//		if( result <  0 ) {
//			perror(" Error in closing handle ");
//			return(MVME_ACCESS_ERROR);
//		}
//	}

	/* free mvme block */
	free(mvme);

	return(MVME_SUCCESS);
}/* --------------- end of function mvme_close -------------------*/


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_read
 *  Description:  Read from VME bus.
 *  Parameters :  @param *mvme		VME structure
 *  		  @param *dst		destination pointer
 *  		  @param vme_addr	source address (VME location)
 *  		  @param n_bytes	requested transfer size
 *  		  @return		MVME_SUCCESS,ERROR
 * =====================================================================================
 */
int mvme_read( MVME_INTERFACE *mvme, void *dst, mvme_addr_t vme_addr, mvme_size_t n_bytes)
{
	int		result;
	DMA_INFO 	*info;
	UINT8 *buffer;
	int devHandle;

	EN_VME_DIRECT_TXFER data;
	EN_PCI_IMAGE_DATA idata;



	info = ((DMA_INFO*)mvme->info);

	int i;
	mvme_addr_t addr;
	
        /* -- Single Access */
	//addr = vcon_mapcheck( mvme, vme_addr, n_bytes );

//	if( mvme->dmode == MVME_DMODE_D8 ) {

//	  char* cdst = (char*)dst;
//	  char* csrc = (char*)addr;
//	  for (i=0; i<n_bytes; i+=sizeof(char)){
//	    *cdst++  = *csrc++;
//	  }

//	} else if (mvme->dmode == MVME_DMODE_D16) {

//	  WORD* sdst = (WORD*)dst;
//	  WORD* ssrc = (WORD*)addr;
//	  for (i=0; i<n_bytes; i+=sizeof(WORD)){
//	    *sdst++  = *ssrc++;
//	  }
//	} else 


//	if (mvme->dmode == MVME_DMODE_D32) {
	  
	   
	   vme_closeDevice(mvme->handle); 	
	   	
	   memset( (char*)&idata, 0 , sizeof(EN_PCI_IMAGE_DATA));	
	   
	   devHandle = vme_openDevice("lsi3");	
	    
	   if( devHandle < 0 ) {
		printf("[1] error couldnt open image lsi3 \n ");
		printf(" devHandle = %d \n", devHandle );
		printf(" mvme->handle = %d \n", mvme->handle );

	        return(ERROR);
	   }else {	

          		
		   idata.pciAddress=0xd0000000;
		   idata.pciAddressUpper=0;
		   idata.vmeAddress=0xb0000;
		   idata.vmeAddressUpper=0;
		   idata.size = 0x10000;
		   idata.sizeUpper = 0;
		   idata.readPrefetch=1;
		   idata.prefetchSize= 3;
		   idata.sstMode = TSI148_SST320;
		   idata.dataWidth = EN_VME_D32;
		   idata.addrSpace = EN_VME_A32;
		   idata.type = EN_LSI_DATA;
		   idata.mode = EN_LSI_USER;
		   idata.vmeCycle = 0;
		   idata.sstbSel =0;
		   idata.ioremap = 1;

		   result = vme_enablePciImage( devHandle, &idata );		   
		   if( result < 0 ) {
			printf(" [2] couldn't enable pciImage\n ");
			return(ERROR);
		   }		
			 
		   result = vme_read(devHandle , 0, dst , n_bytes );  
		   
		   if( result < 0 ){
			printf(" vme read error ");
			return(ERROR);
		   } else {
		   	printf(" Read Success @@@@@@@@@@@@@@@@@@@ ");
	     	   }		
	   	   	   

		   result = vme_disablePciImage( devHandle );
		   if( result < 0 ) {
			   printf(" error closing iamge ");
			   return(ERROR);
		   } 
			
		   vme_closeDevice(devHandle );
//		   printf("imaged closed\n "); 
		   
		  
	   
	   }
    
	//  DWORD* ddst = (DWORD*)dst;
///	  DWORD* dsrc = (DWORD*)addr;
//	  for (i=0; i<n_bytes; i+=sizeof(DWORD)){
//	    *ddst++  = *dsrc++;
//	  }
//	} else {
//	  fprintf(stderr,"mvme_read: Invalid dmode %d\n",mvme->dmode);
//	  return(ERROR);
//	}
	
	return(MVME_SUCCESS);
}/* --------------- end of function mvme_read -------------------*/



/*-----------------------------------------------------------------------------
 *  Helper Section!!!
 *-----------------------------------------------------------------------------*/
void *vme_master_window_phys_addr( vme_bus_handle_t bus_handle,
				   vme_master_handle_t handle )
{
	/*
	if( (NULL == handle) || (VME_MASTER_MAGIC != handle->magic) ){
		errno = EINVAL;
		return NULL;
	}
	*/
	return NULL;
	//return handle->ctl.paddr;
}


int vme_create_dma_buffer( vme_bus_handle_t devHandle,
	                   vme_dma_handle_t *devHandledma, 
			   size_t size, 
			   int flags, 
			   void * phys_addr )
{






	return(MVME_SUCCESS);
}



int vme_init( vme_bus_handle_t *handle )
{
	//int result;
	int* devHandle;

	if( handle == NULL ){
		perror("Error, Initialized devHandle is set to NULL!!!");
		return(MVME_INVALID_PARAM);
	}

	*devHandle = vme_openDevice("ctl");
	
	if( devHandle >= 0  ) {
		//handle = &devHandle; /* Set device/bus handle */
		handle = devHandle; /* Set device/bus handle */
	}
	else {
	       perror("Error, Initialization of ctl interface failed");
		return(MVME_NO_INTERFACE);

	}

	//vme_closeDevice(*handle);

	return(MVME_SUCCESS);
}


int vme_term( vme_bus_handle_t devHandle )
{
	
	int result;
	if( devHandle == 0 ){
		perror("Error, Device Handle is NULL cannot close device!");
		return(MVME_NO_INTERFACE);
	}
	
	result = vme_closeDevice( (UINT32)devHandle );
	if( result < 0 ){
		perror("Error, Could NOT close Device");
		return(MVME_NO_INTERFACE);
	}

	devHandle = -1;	
	return(MVME_SUCCESS);	
}


void *vme_mmap_phys( int devHandle, unsigned long paddr, size_t size )
{
	unsigned long mapaddr, off, mapsize;
	size_t ps = getpagesize();
	UINT32 *ptr;
	int result;

	/* Align Page */
	off = paddr % ps;
	mapaddr = paddr - off;
	mapsize = size + off;
	mapsize += (mapsize % ps) ? ps - (mapsize % ps) : 0 ;

	result = vme_mmap( devHandle , off, mapsize, ptr );
	if( result <  0 ){
		perror(" Error in mmap to phys!");
		return NULL;
	}

	return ptr;
}

int vme_unmap_phys( unsigned long vaddr, size_t size,  int devHandle )
{
	unsigned long mapaddr, off, mapsize;
	size_t	ps = getpagesize();
	int result;

	off = vaddr % ps;
	mapaddr = vaddr - off;
	mapsize = size + off;
	mapsize += (mapsize % ps ) ? ps - (mapsize % ps ) : 0 ;

	result = vme_unmap( devHandle, (UINT32)vaddr, (UINT32)mapsize );
        
	if( result < 0 ){
		perror(" Error in unmap!");
		return(ERROR);
	}	

	return(MVME_SUCCESS);
}


/*-----------------------------------------------------------------------------
 *  create pci/vme image windows
 *-----------------------------------------------------------------------------*/
int vme_master_window_create_( vme_bus_handle_t bus_handle,
			      vme_master_handle_t *handle,
			      UINT32 vme_addr, int am, 
			      size_t size, int flags, 
			      void *phys_addr, 
			      INT32 devHandle )
{

	EN_PCI_IMAGE_DATA idata;
	int result;

	if( handle == NULL ){
		perror("Error, handle is NULL!");
		return(MVME_NO_INTERFACE);
	}

	//*handle = (vme_master_handle_t) malloc( sizeof(struct _vme_master_handle));
	if( NULL == handle ) {
		return -1;
	}
	

	idata.vmeAddress = vme_addr;
	idata.size = size;
	idata.addrSpace = am;
	idata.pciAddress = phys_addr;
	
	//(*handle)->ctl.vaddr = vme_addr;
	//(*handle)->ctl.size = size;
	//(*handle)->ctl.am = am;
	//(*handle)->ctl.flags = flags;
	//(*handle)->ctl.paddr = (phys_addr) ? phys_addr : NULL;	
	//(*handle)->deviceHandle = devHandle;	

	result = vme_enablePciImage( devHandle , &idata );
	if( result < 0 ) {
		perror("Error Failed to init Image\n");
		return(MVME_ACCESS_ERROR);
	}
	

	// (*handle)-> magic  = VME_MASTER_MAGIC; /* TO BE CHANGED */	
			

	return(MVME_SUCCESS);
}

int vme_master_window_release( vme_bus_handle_t bus_handle,
			       vme_master_handle_t handle )
{

	//int rval = 0;
	int result;


	if( handle == 0 ) {
		perror("Error: Handle passed as NULL couldn't close image");
		return(MVME_ACCESS_ERROR);
	}

	result = vme_disablePciImage(handle);
	if( result < 0 ){
		perror("Error: Could not close device!");
		return(MVME_ACCESS_ERROR);
	}

	
	return(MVME_SUCCESS);
}

void* vme_master_window_phys_paddr( vme_bus_handle_t bus_handle,
				   vme_master_handle_t *handle )
{
	if( handle == NULL ){
		perror("Error");
		return(MVME_ACCESS_ERROR);
	}

				
	//return (*handle)->ctl.paddr;
	
	return NULL;
}


void* vme_master_window_mmap( vme_bus_handle_t bus_handle,
			     vme_master_handle_t *handle,
			     int flags )
{

	//int result;
	//UINT32 offset, length;
	UINT32 *uptr;

	if( handle == NULL ){
		perror("Error!");
		return(MVME_ACCESS_ERROR);
	}

	
	//result = vme_mmap_phys( bus_handle , (unsigned long)(*handle)->ctl.paddr, (*handle)->ctl.size );
	
	//(*handle)->vptr = uptr;

	return uptr;
}


/* Will be revised */
void* vme_master_window_unmmap( vme_bus_handle_t bus_handle,
		               vme_master_handle_t handle )
{
	
	if( handle == NULL ) {
		perror("Error!");
		return(MVME_ACCESS_ERROR);
	}

	//return vme_unmap_phys( (unsigned long) handle->vptr, handle->ctl.size );

	return NULL;
}



int vcon_mmap( MVME_INTERFACE *mvme, mvme_addr_t vme_addr, mvme_size_t n_bytes)
{


	int off,mapaddr,mapsize,size;
	int ps = getpagesize();
	

	int j;
	void *phys_addr = NULL;
	VME_TABLE *table;


	table = (VME_TABLE*)mvme->table;
	
	off = vme_addr % ps;
	mapaddr = vme_addr - off;
	mapsize = size + off;
	mapsize += (mapsize % ps ) ? ps - (mapsize % ps ) : 0 ;



	j = 0;

	while( table[j].valid ) {
		j++;	

		if( j < MAX_VME_SLOTS ) {
			table[j].low = vme_addr;
			table[j].am  = ((mvme->am == 0) ? MVME_AM_DEFAULT : mvme->am);
			table[j].nbytes = n_bytes;


			if( vme_master_window_create_( (vme_bus_handle_t)mvme->handle,
						      &table[j].wh,
						      table[j].low,
						      table[j].am,
						      table[j].nbytes,
						      0,
						      NULL, mvme->handle)) {
				perror("Error creating the window");
				return(MVME_ACCESS_ERROR);
			}

			table[j].ptr = (DWORD*)vme_master_window_mmap((vme_bus_handle_t)mvme->handle, table[j].wh, 0);

			if( table[j].ptr == NULL ){
				perror("Error mapping the window");
				vme_master_window_release((vme_bus_handle_t)mvme->handle,table[j].wh);
				table[j].wh = 0;
				return(MVME_ACCESS_ERROR);
			}

			phys_addr - vme_master_window_phys_addr((vme_bus_handle_t)mvme->handle,table[j].wh);

			if( phys_addr == NULL ){
				perror("vme_master_window_phys_addr");
				fprintf(stderr,"vcon_mmap: Mapped VME AM 0x%02x , addr 0x%08x, size 0x%08x at address %p\n",table[j].am ,
															    (int)vme_addr,
															    (int)n_bytes,
															    phys_addr);
				table[j].valid = 1;
				table[j].high  = table[j].low + table[j].nbytes;
			}
		}
		else{
			return(MVME_ACCESS_ERROR);
		}
		
	}


	return(MVME_SUCCESS);
}




int vcon_unmap( MVME_INTERFACE *mvme, mvme_addr_t vme_addr, mvme_size_t size )
{
	int 		j,result;
	VME_TABLE	*table;
	

	table = (VME_TABLE*)mvme->table;


	for( j = 0; table[j].valid; j++ ) {
		if( (vme_addr == table[j].low) && ((vme_addr+size) == table[j].high )){
			break;
		}
	}

	if( !table[j].valid ) {
		return(MVME_SUCCESS);
	}

	/* remove map */
	if( table[j].ptr ) {
		result = vme_master_window_unmmap( (vme_bus_handle_t)mvme->handle, table[j].wh );
		if( result < 0 ){
			perror("vme_master_window_unmap");
			return(ERROR);
		}

		result = vme_master_window_release( (vme_bus_handle_t)mvme->handle, table[j].wh );
		if(result < 0 ){
			perror("vme_master_window_release");
			return(ERROR);
		}
	}

	table[j].wh = 0;
	return(MVME_SUCCESS);
}


mvme_addr_t vcon_mapcheck( MVME_INTERFACE *mvme, mvme_addr_t vme_addr, mvme_size_t n_bytes )
{
	int j;
	int result;
	VME_TABLE *table;
	mvme_addr_t addr;
	
	table = (VME_TABLE*)mvme->table;
	
	for( j=0; table[j].valid; j++ ){
		if( mvme->am != table[j].am )
			continue;

		if( (vme_addr >= table[j].low) &&
		    ((vme_addr + n_bytes) < table[j].high) ){
			break;
		}
	}
	
	if( !table[j].valid ){
		if( vme_addr > A32_CHUNK ){
			addr = vme_addr & (~A32_CHUNK);
	        }else{
			addr = 0x00000000;
		}

		result = vcon_mmap( mvme, addr, mvme->am );
		if(result < 0 ){
			perror("cannot create vme map window");
			abort();
		}
	}
	
	for(j = 0; table[j].valid; j++ ){
		if( mvme->am != table[j].am ){
			continue;
		}
		
		if( (vme_addr >= table[j].low) &&
		    ((vme_addr + n_bytes) < table[j].high) ){
			break;
		}
	}

	if( !table[j].valid ){
		perror("Map not found at j = %d ");
		printf(" j = %d \n ", j );
		abort();
	}

	addr = (mvme_addr_t)(table[j].ptr) + (mvme_addr_t)(vme_addr - table[j].low);
	
	return addr;
}

void Write_Value( MVME_INTERFACE *myvme,  mvme_addr_t vme_Addr, EN_PCI_IMAGE_DATA *data, UINT8 *Value  )
{
  int devHandle, result;
  UINT32 Offset;
  int numbyts = 0;	
  UINT32 pci_addr = 0x00000000;	

  if (myvme->dmode == MVME_DMODE_D16){
 	
//	printf("\n\t\t\t D16 Mode \t "); 
	data->pciAddress=pci_addr;
        data->pciAddressUpper=0;
        data->pciBusSpace=0;
	data->vmeAddress=_VME_BASE_ADDR_;
        data->vmeAddressUpper=0;
        data->size = 0x10000;
        data->sizeUpper = 0;
        data->postedWrites = 1;
	data->readPrefetch=1;
        data->prefetchSize= 3;
        data->dataWidth = EN_VME_D16;
        data->addrSpace = EN_VME_A32;
        data->type = EN_LSI_DATA;
        data->mode = EN_LSI_USER;
        data->vmeCycle = 0;
        data->sstbSel =0;
        data->ioremap = 1;
	numbyts = 2;

  }
  else if (myvme->dmode == MVME_DMODE_D32){
 	
//	printf("\n\t\t\t D32 Mode \t "); 
	data->pciAddress=pci_addr;
	data->pciAddressUpper=0;
        data->vmeAddress=_VME_BASE_ADDR_;
        data->vmeAddressUpper=0;
        data->size = 0x10000;
        data->sizeUpper = 0;
        data->readPrefetch=1;
        data->prefetchSize= 3;
        data->dataWidth = EN_VME_D32;
        data->addrSpace = EN_VME_A32;
        data->type = EN_LSI_DATA;
        data->mode = EN_LSI_USER;
        data->vmeCycle = 0;
        data->sstbSel =0;
        data->ioremap = 1;
	numbyts = 4;

  }
//  	 printf("\n VME Base addr = 0x%02x \n ", _VME_BASE_ADDR_ );
//	 printf(" VME Offset + VME BASE  = 0x%02x \n ", vme_Addr );
//	 Offset =  vme_Addr - _VME_BASE_ADDR_;
//	 printf(" VME Addr - VME BASE  = 0x%02x \n ", Offset );

        Offset = vme_Addr - _VME_BASE_ADDR_;
	devHandle = vme_openDevice("lsi6");
	if( devHandle < 0 ){
		printf("[Write_Value] Unable to open PCI image lsi6, to write value %x \n ",*Value);
		return(MVME_ERROR);
	}

	
	result = vme_enablePciImage(devHandle, data );
	if( result < 0 ) {
		printf("[Write_value] Failed to enable PCI Image\n");
		return(MVME_ERROR);
	}
	
//	printf("\n\t [Trying to Write] devHandle = [%d], Offset = [0x%02x], SRC = [0x%02x], numbyts = [%d] \n ", devHandle, 
//											       Offset,
//											       *Value,
//											       numbyts );
	result = vme_write( devHandle, Offset , Value , numbyts );
	
	if( result < 0 ) {
		printf("[Write_value] Failed to write data \n");
	}else {
//	   printf(" Value Read = <<<< %d >>>> \n ", addr );
	}

	result = vme_disablePciImage( devHandle );
	if( result < 0 ){
		printf("[Write_value] Failed to disable PCI Image\n");
		return(MVME_ERROR);
	}
	vme_closeDevice(devHandle);
}



int mvme_write_value(MVME_INTERFACE *mvme, mvme_addr_t vme_addr, DWORD value)
{
  mvme_addr_t addr;
  UINT8 vv = (UINT8)value;
  
  EN_PCI_IMAGE_DATA idata;
     
  memset( (char*)&idata, 0 , sizeof(EN_PCI_IMAGE_DATA));

  Write_Value( mvme,  vme_addr, &idata, &vv );

  return MVME_SUCCESS;
}



void Read_Value( MVME_INTERFACE *myvme,  mvme_addr_t vme_Addr, EN_PCI_IMAGE_DATA *data, DWORD *DST )
{
  int devHandle, result;
  UINT32 Offset;
  int numbyts;	
  UINT32 pci_addr = 0x00000000;	

  if (myvme->dmode == MVME_DMODE_D16){
 	
	data->pciAddress=pci_addr;
        data->pciAddressUpper=0;
        data->vmeAddress=_VME_BASE_ADDR_;
        data->vmeAddressUpper=0;
        data->size = 0x10000;
        data->sizeUpper = 0;
        data->readPrefetch=1;
        data->prefetchSize= 3;
        data->sstMode = TSI148_SST320;
        data->dataWidth = EN_VME_D16;
        data->addrSpace = EN_VME_A32;
        data->type = EN_LSI_DATA;
        data->mode = EN_LSI_USER;
        data->vmeCycle = 0;
        data->sstbSel =0;
        data->ioremap = 1;
	numbyts = 2;

  }
  else if (myvme->dmode == MVME_DMODE_D32){
 	
	data->pciAddress=pci_addr;
        data->pciAddressUpper=0;
        data->vmeAddress=_VME_BASE_ADDR_;
        data->vmeAddressUpper=0;
        data->size = 0x10000;
        data->sizeUpper = 0;
        data->readPrefetch=1;
        data->prefetchSize= 3;
        data->sstMode = TSI148_SST320;
        data->dataWidth = EN_VME_D32;
        data->addrSpace = EN_VME_A32;
        data->type = EN_LSI_DATA;
        data->mode = EN_LSI_USER;
        data->vmeCycle = 0;
        data->sstbSel =0;
        data->ioremap = 1;
	numbyts = 4;
  }


  	Offset = vme_Addr - _VME_BASE_ADDR_;

	devHandle = vme_openDevice("lsi0");
	if( devHandle < 0 ){
		printf("[Read_Value] Unable to open PCI image lsi0 \n ");
		return(MVME_ERROR);
	}

	
	result = vme_enablePciImage(devHandle, data );
	if( result < 0 ) {
		printf("[Read_value] Failed to enable PCI Image\n");
		return(MVME_ERROR);
	}
	result = vme_read( devHandle, Offset , DST, numbyts );
	if( result < 0 ) {
		printf("[Read_value] Failed to read data \n");
	}else {
	//   printf(" Value Read = <<<< %d >>>> \n ", addr );
	}

	result = vme_disablePciImage( devHandle );
	if( result < 0 ){
		printf("[Read_value] Failed to disable PCI Image\n");
		return(MVME_ERROR);
	}
	vme_closeDevice(devHandle);
}





DWORD mvme_read_value(MVME_INTERFACE *mvme, mvme_addr_t vme_addr)
{
  mvme_addr_t addr;
  DWORD dst = 0xFFFFFFFF;
  EN_PCI_IMAGE_DATA idata;
     
  memset( (char*)&idata, 0 , sizeof(EN_PCI_IMAGE_DATA));

  Read_Value( mvme,  vme_addr, &idata, &dst );

      return(dst);
}




int mvme_set_am( MVME_INTERFACE *mvme, int am )
{
	mvme->am = am;
	return MVME_SUCCESS;

}

int EXPRT mvme_get_am( MVME_INTERFACE *mvme, int *am )
{
	*am = mvme->am;
	return MVME_SUCCESS;

}

int mvme_set_dmode( MVME_INTERFACE *mvme, int dmode )
{
	
	mvme->dmode = dmode;
	return MVME_SUCCESS;
}

int mvme_get_dmode( MVME_INTERFACE *mvme, int *dmode )
{
	*dmode = mvme->dmode;
	return MVME_SUCCESS;

}

int mvme_sysreset( MVME_INTERFACE *mvme )
{
	int result;

//	result = 	

//	if( 
	return MVME_SUCCESS;

}




#ifdef MAIN_ENABLE


int main() 
{
	int status;
	int myinfo = VME_INTERRUPT_SIGEVENT;
	
	MVME_INTERFACE *myvme;
	int vmeio_status;


	status = mvme_open( &myvme, 0 );
	status = mvme_set_am( &myvme, 0x09 );
	status = mvme_set_dmode( myvme, MVME_DMODE_D32 );
	status = mvme_read( myvme, &vmeio_status, 0x4078001C, 4 );
	printf(" VMEIO Status : 0x%x \n", vmeio_status );
	
	mvme_write_value( myvme, 0x40780010, 0x0 );
	printf(" Value : 0x%lx \n", mvme_read_value(myvme, 0x40780018 ) );
	
	mvme_write_value( myvme, 0x40780010, 0x3 );
	printf(" Value : 0x%lx \n", mvme_read_value(myvme, 0x40780018 ) );

	status = mvme_close(myvme);



	return 0;
}
#endif




