#include "v830.h"
#include <stdio.h>
#include <assert.h>

/**
 * Read and display the curent status of the module.
 * @param *mvme VME structure
 * @param  base Module base address
 * @return MVME_SUCCESS
 */
int  v82X_Status(MVME_INTERFACE *mvme, DWORD base) {
	int   cmode;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	//-------------------------------------------------
	printf("\n--- Control Register:\n");
	int csr = mvme_read_value(mvme, base+V82X_CR_RW);
	printf("  CSR                      : 0x%04x\n", csr);
	printf("  CSR ACQ MODE             : 0x%04x\n", csr&0x3);
	printf("  CSR HEADER ENA           : 0x%04x\n", csr&0x32);
	
	printf("\n--- Status Register:\n");
	int sta = mvme_read_value(mvme, base+V82X_ST_RO);
	printf("  STA                      : 0x%02x\n", sta&0xff);
	printf("  DREADY                   : 0x%02x\n", sta&0x01);
	printf("  ALMOST FULL              : 0x%02x\n", sta&0x02);
	printf("  FULL                     : 0x%02x\n", sta&0x04);
	printf("  GLOBAL DREADY            : 0x%02x\n", sta&0x08);
	printf("  GLOBAL BUSY              : 0x%02x\n", sta&0x10);
	printf("  TERM ON                  : 0x%02x\n", sta&0x20);
	printf("  TERM OFF                 : 0x%02x\n", sta&0x40);
	printf("  BERR                     : 0x%02x\n", sta&0x80);
	
	printf("\n--- Interrupt:\n");
	int v = mvme_read_value(mvme, base+V82X_IRL_RW);
	printf("  ILR                      : 0x%02x\n", v&0x7);
	
	
	//-------------------------------------------------
	
	mvme_set_dmode(mvme, cmode);
	return 0;
}
		
/*****************************************************************/
void v82X_SoftReset(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
 
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V82X_MODULE_RESET_WO, 0);
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v82X_SoftClear(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
 
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V82X_SOFT_CLEAR_WO, 0);
  mvme_set_dmode(mvme, cmode);
}

/**
 * Set Acquisition Mode to Trigger Random
 */
void v82X_SetTriggerRandom(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V82X_CR_SET_WO, 0x1);
  mvme_write_value(mvme, base+V82X_CR_CLEAR_WO, 0x2);
  mvme_set_dmode(mvme, cmode);
}

/**
 * Set Acquisition Mode to Trigger Disabled
 */
void v82X_SetTriggerDisabled(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V82X_CR_CLEAR_WO, 0x1);
  mvme_write_value(mvme, base+V82X_CR_CLEAR_WO, 0x2);
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v82X_SoftTrigger(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V82X_SOFT_TRIGGER_WO, 0);
  mvme_set_dmode(mvme, cmode);
}

/**
 * Read one Counter
 * @param *mvme VME structure
 * @param  base Module base address
 * @param n counter number (0-31)
 */
int v82X_ReadCounter(MVME_INTERFACE *mvme, DWORD base, int n)
{
	int cmode;
	DWORD v;
	assert( n>=0 && n<32);
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D32);
	v=mvme_read_value(mvme, base+V82X_COUNTER_BASE_RO+n*4);
	mvme_set_dmode(mvme, cmode);
	return v;
}
/**
 * Read Trigger Counter
 * @param *mvme VME structure
 * @param  base Module base address
 */
int v830_ReadTriggerCounter(MVME_INTERFACE *mvme, DWORD base)
{
	int cmode;
	DWORD v;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D32);
	v=mvme_read_value(mvme, base+V830_TRIG_CNT_RO);
	mvme_set_dmode(mvme, cmode);
	return v;
}
/**
 * Read Event Number
 * @param *mvme VME structure
 * @param  base Module base address
 */
int v830_EvtCounter(MVME_INTERFACE *mvme, DWORD base)
{
	int cmode;
	WORD v;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	v=mvme_read_value(mvme, base+V830_EVT_CNT_RO);
	mvme_set_dmode(mvme, cmode);
	return v;
}


/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
