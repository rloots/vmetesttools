//#include "test.h"
#include <stdlib.h>
#include <sys/time.h>

#include <stdio.h>
#include <string.h>
#include <curses.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <asm/page.h>
#include "vconvme.h"
#include "v792.h"
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include <byteswap.h>

	DWORD VMEIO_BASE = 0x0;
	DWORD V792_BASE = 0xb0000;
  
	MVME_INTERFACE *myvme;
int done;
int CTRL_devHandle;

int getVerror(int timeout)
{
    int retval;
    UINT32 intNum;
    UINT32 selectedInts;
    EN_VME_INT_INFO info;
	int result, i;

    /*
     * Note: This function is not thread safe - good enough for a test
     */

    selectedInts = 1L <<EN_IRQ2;
    intNum = 0;
    retval = vme_waitInterrupt(CTRL_devHandle, selectedInts,
                               timeout, &intNum);
    printf(" Int received 0x%08x \n",intNum );
    info.intNum = EN_IRQ2; /* get info for VIRQ1 */

        result = vme_readInterruptInfo(CTRL_devHandle, &info );
        if ( result < 0 )
        {
            printf("Error - failed to read interrupt info (%s)\n", 
                        strerror(errno));
        }
        else
        {
            printf("Number of interrupts since last call: %u\n", info.numOfInts);
            for ( i = 0; i < info.vecCount; i++ )
            {
                printf("%02d     0x%02X\n", i, info.vectors[i]);
            }

	}
    return( (retval < 0) ? retval : ((intNum >> 1)&0xf));
}

void sighandler(int num)
{
    done = 1;
}

void *verr_thread(void *arg)
{
    int rc;

    printf("Thread starting\n");
    while (1)
    {
        pthread_testcancel();
        rc = getVerror(0);
	//rc = 2;
        if(rc < 0)
        {
            /*
             * Call will timeout - we need the timeout to allow the
             * thread to be cancelled. Note current Solaris driver has a 
             * bug where it does not set the error code for 'ETIME' in the
             * case of a timeout.
             */
            if((errno != ETIME) && (errno !=0))
            {
                printf("Error - failed to receive interrupt, errno %d (%s)\n", 
                       errno, strerror(errno));
            }
        }
        else
        {
			//printf(" int received = %d \n",rc );
			if( rc == 2 ){
				DWORD temp[128/sizeof(DWORD)];
				int bb = 128;
				int dmaret = mvme_read(myvme,temp,0xb0000,bb);
				if( dmaret > 0 ){
					int j;
					for(j = 0; j < bb; j++)
						printf(" 0x%08x \n",temp[j] );	
				}
			}

        }
    }
}

int main (int argc, char* argv[]) {
	int status, csr, i;

	if (argc>1) {
		sscanf(argv[1],"%lx",&V792_BASE);
	}

	mvme_open(&myvme,0);
	//v792_SoftReset(myvme,V792_BASE);
	printf(" dword swap 0x%08x \n",(0x101022ed) );
	v792_Setup(myvme,V792_BASE,1);
    v792_DataClear(myvme, V792_BASE );
		{
		WORD     threshold[32];
		for (i=0;i<V792_MAX_CHANNELS;i++) {
			if( i >= 0 && i <= 16 ){
				threshold[i] = 0x000;
			}
			else{ 
				threshold[i] = 0x7ff;
			}
		//	printf("Threshold[%2i] = 0x%4.4x\t   -  \n ", i, threshold[i]);

		}
		
		v792_ThresholdWrite(myvme, V792_BASE, threshold);
	}


	DWORD *temp;
	int bb = 128;
	status = mvme_close(myvme);

    pthread_t thread_id;

    printf("VME Int Test\n");

    signal(SIGINT, sighandler);

    /*  Open the Control device */
    CTRL_devHandle = vme_openDevice( "ctl" );
    if ( CTRL_devHandle < 0 )
    {   
        printf("Error - failed to open ctl\n");
        exit(0);   
    }
 
    printf("Starting thread.....\n");
    pthread_create(&thread_id, NULL, verr_thread, NULL);

    printf("Sleeping....\n");
    sleep(10);

    printf("Writing - press ctrl-C to exit.....\n");

    while(!done)
    {
        printf("looping...\n");
        sleep(1);
    }
    printf("Exit on signal\n");

    /* Shutdown thread */
    printf("Cancel thread\n");
    pthread_cancel(thread_id);
    pthread_join(thread_id, NULL);

    printf("Close control device\n");
    vme_closeDevice(CTRL_devHandle);

    printf("Exit\n");

	return 1;
}	

/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
