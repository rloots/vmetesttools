/**
 * @file
 */

#ifndef  V785_INCLUDE_H
#define  V785_INCLUDE_H

#include <stdio.h>
#include <string.h>
#include "mvmestd.h"



class CAENv7XX {
public:
	union StatusRegister1 {
		int raw;
		struct {
			unsigned DataReady:1; // bit0
			unsigned GlobalDataReady:1;
			unsigned Busy:1;
			unsigned GlobalBusy:1;
			unsigned Amnesia:1;
			unsigned Purge:1;
			unsigned TermOn:1;
			unsigned TermOff:1;
			unsigned EventReady:1; //bit 8
		};
		StatusRegister1():raw(0){};
	};
	union StatusRegister2 {
		int raw;
		struct {
			unsigned _pad_1:1; //bit0
			unsigned BufferEmpty:1;
			unsigned BufferFull:1;
			unsigned _pad_2:1;
			unsigned PB:4;
			//unsigned DSEL0:1;
			//unsigned DSEL1:1;
			//unsigned CSEL0:1;
			//unsigned CSEL1:1;
		};
		StatusRegister2():raw(0){};
	};
	union ControlRegister1 {
		int raw;
		struct {
			unsigned _pad_1:2;
			unsigned BlkEnd:1;
			unsigned _pad_2:1;
			unsigned ProgReset:1;
			unsigned BErr:1;
			unsigned Align64:1;
		};
		ControlRegister1():raw(0){};
	};
	union BitSet2Register {
		int raw;
		struct {
			unsigned MemTest:1;
			unsigned OffLine:1;
			unsigned ClearData:1;
			unsigned OverRange:1;
			unsigned LowThresh:1;
			unsigned _pad_1:1;//bit5
			unsigned TestAcq:1;
			unsigned SLDEnable:1;
			unsigned StepTH:1;
			unsigned _pad_2:2;//bits 9-10
			unsigned AutoIncr:1;
			unsigned EmptyProg:1;
			unsigned SlideSubEnable:1;
			unsigned AllTrg:1;
		};
		BitSet2Register():raw(0){};
		void clear() {raw=0;};
	};

	enum DataType {
		DATA=0,
		HEADER=2,
		FOOTER=4,
		INVALID=6
	};
	union DataHeader {
		DWORD raw;
		struct {
			unsigned _pad_1:8;
			unsigned cnt:6;
			unsigned _pad_2:2;
			unsigned crate:8;
			unsigned type:3;
			unsigned geo:5;
		};
		DataHeader(DWORD v) { raw=v; };
	};
	union DataFooter {
		DWORD raw;
		struct {
			unsigned evtCnt:24;
			unsigned type:3;
			unsigned geo:5;
		};
		DataFooter(DWORD v) { raw=v; };
	};

	union DataEntry {
		DWORD raw;
		struct {
			unsigned adc:12;
			unsigned ov:1;
			unsigned un:1;
			unsigned _pad_1:2;
			unsigned channel:5;
			unsigned _pad_2:3;
			unsigned type:3;
			unsigned geo:5;
		};
		DataEntry(DWORD v) { raw=v; };
	};


public:
	CAENv7XX(MVME_INTERFACE *_vme,DWORD _base):
		evtCnt(0),
		mvme(_vme),base(_base)
		{};
	int DataReady();
	int ThresholdWrite();
	void ThresholdRead();
	void getStatus();
	DWORD EvtCntRead();
	void EvtCntReset();
	void StatusRegister1Read();
	void StatusRegister2Read();
	void ControlRegister1Read();
	void ControlRegister1Write();
	void BitSet2RegisterRead();
	void BitSet2RegisterSet(BitSet2Register& r);
	void BitSet2RegisterClear(BitSet2Register& r);
public:
	static const int MAX_CHANNELS=32;
	static const DWORD FIRM_REV=(0x1000),
		GEO_ADDR_RW   = (0x1002),
		BIT_SET1_RW   = (0x1006),
		BIT_CLEAR1_WO = (0x1008),
		SR1_RO        = (0x100E),
		CR1_RW        = (0x1010),
		SINGLE_RST_WO = (0x1016),
		SR2_RO        = (0x1022),
		EVT_CNT_L_RO  = (0x1024),
		EVT_CNT_H_RO  = (0x1026),
		INCR_EVT_WO   = (0x1028),
		INCR_OFFSET_WO= (0x102A),
		BIT_SET2_RW   = (0x1032),
		BIT_CLEAR2_WO = (0x1034),
		TEST_EVENT_WO = (0x103E),
		EVT_CNT_RST_WO= (0x1040),
		THRES_BASE    = (0x1080),
		SOFT_RESET    = (0x1<<7);
	WORD threshold[MAX_CHANNELS];
	DWORD evtCnt;
	StatusRegister1 sr1;
	StatusRegister2 sr2;
	ControlRegister1 cr1;
	BitSet2Register bs2r;
protected:
	static const DWORD REG_BASE=(0x1000);
	MVME_INTERFACE * const mvme;
	const DWORD base;
};

/**
 * CAEN VME peak ADC V785.
 * Very similar to charge ADC V792
 */
class CAENv785 : public CAENv7XX {
public:
	CAENv785(MVME_INTERFACE *_vme,DWORD _base):
		CAENv7XX(_vme,_base){};
	int DataRead(DWORD *pdest, int nentry=MAX_CHANNELS+2);
public:
	static const DWORD CR1_RW = (0x100E);
};


#endif

/* emacs
 * Local Variables:
 * mode:C++
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
