/*
 * =====================================================================================
 *
 *       Filename:  invptrtest.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  03/15/2012 04:30:26 PM
 *       Revision:  none
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog:
 * =====================================================================================
 */

#include <stdlib.h>


int main()
{
	char *x;

	x = (char*) malloc(sizeof(char)*10);
	x[9] = 'a';
	free(x);

	

	return 0;
}
