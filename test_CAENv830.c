#include "mvmestd.h"
#include "daqsrt.hh"
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <cstring>
#include <cassert>

#include "v830.h"



int MAXEVENT=1000000000;

#include <sys/time.h>
inline double tdiff(struct timeval T1, struct timeval T2) {
	return ( static_cast<double>(T2.tv_sec) - static_cast<double>(T1.tv_sec) ) +
		(static_cast<double>(T2.tv_usec) - static_cast<double>(T1.tv_usec))/1000000.0;
}


int main (int argc, char* argv[]) {

	DWORD V82X_BASE = 0x40000;
	MVME_INTERFACE *myvme;

	if (argc>1) {
		sscanf(argv[1],"%lx",&V82X_BASE);
	}
	
	// Test under vmic   
	int status = mvme_open(&myvme, 0);
	if (status!=1) exit(1);
  
	// Set am to A24 non-privileged Data
	mvme_set_am(myvme, MVME_AM_A24_ND);

	// Set dmode to D16
	mvme_set_dmode(myvme, MVME_DMODE_D16);
	
	// Get Firmware revision
	int csr = mvme_read_value(myvme, V82X_BASE+V82X_FIRM_REV_RO);
	printf("Firmware revision: 0x%x\n", csr&0xff);
	if (csr==0xff || csr==0xffff) return 1;
	
	v82X_SoftReset(myvme, V82X_BASE);
	v82X_SetTriggerRandom(myvme, V82X_BASE);
	//v82X_SetTriggerDisabled(myvme, V82X_BASE);
	v82X_Status(myvme, V82X_BASE);
	
	v82X_SoftClear(myvme, V82X_BASE);
	sleep(1);
	printf("Trigger Counter: %d\n",v830_ReadTriggerCounter(myvme, V82X_BASE));
	printf("Event Number: %d\n",v830_EvtCounter(myvme, V82X_BASE));
	for (int i=0;i<32;i++) {
		int v=v82X_ReadCounter(myvme, V82X_BASE,i);
		printf("Counter[%2d]=%8d\n",i,v);
	}
	v82X_SoftTrigger(myvme, V82X_BASE);
	for (int j=0;j<MAXEVENT;j++) {
		usleep(1000000);
		v82X_SoftTrigger(myvme, V82X_BASE);
		printf("Trigger Counter: %d\n",v830_ReadTriggerCounter(myvme, V82X_BASE));
		printf("Event Number: %d\n",v830_EvtCounter(myvme, V82X_BASE));
		{
			int MEB_use_head=0;
			int cmode;
			mvme_get_dmode(myvme, &cmode);
			mvme_set_dmode(myvme, MVME_DMODE_D32);
			if (MEB_use_head) {
				int head=mvme_read_value(myvme,V82X_BASE +V830_MEB_BASE_RO);
				printf("MEBhead=%08x\n",head);
				for (int i=1;i<33;i++){
					int v=mvme_read_value(myvme,V82X_BASE +V830_MEB_BASE_RO+i*4);
					printf("MEB[%2d]=%8d\n",i,v);
				}
			} else {
				const int nw=32;
				DWORD v[nw];
				mvme_read(myvme,v,V82X_BASE +V830_MEB_BASE_RO,nw*4);
				for (int i=0;i<nw;i++){
					printf("%8d ",v[i]);
					if (i%8==7) printf("\n");
				}
				printf("\n");
			}
			mvme_set_dmode(myvme, cmode);
		}
// 		for (int i=0;i<32;i++) {
// 			int v=v82X_ReadCounter(myvme, V82X_BASE,i);
// 			printf("Counter[%2d]=%8d\n",i,v);
// 		}
		if (j%1==0) v82X_SoftClear(myvme, V82X_BASE);	
	}
		
	
	status = mvme_close(myvme);
	return 0;
}	

/* emacs
 * Local Variables:
 * mode:C++
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
