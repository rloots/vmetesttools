/*
 * =====================================================================================
 *
 *       Filename:  vmecon.c
 *
 *    Description:  VME enhacned interface for the Concurrent VME board processor(tsi148)
 *    		    using the mvmestd.h VME call convention.This modification is 
 *    		    done outside the Midas source tree, and should be treated as 
 *    		    experimental.
 * 		    ( :WARNING:01/22/2009 01:52:07 PM:LCP )
 *
 *    References:   This code was taken from Pierre-Andre Amaudruz vmicvme.c source 
 *                  file. We applied modifications to fit our tsi148 chip & universe chip.   		    
 *
 *        Version:  1.0
 *        Created:  01/22/2009 12:44:03 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Bases Sciences
 *
 * =====================================================================================
 */
#include	<string.h>
#include	<stdlib.h>
#include	<stdio.h>
#include	<signal.h>
#include	<assert.h>
#include	<unistd.h>
#include	<asm/page.h>
#include 	<sys/ioctl.h>
#include 	<errno.h>


/* #####   HEADER FILE INCLUDES -Local-  ############################################ */
#include 	"vmecon.h"
#include 	"vmecondrv.h" 


#define _VME_BASE_ADDR 0xb0000

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_open
 *  Description:  Open VME channel. One bus handle per crate.
 *  Parameters :  @param *mvme	// VME structure
 *  		  @param index	// VME interface index
 *  		  @return  MVME_SUCCESS,ERROR
 * =====================================================================================
 */
int mvme_open (MVME_INTERFACE **mvme, int index) 
{	
	vme_bus_handle_t devHandle;
	int devHandledma = 0;
	int result;
	DMA_INFO* info;
	VME_TABLE2 *table;

	if( index != 0 ){
		perror("[mvme_open]: index != 0 ");
		(*mvme)->handle = 0;
		return(MVME_INVALID_PARAM);
	}


	
	/* ==== MIDAS STUFF === */
	/* Allocate MVME_INTERFACE */
	*mvme = (MVME_INTERFACE*)calloc(1,sizeof(MVME_INTERFACE));

	/* Allocate MVME_TABLE for the *mvme interface */
	(*mvme)->table = (char*)malloc(MAX_VME_SLOTS*sizeof(VME_TABLE2));
	
	/* Initialize the table, fill it with 0 with the size = MAX_VME_SLOTS*sizeof(VME_TABLE) */
	//memset( (char*)(*mvme)->table, 0, MAX_VME_SLOTS*sizeof(VME_TABLE2) );

	/* Set Default Parameters */
	(*mvme)->am = MVME_AM_DEFAULT;
       /* ===================== */	


       /* === CONCURRENT VME STUFF  === */
       /* Init device */
       result = vme_init(  (vme_bus_handle_t*) &((*mvme)->handle) );
       if( result < 0 ){
		perror("Error initializing the Concurrent vme bus");
		(*mvme)->handle=0;
		return(MVME_NO_INTERFACE);
	}

	       


	mvme_size_t res = 0x10000;

	result = vcon_init_devices( *mvme, _VME_BASE_ADDR,res);
	if( result < 0 ){
		perror("[mvme_open] Failed to init devices\n");
		(*mvme)->handle=0;
		return(MVME_NO_INTERFACE);
	}	
	
	table = (VME_TABLE*)(*mvme)->table;
	int k = 0;
	while( k < 3 ){
		printf(" Handles in mvme table are %u \n",table[k].devHandle);
		k++;
	}
		
	info = (DMA_INFO*) calloc(1,sizeof(DMA_INFO));
	info->dma_handle = table[2].devHandle;	
	
	
	(*mvme)->info = info;
	printf("mvme_open:[Concurrent tsi148] \n");
	printf("VME Bus Handle			= 0x%x\n",(*mvme)->handle);
	printf("DMA Handle			= 0x%x\n",&info->dma_handle );
	printf("DMA area size			=  %d bytes \n",DEFAULT_DMA_NBYTES);
	printf("DMA physical address		=  %p\n",(unsigned long *)info->dma_ptr);

	return(MVME_SUCCESS);
}/* --------------- end of function mvme_open -------------------*/


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_close
 *  Description:  close and release(free) ALL openend VME channels
 *  Parameters :  @param *mvme	VME_structure
 *  		  @return MVME_SUCCESS,ERROR
 * =====================================================================================
 */
int mvme_close ( MVME_INTERFACE *mvme )
{
	int 		j;
	int 		result;
	VME_TABLE2 	*table;
	//DMA_INFO   	*info;
	int 		devHandle;

	table = (VME_TABLE2*)mvme->table;
//	info  = ((DMA_INFO*)mvme)->info;

	printf("mvme_close:\n");
	printf("Bus handle		= 0x%x\n",mvme->handle);
	printf("DMA handle		= 0x%x\n",table[2].devHandle);

	devHandle = (int)mvme->handle;
	

	/* 1. Close DMA channel */
	result =  vme_freeDmaBuffer(table[2].devHandle);
	if( result < 0 ){
		perror("Error - Could not free DMA buffer " );
		return(MVME_ACCESS_ERROR);
	}
	
	/* 2. Close PCI D16 Image */
	result = vme_disablePciImage(table[0].devHandle );
	if( result < 0 ){
		perror("[mvme_close] Failed to close PCI D16 Image ");
		return(MVME_ACCESS_ERROR);
	}

	/* 3. Close PCI D32 Image */
	result = vme_disablePciImage(table[1].devHandle );
	if( result < 0 ){
		perror("[mvme_close] Failed to close PCI D32 Image ");
		return(MVME_ACCESS_ERROR);
	}



	for(j = 0; j < 3; j++) {
		devHandle = table[j].devHandle;
		if( devHandle >=0 )
			vme_closeDevice(devHandle);
	}

	free(mvme->info);			/* Free DMA allocation */
	

	/* free table pointer */
	free (mvme->table);
	mvme->table = NULL;
	/* free mvme block */
	free(mvme);

	return(MVME_SUCCESS);
}/* --------------- end of function mvme_close -------------------*/


int mvme_write(MVME_INTERFACE *mvme, mvme_addr_t vme_addr, void *src, mvme_size_t n_bytes)
{

	int	devHandle,result;
	DMA_INFO 	*info;
	VME_TABLE2 *table;

	if( n_bytes > DEFAULT_DMA_NBYTES) {
		perror("[mvme_write] Attempt to DMA bytes > DEFAULT_DMA_NBYTES\n");
		return(MVME_ERROR);
	}

	table = (VME_TABLE2*)mvme->table;

	devHandle = table[2].devHandle;

	result = vme_write( devHandle, 0x0, src, n_bytes );
	if( result < 0 ){
		perror("[mvme_write] Failed to unmap VME image");
		return(MVME_ERROR);
	}


  return MVME_SUCCESS;
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mvme_read
 *  Description:  Read from VME bus.
 *  Parameters :  @param *mvme		VME structure
 *  		  @param *dst		destination pointer
 *  		  @param vme_addr	source address (VME location)
 *  		  @param n_bytes	requested transfer size
 *  		  @return		MVME_SUCCESS,ERROR
 * =====================================================================================
 */
int mvme_read( MVME_INTERFACE *mvme, void *dst, mvme_addr_t vme_addr, mvme_size_t n_bytes)
{
	int	devHandle,result;
	DMA_INFO 	*info;
	VME_TABLE2 *table;

	if( n_bytes > DEFAULT_DMA_NBYTES) {
		perror("[mvme_read] Attempt to DMA bytes > DEFAULT_DMA_NBYTES\n");
		return(MVME_ERROR);
	}

	table = (VME_TABLE2*)mvme->table;

	devHandle = table[2].devHandle;

	result = vme_read( devHandle, 0x0, dst, n_bytes );
	if( result < 0 ){
		perror("[mvme_read] Failed to unmap VME image");
		return(MVME_ERROR);
	}

	return(MVME_SUCCESS);
}/* --------------- end of function mvme_read -------------------*/


int mvme_write_value(MVME_INTERFACE *mvme, mvme_addr_t vme_addr, DWORD value)
{
  mvme_addr_t addr;
  UINT8 vv = (UINT8)value;
  
  EN_PCI_IMAGE_DATA idata;
     
  memset( (char*)&idata, 0 , sizeof(EN_PCI_IMAGE_DATA));

  Write_Value( mvme,  vme_addr, &idata, &vv );

  return MVME_SUCCESS;
}

DWORD mvme_read_value(MVME_INTERFACE *mvme, mvme_addr_t vme_addr)
{
  mvme_addr_t addr;
  DWORD dst = 0xFFFFFFFF;
  EN_PCI_IMAGE_DATA idata;
     
  memset( (char*)&idata, 0 , sizeof(EN_PCI_IMAGE_DATA));

  Read_Value( mvme,  vme_addr, &idata, &dst );

      return(dst);
}

int mvme_set_am( MVME_INTERFACE *mvme, int am )
{
	mvme->am = am;
	return MVME_SUCCESS;

}

int EXPRT mvme_get_am( MVME_INTERFACE *mvme, int *am )
{
	*am = mvme->am;
	return MVME_SUCCESS;

}

int mvme_set_dmode( MVME_INTERFACE *mvme, int dmode )
{
	
	mvme->dmode = dmode;
	return MVME_SUCCESS;
}

int mvme_get_dmode( MVME_INTERFACE *mvme, int *dmode )
{
	*dmode = mvme->dmode;
	return MVME_SUCCESS;

}

int mvme_sysreset( MVME_INTERFACE *mvme )
{
	int result;

	return MVME_SUCCESS;

}


/* Interupts */
int mvme_set_bIt( MVME_INTERFACE *mvme, int mode )
{
	return MVME_SUCCESS;
}

int mvme_get_bIt( MVME_INTERFACE *mvme, int *mode )
{
	return MVME_SUCCESS;
}


/** Test */

#ifdef MAIN_ENABLE
int main() 
{
	int status;
	int myinfo = VME_INTERRUPT_SIGEVENT;
	
	MVME_INTERFACE *myvme;
	int vmeio_status;


	status = mvme_open( &myvme, 0 );
	status = mvme_set_am( &myvme, 0x09 );
	status = mvme_set_dmode( myvme, MVME_DMODE_D32 );
	status = mvme_read( myvme, &vmeio_status, 0x4078001C, 4 );
	printf(" VMEIO Status : 0x%x \n", vmeio_status );
	
	mvme_write_value( myvme, 0x40780010, 0x0 );
	printf(" Value : 0x%lx \n", mvme_read_value(myvme, 0x40780018 ) );
	
	mvme_write_value( myvme, 0x40780010, 0x3 );
	printf(" Value : 0x%lx \n", mvme_read_value(myvme, 0x40780018 ) );

	status = mvme_close(myvme);



	return 0;
}
#endif




