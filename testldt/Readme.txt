File: testldt.c

This is a simple test program to demonstrate the useage of the cctldt driver
module.

It comprises two tests:

1/ The LDT is set up into PIT mode with a frequency of 100Hz. A thread is 
started to count 'notifications' which occur every time the timer expires.
The main thread sleeps for 1 second and then prints out the number of
notifications. This should be 100 if the PIT is working correctly. Once this is
done the thread is deleted.

2/ The LDT is set to LDT mode and cleared. The timer is then started and the
main program goes into a loop for 5 x 1 second calls to 'sleep'. After
each call to sleep the timer value is read back and printed on screen.

Sample output
=============

Timer opened on fd 3
PIT Test - count ticks for a second. Timer set to 100Hz so
it should be 100 ticks
Start timer
Thread start
Stop timer...
Ticks: 100
Cancel thread...
LDT test - count for 5 seconds reading timer value each second
Start timer
Loop: 0 Timer count: 0001001641
Loop: 1 Timer count: 0002002500
Loop: 2 Timer count: 0003003362
Loop: 3 Timer count: 0004004222
Loop: 4 Timer count: 0005005081
Stop timer...
Done


