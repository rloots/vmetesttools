/*
 * =====================================================================================
 *
 *       Filename:  sis3302reset.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  10/08/2015 02:32:17 AM
 *       Revision:  none
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog:
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <assert.h>
#include <byteswap.h>
#include <time.h>


#include "midas.h"
//#include "mcstd.h"
#include "mvmestd.h"
//#include "msystem.h"

#include "v792.h"
#include "sis3320drv.h"
#include "sis3302gamma.h"


DWORD SIS3302_BASE = 0x10000000;
MVME_INTERFACE *k600vme;


int main( int argc, char* arg[] )
{
	int status, csr, i;


	// Test under vmic
	status = mvme_open(&k600vme, 0);
	if (status!=1) exit(1);

     uint32_t mod_id = sis3320_RegisterRead(k600vme, SIS3302_BASE,SIS3302_MODID);

     uint32_t major = mod_id&0xffff;
     uint32_t idd = (mod_id>>16)&0xffff;
     printf(" ---=====---- SIS3302[0x%x]: Version [0x%x], Firmware: major: 0x%x minor: 0x%0x ----=======----=-=-=-=-=-=-=- \n",SIS3302_BASE, idd,major&0xff00,major&0x00ff );






	printf(" Doing vme reset ... \n");

	/* vme reset */
	sis3320_RegisterWrite(k600vme,SIS3302_BASE, 0x400 ,0x0);

	usleep(10000);
	

	printf(" sis3302 vme reset done\n");
	return 0;
}
