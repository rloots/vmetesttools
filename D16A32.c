/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  initpciD16
 *  Description :  
 *  Parameters  :  
 *  Returns     :  
 * =====================================================================================
 */
int initpciD16A32()
{
	int devHandle, result;
	EN_PCI_IMAGE_DATA data16;
	
	data16.pciAddress=0x00000000;
        data16.pciAddressUpper=0;
        data16.pciBusSpace=0;
	data16.vmeAddress=0x00000000;
        data16.vmeAddressUpper=0;
	data16.size = 0x100000;
        data16.sizeUpper = 0;
	data16.postedWrites = 0;
	data16.readPrefetch=0;
        data16.prefetchSize= 0;
        data16.dataWidth = EN_VME_D16;
	data16.addrSpace = EN_VME_A32;		/*Default*/
        data16.type = EN_LSI_DATA;
        data16.mode = EN_LSI_USER;
        data16.vmeCycle = 0;
        data16.sstbSel =0;
        data16.ioremap = 1;

	devHandle = vme_openDevice("lsi0");
	if( devHandle < 0 ){
		perror("[initpciD16] Could not create device for d16\n");
		return(MVME_ERROR);
	}else{
		result = vme_enablePciImage( devHandle, &data16 );
		if( result < 0 ){
			perror("[initpciD16] Could not create PCI Image {2} for d16\n");
			return(MVME_ERROR);
		}
	}/**D16*/

	devHandle_D16_A32 = devHandle;
	return(MVME_SUCCESS);
}
