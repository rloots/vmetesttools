#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <assert.h>
#include <byteswap.h>
#include <time.h>


#include "midas.h"
//#include "mcstd.h"
#include "mvmestd.h"
//#include "msystem.h"

#include "v792.h"
#include "sis3320drv.h"
#include "sis3302gamma.h"

//DWORD V792_BASE = 0x20000;
DWORD V792_BASE  	=	0x220000;

DWORD SIS3320_BASE = 0x10000000;
uint32_t nof_raw_data_words = 16;
uint32_t event_length_lwords = 512;

uint32_t gFirTriggerThresholdValue = 20;
uint32_t gPeakingTime = 4;
uint32_t gSumGapTime  = 16;
uint32_t gInternalGateLength = 20;
uint32_t gInternalTriggerPulseLength = 30;
uint32_t gshift_factor = 0;


uint32_t gRawDataSampleStartIndex = 0;
uint32_t gRawDataSampleLength = 0;
uint32_t gEndAddressThreshold = 70;  // 2 * samples
uint32_t gPreTriggerDelay = 30;



uint32_t gFIREnergyGap=0;
uint32_t gFIREnergyPeak=0;
uint32_t gFIREnergyLength=0;
uint32_t gFIRTauFactor=0;
uint32_t gEnergyGateLength=0;

uint32_t gEnergySampleLength = 0;
uint32_t gEnergySampleStartIndex1 = 0;
uint32_t gEnergySampleStartIndex2 = 0;
uint32_t gEnergySampleStartIndex3 = 0;



MVME_INTERFACE *k600vme;
DWORD prev_eventcount;
void V792_SETUP()
{
	/* base address */
	v792_Setup(k600vme,V792_BASE,0x3);
}
/*  THIS IS CURRENTLY CALLED BY readout thread */
int my_poll_eventB(INT source, INT count, BOOL test )
{
  //int i;
  //int status = 0;
  DWORD current_eventcount = 0;
  int current_size = 0;
  DWORD the_diff = 0x0;

  //FILE* fl = fopen("QDC_POLLEVENT.log","a+");
  
  int drdy = v792_DataReady(k600vme, V792_BASE);
  //Get event count
 // v792_EvtCntRead(k600vme, V792_BASE, &current_eventcount);

  //fprintf(fl,"Current Event count %d \n",current_eventcount );
 
 // the_diff = current_eventcount - prev_eventcount;

  if(drdy){
	  return drdy;
  }

 // if( the_diff >= QDC_HighWater ){
  //  prev_eventcount = current_eventcount;
   // current_size = the_diff;
    //fprintf(fl,"Event Count Diff [%d] >= QDC_HighWater [%d] : prev event count %d \n",the_diff,QDC_HighWater,prev_eventcount );
   // current_eventcount = 0;
    //fclose(fl);
   // return current_size;
 // }


  //fclose(fl);
  return 0;
}


void sis3302_EnergySetupGP_Register()
{


	
	uint32_t datum = ((gFIREnergyPeak&0x300)<<8) + (gFIREnergyPeak&0xff) + (gFIREnergyGap<<8);

	sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_ENERGY_SETUP_GP_ADC12,datum);

	uint32_t energy_gp_reg = sis3320_RegisterRead(k600vme, SIS3320_BASE,SIS3302_ENERGY_SETUP_GP_ADC12);

	uint32_t epeaking_time_top = (energy_gp_reg>>16)&0x3;
	uint32_t epeaking_time_lower = (energy_gp_reg&0xff);
	uint32_t epeaking_time = (epeaking_time_top<<8) + epeaking_time_lower;

	uint32_t egap_time = (energy_gp_reg>>8)&0xff;

	uint32_t edec = (energy_gp_reg>>28)&0x3;

	printf(" -------------------- Energy Setup GP Register -------------- \n");
	printf(" Peaking Time [max. 1023]   		: 0x%x [%d] \n",epeaking_time,epeaking_time);
	printf(" Gap Time [max. 255] 			: 0x%x [%d] \n",egap_time,egap_time);
	printf(" Decimation: 1/2/4/8 			: 0x%x [%d] \n",edec,edec);


}

void sis3302_EnergyGateLength_Register()
{

	uint32_t datum = gEnergyGateLength&0xffff;

	sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_ENERGY_GATE_LENGTH_ADC12,datum);


	uint32_t egatelength = sis3320_RegisterRead(k600vme, SIS3320_BASE,SIS3302_ENERGY_GATE_LENGTH_ADC12);

	uint32_t gatelength = egatelength&0xffff;

	printf(" ------------------- Energy Gate Length Register -------------- \n");
	printf(" Energy Gate Length 			: 0x%x [%d] \n",gatelength,gatelength);
}


void sis3302_EnergySample_Registers()
{

	uint32_t datum = gEnergySampleLength&0x7ff;
	sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_ENERGY_SAMPLE_LENGTH_ADC12,datum);


	datum = gEnergySampleStartIndex1&0xffff;
	sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_ENERGY_SAMPLE_START_INDEX1_ADC12,datum);
	datum = gEnergySampleStartIndex2&0xffff;
	sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_ENERGY_SAMPLE_START_INDEX2_ADC12,datum);
	datum = gEnergySampleStartIndex3&0xffff;
	sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_ENERGY_SAMPLE_START_INDEX3_ADC12,datum);






	uint32_t energysamplelength_reg  = sis3320_RegisterRead(k600vme, SIS3320_BASE,SIS3302_ENERGY_SAMPLE_LENGTH_ADC12);
	uint32_t energysamplestartindex1_reg  = sis3320_RegisterRead(k600vme, SIS3320_BASE,SIS3302_ENERGY_SAMPLE_START_INDEX1_ADC12);
	uint32_t energysamplestartindex2_reg  = sis3320_RegisterRead(k600vme, SIS3320_BASE,SIS3302_ENERGY_SAMPLE_START_INDEX2_ADC12);
	uint32_t energysamplestartindex3_reg  = sis3320_RegisterRead(k600vme, SIS3320_BASE,SIS3302_ENERGY_SAMPLE_START_INDEX3_ADC12);

	uint32_t esl = energysamplelength_reg&0x7ff;
	uint32_t essi1 = energysamplestartindex1_reg&0xffff;
	uint32_t essi2 = energysamplestartindex2_reg&0xffff;
	uint32_t essi3 = energysamplestartindex3_reg&0xffff;

	printf(" -------------------- Energy Sample Registers --------------------- \n");
	printf(" Energy Sample Length				: 0x%x [%d] \n",esl,esl);
	printf(" Energy Sample Start Index1 			: 0x%x [%d] \n",essi1,essi1);
	printf(" Energy Sample Start Index2 			: 0x%x [%d] \n",essi2,essi2);
	printf(" Energy Sample Start Index3 			: 0x%x [%d] \n",essi3,essi3);
}



void sis3302_PreTriggerADC12()
{

	uint32_t preTrigDelay = gPreTriggerDelay;
	uint32_t GateLength = 300;


	uint32_t preTrigGate_Register = ((preTrigDelay&0x1ff)<<16) + (GateLength&0xffff);

	//sis3320_RegisterWrite(k600vme,SIS3320_BASE,0x02000008,0x010203ff);
	sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC12,preTrigGate_Register); //Pretrigger = 20; trig window = 448
	uint32_t reg_read;
	reg_read = sis3320_RegisterRead(k600vme, SIS3320_BASE,SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC12);

	uint32_t pretrigger = (reg_read>>16)&0x1ff;
	uint32_t gate_length = (reg_read)&0xffff;

	printf(" ----------------- PRETRIGGER DELAY & TRIGGER GATE LENGTH \n");
	printf("Pretrigger Delay 			: [%d]\n",pretrigger); 
	printf("Trigger Gate Length			: [%d]\n",gate_length);
}

void sis3302_LED( int led_switch )
{
	int csr;

	printf(" ----------------- USER LED ------------- \n");
	if( led_switch == 1 ){
		  printf("----LED On\n");
		  csr = sis3320_RegisterRead(k600vme, SIS3320_BASE, 0x0);
  		  //printf(" csr *before* setting LED on:  0x%x , led switch %d \n", csr,csr&0x1 );
  		  sis3320_RegisterWrite(k600vme,SIS3320_BASE,0x0,csr|0x1);
 		  //csr = sis3320_RegisterRead(k600vme, SIS3320_BASE, 0x0);
  		  printf(" csr *after* setting LED on:  0x%x , led switch %d \n", csr,csr&0x1 );
	}else if( led_switch == 0 ){
		  printf("---LED Off\n");
	          csr = sis3320_RegisterRead(k600vme, SIS3320_BASE, 0x0);
  		  //printf(" csr *before* setting LED on:  0x%x , led switch %d \n", csr,csr&0x1 );
  		  sis3320_RegisterWrite(k600vme,SIS3320_BASE,0x0,csr|0x10000);
 		  csr = sis3320_RegisterRead(k600vme, SIS3320_BASE, 0x0);
  		  //printf(" csr *after* setting LED on:  0x%x , led switch %d \n", csr,csr&0x1 );
	}

}


void sis3302_ClockStatus()
{
	int myclock;
	int aqui_control;
	aqui_control = sis3320_RegisterRead(k600vme, SIS3320_BASE, SIS3302_ACQUISTION_CONTROL);

	myclock = (aqui_control>>12)&0x7;


	printf(" --------------- CLOCK SETTING ------------------ \n");
	switch(myclock){
		case 0:
			printf("Clock Internal 100 MHz\n");
			break;
		case 1:
			printf("Clock Internal 50 MHz\n");
			break;
		case 2:
			printf("Clock Internal 25 MHz\n");
			break;
		case 3:
			printf("Clock Internal 10 MHz\n");
			break;
		case 4:
			printf("Clock Internal 1 MHz\n");
			break;
		case 5:
			printf("Clock Internal 100 MHz\n");
			break;
		case 6:
			printf("Clock External clock, LEMO front panel , min. 1 MHz\n");
			break;
		case 7:
			printf("Clock Second Internal 100 MHz\n");
			break;
		default:
			printf("Unknown clock config\n");
	}


}

void sis3302_FrontPanelLemoIn_1(int lemo_switch)
{
	uint32_t aqui_control;
	aqui_control = sis3320_RegisterRead(k600vme, SIS3320_BASE, 0x10);

	printf(" Front Panel Lemo In 1 Status <<%d>> \n",(aqui_control>>10)&0x1);
	//Enable
	if(lemo_switch == 1 ){
		sis3320_RegisterWrite(k600vme,SIS3320_BASE,0x10,aqui_control|0x400);
		printf(" Front Panel Lemo In 1 Enabled <<%d>> \n",(aqui_control>>10)&0x1);
	}else if(lemo_switch == 0){
		sis3320_RegisterWrite(k600vme,SIS3320_BASE,0x10,aqui_control|0x4000000);
		printf(" Front Panel Lemo In 1 Disabled <<%d>> \n",(aqui_control>>10)&0x1);
	}

}

void sis3302_FrontPanelLemoIn_2(int lemo_switch)
{
	uint32_t aqui_control;
	aqui_control = sis3320_RegisterRead(k600vme, SIS3320_BASE, 0x10);

	printf(" Front Panel Lemo In 2 Status <<%d>> \n",(aqui_control>>9)&0x1);
	//Enable
	if(lemo_switch == 1 ){
		sis3320_RegisterWrite(k600vme,SIS3320_BASE,0x10,aqui_control|0x200);
		printf(" Front Panel Lemo In 2 Enabled <<%d>> \n",(aqui_control>>9)&0x1);
	}else if(lemo_switch == 0){
		sis3320_RegisterWrite(k600vme,SIS3320_BASE,0x10,aqui_control|0x2000000);
		printf(" Front Panel Lemo In 2 Disabled <<%d>> \n",(aqui_control>>9)&0x1);
	}

}

void sis3302_FrontPanelLemoIn_3(int lemo_switch)
{
	uint32_t aqui_control;
	aqui_control = sis3320_RegisterRead(k600vme, SIS3320_BASE, SIS3302_ACQUISTION_CONTROL);

	printf(" Front Panel Lemo In 3 Status <<%d>> \n",(aqui_control>>8)&0x1);
	//Enable
	if(lemo_switch == 1 ){
		sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_ACQUISTION_CONTROL,(aqui_control|0x100));
		printf(" Front Panel Lemo In 3 Enabled <<%d>> \n",(aqui_control>>8)&0x1);
	}else if(lemo_switch == 0){
		sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_ACQUISTION_CONTROL,(aqui_control|0x1000000));
		printf(" Front Panel Lemo In 3 Disabled <<%d>> \n",(aqui_control>>8)&0x1);
	}

}


void sis3302_EndThreshold_Register(uint32_t end_address_threshold)
{
	 uint32_t address_threshold_value = (end_address_threshold<<2);

	 sis3320_RegisterWrite(k600vme,SIS3320_BASE, SIS3302_END_ADDRESS_THRESHOLD_ALL_ADC,address_threshold_value);

	 printf(" -------------------- END ADDRESS THRESHOLD ADC ----------- \n");
	 printf(" ADC 						: [0x08%x]\n",SIS3302_END_ADDRESS_THRESHOLD_ALL_ADC);
	 printf(" End address Threshold value 			: [0x%08x] \n",address_threshold_value&0xfffffc);
}

void sis3302_ADC1_Event_Configuration()
{

	uint32_t event_conf;
	uint32_t adc_id = (0xad000000);
	event_conf = EVENT_CONF_ADC1_INTERN_TRIGGER_ENABLE_BIT + EVENT_CONF_ADC1_INTERN_GATE_ENABLE_BIT+ adc_id;	
	sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_EVENT_CONFIG_ADC12,event_conf);
	event_conf = sis3320_RegisterRead(k600vme, SIS3320_BASE, SIS3302_EVENT_CONFIG_ADC12);

	printf(" ---------   Event Config Register ADC 1/2 ------------ : 0x%x \n",event_conf);
	printf(" ADC HEADER ID  				: 0x%x \n",(event_conf>>19)&0xfff);
	printf(" ADC 1 Input invert bit				: %d \n",(event_conf&0x1));
	printf(" ADC 1 Internal Trigger Enable(asynchronous) 	: %d \n",((event_conf>>2)&0x1));
	printf(" ADC 1 External Trigger Enable(synchronous) 	: %d \n",((event_conf>>3)&0x1));
	printf(" ADC 1 Internal Gate Enable  			: %d \n",((event_conf>>4)&0x1));
	printf(" ADC 1 External Gate Enable  			: %d \n",((event_conf>>5)&0x1));
	printf(" ADC 1 ADC N - 1 Next Neighbour gate enable  	: %d \n",((event_conf>>6)&0x1));
	printf(" ADC 1 ADC N + 1 Next Neighbour gate enable  	: %d \n",((event_conf>>7)&0x1));
}


void sis3302_TriggerThreshold_ADC1(uint32_t trap_threshold)
{

	uint32_t mytrap = ( trap_threshold & 0x1ffff ) + 0x10000;

	uint32_t trigthres_set = 0x0000000     //enable GT
				+ mytrap;   // threshold = 256
	sis3320_RegisterWrite(k600vme,SIS3320_BASE, SIS3302_TRIGGER_THRESHOLD_ADC1,trigthres_set);
	uint32_t trig_thres;
	trig_thres = sis3320_RegisterRead(k600vme, SIS3320_BASE, SIS3302_TRIGGER_THRESHOLD_ADC1);

	uint32_t trap_value = (trig_thres)&0xffff;
	uint32_t cfd_control = (trig_thres>>20)&0x7;
	uint32_t ext_thres_mode = (trig_thres>>23)&0x1;
	uint32_t trigger_mode_gt = (trig_thres>>25)&0x1;
	uint32_t disable_trigger = (trig_thres>>26)&0x1;

	printf(" ----------- Trigger Threshold ADC 1 -------------- \n");
	printf(" Trigger Threshold Reg: ADC1 \n");
	printf(" Trapezoidal threshold value	: [%d] \n",trap_value);
	printf(" CFD Control bits	    	: [%d] \n",cfd_control);
	printf(" Ext_Threshold_Mode		: [%d] \n",ext_thres_mode);
	printf(" Trigger Mode GT		: [%d] \n",trigger_mode_gt);
	printf(" Disable Trigger Output		: [%d] \n",disable_trigger);

	switch(cfd_control)
	{

		case 0:printf("CFD Function Disabled\n");break;
		case 1:printf("CFD Function Disabled\n");break;
		case 2:printf("CFD Function enabled with 75%% \n");break;
		case 3:printf("CFD Function enabled with 50%% \n");break;
		default:
			printf(" Unknown CFD Control Value \n");
	};


	if(ext_thres_mode == 0 ){

		uint32_t val = (gPeakingTime&0xffff);	
		if( val < 16){
			gshift_factor = 16;	
		}else if( val > 15 && val < 32 )	
		{
			gshift_factor = 32;
		} else if( val > 31 && val < 64 )	
		{
			gshift_factor = 64;
		} else if( val > 63 && val < 128 )	
		{
			gshift_factor = 128;
		} else if( val > 127 && val < 256 )	
		{
			gshift_factor = 512;
		} else if( val > 255 && val < 512 )	
		{
			gshift_factor = 1024;
		} 
		printf(" ---=== Extended Threshold Mode = [%d] ==== ----- \n",ext_thres_mode);
		printf(" 25 bit running sum is shifted to the right by : [%d] , Peaking Time [%d] \n",gshift_factor,gPeakingTime);
		
		uint32_t temp_adc_counts = 0;
		if(gPeakingTime != 0)
		temp_adc_counts = (gshift_factor*gFirTriggerThresholdValue)/ (gPeakingTime&0xffff);
	        
		printf(" FIR Trigger ADC Counts : [%d] \n",temp_adc_counts);
		printf(" Flat Time: gSumGapTime[%d] - gPeakingTime[%d] = [%d] \n", gSumGapTime,gPeakingTime,(gSumGapTime - gPeakingTime));	

	}//if



}//sis3302_TriggerThreshold_ADC1

void sis3302_ADC1_Trigger_Setup_Register(uint32_t Peak_val, uint32_t Sumg_val)
{
	uint32_t trig_setup;
	//trig_setup = sis3320_RegisterRead(k600vme, SIS3320_BASE, 0x02000030);
	//printf("Trigger Setup Before Setting: 0x%x \n",trig_setup);
	//trig_setup |= (((0x00000000|0x14))<<24);
	//trig_setup |= (((0x00000000|0x30))<<16);
	//trig_setup |= (((0x00000000|0x10))<<8);

	uint32_t sumg_val = Sumg_val;
	uint32_t peak_val = Peak_val;
	uint32_t pulse_length = gInternalTriggerPulseLength;
	uint32_t internal_gate_length = gInternalGateLength;

	trig_setup = ((internal_gate_length&0x3f)<<24)+((pulse_length &0xff)<<16)  + ( (sumg_val&0xff)<<8) + (peak_val&0xff) ;

	//printf("Trigger Setup After Setting: 0x%x \n",trig_setup);
	sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_TRIGGER_SETUP_ADC1,trig_setup); 
	if( trig_setup != 0 ){
	trig_setup = sis3320_RegisterRead(k600vme, SIS3320_BASE, SIS3302_TRIGGER_SETUP_ADC1);
	uint32_t peaking_time = trig_setup&0xff;
	uint32_t sumg = (trig_setup>>8)&0xff;
	uint32_t trig_pulse_length = (trig_setup>>16)&0xff;
	uint32_t gate_length = (trig_setup>>24)&0x3f;


	printf(" ---------------- TRIGGER SETUP REGISTER ADC1 ----------------- \n");
	printf(" peaking time				: [%d] \n",peaking_time);
 	printf(" sumg time   				: [%d] \n",sumg);
	printf(" Internal Pulse Length 			: [%d] \n",trig_pulse_length);
	printf(" Internal Gate  Length 			: [%d] \n",gate_length);

		

	}else{
		printf(" Trigger Setup Register is Zero: 0x%x \n",trig_setup );
	}
}


void sis3302_SetLemoModes(int lemoOut, int lemoIn ) 
{





}

void sis3302_LemoMode( )
{
	int mcamode;
	int aqui_control;
	aqui_control = sis3320_RegisterRead(k600vme, SIS3320_BASE, 0x10);

	mcamode = (aqui_control>>3)&0x1;
	int newmode = aqui_control | 0x1;
	sis3320_RegisterWrite(k600vme,SIS3320_BASE,0x10,newmode);
	
	aqui_control = sis3320_RegisterRead(k600vme, SIS3320_BASE, 0x10);
	printf(" Lemo  Mode, configured via MCA MODE [%d] , newmode [0x%x]\n",mcamode,newmode);
	
	if(mcamode==0) {
		int lemo_out = (aqui_control>>4)&0x3;
		int lemo_in = (aqui_control)&0x7;
		switch(lemo_out){
			case 0:
				printf("Lemo output 3 -> ADC sample logic armed \n");
				printf("Lemo output 2 -> ADCx event sampling busy\n");
				printf("Lemo output 1 -> Trigger Lemo output\n");
				break;
			case 1:
				printf("Lemo output 3 -> ADC sample logic armed \n");
				printf("Lemo output 2 -> ADCx event sampling busy OR ADC sample logic not armed (veto) \n");
				printf("Lemo output 1 -> Trigger Lemo output\n");
				break;
			case 2:
				printf("Lemo output 3 -> ADC N+1 Neighbour Trigger/Gate Out\n");
				printf("Lemo output 2 -> Trigger Lemo output\n");
				printf("Lemo output 1 -> ADC N-1 Neighbour Trigger/Gate Out\n");
				break;
			case 3:
				printf("Lemo output 3 -> ADC N+1 Neighbour Trigger/Gate Out\n");
				printf("Lemo output 2 -> ADC Sampling busy OR ADC sample logic not armed (veto) \n");
				printf("Lemo output 1 -> ADC N-1 Neighbour Trigger/Gate Out \n");
				break;
			default:
				printf("Unknown mcamode 0 lemo Lemo output config\n");
		}//switch
		switch(lemo_in){
			case 0:
				printf("Lemo input 3 -> Trigger \n");
				printf("Lemo input 2 -> Timestamp Clear \n");
				printf("Lemo input 1 -> Veto \n");
				break;
			case 1:
				printf("Lemo input 3 -> Trigger \n");
				printf("Lemo input 2 -> Timestamp Clear \n");
				printf("Lemo input 1 -> Gate \n");
				break;
			case 2:
				printf("Lemo input 3 -> reserved \n");
				printf("Lemo input 2 -> reserved \n");
				printf("Lemo input 1 -> reserved \n");
				break;
			case 3:
				printf("Lemo input 3 -> reserved \n");
				printf("Lemo input 2 -> reserved \n");
				printf("Lemo input 1 -> reserved \n");
				break;
		        case 4:
				printf("Lemo input 3 -> ADC N+1 Neighbour Trigger/Gate In \n");
				printf("Lemo input 2 -> Trigger \n");
				printf("Lemo input 1 -> ADC N-1 Neighbour Trigger/Gate In  \n");
				break;
			case 5:
				printf("Lemo input 3 -> ADC N+1 Neighbour Trigger/Gate In \n");
				printf("Lemo input 2 -> Timestamp Clear  \n");
				printf("Lemo input 1 -> ADC N-1 Neighbour Trigger/Gate In \n");
				break;
			case 6:
				printf("Lemo input 3 -> ADC N+1 Neighbour Trigger/Gate In\n");
				printf("Lemo input 2 -> Veto \n");
				printf("Lemo input 1 -> ADC N-1 Neighbour Trigger/Gate In \n");
				break;
			case 7:
				printf("Lemo input 3 -> ADC N+1 Neighbour Trigger/Gate In \n");
				printf("Lemo input 2 -> Gate \n");
				printf("Lemo input 1 -> ADC N-1 Neighbour Trigger/Gate In \n");
				break;
			default:
				printf("Unknown mcamode 0 lemo Lemo input config\n");
		}//switch

	}else if(mcamode == 1){
		int lemo_out = (aqui_control>>4)&0x3;
		int lemo_in = (aqui_control)&0x7;

		switch(lemo_out){
			case 0:
				printf("Lemo output 3 -> ADC sample logic armed \n");
				printf("Lemo output 2 -> ADCx event sampling busy\n");
				printf("Lemo output 1 -> Trigger Lemo output\n");
				break;
			case 1:
				printf("Lemo output 3 -> Multiscan first scan signal \n");
				printf("Lemo output 2 -> LNE \n");
				printf("Lemo output 1 -> Scan Enable \n");
				break;
			case 2:
				printf("Lemo output 3 -> Scan Enable \n");
				printf("Lemo output 2 -> LNE \n");
				printf("Lemo output 1 -> Trigger Lemo output \n");
				break;
			case 3:
				printf("Lemo output 3 -> reserved \n");
				printf("Lemo output 2 -> reserved \n");
				printf("Lemo output 1 -> reserved \n");
				break;
			default:
				printf("Unknown mcamode 1 lemo Lemo output config\n");
		}//switch
		switch(lemo_in){
			case 0:
				printf("Lemo input 3 -> reserved  \n");
				printf("Lemo input 2 -> External MCA_Start (histogram ptr reset/start pulse) \n");
				printf("Lemo input 1 -> External next pulse (LNE) \n");
				break;
			case 1:
				printf("Lemo input 3 -> Trigger \n");
				printf("Lemo input 2 -> external MCA_Start (histogram ptr reset/start pulse) \n");
				printf("Lemo input 1 -> external next pulse (LNE) \n");
				break;
			case 2:
				printf("Lemo input 3 -> Veto \n");
				printf("Lemo input 2 -> external MCA_Start (histogram ptr reset/start pulse) \n");
				printf("Lemo input 1 -> external next pulse (LNE) \n");
				break;
			case 3:
				printf("Lemo input 3 -> Gate \n");
				printf("Lemo input 2 -> external MCA_Start (histogram ptr reset/start pulse) \n");
				printf("Lemo input 1 -> external next pulse (LNE) \n");
				break;
			case 4: 
				printf("Reserved all \n");
				break;
			case 5: 
				printf("Reserved all \n");
				break;
			case 6: 
				printf("Reserved all \n");
				break;
			case 7: 
				printf("Reserved all \n");
				break;
			default:
				printf("Unknown mcamode 1 lemo Lemo input config\n");
		}//switch



	}//if-else

	

}//LemoOutMode



void sis3302_SetMCAMode(int mcamode )
{
	int mca;

	if( mcamode == 0){
		 // printf("MCA Mode Disabled \n");
		 // mca = sis3320_RegisterRead(k600vme, SIS3320_BASE, 0x10);
  		 // printf(" Acquisition Control Register: 0x%x, MCA Mode: %d \n",mca,(mca>>3)&0x1);
		//  sis3320_RegisterWrite(k600vme,SIS3320_BASE,0x10,mca|0x80000);
		  mca = sis3320_RegisterRead(k600vme, SIS3320_BASE, 0x10);
  		  printf("Disabled:  Acquisition Control Register: 0x%x, MCA Mode: %d \n",mca,(mca>>3)&0x1);
	}else if(mcamode ==1){
		  printf("MCA Mode Enabled \n");
		  mca = sis3320_RegisterRead(k600vme, SIS3320_BASE, 0x10);
  		  printf(" Acquisition Control Register: 0x%x, MCA Mode: %d \n",mca,(mca>>3)&0x1);
		  sis3320_RegisterWrite(k600vme,SIS3320_BASE,0x10,mca|0x8);
		  mca = sis3320_RegisterRead(k600vme, SIS3320_BASE, 0x10);
  		  printf("Enabled:  Acquisition Control Register: 0x%x, MCA Mode: %d \n",mca,(mca>>3)&0x1);
	}
}

void sis3302_EventConfigADC()
{

	uint32_t evconfig = sis3320_RegisterRead(k600vme, SIS3320_BASE, 0x02000000 );

	printf(" Event config register for ADC 1/2 : 0x%x \n",evconfig );






}

void sis3302_Status(MVME_INTERFACE *mvme, uint32_t base )
{





}



void sis3302_AcquisitionControl()
{

	uint32_t acontrol_setup = SIS3302_ACQ_SET_CLOCK_TO_SECOND_100MHZ 
		+ SIS3302_ACQ_SET_LEMO_IN_MODE1	//Trigger in on LEMO 3
		+ SIS3302_ACQ_SET_LEMO_OUT_MODE0	//Trigger out on LEMO 1
		+ SIS3302_ACQ_ENABLE_EXTERNAL_LEMO_IN3; // Trigger in enable

	sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_ACQUISTION_CONTROL,acontrol_setup);


	uint32_t acontrol = sis3320_RegisterRead(k600vme, SIS3320_BASE, SIS3302_ACQUISTION_CONTROL);
	
	uint32_t status_mca_mscan_busy = (acontrol>>21)&0x1;
	uint32_t status_mca_scan_busy = (acontrol>>20)&0x1;
	uint32_t status_end_adr_threshold = (acontrol>>19)&0x1;
	uint32_t status_adc_busy = (acontrol>>18)&0x1;
	uint32_t status_adc_arm_bank2 = (acontrol>>17)&0x1;
	uint32_t status_adc_arm_bank1 = (acontrol>>16)&0x1;
	uint32_t status_ored_internal_trigger = (acontrol>>6)&0x1;
	uint32_t status_mca_mode = (acontrol>>6)&0x1;

	printf(" ------------------- ACQUISITION CONTROL  ------------------------------------------------- \n");
	printf(" Status of MCA Multiscan Busy 						: [%d]\n",status_mca_mscan_busy);
	printf(" Status of MCA Scan Busy 						: [%d]\n",status_mca_scan_busy);
	printf(" Status of End Address Threshold Flag					: [%d]\n",status_end_adr_threshold);
	printf(" Status of ADC-Sample-Logic Busy(Armed)					: [%d]\n",status_adc_busy);
	printf(" Status of ADC-Sample-Logic Armed Bank2					: [%d]\n",status_adc_arm_bank2);
	printf(" Status of ADC-Sample-Logic Armed Bank1					: [%d]\n",status_adc_arm_bank1);
	printf(" Status ored Internal Trigger to External Trigger In (Feedback)		: [%d]\n",status_ored_internal_trigger);
	printf(" Status MCA Mode Bit							: [%d]\n",status_mca_mode);

}



void sis3302_RawDataBuffer_Configuration()
{

	uint32_t sample_length 		    = (gRawDataSampleLength & 0xfffc) << 16;
	uint32_t sample_start_index	    = gRawDataSampleStartIndex & 0xfffe;

	uint32_t rawdata_reg = sample_length + sample_start_index;

	sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_RAW_DATA_BUFFER_CONFIG_ADC12,rawdata_reg);

	rawdata_reg = sis3320_RegisterRead(k600vme,SIS3320_BASE,SIS3302_RAW_DATA_BUFFER_CONFIG_ADC12);
	printf(" --------------------- RAW DATA BUFFER CONFIGURATION --------------------- \n");
	printf(" Raw Data Sample Length				: [%d]\n",( (rawdata_reg>>16)&0xfffc));
	printf(" Raw Data Sample Start Index			: [%d]\n",( rawdata_reg & 0xfffe ));

}

int sis3302_Setup(MVME_INTERFACE *mvme, uint32_t base, uint32_t mode )
{

	switch(mode){
		case 0x1:{
				 //Control Register
				 //
				 sis3320_RegisterWrite(k600vme, SIS3320_BASE, 0x0,0x00000000 );

				 //User LED off
				 //
				 //
				 sis3302_LED(0);
				

				 //Clock Status
				 //
				 //
				 sis3302_ClockStatus();
 
				 
				 //Acquisition Register 
				 //
				 sis3302_AcquisitionControl();
			 
				 //MCA Mode == 0
				 //
				 sis3302_SetMCAMode(0);
				


				 //FIR Filter Setup
				 //
				 
				 


				 //sis3302_EventConfigADC();
				 //Trigger Threshold
				 //
				 
	 			 sis3302_ADC1_Event_Configuration();
				 sis3302_TriggerThreshold_ADC1(gFirTriggerThresholdValue);
				 sis3302_ADC1_Trigger_Setup_Register(gPeakingTime,gSumGapTime);
				 sis3302_PreTriggerADC12();
				 sis3302_EndThreshold_Register(gEndAddressThreshold);
				 sis3302_RawDataBuffer_Configuration();
				 sis3302_EnergySetupGP_Register();
				 sis3302_EnergyGateLength_Register();
				 sis3302_EnergySample_Registers();

				 //sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_END_ADDRESS_THRESHOLD_ALL_ADC,0x100);
				 //uint32_t address_threshold_value = 2*event_length_lwords;
				 //uint32_t address_threshold_value = 4<<2;

				 //sis3320_RegisterWrite(k600vme,SIS3320_BASE, SIS3302_END_ADDRESS_THRESHOLD_ALL_ADC,address_threshold_value);



				//DACOffset
				//
				

				 //Trigger OUT
				 //
				 sis3302_LemoMode();
				 sis3302_FrontPanelLemoIn_1(1);
				 sis3302_FrontPanelLemoIn_2(0);
				 sis3302_FrontPanelLemoIn_3(1);

				

				 //Raw Data
				 //
		


				 //ADC Channel 1 
				 //
			
				
				


				
				// sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_TRIGGER_THRESHOLD_ADC1,0x2000000);
				// uint32_t trig_thres;
				 //trig_thres = sis3320_RegisterRead(k600vme,SIS3320_BASE,0x02000034);
				 //printf("Trigger Threshold: 0x%x \n",trig_thres );
				 //sis3320_RegisterWrite(k600vme, SIS3320_BASE, SIS3320_START_DELAY, 0x20); // PreTrigger
				 //sis3320_RegisterWrite(k600vme, SIS3320_BASE, SIS3320_STOP_DELAY, 0x80); // PostTrigger

				 //sis3320_RegisterWrite(k600vme, SIS3320_BASE, SIS3320_KEY_ARM, 0x0);  // Arm Sampling
				 //sis3320_RegisterWrite(k600vme, SIS3320_BASE, 0x404, 0x0);  // Arm Sampling
				 break;
			 }
		case 0x2:break;
		case 0x3:break;
		default :
			printf("Unknown Setup mode\n");
			return -1;

	}//switch


	return 0;
}/* sis3302_Setup*/


int sis3302_read_A32BLT_ADC_MEMORY(uint32_t module_addr,
				   uint32_t adc_channel,
				   uint32_t memory_start_addr,
				   uint32_t *data_buffer,
				   uint32_t req_lwords,
				   uint32_t *got_lwords)
{

int return_code;

uint32_t data;
uint32_t addr, got_nof_lwords;
uint32_t index_num_data;

uint32_t max_page_lword_length, page_lword_length_mask;
uint32_t page_byte_addr_mask;
uint32_t next_memory_byte_addr;
uint32_t rest_req_lwords;
uint32_t sub_memory_byte_addr;
uint32_t sub_req_lwords;
uint32_t sub_max_page_lword_length;
uint32_t sub_page_addr_offset;

max_page_lword_length = SIS3302_MAX_ADC_MEMORY_PAGE_BYTE_LENGTH/4;
page_lword_length_mask = max_page_lword_length - 1 ;

page_byte_addr_mask = SIS3302_MAX_ADC_MEMORY_PAGE_BYTE_LENGTH -1;
next_memory_byte_addr = memory_start_addr & (SIS3302_MAX_ADC_MEMORY_PAGE_BYTE_LENGTH-4);
rest_req_lwords = req_lwords;

printf(" ----=== [ sis3302_read_A32BLT_ADC_MEMORY ] ===---- \n");
printf(" max_page_lword_length = 0x%08x \n",max_page_lword_length);
printf(" page_lword_length_mask = 0x%08x \n", page_lword_length_mask );
printf(" page_byte_addr_mask = 0x%08x \n", page_byte_addr_mask);
printf(" next_memory_byte_addr = 0x%08x \n",next_memory_byte_addr);
printf(" req_lwords = 0x%08x \n", req_lwords);
printf(" rest_req_lwords = 0x%08x \n", rest_req_lwords);


if( ((4*rest_req_lwords) + next_memory_byte_addr) > SIS3302_MAX_ADC_MEMORY_PAGE_BYTE_LENGTH ){
	return -1;
}

return_code = 0;
index_num_data = 0x0;
printf("\n");
printf(" ----=============== VME READOUT LOOP STARTED ================-------------- \n");
do{

	sub_memory_byte_addr = (next_memory_byte_addr & page_byte_addr_mask);
	sub_max_page_lword_length = max_page_lword_length - (sub_memory_byte_addr >> 2);

	printf(" sub_memory_byte_addr = 0x%08x \n",sub_memory_byte_addr);
	printf(" sub_max_page_lword_length = 0x%08x \n",sub_max_page_lword_length);

	if( rest_req_lwords >= sub_max_page_lword_length)
		sub_req_lwords = sub_max_page_lword_length;
	else
		sub_req_lwords = rest_req_lwords;

	sub_page_addr_offset = (next_memory_byte_addr >> SIS3302_ADC_MEMORY_PAGE_REG_ADDR_SHIFT) & SIS3302_ADC_MEMORY_PAGE_REG_ADDR_MASK;

	data = sub_page_addr_offset;
	sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_ADC_MEMORY_PAGE_REGISTER,data);

	addr = module_addr+SIS3302_ADC1_OFFSET
	       +(SIS3302_NEXT_ADC_OFFSET*adc_channel)+(sub_memory_byte_addr);

	printf(" addr = 0x%08x \n",addr);
	printf(" sub_req_lwords = 0x%08x \n",sub_req_lwords);
	printf(" sub_page_addr_offset = 0x%08x \n",sub_page_addr_offset);
	printf(" sub_memory_byte_addr = 0x%08x \n",sub_memory_byte_addr);
	printf("\n");

	
	uint32_t got_nof_lwords = mvme_read( k600vme, data_buffer,addr, sub_req_lwords );
			

	index_num_data += got_nof_lwords;
	next_memory_byte_addr += (4*sub_req_lwords);
	rest_req_lwords -= sub_req_lwords;

}while( (return_code == 0) && (rest_req_lwords>0));

*got_lwords = index_num_data;

printf(" ------ VME READ DONE ----- \n");
printf(" index_num_data = 0x%08x[%d] \n",index_num_data,index_num_data);
printf(" next_memory_byte_addr = 0x%08x \n",next_memory_byte_addr);
printf(" rest_req_lwords = [%d] \n",rest_req_lwords);


}

/*
 * 64k Sample 0x10000
 * 256k Sample 0x40000
 * 1mb Sample 0x100000
 * 4mb Sample 0x400000    
 */
int  sis3302_read_adc_channel( uint32_t module_addr, uint32_t vme_read_mode, uint32_t adc_i,
			       uint32_t event_sample_start_addr,
			       uint32_t event_sample_length,
			       uint32_t *uint_buffer_adc )
{


	uint32_t max_page_sample_length = 0x400000; // 4 MSample
	uint32_t page_sample_length_mask = max_page_sample_length - 1;

	uint32_t next_event_sample_start_addr = (event_sample_start_addr & 0x01fffffc);
	uint32_t rest_event_sample_length     = (event_sample_length & 0x03ffffffc);

	if( rest_event_sample_length >= 0x2000000 )
		rest_event_sample_length = 0x2000000;
	
	uint32_t status = 0;
	uint32_t index_num_data = 0x0;
	uint32_t adc_channel = 1;
	do {
		uint32_t sub_event_sample_addr = (next_event_sample_start_addr & page_sample_length_mask);
		uint32_t sub_max_page_sample_length = max_page_sample_length - sub_event_sample_addr;
		
		//Sample Length
		uint32_t sub_event_sample_length = 0;
		if( rest_event_sample_length >= sub_max_page_sample_length)
			sub_event_sample_length = sub_max_page_sample_length;
		else
			sub_event_sample_length = rest_event_sample_length;		


		//Page offset
		uint32_t sub_page_addr_offset = (next_event_sample_start_addr >> 22 ) &0x7;
	
		printf(" sub_event_sample_addr = [0x%08x] \n \
			 sub_event_sample_length = [0x%08x] \n \
			 sub_page_addr_offset = [0x%08x] ", sub_event_sample_addr, 
			 				       sub_event_sample_length,
							       sub_page_addr_offset );



		//DMA request number of words and bytes
	 	uint32_t dma_request_nof_lwords = (sub_event_sample_length) / 2;
		uint32_t dma_adc_addr_offset_bytes = (sub_event_sample_addr)  * 2;

		printf(" dma_adc_addr_offset_bytes = [0x%08x] \n \
			 dma_request_nof_lwords = [0x%08x] \n \
			 sub_page_addr_offset = [0x%08x] ",dma_adc_addr_offset_bytes,
			 				   dma_request_nof_lwords,
							   sub_page_addr_offset );
			//Set Page
		//
		uint32_t addr = module_addr + SIS3302_ADC_MEMORY_PAGE_REGISTER;
		uint32_t data = sub_page_addr_offset;
		sis3320_RegisterWrite(k600vme,SIS3320_BASE, SIS3302_ADC_MEMORY_PAGE_REGISTER, data );



		// Read
		//
		addr = module_addr + SIS3302_ADC1_OFFSET
		       + (SIS3302_NEXT_ADC_OFFSET*adc_channel)
		       + (dma_adc_addr_offset_bytes);
		uint32_t req_nof_lwords = dma_request_nof_lwords;

		printf(" addr = [0x%08x] \n \
			 req_nof_lwords = [0x%08x] \n \
			 sub_page_addr_offset = [0x%08x] ",addr, req_nof_lwords,sub_page_addr_offset);


		// VME Read
		//
		//Singles
		if( vme_read_mode == 0 ){
			int i =0;
			for(i=0;i<req_nof_lwords;i++){
			uint32_t vword = sis3320_RegisterRead(k600vme, SIS3320_BASE, SIS3302_ADC1_OFFSET+(i<<2) );
			if(vword < 0){
				printf("sis3302_read_adc_channel: return code = 0x%08x \n",vword);
				return -1;
			}
			uint_buffer_adc[i+index_num_data] = vword;
			if( vword == 0xdeadbeef)
				printf("vword (trailer) = 0x%08x \n",vword);
			}

		//DMA
		}else if(vme_read_mode == 1){
//			uint32_t got_nof_lwords = mvme_read( k600vme, &uint_buffer_adc[index_num_data],
//							     addr, req_nof_lwords );
			uint32_t got_nof_lwords = mvme_read( k600vme, uint_buffer_adc,
							     addr, req_nof_lwords );
			
			if( got_nof_lwords < 0 ){
				printf("\n Return Error: mvme_read \n");
				printf(" vme_addr: [0x%08x ] \n ", addr );	
				printf(" req_nof_lwords: [0x%08x] \n",req_nof_lwords);
				return -1;
			}//If
						
			if( got_nof_lwords != req_nof_lwords ){
				printf(" Return and Requested Length not matching \n \
					 got_nof_lwords: [0x%08x]		  \n \
					 req_nof_lwords: [0x%08x] \n",got_nof_lwords,req_nof_lwords);
				return -1;
			}//If
			
			index_num_data = index_num_data + got_nof_lwords;
		}//if-else


		next_event_sample_start_addr = next_event_sample_start_addr + sub_event_sample_length;
		rest_event_sample_length = rest_event_sample_length - sub_event_sample_length;

		printf(" next_event_sample_start_addr 	= [0x%08x]\n \
			 rest_event_sample_length	= [0x%08x]\n \
			 addr				= [0x%08x]\n \
			 req_nof_lwords			= [0x%08x]\n",next_event_sample_start_addr,
			 rest_event_sample_length,addr,req_nof_lwords);


	}while( (status == 0) && ( rest_event_sample_length > 0 ) );


	return 0;
}

int main (int argc, char* argv[]) {

	DWORD VMEIO_BASE = 0x0;

	int status, csr, i;

  	uint32_t *data, *blt_data,armed;
	uint32_t rawdata[512];
	uint32_t bank1_armed_flag = 0;
	
	if (argc>1) {
		sscanf(argv[1],"%lx",&V792_BASE);
	}

	// Test under vmic
	status = mvme_open(&k600vme, 0);
	if (status!=1) exit(1);


	uint32_t event_sample_start_addr = 0x0;

	uint32_t event_sample_length = 0x200;



	/* vme reset */
	sis3320_RegisterWrite(k600vme,SIS3320_BASE, 0x400 ,0x0);

	usleep(10000);
	/* Setup */	
	sis3302_Setup(k600vme, SIS3320_BASE, 0x1);

	event_length_lwords = event_length_lwords + nof_raw_data_words;
	
	uint32_t dma_request_nof_lwords = event_length_lwords;
	uint32_t dma_got_nof_lwords=0x0;
	data = malloc(sizeof(uint32_t)*dma_request_nof_lwords);
	blt_data = malloc(sizeof(uint32_t)*dma_request_nof_lwords);

	int j = 0;

	for(j=0;j<dma_request_nof_lwords;j++){
		data[j] = 0x0;
		blt_data[j] = 0x0;
	}

	//Start Readout Loop
	//
//	sis3320_RegisterWrite(k600vme,SIS3320_BASE, SIS3302_KEY_DISARM_SAMPLE_ARM_BANK1	,0x0);
//	bank1_armed_flag = 1; //start condition


		
	printf("\n ---- Start Readout Loop ----\n");
	uint32_t calc_event_length = 0x6 + (nof_raw_data_words/2) + event_length_lwords;
	printf(" calc_event_length 	= 0x%08x [%d] \n",calc_event_length,calc_event_length);
	printf(" nof_raw_data_words 	= 0x%08x [%d] \n", nof_raw_data_words,nof_raw_data_words);
	printf(" event_length_lwords 	= 0x%08x [%d] \n",event_length_lwords,event_length_lwords);
	printf(" dma_request_nof_lwords = 0x%08x [%d] \n",dma_request_nof_lwords,dma_request_nof_lwords);
	printf(" dma_got_nof_lwords	= 0x%08x [%d] \n",dma_got_nof_lwords,dma_got_nof_lwords);
	printf("\n");

	int mycounter = 0;
	int index_raw_counter = 0;
	do {	

		printf(" --------- Readout Loop Started : Loop Counter [%d] =====----- \n",mycounter );
	//Clear Times stamp
	//sis3320_RegisterWrite(k600vme,SIS3320_BASE, 0x41C ,0x0);
	sis3320_RegisterWrite(k600vme,SIS3320_BASE, SIS3302_KEY_DISARM_SAMPLE_ARM_BANK1	,0x0);
	bank1_armed_flag = 1; //start condition
	
	printf(" (1) ---- Bank 1 armed [%d] ----- \n",bank1_armed_flag);



	//disarm bank and arm alternative bank
	//
//	uint32_t read_addr = 0x0;
//	if(bank1_armed_flag == 1){
//		read_addr = SIS3302_KEY_DISARM_SAMPLE_ARM_BANK2;
//		bank1_armed_flag = 0; // bank 2 is armed
//		printf("Bank 1 Armed, disarming \n");
//		printf("Arming Bank 2\n");
//	}else{
//		read_addr = SIS3302_KEY_DISARM_SAMPLE_ARM_BANK1;
//		bank1_armed_flag = 1; //bank 1 is armed
//		printf("Bank 2 Armed ,disarming\n");
//		printf("Arming Bank 1\n");
//	}
//	sis3320_RegisterWrite(k600vme,SIS3320_BASE,read_addr,0x0);



	//Poll-Event
	int poll_counter = 0;
	int poll_loop_valid = 1;
	do{

		armed = sis3320_RegisterRead(k600vme, SIS3320_BASE, SIS3302_ACQUISTION_CONTROL);

		if(bank1_armed_flag == 1){
			if( (armed &0x10000) == 0x10000){
				poll_loop_valid = 0;
				printf("Bank1  Valid ===== *** \n");
			}
		}else{
			if( (armed &0x20000) == 0x20000){
				poll_loop_valid = 0;
				printf("Bank2 Valid ======= ***** \n");
			}
		}//if-else


		poll_counter++;
		if(poll_counter == 500){
			poll_counter = 0;
			poll_loop_valid = 0;
			//printf(" Counter = [%d] \n", mycounter*1000);
		}		
	}while(poll_loop_valid == 1);

	//prepare page register
	//
	//uint32_t page_reg_ptr = 0x0; 	//Bank2 is armed and Bank1 ( page 0 ) has to be readout
	//if(bank1_armed_flag == 1){	//Bank1 is armed and Bank2 ( page 4 ) has to be readout
	//	page_reg_ptr = 0x4;	
	//}
	//sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_ADC_MEMORY_PAGE_REGISTER,page_reg_ptr);


	//Wait for Threshold flag
	//
	printf(" Waiting for END Address Threshold to be Reached ... \n ");
	int mycnt = 0;
	do{
		armed = sis3320_RegisterRead(k600vme,SIS3320_BASE,SIS3302_ACQUISTION_CONTROL);

		//printf("armed = 0x%08x \n" ,armed);
		//if( (armed &0x40000) == 0x40000)
			//printf("I'm still busy ... \n");
		uint32_t status_threshold_flags = (armed>>24)&0xff;
		uint32_t stat_thres_adc1_flag = (status_threshold_flags&0x1);
		uint32_t stat_thres_adc2_flag = (status_threshold_flags&0x2);
		uint32_t stat_thres_adc3_flag = (status_threshold_flags&0x4);
		uint32_t stat_thres_adc4_flag = (status_threshold_flags&0x8);
		uint32_t stat_thres_adc5_flag = (status_threshold_flags&0x10);
		uint32_t stat_thres_adc6_flag = (status_threshold_flags&0x20);
		uint32_t stat_thres_adc7_flag = (status_threshold_flags&0x40);
		uint32_t stat_thres_adc8_flag = (status_threshold_flags&0x80);

		mycnt++;
//		printf(" Status ADC 1 End Address Threshold Flag : [0x%x] , counter [%d] \n",stat_thres_adc1_flag,mycnt);	
//		printf(" Status ADC 2 End Address Threshold Flag : [0x%x] \n",stat_thres_adc2_flag);	
//		printf(" Status ADC 3 End Address Threshold Flag : [0x%x] \n",stat_thres_adc3_flag);	
//		printf(" Status ADC 4 End Address Threshold Flag : [0x%x] \n",stat_thres_adc4_flag);	
//		printf(" Status ADC 5 End Address Threshold Flag : [0x%x] \n",stat_thres_adc5_flag);	
//		printf(" Status ADC 6 End Address Threshold Flag : [0x%x] \n",stat_thres_adc6_flag);	
//		printf(" Status ADC 7 End Address Threshold Flag : [0x%x] \n",stat_thres_adc7_flag);	
//		printf(" Status ADC 8 End Address Threshold Flag : [0x%x] \n",stat_thres_adc8_flag);	

	}while((armed & 0x80000) != 0x80000);
	printf("End Adddress Threshold was reached ... \n");


	sis3320_RegisterWrite(k600vme,SIS3320_BASE,SIS3302_KEY_VME_DISARM_SAMPLE_LOGIC,0x0);
	usleep(1000000);


	uint32_t next_sample_addr;
	next_sample_addr= sis3320_RegisterRead(k600vme, SIS3320_BASE, SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC1 );
	printf(" ------======== READING SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC1 =====-------- \n");
	printf("next_sample_addr: 0x%08x\n",next_sample_addr&0xffffff);
	printf("next_sample_addr (bank flag): 0x%x\n",((next_sample_addr>>24)&0x1));

	uint32_t event_next_sample_addr;
	//event_next_sample_addr= sis3320_RegisterRead(k600vme, SIS3320_BASE, SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC1 );
//	printf("event_next_sample_addr: 0x%d \n",event_next_sample_addr);

	//event_next_sample_addr = event_next_sample_addr & 0x1ffffff;
	//if(next_sample_addr != event_next_sample_addr){
	//	printf(" Read ADC Channel [%d] , next_sample_addr = 0x%08x, event_next_sample_addr = 0x%08x \n",next_sample_addr,event_next_sample_addr );
//		return -1;
//	}

	uint32_t return_code;

	uint32_t readout_mode = 1;

	printf(" --------================== READING VME =============----------------- \n");
	return_code = sis3302_read_A32BLT_ADC_MEMORY(SIS3320_BASE,0,0x0,blt_data,((next_sample_addr & 0x3ffffc)>>1),&dma_got_nof_lwords);
//	return_code = sis3302_read_A32BLT_ADC_MEMORY(SIS3320_BASE,0,0x0,blt_data,gRawDataSampleLength,&dma_got_nof_lwords);
			
		
	if( return_code < 0 ){
		print(" sis3302_read_adc_channel: return_code = 0x%08x at addr = 0x%08x \n",return_code,SIS3320_BASE);
	}


	uint32_t wrap_sample_offset = ((event_next_sample_addr) &0x3);
	uint32_t wrap_read_addr_offset = (event_next_sample_addr & 0x01fffffc)/2;
	uint32_t wrap_read_addr_mask = dma_request_nof_lwords - 1;


//	for(i=0;i<dma_request_nof_lwords;i++)
//		data[index_raw_counter++] = blt_data[i];
// if( wrap_sample_offset == 3 ){
// 	for(i=0;i<dma_request_nof_lwords;i++){
//       		data[i] = ( (blt_data[(wrap_read_addr_offset + i - 1) & wrap_read_addr_mask] >> 16 ) & 0xffff);
//		data[i] = data[i] + ( (blt_data[(wrap_read_addr_offset + i) & wrap_read_addr_mask] << 16 ) & 0xffff0000);		
//	}
// }

// if( wrap_sample_offset == 0 ){
// 	for(i=0;i<dma_request_nof_lwords;i++){
//       		data[i] = ( (blt_data[(wrap_read_addr_offset + i ) & wrap_read_addr_mask] ) & 0xffffffff);
//	}
// }

// if( wrap_sample_offset == 1 ){
// 	for(i=0;i<dma_request_nof_lwords;i++){
//       		data[i] = ( (blt_data[(wrap_read_addr_offset + i ) & wrap_read_addr_mask] >> 16 ) & 0xffff);
//		data[i] = data[i] + ( (blt_data[(wrap_read_addr_offset + i+1) & wrap_read_addr_mask] << 16 ) & 0xffff0000);		
//	}
// }


//if( wrap_sample_offset == 2 ){
// 	for(i=0;i<dma_request_nof_lwords;i++){
//       		data[i] = ( (blt_data[(wrap_read_addr_offset + i + 1) & wrap_read_addr_mask] ) & 0xffffffff);
//	}
// }


		//Readout Sample Data and Readout Data
		//
//		int32_t nof_ADC_Channels = 1;
		
//		int32_t chan_index = 0;
//		for(chan_index = 0; chan_index < nof_ADC_Channels; chan_index++)
//		{
		

//			for (i=0;i<event_length_lwords;i++) {
				//uint32_t adc_id = (data&0xffff);
//				data[i] = sis3320_RegisterRead(k600vme, SIS3320_BASE, SIS3302_ADC1_OFFSET+(i<<2) );
				//  printf("Data[0x%8.8x]=0x%x\n", SIS3320_BASE+SIS3320_ADC1_OFFSET+(i<<2), data);
				//  printf("ADC HEADER ID 0x%x\n", data&0xffff);
				//  printf("TIMESTAMP UPPER  0x%x\n", (data>>16)&0xffff);
				//  printf("TIMESTAMP LOWER  0x%x\n", (data));
//			}

	
//			//if(next_sample_addr != 0){
//				printf("ADC HEADER ID %d \n", data[0]&0xffff);
//				printf("TIMESTAMP UPPER  %d \n", (data[0]>>16)&0xffff);
//				printf("TIMESTAMP LOWER  %d \n", (data[1]));
			//}else {
			//	printf(" No Data !!! \n");
			//}

		



//		}//For: Readout 
	//		for (i=0;i<event_sample_length/2;i++) {
				//uint32_t adc_id = (data&0xffff);
//				data[i] = sis3320_RegisterRead(k600vme, SIS3320_BASE, SIS3302_ADC1_OFFSET+(i<<2) );
				//  printf("Data[0x%8.8x]=0x%x\n", SIS3320_BASE+SIS3320_ADC1_OFFSET+(i<<2), data);
				  //fprintf(fp,"[%d] ADC HEADER ID 0x%x\n", i,data[i]&0xffff);

				  //fprintf(fp,"[%d] TIMESTAMP UPPER  0x%x\n", i,(data[i]>>16)&0xffff);
				  //fprintf(fp,"[%d] TIMESTAMP LOWER  0x%x\n", i,(data[i]));
				  //fprintf(fp,"[%d] porig_data  0x%x\n", i,(blt_data[i]));
			//printf("i = 0x%08x	0x%04x  0x%04x  0x%04x  0x%04x  , porig_data[0x%08x]\n",2*i,
			//		(data[i]&0xffff),(data[i]>>16)&0xffff,(data[i+1]&0xffff),((data[i+1]>>16)&0xffff),blt_data[i]);
	//		}


		mycounter++;
	}while(mycounter < 1 && index_raw_counter < event_sample_length); //Readout Loop

	//event_sample_length = dma_got_nof_lwords;
	FILE *fp = fopen("myoutput.txt","w+");
	//for (i=0;i<event_length_lwords;i++) {
	for (i=0;i<event_sample_length/2;i++) {
				//uint32_t adc_id = (data&0xffff);
//				data[i] = sis3320_RegisterRead(k600vme, SIS3320_BASE, SIS3302_ADC1_OFFSET+(i<<2) );
				//  printf("Data[0x%8.8x]=0x%x\n", SIS3320_BASE+SIS3320_ADC1_OFFSET+(i<<2), data);
				  //fprintf(fp,"[%d] ADC HEADER ID 0x%x\n", i,data[i]&0xffff);

				  //fprintf(fp,"[%d] TIMESTAMP UPPER  0x%x\n", i,(data[i]>>16)&0xffff);
				  //fprintf(fp,"[%d] TIMESTAMP LOWER  0x%x\n", i,(data[i]));
				  //fprintf(fp,"[%d] porig_data  0x%x\n", i,(blt_data[i]));
			uint32_t pdata = __bswap_32(blt_data[i]);
			if( i == 0 ){
			       	fprintf(fp,"i = 0x%08x[%d] ADC HEADER ID 0x%04x , TIMESTAMP = 0x%04x\n", 2*i,i,pdata&0xffff,((pdata>>16)&0xffff));
			}else if( i == 1){
			       	fprintf(fp,"i = 0x%08x[%d] TIMESTAMP LOWER  0x%x , TIMESTAMP UPPER = 0x%04x\n", 2*i,i,pdata&0xffff,((pdata>>16)&0xffff));
			
			}else if(i > 1){
				
				fprintf(fp,"i = 0x%08x[%d] 0x%04x  0x%04x  0x%04x  0x%04x  , porig_data[0x%08x]\n",2*i,i,
					(pdata&0xffff),(pdata>>16)&0xffff,(__bswap_32(blt_data[i+1])&0xffff),((__bswap_32(blt_data[i+1])>>16)&0xffff),pdata);
			}
	}//for
	fclose(fp);
	
			

  free(data); 
  free(blt_data);	
 // printf("ACQUISITION CONTROL: 0x%x \n",armed);
 // uint32_t acq_reg = armed;
 // armed &= 0x40000;
 // printf("Armed:%x\n", (acq_reg>>18)&0x1);
//  printf("BANK 1 Armed:%d\n", (acq_reg>>16)&0x1);
//  printf("BANK 2 Armed:%d\n", (acq_reg>>17)&0x1);

 // uint32_t readme = 0;
 // do {  
 // armed = sis3320_RegisterRead(k600vme, SIS3320_BASE, SIS3302_ACQUISTION_CONTROL);
  //   printf("Armed:%x\n", armed);
//	if((armed & 0x80000) == 0x80000 )
//		readme = 1;
 
//  } while ((armed & 0x80000) != 0x80000 ); // 0x80000 is Status of End Address Threshold Flag

  
 //   sis3320_RegisterWrite(k600vme,SIS3320_BASE, SIS3302_KEY_VME_DISARM_SAMPLE_LOGIC,0x0);
  
  // sis3302_LED(1);
//(readme) {
//uint32_t next_sample_addr;
//ext_sample_addr= sis3320_RegisterRead(k600vme, SIS3320_BASE, SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC1 );
//rintf("next_sample_addr: 0x%d \n",next_sample_addr);

 //sis3320_RegisterWrite(k600vme,SIS3320_BASE, SIS3320_KEY_DISARM,0x0);
//is3320_RegisterWrite(k600vme,SIS3320_BASE, SIS3302_KEY_VME_DISARM_SAMPLE_LOGIC,0x0);
 //sis3320_RegisterWrite(k600vme,SIS3320_BASE, 0x414,0x0);
//or (i=0;i<30;i++) {
    //uint32_t adc_id = (data&0xffff);
//    data[i] = sis3320_RegisterRead(k600vme, SIS3320_BASE, SIS3302_ADC1_OFFSET+(i<<2) );
   //  printf("Data[0x%8.8x]=0x%x\n", SIS3320_BASE+SIS3320_ADC1_OFFSET+(i<<2), data);
  //  printf("ADC HEADER ID 0x%x\n", data&0xffff);
  //  printf("TIMESTAMP UPPER  0x%x\n", (data>>16)&0xffff);
  //  printf("TIMESTAMP LOWER  0x%x\n", (data));
//  }
  //  regWrite(k600vme, SIS3320_BASE, SIS3320_KEY_DISARM, 0x0);  // DisArm Sampling
 

  //for(i=0;i<256;i++){

//}else{ printf("NOT Armed: [%x] \n",armed); } //armed
 // }
 
//    for(i=0;i<256;i++){
	  //rawdata[i] = sis3320_RegisterRead(k600vme, SIS3320_BASE, SIS3320_ADC1_OFFSET+(i<<2) );
//	  rawdata[i] = sis3320_RegisterRead(k600vme, SIS3320_BASE,0x02000020+(i<<2) );
// 	  printf("Raw Data[0x%8.8x]=%d\n", SIS3320_BASE+SIS3320_ADC1_OFFSET+(i<<2), (rawdata[i]>>16)&0xffff);
 //  


//    }
    //printf("ADC HEADER ID %d \n", rawdata[0]&0xffff);
    //printf("TIMESTAMP UPPER  %d \n", (rawdata[0]>>16)&0xffff);
    //printf("TIMESTAMP LOWER  %d \n", (rawdata[1]));
 
 //sis3302_LED(0);
	status = mvme_close(k600vme);
	return 1;
}	

/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
