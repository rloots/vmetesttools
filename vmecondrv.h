/*
 * =====================================================================================
 *
 *       Filename:  vmecondrv.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  19/02/2009 12:15:19
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 * =====================================================================================
 */

#ifndef  _VMECONDRV__INC
#define  _VMECONDRV__INC


/* #####   HEADER FILE INCLUDES   ################################################### */
#include	"vme_api_en.h"	/** Concurrent Enhanced API */
#include	<mvmestd.h>

/* #####   EXPORTED DEFINE STATEMENTS  ################################################# */
#ifndef MIDAS_TYPE_DEFINED
typedef unsigned long int 	DWORD;
typedef unsigned short int 	WORD;
typedef unsigned char	   	BYTE;
#endif

#ifndef SUCCESS
#define SUCCESS	 		(int) 1
#endif

#define ERROR			(int) -1000
#define MVME_ERROR		(int) -1000
#define MAX_VME_SLOTS		(int) 32
#define DEFAULT_SRC_ADD 	0x000000
#define DEFAULT_NBYTES		0xFFFFFF	/* 16MB */
#define DEFAULT_DMA_NBYTES 	0x100000	/* max DMA size in bytes */

/* #####   EXPORTED DATA TYPES   #################################################### */
typedef struct {
	int deviceHandle; 
	void *id;
	ULONG vaddr;
	unsigned long size;
	int am;
	long flags;
	void *paddr;
}vmectl_window_t;

//typedef struct {
//	int handle;
//}vme_bus_handle_t;	

typedef UINT32 vme_master_handle_t;
typedef UINT32 vme_bus_handle_t;


typedef struct {
	int handle;
}vme_dma_handle_t;

typedef struct {
	int handle;
}vme_interrupt_handle_t;

typedef struct {
	vmectl_window_t ctl;
	int magic;
	void *vptr;
}_vme_master_handle_t; 




typedef struct {
	vme_master_handle_t  wh;
	mvme_size_t	nbytes;
	mvme_addr_t	low;
	mvme_addr_t	high;
	int	am;
	int	valid;
	void	*ptr;
}VME_TABLE;


typedef struct {
	UINT32 	devHandle;
	mvme_size_t	nbytes;
	mvme_addr_t	addr;
//	mvme_addr_t	high;
	int	am;
	int	valid;
	void	*ptr;
}VME_TABLE2;


typedef struct {
//	vme_dma_handle_t dma_handle;
	UINT32 dma_handle;
	void *dma_ptr;
}DMA_INFO;


typedef struct {
	vme_interrupt_handle_t	handle;
	int	level;
	int 	vector;
	int 	flags;
}INT_INFO;	

/* FUNCTION PROTOTYPES */
/* Test Functions */
void Write_Value( MVME_INTERFACE *myvme,  mvme_addr_t vme_Addr, EN_PCI_IMAGE_DATA *data, UINT8 *Value  );
void Read_Value( MVME_INTERFACE *myvme,  mvme_addr_t vme_Addr, EN_PCI_IMAGE_DATA *data, DWORD *DST );
/* ------------------ */
void* vme_mmap_phys( int devHandle, unsigned long paddr, size_t size );
void* vme_master_window_phys_paddr( vme_bus_handle_t bus_handle,vme_master_handle_t *handle );
void* vme_master_window_mmap( vme_bus_handle_t bus_handle,vme_master_handle_t *handle,int flags );
void* vme_master_window_unmmap( vme_bus_handle_t bus_handle,vme_master_handle_t handle );
int vme_unmap_phys( unsigned long vaddr, size_t size,  int devHandle );
int vme_master_window_create_( vme_bus_handle_t bus_handle,vme_master_handle_t *handle, UINT32 vme_addr, int am, size_t size, int flags,void *phys_addr );
int vme_init( vme_bus_handle_t *handle );
int vme_term( vme_bus_handle_t devHandle );
int vme_master_window_release( vme_bus_handle_t bus_handle, vme_master_handle_t handle );
int vcon_mmap( MVME_INTERFACE *mvme, mvme_addr_t vme_addr, mvme_size_t n_bytes);
int vcon_unmap( MVME_INTERFACE *mvme, mvme_addr_t vme_addr, mvme_size_t size );

int vcon_init_devices( MVME_INTERFACE *mvme, mvme_addr_t vme_base_addr, mvme_size_t resolution );

mvme_addr_t vcon_mapcheck( MVME_INTERFACE *mvme, mvme_addr_t vme_addr, mvme_size_t n_bytes );

#endif   /* ----- #ifndef _VMECON__INC  ----- */
