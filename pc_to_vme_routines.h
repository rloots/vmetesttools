/***************************************************************************/
/*                                                                         */
/*  Project: SIS                                                           */
/*                                                                         */
/*  Filename: pc_to_vme_routines.h                                         */
/*                                                                         */
/*  Funktion:                                                              */
/*                                                                         */
/*  Autor:                TH                                               */
/*  date:                 21.02.2005                                       */
/*  last modification:    01.03.2009                                       */
/*                                                                         */
/* ----------------------------------------------------------------------- */
/*                                                                         */
/*  SIS  Struck Innovative Systeme GmbH                                    */
/*                                                                         */
/*  Harksheider Str. 102A                                                  */
/*  22399 Hamburg                                                          */
/*                                                                         */
/*  Tel. +49 (0)40 60 87 305 0                                             */
/*  Fax  +49 (0)40 60 87 305 20                                            */
/*                                                                         */
/*  http://www.struck.de                                                   */
/*                                                                         */
/*  � 2009                                                                 */
/*                                                                         */
/***************************************************************************/



//
//--------------------------------------------------------------------------- 
// Include files                                                              
//--------------------------------------------------------------------------- 


int sub_vme_A32D32_read (unsigned int vme_adr, unsigned int* vme_data) ;
int sub_vme_A32D32_write (unsigned int vme_adr, unsigned int vme_data) ;

int sub_load_vme_sis3150_tigersharcs (unsigned int vme_adr, char* loaderfile_path)  ;
int sub_load_vme_sis3150_tigersharcs_without_Reset (unsigned int vme_adr, char* loaderfile_path)  ;
int sub_vme_sis3150_tigersharcs_Reset (unsigned int vme_adr)	;

int sub_vme_A32BLT32_read (unsigned int vme_adr, unsigned int* dma_buffer, unsigned int request_nof_words, unsigned int* got_nof_words)  ;
int sub_vme_A32MBLT64_read (unsigned int vme_adr, unsigned int* dma_buffer, unsigned int request_nof_words, unsigned int* got_nof_words)  ;
